# =======================================================================
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      message(STATUS "+++ Found LHCbFindPackage.cmake at ${LHCbFindPackage_FILE}")
      include(${LHCbFindPackage_FILE})
  else()
      message(STATUS "+++ Did not find LHCbFindPackage.cmake")
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()
# =======================================================================
function(online_gaudi_module name)
  if( ONLINE_BASIC_GAUDI )
    message(STATUS "++++ Calling gaudi_add_module for plugins of ${name}")
    gaudi_add_module(${name} SOURCES ${ARGN})
  elseif ( SUPPORT_GAUDI )
    gaudi_add_module(${name} SOURCES ${ARGN})
  else()
    online_module(${name} ${ARGN})
  endif()
endfunction()
# =======================================================================
