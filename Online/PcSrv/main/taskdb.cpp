//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PcSrv
//
//  Author     : Markus Frank
//==========================================================================
extern "C" int TASKDB_FUNCTION (int argc, char** argv);

/// Main entry point
int main(int argc, char** argv)   {
  return TASKDB_FUNCTION (argc, argv);
}
