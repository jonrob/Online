//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_FIFOWRITER_H
#define ROLOGGER_FIFOWRITER_H

/// Framework include files
#include "LogServer.h"

/// C/C++ include files
#include <string>

/// rologger namespace declaration
namespace rologger  {

  /// Terminal logger class
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class FifoWriter : public Writer  {
  protected:
    int         fifo_fd   { -1 };

  public:
    std::string fifo_name {    };
    bool    print_counter = false;

  public:
    /// Default constructor
    FifoWriter() = default;
    /// Default destructor
    virtual ~FifoWriter() = default;
    /// Second level object initialization
    virtual int initialize()  override;
    /// Handler for all messages
    virtual void handle_payload(const char* topic,
				char* payload, std::size_t plen,
				char* key, std::size_t klen)     override;
  };
}
#endif // ROLOGGER_FIFOWRITER_H
