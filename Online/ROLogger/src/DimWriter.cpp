//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DimWriter.h"
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// C/C++ include files
#include <vector>
#include <cstring>
#include <unistd.h>

using namespace std;
using namespace rologger;


namespace rologger  {

  /// Single node publisher
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class DimPublish  {
  public:
    std::string  host;
    unsigned int serviceID      = -1;
    std::size_t  payload_length = 0;
    std::size_t  payload_max    = 0;
    char*        payload        = nullptr;
    long         dns_ID         = 0;
    /// DIM callback to supply the payload
    static void feed_message(void* tag, void** buf, int* size, int* first);

  public:
    /// Default constructor
    DimPublish(long dns_id, const std::string& host, const std::string& tag);
    /// Move constructor
    DimPublish(DimPublish&& copy) = default;
    /// Copy constructor
    DimPublish(const DimPublish& copy) = default;
    /// Move assignment
    DimPublish& operator=(DimPublish&& copy) = default;
    /// Copy assignment
    DimPublish& operator=(const DimPublish& copy) = default;
    /// Default destructor
    virtual ~DimPublish();

    /// We need it so often: one-at-time 32 bit hash function
    static unsigned int hash32(const char* key) {
      unsigned int hash = 0;
      const char* k = key;
      for (; *k; k++) {
	hash += *k;
	hash += (hash << 10);
	hash ^= (hash >> 6);
      }
      hash += (hash << 3);
      hash ^= (hash >> 11); hash += (hash << 15);
      return hash;
    }
    static unsigned int hash32(const std::string& key) {
      return hash32(key.c_str());
    }
    void set(const void* pay, std::size_t len);
  };

  /// DIM callback to supply the payload
  void DimPublish::feed_message(void* tag, void** buff, int* size, int* /* first */)   {
    static const char* data = "";
    DimPublish* pub = *(DimPublish**)tag;
    if ( pub && pub->payload_length )   {
      *buff = pub->payload;
      *size = pub->payload_length;
      return;
    }
    *buff = (void*)data;
    *size = 0;
  }

  /// Default constructor
  DimPublish::DimPublish(long dns_id, const string& h, const string& tag)
    : host(RTL::str_upper(h))
  {
    if ( !host.empty() )   {
      string svc;
      if ( tag.empty() )
	svc = "/"+host+"/log";
      else
	svc = "/"+host+"/"+tag+"/log";
      serviceID = ::dis_add_service_dns(dns_id, svc.c_str(), "C", 0, 0, feed_message, (long)this);
      // ::printf("Added service %s\n",svc.c_str());
    }
    payload = (char*)::malloc(payload_max=1024);
    payload_length = 0;
  }

  /// Default destructor
  DimPublish::~DimPublish()   {
    if ( serviceID != 0 )   {
      ::dis_remove_service(serviceID);
      serviceID = 0;
    }
  }

  void DimPublish::set(const void* pay, std::size_t len)   {
    if ( len < 10*1024 )   {
      if ( payload_max <= len+1 )  {
	payload = (char*)::realloc(payload, payload_max=len+256);
      }
      ::memcpy(payload, pay, len);
      payload[len]   = 0;
      if      ( len >= 1 && payload[len-1] == '\n' ) payload[len-1] = 0, --len;
      else if ( len >= 2 && payload[len-2] == '\n' ) payload[len-2] = 0, len -= 2;
      else if ( len >= 3 && payload[len-3] == '\n' ) payload[len-3] = 0, len -= 3;
      payload_length = len+1;
      if ( payload_length > 1 )   {
	::dis_update_service(serviceID);
      }
    }
  }
}

/// Default constructor
DimWriter::DimWriter(const string& dns, const string& n, const string& t)
  : dns_name(RTL::str_upper(dns)), name(RTL::str_upper(n)), tag(RTL::str_upper(t))
{
  //  publish_monitor(dns_name, name, tag);
  string svc = name+"/"+tag+"/monitor";
  if ( this->dns_name.empty() )   {
    char text[132];
    ::dis_get_dns_node(text);
    this->dns_name = text;
  }
  if ( 0 == this->dns_ID )   {
    int port        = ::dis_get_dns_port();
    this->dns_ID    = ::dis_add_dns(this->dns_name.c_str(), port);
  }
  char host[128], pid_str[32];
  ::snprintf(pid_str, sizeof(pid_str), "%d", ::lib_rtl_pid());
  ::gethostname(host,sizeof(host));
  const char* dim_host  = ::getenv("DIM_HOST_NODE");
  dim_host = dim_host ? dim_host : host;
  this->output_trailer += "\",\"physical_host\":\"";
  this->output_trailer += RTL::str_lower(host);
  this->output_trailer += "\",\"host\":\"";
  this->output_trailer += RTL::str_lower(dim_host);
  this->output_trailer += "\",\"pid\":\"";
  this->output_trailer += pid_str;
  this->output_trailer += "\",\"utgid\":\""+RTL::processName();
  this->output_trailer += "\",\"systag\":\"SYSTEM\"}\n";
  service = make_unique<DimPublish>(this->dns_ID, dim_host, tag);
  ::dis_start_serving_dns(this->dns_ID, (name+"/"+tag).c_str());
  ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ DNS: '%s'  [%ld]   Server: '%s'",
		   dns.c_str(), this->dns_ID, (name+"/"+tag).c_str());
}

/// Default destructor
DimWriter::~DimWriter()   {
}

/// Handler for all messages
void DimWriter::handle_payload(const char* /* topic */,
			       char* payload,   std::size_t plen,
			       char* /* key */, std::size_t /* klen */)
{
  static constexpr long PAYLOAD_MATCH = 0x73656D697440227BL;  // *(long*)"{\"@timest";
  int match = *(long*)payload == PAYLOAD_MATCH;
  if ( match )   {
    service->set(payload, plen);
  }
  else   {
    struct tm tim;
    char   time_str[128];
    time_t now = ::time(0);
    string output;
    ::localtime_r(&now, &tim);
    ::strftime(time_str,sizeof(time_str),"{\"@timestamp\":\"%Y-%m-%dT%H:%M:%S.000000%z\",\"message\":\"",&tim);
    if ( payload[0] && payload[0] == ' ' ) ++payload;
    output.reserve(512);
    output  = time_str;
    output += payload;
    output += this->output_trailer;
    service->set(output.c_str(), output.length());
  }
}
