//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "FifoWriter.h"
#include <RTL/Logger.h>
#include <RTL/graphics.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// C/C++ include files
#include <vector>
#include <cerrno>
#include <cstring>

namespace  {

  int help()   {
    ::printf("run_fifo_logger -opt [-opt]                                      \n"
	     "   Run terminal client to display messages from rologger.        \n"
	     "                                                                 \n"
	     "     -h             Print this help.                             \n"
	     "     -f             Supply FIFO name                             \n"
	     "     -F             dto.                                         \n"
	     "     -I             Connect to stdin as message source and       \n"
	     "                    forward the message to terminal.             \n"
	     "     -K             Connect rologger instance directly and          \n"
	     "                    display all matching messages to terminal.   \n"
	     "     -L             Connect to all services on the main pub.     \n"
	     "                    instance on ecs03 (DEFAULT).                 \n"
	     "     -N             Run node logger (pick up messages from one   \n"
	     "                    node directly. Do not connect to main        \n"
	     "                    publishing instance.                         \n"
	     "     -b <broker>    Set DIM message broker.                      \n"
	     "     -n <name>      Publishing instance name.                    \n"
	     "     -Q             Additional tag to create DIM service names.  \n"
	     "     -P             Publish statistics                           \n"
	     "     -R             Print RAW messages                           \n");
    return 0;
  }
}

extern "C" int run_fifo_logger (int argc, char **argv) {
  using namespace rologger;
  std::string          dns, tag = "Elastic", name = RTL::processName();
  std::string          broker, value, fifo;
  std::vector<char*>   args;
  int  output_level  = LIB_RTL_INFO;
  int  consumer_type = DIMLOG_CONSUMER;
  bool print_help    = false;
  bool publish_stats = false;
  bool print_counter = false;
  bool print_raw     = false;

  if ( ::getenv("DIM_DNS_NODE") )  {
    dns = ::getenv("DIM_DNS_NODE");
  }
  for( int i=0; i<argc; ++i )   {
    char c = argv[i][0];
    if ( c == '-' )   {
      c = argv[i][1];
      switch(c)   {
      case 'b':
	broker = argv[++i];
	break;
      case 'c':
	print_counter = true;
	break;
      case 'h':
	help();
	print_help = true;
	args.push_back(argv[i]);
	break;
      case 'P':
	publish_stats = true;
	break;
      case 'n':
	name = argv[++i];
	break;
      case 'D':
	dns = argv[++i];
	break;
      case 'R':
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Print RAW messages.");
	print_raw = true;
	break;
      case 'L':
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Consumer type:        'DIMLOG_CONSUMER'");
	consumer_type = DIMLOG_CONSUMER;
	break;
      case 'N':
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Consumer type:        'DIMNODE_CONSUMER'");
	consumer_type = DIMNODE_CONSUMER;
	break;
      case 'I':
	consumer_type = STDIN_CONSUMER;
	break;
      case 'f':
      case 'F':
	fifo = argv[++i];
	break;
      case 'Q':
	tag = argv[++i];
	break;
      default:
	args.push_back(argv[i]);
	break;
      }
      continue;
    }
    args.push_back(argv[i]);
  }

  RTL::Logger::install_rtl_printer(output_level);
  auto imp = Consumer::create(consumer_type, args);
  if ( !imp.get() )  {
    return EINVAL;
  }
  Consumer   consumer(std::move(imp));
  FifoWriter writer;
  if ( print_help )   {
    return 0;
  }
  writer.fifo_name     = fifo;
  writer.print_raw     = print_raw;
  writer.print_counter = print_counter;
  writer.initialize();
  consumer.add_writer(&writer);
  if ( !consumer.listen(broker) )    {
    return EINVAL;
  }
  if ( publish_stats )   {
    consumer.publish_monitor(dns, name, tag);
  }
  consumer.run();
  return 0;
}
