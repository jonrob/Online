#!/bin/bash
. /group/online/dataflow/scripts/preamble.sh
#
#
dns_node=ECS03;
match_args=
utgid_args=
veto_args=
kafka_use=
node_use=
#
counter_arg=
sel_args=
kafka_group="-g LogViewer_`hostname -s`_$$";
kafka_broker="10.128.96.12:30400";
kafka_partition="partition_logs";
#
node_match="-m (.*)";
node_sel="-S (.*)/LOGS";
#
output_level=1;
#
usage()
{
    echo -e "\033[31;1;7;15m
 Usage: $0 -arg [-arg] \033[1;49;39;27m

             -m(atch) <regex>      Match output from different nodes.
             -u(tgid) <regex>      Match output from specific tasks.
             -v(eto)  <regex>      Veto certain message content.
\033[34;1m
     Mode: -L                  Access messages from DIM broker (Default)  \033[1;49;39m
             -d(ns)   <dns-node>   Default: ${dns_node}

\033[34;1m
     Mode: -K                  Directly access messages from kafka broker \033[1;49;39m
             -b(roker) <broker>    ip:port  Default: ${kafka_broker}
             -t(opic)  <topic>     Kafka topic name Default: ${kafka_partition}
             -g(roup)  <group-id>  Kafka consumer group identifier
             -c(counter)           Output message counter (for debugging)
\033[34;1m
     Mode: -N                  Message access to an individual node       \033[1;49;39m
             -b(roker) <node>      Node name of the bugger to listen to.
\033[0;24;27;39;49m
           Arguments given: $*
           Ctrl-C to leave terminal....
\033[27;0;";
    sleep 100;
    exit 13;    # EACCES
}
#
while [[ "$1" == -* ]]; do
    #echo "Arg:$1 $2 [$*]";
    a1=`echo $1 | tr A-Z a-z | cut -b 1,2`;
    case ${a1} in
        -b)
	    message_broker="${2}";
	    shift;
            ;;
	-c)
	    counter_arg="-counter";
            ;;
	-d)
	    dns_node="${2}";
	    shift;
            ;;
	-g)
	    kafka_group="-g ${2}";
	    shift;
            ;;
        -k)
	    kafka_use="yes";
            ;;
        -t)
	    kafka_partition="${2}";
	    shift;
            ;;
        -n)
	    node_use="yes";
	    match_args=${node_match};
	    sel_args=${node_sel};
            ;;
        -m)
	    sel_args="${sel_args} -S ${2} ";
	    match_args="${match_args} -m ${2} ";
	    shift;
            ;;
        -s)
	    sel_args="${sel_args} -S ${2} ";
	    shift;
            ;;
        -u)
            utgid_args="${utgid_args} -u ${2} ";
	    shift;
            ;;
        -v)
            veto_args="${veto_args} -V ${2} ";
	    shift;
            ;;
	-o)
	    output_level="${2}";
	    shift;
            ;;
	*)
	    usage $*;
	    ;;
    esac
    shift;
done;
#
export DIM_DNS_NODE=${dns_node};
#
echo "kafka_use: ${kafka_use}  node_use: ${node_use}";
#
run_log_viewer()
{
    #CMD="exec -a LogViewer_$$ gdb --args gentest libROLogSrv.so run_output_logger ${veto_args} ${utgid_args}";
    CMD="exec -a LogViewer_$$ gentest libROLogSrv.so run_output_logger ${veto_args} ${utgid_args}";
    if test -n "${kafka_use}"; then
	echo "${CMD} -K -b ${kafka_broker} -t ${kafka_partition} -O ${output_level} ${match_args} ${counter_arg} ${kafka_group}";
    elif test -n "${node_use}"; then
	echo "${CMD} -N -b ${message_broker} -O ${output_level} ${sel_args}";
    else
	echo "${CMD} -L -b /ECS03/ROLogBridge -d ecs03 -O ${output_level} ${match_args} ${sel_args} ${counter_arg}";
	#gdb --args gentest libROLogSrv.so run_output_logger -b /ECS03/KafkaLog -d ecs03 -O 1 ${match_args} ${veto_args} ${sel_args}
    fi;
}
command=`run_log_viewer`;
echo "Command: ${command}";
#val ${command};
`run_log_viewer`
sleep 100;
