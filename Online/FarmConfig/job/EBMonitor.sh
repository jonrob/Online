#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
export EXPAND_TAE=FULL;
export MBM_INCLUSION=USER;
export MBM_EVENT_EXPLORER=YES;
export MBM_INPUT_BUFFER=Output;
export DEBUG=YES;
execute `gaudi_event_task ${FARMCONFIGROOT}/job/Passthrough.py`;
