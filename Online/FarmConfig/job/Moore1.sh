#!/bin/bash
# =========================================================================
#
#  Default script to start the HLT1 task on the HLT farm
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
if test "`uname -a | grep \".el6\"`" != "";   # SLC6
then
    export CMTCONFIG=x86_64-slc6-gcc62-opt;
elif test "`uname -a | grep el7`" != "";      # SLC7 (Centos 7)
then
    export CMTCONFIG=x86_64-centos7-gcc62-opt
fi;
export CHECKPOINTING_CONFIG=${CMTCONFIG};
##echo "[ERROR] Moore1: version: ${MOORE_VERSION} Using ${CHECKPOINTING_CONFIG} CMTCONFIG:${CMTCONFIG}";
export NiceLevel=10;
export DAQ_INHIBIT_FINALIZE=ON;
. /group/hlt/MOORE/${MOOREONLINE_VERSION}/InstallArea/runMooreHlt1Online_EFF.sh ${DIM_DNS_NODE} ${PARTITION_NAME} ${NBOFSLAVES};
#. ${FARMCONFIGROOT}/job/runMooreHlt1Online_EFF.sh ${DIM_DNS_NODE} ${PARTITION_NAME} ${NBOFSLAVES};
