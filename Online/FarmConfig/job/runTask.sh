#!/bin/bash
# =========================================================================
#
#  Default script to start any task on the HLT farm.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
if test -z "${DIM_DNS_NODE}55555"; then  # This should be removed at some point. The check is done in runFarmTask.sh!!!!!
    if [ -r /etc/sysconfig/dim ]; then
	. /etc/sysconfig/dim
	export DIM_DNS_NODE=`echo $DIM_DNS_NODE | tr a-z A-Z`
    fi
    if [ -z $DIM_DNS_NODE ]; then
	echo DIM_DNS_NODE undefined and /etc/sysconfig/dim not readable
        # exit 1
    fi;
fi;

if test -z "${HOST}"; then
    export HOST="${DIM_HOST_NODE}";
fi;
#
#
. ./preamble.sh;
#
if test "`uname -a | grep el9`" != "";
then
    export CMTCONFIG=x86_64_v2-el9-gcc12-do0;
    #. /group/online/dataflow/cmtuser/OnlineRelease/install.dataflow.LCG_102.x86_64-centos9-gcc11-dbg/bin/thisonline.sh;
    . /group/online/dataflow/cmtuser/OnlineRelease/setup.${CMTCONFIG}.vars;
    export LC_ALL=C;
else
    export CMTCONFIG=x86_64_v2-centos7-gcc11-do0;
    . /group/online/dataflow/cmtuser/OnlineRelease/setup.${CMTCONFIG}.vars;
fi;
if test "${PARTITION_NAME}" = "TDET" -a "`echo ${HOST}|tr a-z A-Z|cut -b 1-5`" = "MON01"; then
    . /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r21/setup.${CMTCONFIG}.vars;
    echo "Running special TDET setup....   ${FARMCONFIGROOT}";
fi;
#
cd $FARMCONFIGROOT/job;
#
export SUBFARM=`echo ${DIM_DNS_NODE} | tr a-z A-Z`;
export STATIC_OPTS=${FARMCONFIGROOT}/options;
export DYNAMIC_OPTS=/group/online/dataflow/options/${PARTITION_NAME};
if test -z "${DATAINTERFACE}"; then
    DATAINTERFACE=`python /group/online/dataflow/scripts/getDataInterface.py`;
fi;
export DATAINTERFACE;
#
export PREAMBLE_OPTS=${FARMCONFIGROOT}/options/Empty.opts;
export ONLINETASKS=/group/online/dataflow/templates;
export INFO_OPTIONS=${DYNAMIC_OPTS}/OnlineEnv.opts;
export MBM_SETUP_OPTIONS=${DYNAMIC_OPTS}/MBM_setup.opts;
export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH};
export MONITORING_DISTBOX=MON0101;
#
# echo "[ERROR] INFO_OPTIONS=${INFO_OPTIONS}";
#
PRINT_COMMAND_LINE=; ####YES;
enable_testing()
{
    export PATH=/group/online/dataflow/cmtuser/ONLINE_TEST/InstallArea/${CMTCONFIG}/bin:${PATH};
    export PYTHON_PATH=/group/online/dataflow/cmtuser/ONLINE_TEST/InstallArea/${CMTCONFIG}/bin:${PYTHON_PATH};
    export LD_LIBRARY_PATH=/group/online/dataflow/cmtuser/ONLINE_TEST/InstallArea/${CMTCONFIG}/lib:${LD_LIBRARY_PATH};
}
#
execute()
{
    if test -n "${PRINT_COMMAND_LINE}"; then
	echo "${UTGID} [INFO] INFO_OPTIONS:       ${INFO_OPTIONS}";
	echo "${UTGID} [INFO] MBM_SETUP_OPTIONS:  ${MBM_SETUP_OPTIONS}";
	echo "${UTGID} [INFO] $*";
    fi;
    $*;
}
if test -f ./${TASK_TYPE}.sh; then
    execute . ./${TASK_TYPE}.sh $*;
    #
elif test -f ./tests/${TASK_TYPE}.sh; then
    execute . ./tests/${TASK_TYPE}.sh $*;
    #
elif test -f "${OPTIONS}"; then
    execute `dataflow_task ${CLASS}` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
    #
elif test -f ./${PARTITION_NAME}${TASK_TYPE}.sh; then
    execute . ./${PARTITION_NAME}${TASK_TYPE}.sh;
    #
elif test -f ${SMICONTROLLERROOT}/scripts/${TASK_TYPE}.sh; then
    cd ${SMICONTROLLERROOT}/scripts;
    execute . ${SMICONTROLLERROOT}/scripts/${TASK_TYPE}.sh;
    #
elif test -f "${STATIC_OPTS}/${TASK_TYPE}.opts"; then
    execute `dataflow_task ${CLASS}` -opts=${STATIC_OPTS}/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
    #
elif test -f "${STATIC_OPTS}/${TASK_TYPE}.py"; then
    execute `gaudi_task ${STATIC_OPTS}/${TASK_TYPE}.py`;
    #
elif test -f "${FARMCONFIGROOT}/job/${TASK_TYPE}.py"; then
    execute `gaudi_event_task ${FARMCONFIGROOT}/job/${TASK_TYPE}.py`;
    #
elif test -n "`echo ${UTGID} | grep TestEvent`"; then
    execute `gaudi_event_task ${FARMCONFIGROOT}/job/PassThrough.py`;
    #
elif test -f ./${PARTITION_NAME}DefaultTask.sh; then
    execute . ./${PARTITION_NAME}DefaultTask.sh;
    #
else
    echo "ERROR  Running default dummy task ";
    cd ${SMICONTROLLERROOT}/scripts;
    execute exec -a ${UTGID} genPython.exe ${SMICONTROLLERROOT}/scripts/DummyTask.py -utgid ${UTGID} -partition 103 -print ${OUTPUT_LEVEL};
fi;
