#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
rm -f /dev/shm/bm_buff_Events_${PARTITION};
rm -f /dev/shm/bm_comm_Events_${PARTITION};
rm -f /dev/shm/bm_ctrl_Events_${PARTITION};
#
rm -f /dev/shm/bm_comm_Output_${PARTITION};
rm -f /dev/shm/bm_buff_Output_${PARTITION};
rm -f /dev/shm/bm_ctrl_Output_${PARTITION};
#
export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/Empty.opts;
export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/HLT2CrashHandler.opts;
execute `dataflow_task Class0` -opts=${STATIC_OPTS}/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
