"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import Gaudi.Configuration as Gaudi

import GaudiOnline
import OnlineEnvBase as OnlineEnv

from Configurables import Online__FlowManager as FlowManager

application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
application.setup_mbm_access('Events', True)

application.setup_hive(FlowManager("EventLoop"), 40)
application.setup_monitoring()

have_exp = True
algs = [Configurables.Online__EventSize('EventSize')]
if have_exp:
  algs.append(GaudiOnline.store_explorer(print_freq=0.00002))
application.setup_algorithms(algs, 1.0)
# Only act on TAE events: Set to True
application.config.onlyTAE      = False
# Ignore TAE events? Set flag to True
application.config.ignoreTAE    = False
# Have TAE leaves separated in TES
application.config.expandTAE    = True
# In addition convert to RawEvent structures
application.input.MakeRawEvent  = False

application.config.numThreadSvcName       = 'NumInstances'
application.config.burstPrintCount        = 30000
application.passThrough.BurnCPU           = 0
application.passThrough.DelayTime         = 0
application.updateAndReset.saveHistograms = 1
application.monSvc.DimUpdateInterval      = 1
application.monSvc.cycleTimeService       = "<proc>/NextHistogramReset"


# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode               = 1
application.config.numEventThreads        = 15
application.config.MBM_numConnections     = 8
application.config.MBM_numEventThreads    = 5
#
application.config.execMode               = 1
application.config.numEventThreads        = 3
application.config.MBM_numConnections     = 1
application.config.MBM_numEventThreads    = 1
#
# Enable this for debugging
#
# application.config.numEventThreads     = 1
# application.config.MBM_numConnections  = 1
# application.config.MBM_numEventThreads = 1
#
application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
]
#
print('Setup complete....')

