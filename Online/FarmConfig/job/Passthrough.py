"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import GaudiOnline
import Configurables
import Gaudi.Configuration as Gaudi
import OnlineEnvBase as OnlineEnv
from Configurables import Online__FlowManager as FlowManager
#

def setup_application():
  #
  # -----------------------------------------------------------------------------
  application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                        partitionName=OnlineEnv.PartitionName,
                                        partitionID=OnlineEnv.PartitionID,
                                        classType=GaudiOnline.Class1)
  application.setup_fifolog()
  application.config.debug         = False
  #
  # -----------------------------------------------------------------------------
  # Default TAE handling
  # Have TAE leaves separated in TES
  application.config.expandTAE     = False
  # Ignore TAE events? Set flag to True
  application.config.ignoreTAE     = False
  application.config.onlyTAE       = False
  #
  # -----------------------------------------------------------------------------
  expand_tae = os.getenv("EXPAND_TAE")
  if expand_tae:
    application.config.expandTAE   = True
  #
  # -----------------------------------------------------------------------------
  only_tae = os.getenv("ONLY_TAE")
  if only_tae:
    application.config.onlyTAE     = True
  #
  # -----------------------------------------------------------------------------
  input_buffer_name = os.getenv("MBM_INPUT_BUFFER","Events")
  application.setup_mbm_access(input_buffer_name, True)
  #
  algorithms = []
  # -----------------------------------------------------------------------------
  # Event size algorithm
  ev_size = Configurables.Online__EventSize('EventSize')
  algorithms.append(ev_size)
  #
  # -----------------------------------------------------------------------------
  # Store explorer algorithm
  if os.getenv("MBM_EVENT_EXPLORER"):
    explorer = GaudiOnline.store_explorer(print_freq=0.01)
    algorithms.append(explorer)
  #
  # -----------------------------------------------------------------------------
  # Output writer algorithm
  if os.getenv("MBM_OUTPUT_BUFFER"):
    output_buffer = os.getenv("MBM_OUTPUT_BUFFER")
    writer = application.setup_mbm_output(output_buffer)
    writer.MBM_maxConsumerWait     = 10
    writer.MBM_allocationSize      = 1024*1024*10
    writer.MBM_burstPrintCount     = 25000
    writer.MaxEventsPerTransaction = 10
    writer.UseRawData              = False
    writer.RequireODIN             = True
    #writer.UseTAELeaves            = True
    writer.UseRawGuard             = True
    algorithms.append(writer);
  #
  # -----------------------------------------------------------------------------
  have_odin = True
  #
  application.setup_hive(FlowManager("EventLoop"), 40)
  application.setup_monitoring(have_odin=have_odin)
  #
  application.setup_algorithms(algorithms, 1.0)
  #
  # Only here the input algorithm is defined. Set options accordingly
  application.input.MakeRawEvent     = False  # Default
  if expand_tae and expand_tae=='FULL':
    application.input.MakeRawEvent   = True
  #
  # Enable CPU burning if requested
  if os.getenv("BURN_CPU"):
    application.passThrough.BurnCPU   = 1
    application.passThrough.MicroDelayTime = 1000.0*float(os.getenv("BURN_CPU"))
  #
  #
  application.monSvc.DimUpdateInterval   = 5
  application.monSvc.CounterClasses = [
    "AuxCounters 10 (.*)EventInput/daqCounter.(.*)",
    "DAQErrors    3 (.*)EventInput/daqError.(.*)"
  ];
  #
  # -----------------------------------------------------------------------------
  application.config.burstPrintCount     = 30000
  #
  # Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
  application.config.execMode            = 1
  application.config.numEventThreads     = 15
  application.config.MBM_numConnections  = 8
  application.config.MBM_numEventThreads = 5
  #
  application.config.numEventThreads     = 15
  application.config.MBM_numConnections  = 3
  application.config.MBM_numEventThreads = 4
  #
  application.config.numEventThreads     = 4
  application.config.MBM_numConnections  = 2
  application.config.MBM_numEventThreads = 2
  #
  if os.getenv("numEventThreads"):
    application.config.numEventThreads     = int(os.getenv("numEventThreads"))
  if os.getenv("MBM_numConnections"):
    application.config.MBM_numConnections  = int(os.getenv("MBM_numConnections"))
  if os.getenv("MBM_numEventThreads"):
    application.config.MBM_numEventThreads = int(os.getenv("MBM_numEventThreads"))

  application.enableUI()  
  #
  application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
  ]
  #
  #
  if os.getenv("MBM_INCLUSION"):
    mode = os.getenv("MBM_INCLUSION")
    mode = mode.upper()
    if mode == "ONE":
      application.config.MBM_requests = [
        'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
        'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
      ]
      if application.config.MBM_numConnections < 1:
        application.config.MBM_numConnections = 2
      application.config.MBM_numEventThreads  = application.config.MBM_numConnections + 1
    elif mode == "VIP":
      application.config.MBM_requests = [
        'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0',
        'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0'
      ]
      if application.config.MBM_numConnections < 1:
        application.config.MBM_numConnections = 2
      application.config.MBM_numEventThreads  = application.config.MBM_numConnections + 1
    elif mode == "USER":
      application.config.MBM_requests = [
        'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0',
        'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
      ]
      application.config.MBM_numConnections  = 1
  #
  # Enable this for debugging
  #
  _dbg = os.getenv("DEBUG")
  if _dbg:
    application.config.execMode            = 0
    application.config.numEventThreads     = 1
    application.config.MBM_numConnections  = 1
    application.config.MBM_numEventThreads = 1
  #
  #
  print('Setup complete.... Have ODIN: '+str(have_odin))
  return application

# -------------------------------------------------------------------------------
setup_application()
