import os, sys
input_file_directory = '/hlt2/objects'
option_directory     = '/group/online/dataflow/options'
node_list_file_name  = 'NodeList.opts'
run_list_file_name   = 'RunList.opts'

class FileProjector:
  """
     Object to project options for a given runlist
  """
  def __init__(self, partition):
    self.partition = partition

  def get_option(self, fname, opt_name):
    lines = open(fname,'r').readlines()
    items = None
    for l in lines:
      data = l.replace('//','#').replace('OnlineEnv.','').replace('{','[').replace('}',']').replace(' ','').replace(';','')
      if data.find('NodeList') != 0: continue
      data = data.replace('NodeList=','')
      items = eval(data)
      break
    return items

  def read_node_list(self):
    fname = option_directory+os.sep+self.partition+os.sep+node_list_file_name
    return self.get_option(fname, 'NodeList')

  def read_run_list(self):
    fname = option_directory+os.sep+self.partition+os.sep+run_list_file_name
    return self.get_option(fname, 'DeferredRuns')

  def get_file_list_run(self, partition, run):
    dirname   = input_file_directory+os.sep+partition+os.sep+str(run)
    files     = os.listdir(dirname)
    file_list = [dirname+os.sep+f for f in files]
    return file_list

  def get_file_list(self):
    runs = self.read_run_list()
    all_files = {}
    for item in runs:
      part, run = item.split('/')
      all_files[run] = self.get_file_list_run(part, run)
    return self.concat_files(all_files)

  def concat_files(self, files):
    all_files = []
    for run,files in files.items():
      for f in files:
        all_files.append(f)
    return all_files

  def project_files(self, host, nodes, files):
    num_nodes = len(nodes)
    host_name = host.upper()
    host_files = []
    idx_nodes = 0
    for f in nodes:
      if f.upper() == host_name:
        break
      idx_nodes = idx_nodes + 1
    idx_files = 0
    for f in files:
      if idx_files == idx_nodes:
        host_files.append(f)
      idx_files = idx_files + 1
      if idx_files%num_nodes == 0: idx_files = 0
    return host_files

  def build_options(self, host):
    nodes = self.read_node_list()
    files = self.get_file_list()
    host_files = self.project_files(host, nodes, files)
    opts  = 'OnlineEnv.InputFiles = '+str(host_files).replace('[','{\n   ').replace(']','}').replace("'",'"').replace(',',',\n  ')+';\n'
    return opts

def make_dummy_node_list(count):
  nam = 'hlt2'
  nnode = 35
  nsf = int((count+nnode)/nnode)
  nodes = []
  for i in range(nsf):
    for j in range(nnode):
      node = '%s%02d%02d'%(nam, i+1, j+1, )
      nodes.append(node)
      if len(nodes) == count: break

  opts = 'NodeList = '+str(nodes)
  opts = 'OnlineEnv.' + opts.replace('[','{').replace(']','}').replace("'",'"') + ';\n\n'
  fname = option_directory+os.sep+'LHCb2'+os.sep+'NodeList.opts'
  print(len(nodes))
  print(fname)
  open(fname,'w').write(opts)

def test_options(host):
  make_dummy_node_list(170)
  fp = FileProjector('LHCb2')
  nodes = fp.read_node_list()
  print(nodes)
  runs = fp.read_run_list()
  print('Run-list: %s'%(str(runs),))
  files = fp.get_file_list()
  print('+++ Got %d files in total.'%(len(files)))

  the_nodes = [host]
  for host in the_nodes:
    my_files = fp.project_files(host, nodes, files)
    #for f in my_files:
    #  print(f)
    print('+++ Got %d files for node: %s Fraction files: %f fraction nodes: %f.'%(len(my_files), host, len(my_files)/len(files), 1e0/len(nodes), ))
    opts = 'OnlineEnv.InputFiles = '+str(my_files).replace('[','{\n   ').replace(']','}').replace("'",'"').replace(',',',\n  ')+';\n'
    print(opts)

def make_options():
  import traceback, errno
  try:
    fp    = FileProjector('LHCb2')
    host  = os.environ['DIM_HOST_NODE']
    opts  = fp.build_options(host)
    fname = '/tmp/InputFiles.opts'
    open(fname,'w').write(opts)
    print('%s'%fname)
  except Exception as exc:
    traceback.print_exception(*sys.exc_info())
    print('+++ Failed to project input files for node: '+host+": "+str(exc))
    sys.exit(errno.EINVAL)
  else:
    pass

if __name__ == '__main__':
  make_options()
