#!/bin/bash
# =========================================================================
#
#  Farm worker node controller startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
export UTGID;
LOG_FILE="/tmp/${PARTITION}_Controller_SMI.log";
### rm -f ${LOG_FILE};
unset LOGFIFO;
touch $LOG_FILE;
export LOGFILE_APPEND=${LOG_FILE};
## exec -a ${UTGID} genRunner.exe $* 2>&1 > /dev/null;
## exec -a ${UTGID} genRunner.exe $* 2>&1 > ${LOG_FILE};
exec -a ${UTGID} genRunner.exe $*;
