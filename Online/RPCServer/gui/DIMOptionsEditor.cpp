//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/DIMOptionsEditor.h"
#include "RPC/GUI/GuiException.h"
#include "RPC/GUI/GuiMsg.h"
#include "CPP/IocSensor.h"
#include "CPP/Event.h"
#include "RPC/DataflowRPC.h"
#include "RPC/DimClient.h"
#include "dim/dic.h"

/// ROOT include files
#include "TGComboBox.h"
#include "TGTextEntry.h"

#include <iostream>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>


ClassImp(xmlrpc::DIMOptionsEditor)

using namespace std;
using namespace xmlrpc;

/// Standard initializing constructor
DIMOptionsEditor::DIMOptionsEditor(TGWindow* p, CPP::Interactor* g)
  : OptionsEditor(p,g)
{
}

/// GUI handler: Default destructor
DIMOptionsEditor::~DIMOptionsEditor()   {
  if ( response_id ) ::dic_release_service(response_id);
}

/// Create/Reset communication client
void DIMOptionsEditor::createClient(GuiCommand* c)    {
  if( dns.empty() )   {
    dns = c->dns;
    ::dic_set_dns_node(dns.c_str());
  }
  else   {
    GuiException(kMBOk,kMBIconAsterisk,"DIM only interface: DNS restrictions:",
		 "Once set, the dns node %s cannot be changed!",
		 dns.c_str()).send(gui);
    return;
  }
  this->OptionsEditor::createClient(c);
  string name = process + "/properties/publish";
  optsBatch = 0;
  if ( response_id ) ::dic_release_service(response_id);
  response_id = ::dic_info_service(name.c_str(),MONITORED,0,0,0,dim_process_properties,(long)this,0,0);
  GuiMsg("Subscribed to DIM service: %s",name.c_str());
}

/// DIM callback to handle commands
void DIMOptionsEditor::dim_process_properties(void* tag, void* address, int* size)   {
  if ( tag && address && size )  {
    int len = *size;
    DIMOptionsEditor* e = *(DIMOptionsEditor**)tag;
    if ( e && len>0 )   {
      e->dim_load_properties((char*)address, len);
    }
  }
}

void DIMOptionsEditor::dim_load_properties(char* address, int len)   {
  {
    lock_guard<mutex> lck(lock);
    char* p = address, *e = p+len, *n, *v, *c;
    properties.clear();
    while(p < e)  {
      c = p;
      n = ::strchr(c,'/');
      if ( n )  {
	*n = 0;
	++n;
	v = ::strchr(n,' ');
	if ( v ) {
	  *v = 0;
	  ++v;
	  p = v + ::strlen(v) + 1;
	  properties.push_back(ObjectProperty(c,n,v));
	  properties.back().flag = OPTION_ENABLED;
	}
      }
      else   {
	GuiException(kMBOk,kMBIconStop,"DIM: Load Options",
		     "DIM: dim_load_properties: "
		     "Failed to update option: %s "
		     "[Bad formatted command. Unknwon failure]",c).send(gui);
	return;
      }
    }
  }
  GuiMsg("DIM: Loaded %ld DIM properties from service in WinccOA compatible format.",
	 properties.size()).send(gui);
  --optsBatch;
  show_next_table_batch();
}

/// Load options of a single client
void DIMOptionsEditor::loadOptions()   {
  string name = process + "/properties/set";
  std::string msg = "publishProperties";
  int sc = ::dic_cmnd_callback(name.c_str(),(void*)msg.c_str(),msg.length()+1,0,(long)this);
  if ( sc != 1 )   {
    GuiException(kMBOk,kMBIconStop,"DIM: Load Options",
		 "DIM: Failed dic_cmnd_callback(%s) [No client]",name.c_str()).send(gui);
    return;
  }
  GuiMsg("DIM: Load Options: dic_cmnd_callback(%s) sent publishProperties.",
	 name.c_str()).send(gui);
}

/// Send single option to the target process
void DIMOptionsEditor::sendOption(const Row& row)    {
  string target = process + "/properties/set";
  string nam    = row.label->GetText();
  for(auto& p : properties)  {
    string pnam = p.client+"."+p.name;
    if ( pnam == nam )   {
      GuiMsg("DIM: Sending option %s::%s::%s  %s.%s = %s [row:%ld]",
	     dns.c_str(), node.c_str(), process.c_str(),
	     p.client.c_str(), p.name.c_str(), p.value.c_str(), row.id)
	.send(gui);
      if ( !process.empty() )  {
	try  {
	  vector<char> msg;   {
	    using namespace boost::iostreams;
	    back_insert_device<vector<char> > sink{msg};
	    stream<back_insert_device<vector<char> > > os{sink};
	    os << "setProperties" << '\0';
	    os << p.client << "/" << p.name << " " << p.value << '\0';
	    p.flag = OPTION_ENABLED;
	  }
	  lock_guard<mutex> lck(lock);
	  int sc = ::dic_cmnd_callback(target.c_str(),msg.data(),msg.size(),0,(long)this);
	  if ( sc != 1 )   {
	    GuiException(kMBOk,kMBIconStop,"DIM: Send Option",
			 "DIM: Failed dic_cmnd_callback(%s) [No client]",target.c_str()).send(gui);
	    return;
	  }
	  row.label->SetBackgroundColor(color_sent);
	  row.option->SetBackgroundColor(color_sent);
	  p.flag = OPTION_ENABLED;
	  GuiMsg("DIM: Successfully updated %s::%s::%s  %s.%s = %s [row:%ld]",
		 dns.c_str(), node.c_str(), process.c_str(),
		 p.client.c_str(), p.name.c_str(), p.value.c_str(), row.id)
	    .send(gui);
	  return;
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconAsterisk,"RPC Exception",e.what()).send(gui);
	}
      }
      GuiException(kMBOk,kMBIconStop,"DIM: Send Option",
		   "DIM sendOption: No valid RPC client present. How can this happen?").send(gui);
      return;
    }
  }
  GuiException(kMBOk,kMBIconStop,"DIM: Send Option",
	       "DIM: sendOption: Cannot locate property: %s = %s",
	       nam.c_str(), row.option->GetText()).send(gui);
}

/// Send all changed options to the client process
void DIMOptionsEditor::sendChanges()    {
  string target = process + "/properties/set";
  vector<char> msg;   {
    using namespace boost::iostreams;
    back_insert_device<vector<char> > sink{msg};
    stream<back_insert_device<vector<char> > > os{sink};
    for(auto& p : properties)  {
      if ( p.flag )    {
	os << p.client << "/" << p.name << " " << p.value << "\0";
	p.flag = OPTION_ENABLED;
      }
    }
  }
  lock_guard<mutex> lck(lock);
  int sc = ::dic_cmnd_callback(target.c_str(),msg.data(),msg.size(),0,(long)this);
  if ( sc != 1 )   {
    GuiException(kMBOk,kMBIconStop,"DIM: Send Changes",
		 "DIM: Failed dic_cmnd_callback(%s) [No client]",target.c_str()).send(gui);
    return;
  }
  update_sent_options();
}
