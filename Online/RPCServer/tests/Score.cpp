//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Score.h"

Online::rpc::test::_TestCount::_TestCount(int) {
}

Online::rpc::test::_TestCount::~_TestCount() {
  logger("TEST>  Counts: ") << "Total score: " << counter << std::endl;
}

Online::rpc::test::_TestCount& Online::rpc::test::_TestCount::instance() {
  static _TestCount inst(0);
  return inst; 
}

