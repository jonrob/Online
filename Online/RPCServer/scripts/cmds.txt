export DEBUG=-debug
export DEBUG=

setup()  {
   cd $HOME/cmtuser/OnlineDev_v7r3;./run bash;
}

start_ecs03()  {
   HH=`echo ${HOST} | tr a-z A-Z`;
   pkill -9 gentest;
   xterm -geo 132x20 -title RPC_${HH}_bridge     -e "gentest libRPCcli.so run_http_rpc_bridge -port 2600 ${DEBUG} -threads 10" &
   xterm -geo 132x20 -title RPC_${HH}_DomainInfo -e "gentest libRPCTests.so romon_domainrpc -from=ecs03 -to=ecs03 -print=2 -utgid=RPCDomainInfo -type=http -port=2601 -threads=5 -threads=4" &
}

start_sfc()  {
   HH=`echo ${HOST} | tr a-z A-Z`;
   pkill gentest;
   xterm -geo 132x20 -title RPC_${HH}_bridge -e "gentest libRPCcli.so run_http_rpc_bridge -port 2600 ${DEBUG} -thread 5"&
}

start_node()  {
   HH=`echo ${HOST} | tr a-z A-Z`;
   pkill gentest;
   xterm -geo 132x20 -title LHCb_${HH}_UITest_0 -e "export UTGID=LHCb_${HH}_UITest_0;gentest libDataflow.so dataflow_run_task -opt=Online/Dataflow/options/UI_test.opts -mon=Dataflow_DIMMonitoring -auto" &
   xterm -geo 132x20 -title LHCb_${HH}_UITest_1 -e "export UTGID=LHCb_${HH}_UITest_1;gentest libDataflow.so dataflow_run_task -opt=Online/Dataflow/options/UI_test.opts -mon=Dataflow_DIMMonitoring -auto" &
}

rpc_display()  {
   gentest libRPCgui.so test_table_text;
}


