//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_RPC_RPCPROPERTYUI_H
#define ONLINE_RPC_RPCPROPERTYUI_H

// C/C++ include files
#include <memory>

// Framework include files
#include <CPP/ObjectProperty.h>

namespace rpc   {

  class dimxmlrpc;
  class dimjsonrpc;
  class httpxmlrpc;
  class httpjsonrpc;

  template <typename SERVER> class ServerUI   {
  public:
    std::unique_ptr<SERVER> server;
    void      start();
    void      stop();
    SERVER&   get_server() { return *server; }
  };
  template <typename PROTOCOL> class ProtocolUI   {
  public:
    std::unique_ptr<PROTOCOL> call;
    PROTOCOL& get_call() { return *call; }
  };

  template <typename SERVER, typename PROTOCOL> class RpcPropertyUI 
    : public RpcUI,
      public ProtocolUI<PROTOCOL>,
      public ServerUI<SERVER>
  {
  public:
    /// Default constructor
    RpcPropertyUI() = default;
    /// Default destructor
    virtual ~RpcPropertyUI() = default;
    void start_server()   override;
    void stop_server()   override;
  };

  template <typename SERVER, typename CALL> inline
  int define_server(SERVER& server, CALL& call)    {
    server.define("clients",             call.make(&UI::clients));
    server.define("allProperties",       call.make(&UI::allProperties));
    server.define("namedProperties",     call.make(&UI::namedProperties));
    server.define("clientProperties",    call.make(&UI::clientProperties));
    server.define("property",            call.make(&UI::property));
    server.define("setPropertyObject",   call.make(&UI::setPropertyObject));
    server.define("setProperty",         call.make(&UI::setProperty));
    server.define("setProperties",       call.make(&UI::setProperties));
    server.define("setPropertiesByName", call.make(&UI::setPropertiesByName));
    server.define("interpreteCommand",   call.make(&UI::interpreteCommand));
    server.define("interpreteCommands",  call.make(&UI::interpreteCommands));
    return 1;
  }

  template <typename SERVER, typename PROTOCOL>
  void RpcPropertyUI<SERVER, PROTOCOL>::start_server()   {
    define_server(this->get_server(), this->get_call());
    this->get_server().start(true);
  }

  template <typename SERVER, typename PROTOCOL>
  void RpcPropertyUI<SERVER, PROTOCOL>::stop_server()   {
    this->get_server().stop();
    this->get_server().remove_all();
  }
}
#endif  // ONLINE_RPC_RPCPROPERTYUI_H
