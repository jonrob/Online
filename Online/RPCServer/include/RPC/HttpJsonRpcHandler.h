//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_RPC_HTTPJSONRPCHANDLER_H
#define ONLINE_RPC_HTTPJSONRPCHANDLER_H

// Framework include files
#include <HTTP/HttpCacheHandler.h>
#include <RPC/HttpRpcServer.h>
#include <RPC/JSONRPC.h>

/// Namespace for the http based jsonrpc implementation
namespace rpc  {

  class HttpJsonRpcHandler : public HttpServer::Handler, public http::HttpCacheHandler  {
    
  public:
    /// Definition of the RPC callback map
    typedef std::map<std::string, jsonrpc::Call> Calls;
    /// The map of registered JSON-RPC calls
    Calls calls;
    /// Compression parameters
    compression_params_t compression;

  public:
    /// Standard constructor
    using HttpServer::Handler::Handler;

    /// HttpServer::Handler overload: Stop the rpc service
    virtual void stop()  override;

    /// HttpServer::Handler overload: Remove a callback from the rpc interface
    virtual bool remove(const std::string& name)  override;

    /// HttpServer::Handler overload: Handle a request and produce a reply.
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep) override;

    /// Handle a request and produce a reply.
    virtual void handle_server_request(const http::Request& req, http::Reply& rep);

    /// Handle a request and produce a reply.
    virtual void handle_bridge_request(const std::string& dns, 
				       const std::string& server,
				       const http::Request& req, http::Reply& rep);
  };
}       /* End  namespace jsonrpc         */
#endif  /* ONLINE_RPC_HTTPJSONRPCHANDLER_H */
