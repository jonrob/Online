//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONITORSVC_H
#define ONLINE_GAUCHO_MONITORSVC_H

#include <Gaucho/IGauchoMonitorSvc.h>
#include <Gaudi/Accumulators.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GaudiKernel/IUpdateable.h>
#include <GaudiKernel/Service.h>

#include <map>
#include <string>
#include <typeinfo>

// Forward declarations
class ISvcLocator;
class IIncidentSvc;
namespace Online {
  class MyDimErrorHandler;
  class MonitorInterface;
} // namespace Online

namespace AIDA {
  class IBaseHistogram;
}

/** @class MonitorSvc MonitorSvc.h Gaucho/MonitorSvc.h

    This class implements the IMonitorSvc interface, and publishes Gaudi variables
    to outside monitoring processes with Dim.

    A DimPropServer is started which takes string commands in the format
    Algorithm.Property and returns the value of the property.

    @author Philippe Vannerem
    @author Jose Helder Lopes Jan. 2005
    @author Jose Helder Lopes 2006/12/26: Modified to publish general c-structures and AIDA::IProfile1D
    @author Juan Otalora Goicochea 2007/11/20: MonObjects
*/
class MonitorSvc : virtual public IGauchoMonitorSvc,
                   virtual public IIncidentListener,
                   virtual public IUpdateableIF,
                   virtual public Service {
  typedef std::map<const IInterface*, std::set<std::string>> InfoNamesMap;
  std::vector<std::string>                                   m_CounterClasses;
  std::vector<std::string>                                   m_HistogramClasses;
  std::unique_ptr<Online::MonitorInterface>                  m_MonIntf;
  std::string                                                m_utgid;
  std::string                                                m_expandInfix;
  /// Partition name for histogram services
  std::string                                                m_partname;
  /// Process name for histogram services
  std::string                                                m_procName;
  /// Program name for histogram services
  std::string                                                m_progName;
  /// Name of publishing service for the absolute timeout of the next histogram reset
  std::string                                                m_cycleTimeService;
  /// Map for info entities
  std::map<std::string, void*>                               m_InfoMap      { };
  /// Map of clients to monitoring items
  InfoNamesMap                                               m_InfoNamesMap { };
  /// Reference to the IncidentSvc instance
  SmartIF<IIncidentSvc>      m_incidentSvc{};
  Online::MyDimErrorHandler* m_errh{nullptr};
  int                        m_maxNumCountersMonRate;
  int                        m_CounterInterval;
  int                        m_uniqueServiceNames;
  int                        m_monsysrecover{0};

  /// Flag to suppress rates
  int m_disableMonRate;
  /// Flag to disable publishing dim properties
  int m_disableDimPropServer;
  /// Flag to disable dim commands
  int m_disableDimCmdServer;
  int m_disableMonObjectsForBool;
  int m_disableMonObjectsForInt;
  int m_disableMonObjectsForLong;
  int m_disableMonObjectsForDouble;
  int m_disableMonObjectsForString;
  int m_disableMonObjectsForPairs;
  int m_disableMonObjectsForHistos;

  int m_disableDeclareInfoBool;
  int m_disableDeclareInfoInt;
  int m_disableDeclareInfoLong;
  int m_disableDeclareInfoDouble;
  int m_disableDeclareInfoString;
  int m_disableDeclareInfoPair;
  int m_disableDeclareInfoFormat;
  int m_disableDeclareInfoHistos;

  int  m_updateInterval;
  int  m_runno{0};
  bool m_expandCounterServices;
  bool m_saveInter;
  bool m_runAware;
  bool m_monRateDeclared;
  bool m_DontResetCountersonRunChange;
  bool m_useDStoreNames;
  bool m_i_startState;
  bool m_started{false};
  bool m_haveRates{true};
  bool m_histUpdateOnStop;

  StatusCode i_start();
  StatusCode i_stop();

  void i_unsupported( const std::string& name, const std::type_info& typ, const IInterface* owner );
  template <typename T>
  void i_declareCounter( const std::string& name, const T& var, const std::string& desc, const IInterface* owner );
  template <typename T>
  void i_declarePair( const std::string& name, const T& var1, const T& var2, const std::string& desc,
                      const IInterface* owner );
  template <typename T>
  void i_declareAtomic( const std::string& name, const T& var, const std::string& desc, const IInterface* owner );

public:
  MonitorSvc( const std::string& name, ISvcLocator* sl );
  virtual ~MonitorSvc();

  // IInterface pure member functions
  StatusCode queryInterface( const InterfaceID& riid, void** ppvIF ) override;
  // Service pure virtual member functions
  StatusCode initialize() override;
  StatusCode start() override;
  StatusCode finalize() override;
  void       Lock( void ) override;
  void       UnLock( void ) override;
  void       createCounterSubSys();
  void       createHistSubSys();
  void       handle( const Incident& inc ) override;

  /** Declare monitoring information
      @param name Monitoring information name knwon to the external system
      @param var  Monitoring Listener address
      @param desc Textual description
      @param owner Owner identifier of the monitoring information
      (needed to peform clean up
  */
  void declareInfo( const std::string& name, const bool& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const int& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const unsigned int& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const int& var, const int&, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const long& var, const long&, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const long& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const unsigned long& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const double& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const float& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const Gaudi::Accumulators::AveragingCounter<long>&,
                    const std::string& desc, const IInterface* owner ) override;
  void declareInfo( const std::string& name, const Gaudi::Accumulators::AveragingCounter<double>&,
                    const std::string& desc, const IInterface* owner ) override;
  void declareInfo( const std::string& name, const Gaudi::Accumulators::BinomialCounter<bool>&, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::string& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::pair<double, double>& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const StatEntity& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const AIDA::IBaseHistogram* var, const std::string& desc,
                    const IInterface* owner ) override;

  // We can not modify IMonitorSvc then we use this method to declare MonObjects...
  void declareInfo( const std::string& name, const std::string& format, const void* var, int size,
                    const std::string& desc, const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::string& format, unsigned int* const var, int size,
                    const std::string& desc, const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::string& format, unsigned long* const var, int size,
                    const std::string& desc, const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::atomic<int>& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::atomic<long>& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::atomic<float>& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const std::atomic<double>& var, const std::string& desc,
                    const IInterface* owner ) override;
  void declareInfo( const std::string& name, const Gaudi::Monitoring::Hub::Entity& var, const std::string& desc ) override;

  /** Undeclare monitoring information
      @param name Monitoring information name knwon to the external system
      @param owner Owner identifier of the monitoring information
  */
  void undeclareInfo( const std::string& name, const IInterface* owner) override;
  void undeclareInfo( const std::string& name) override;

  /** Undeclare monitoring information
      @param owner Owner identifier of the monitoring information
  */
  void undeclareAll( const IInterface* ) override;

  /** Get the names for all declared monitoring informations for a given
      owner. If the owner is NULL, then it returns for all owners
  */
  std::set<std::string>* getInfos( const IInterface* = 0 ) override { return 0; }

  /** Update monitoring information
      @param name Monitoring information name knwon to the external system
      @param owner Owner identifier of the monitoring information
  */
  StatusCode update( int runno )  override;
  void updatePerSvc( unsigned long ref ) override;

  /** Update all monitoring information
      @param owner Owner identifier of the monitoring information
  */
  void       resetHistos( const IInterface* owner = 0 ) override;
  void       setRunNo( int runno ) override;

private:
  template <typename T>
  void i_declareHistogram( const std::string& name, T* var, const std::string& desc, const IInterface* owner );

  std::pair<std::string, std::string> registerDimSvc( const std::string& name, const std::string& dimPrefix,
                                                      const IInterface* owner, bool isComment );
  void                                undeclService( std::string infoName );
  void                                updateService( std::string infoName, bool endOfRun );

  std::string infoOwnerName( const IInterface* owner );
  std::string extract( const std::string mascara, std::string value );

  void StopUpdate() override;

public:
  void declareMonRateComplement( int& runNumber, unsigned int& triggerConfigurationKey, int& cycleNumber,
                                 double& deltaT, double& offsetTimeFirstEvInRun, double& offsetTimeLastEvInCycle,
                                 double& offsetGpsTimeLastEvInCycle ) override;
  void StartSaving( std::shared_ptr<Online::TaskSaveTimer>& timer ) override;
  void StopSaving() override;

  void enableMonObjectsForBool() override { m_disableMonObjectsForBool = 0; }
  void enableMonObjectsForInt() override { m_disableMonObjectsForInt = 0; }
  void enableMonObjectsForLong() override { m_disableMonObjectsForLong = 0; }
  void enableMonObjectsForDouble() override { m_disableMonObjectsForDouble = 0; }
  void enableMonObjectsForString() override { m_disableMonObjectsForString = 0; }
  void enableMonObjectsForPairs() override { m_disableMonObjectsForPairs = 0; }
  void enableMonObjectsForHistos() override { m_disableMonObjectsForHistos = 0; }

  void disableMonObjectsForBool() { m_disableMonObjectsForBool = 1; }
  void disableMonObjectsForInt() { m_disableMonObjectsForInt = 1; }
  void disableMonObjectsForLong() { m_disableMonObjectsForLong = 1; }
  void disableMonObjectsForDouble() { m_disableMonObjectsForDouble = 1; }
  void disableMonObjectsForString() { m_disableMonObjectsForString = 1; }
  void disableMonObjectsForPairs() { m_disableMonObjectsForPairs = 1; }
  void disableMonObjectsForHistos() { m_disableMonObjectsForHistos = 1; }
};

#endif // ONLINE_GAUCHO_MONITORSVC_H
