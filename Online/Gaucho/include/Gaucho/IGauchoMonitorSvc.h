//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef KERNEL_IGAUCHOMONITORSVC_H
#define KERNEL_IGAUCHOMONITORSVC_H 1

// Include files
// from STL
#include <Gaudi/Accumulators/Histogram.h>
#include <atomic>
#include <memory>
#include <string>

// from Gaudi
#include <Gaudi/Accumulators.h>
#include <Gaudi/MonitoringHub.h>
#include <GaudiKernel/IMonitorSvc.h>

class DimService;
namespace Online   {  class TaskSaveTimer;  }

static const InterfaceID IID_IGauchoMonitorSvc( "IGauchoMonitorSvc", 1, 0 );

/** @class IGauchoMonitorSvc IGauchoMonitorSvc.h Kernel/IGauchoMonitorSvc.h
 *
 *
 *  @author Marco CLEMENCIC
 *  @date   2008-05-22
 */

class MonObject;

class IGauchoMonitorSvc : virtual public IMonitorSvc {
public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IGauchoMonitorSvc; }

  virtual StatusCode update( int runno )                                             = 0;
  virtual void       updatePerSvc( unsigned long ref )                               = 0;

  virtual void declareMonRateComplement( int& runNumber, unsigned int& triggerConfigurationKey, int& cycleNumber,
                                         double& deltaT, double& offsetTimeFirstEvInRun,
                                         double& offsetTimeLastEvInCycle, double& offsetGpsTimeLastEvInCycle ) = 0;
  virtual void declareInfo( const std::string& name, const float& var, const std::string& desc,
                            const IInterface* owner )                                                          = 0;
  virtual void declareInfo( const std::string& name, const bool& var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const int& var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const unsigned int& var, const std::string& desc,
                            const IInterface* owner )                                                          = 0;
  virtual void declareInfo( const std::string& name, const unsigned long& var, const std::string& desc,
                            const IInterface* owner )                                                          = 0;
  virtual void declareInfo( const std::string& name, const int& var, const int&, const std::string& desc,
                            const IInterface* owner )                                                          = 0;
  virtual void declareInfo( const std::string& name, const long& var, const long&, const std::string& desc,
                            const IInterface* owner )                                                          = 0;
  virtual void declareInfo( const std::string& name, const long& var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const double& var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const std::string& var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const std::pair<double, double>& var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const AIDA::IBaseHistogram* var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const StatEntity& var, const std::string& desc,
                            const IInterface* owner ) override                                                 = 0;
  virtual void declareInfo( const std::string& name, const Gaudi::Accumulators::AveragingCounter<long>&,
                            const std::string& desc, const IInterface* owner )                                 = 0;
  virtual void declareInfo( const std::string& name, const Gaudi::Accumulators::AveragingCounter<double>&,
                            const std::string& desc, const IInterface* owner )                                 = 0;
  virtual void declareInfo( const std::string& name, const Gaudi::Accumulators::BinomialCounter<bool>&,
                            const std::string& desc, const IInterface* owner )                                 = 0;
  virtual void declareInfo( const std::string& name, const std::string& format, const void* var, int size,
                            const std::string& desc, const IInterface* owner ) override                        = 0;

  virtual void declareInfo( const std::string& name, const std::string& format, unsigned int* const var, int size,
                            const std::string& desc, const IInterface* owner ) = 0;

  virtual void declareInfo( const std::string& name, const std::string& format, unsigned long* const var, int size,
                            const std::string& desc, const IInterface* owner ) = 0;

  virtual void declareInfo( const std::string& name, const std::atomic<int>& var, const std::string& desc,
                            const IInterface* owner ) = 0;
  virtual void declareInfo( const std::string& name, const std::atomic<long>& var, const std::string& desc,
                            const IInterface* owner ) = 0;
  virtual void declareInfo( const std::string& name, const std::atomic<float>& var, const std::string& desc,
                            const IInterface* owner ) = 0;
  virtual void declareInfo( const std::string& name, const std::atomic<double>& var, const std::string& desc,
                            const IInterface* owner ) = 0;

  virtual void declareInfo( const std::string& name, const Gaudi::Monitoring::Hub::Entity& var, const std::string& desc ) = 0;
  virtual void undeclareInfo( const std::string& name, const IInterface* owner ) override = 0;
  virtual void undeclareInfo( const std::string& name ) = 0;

  virtual void enableMonObjectsForBool()                       = 0;
  virtual void enableMonObjectsForInt()                        = 0;
  virtual void enableMonObjectsForLong()                       = 0;
  virtual void enableMonObjectsForDouble()                     = 0;
  virtual void enableMonObjectsForString()                     = 0;
  virtual void enableMonObjectsForPairs()                      = 0;
  virtual void enableMonObjectsForHistos()                     = 0;
  virtual void Lock( void )                                    = 0;
  virtual void UnLock( void )                                  = 0;
  virtual void resetHistos( const IInterface* )                = 0;
  virtual void setRunNo( int runno )                           = 0;
  virtual void StartSaving( std::shared_ptr<Online::TaskSaveTimer>& timer) = 0;
  virtual void StopSaving()                                    = 0;
  virtual void StopUpdate()                                    = 0;
};
#endif // KERNEL_IGAUCHOMONITORSVC_H
