#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
#  Module initialization script for dataflow
#
#  \author   M.Frank
#  \version  1.0
#  \date     27/03/2018
#
#===============================================================================
from __future__ import absolute_import, unicode_literals
from builtins import str
from builtins import object
import logging
import cppyy
import imp

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def load_rpc_relay():
  import os
  import sys
  import platform
  # Add ROOT to the python path in case it is not yet there....
  sys.path.append(os.environ['ROOTSYS'] + os.sep + 'lib')
  from ROOT import gSystem

  logging.debug('rpc_relay: Loading library: libGauchoServer')
  result = gSystem.Load("libGauchoServer")
  if result < 0:
    raise Exception('rpc_relay: Failed to load the rpc_relay library libGauchoServer: ' + gSystem.GetErrorStr())
  logging.debug('Loading library: libGauchoServerDict')
  result = gSystem.Load("libGauchoServerDict")
  if result < 0:
    raise Exception('rpc_relay: Failed to load the rpc_relay library libGauchoServerDict: ' + gSystem.GetErrorStr())
  logging.debug('rpc_relay: Import rpc_relay namespace from ROOT')
  from ROOT import rpc_relay as module
  return module

# We are nearly there ....
name_space = __import__(__name__)

def import_namespace_item(ns, nam):
  scope = getattr(name_space, ns)
  attr  = getattr(scope, nam)
  setattr(name_space, nam, attr)
  return attr

def import_root(nam):
  setattr(name_space, nam, getattr(ROOT, nam))

# ---------------------------------------------------------------------------
#
try:
  rpc_relay = load_rpc_relay()
  import ROOT
except Exception as X:
  import sys
  logger.error('rpc_relay: +--%-100s--+', 100 * '-')
  logger.error('rpc_relay: |  %-100s  |', 'Failed to load Online base library:')
  logger.error('rpc_relay: |  %-100s  |', str(X))
  logger.error('rpc_relay: +--%-100s--+', 100 * '-')
  sys.exit(1)

# ---------------------------------------------------------------------------
def import_rpc_relay_types():
  logger.info('Import RPC relay types....')
  import_namespace_item('rpc_relay', 'client_t')
  import_namespace_item('rpc_relay', 'server_t')

import_rpc_relay_types()
from ROOT import gaucho as gaucho
rpc_server  = gaucho.gaucho_server
rpc_adapter = gaucho.server_adapter

def _get(self, name):
  msg = 'Property access is not implemented. Cannot access property ' + name
  raise KeyError(msg)


def _set(self, name, value):
  self.set(name, str(value))

def _props(cl):
  cl.__getattr__ = _get
  cl.__setattr__ = _set

_props(rpc_server)

# ------------------------Generic STL stuff can be accessed using std:  -----
std        = cppyy.gbl.std
std_vector = std.vector
std_list   = std.list
std_map    = std.map
std_pair   = std.pair
# ------------------------Generic STL stuff can be accessed using std:  -----


