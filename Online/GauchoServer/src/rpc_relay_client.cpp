//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================

// Framework includes
#include <GauchoServer/rpc_relay_client.h>
#include <GauchoServer/rpc_relay_client_menu.h>
#include <Gaucho/CounterJson.h>
#include <Gaucho/HistJson.h>
#include <Gaucho/TaskRPC.h>
#include <Gaucho/dimhist.h>
#include <Gaucho/RPCdefs.h>
#include <CPP/IocSensor.h>
#include <CPP/TimeSensor.h>
#include <CPP/Interactor.h>
#include <CPP/Event.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <nlohmann/json.hpp>

// C++ include files
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cerrno>
#include <chrono>

#include <HTTP/HttpClient.h>
#include <RPC/RpcClientHandle.h>
#include "RPC/JSONRPC.h"

using namespace Online;

namespace my_tests {

  ///
  /**
   *
   *    \author  M.Frank
   *    \version 1.0
   */
  class my_client : public rpc_relay::client_t   {
  public:
    using client_t::client_t;
    
  }; 
}

using namespace jsonrpc;

namespace  {
  std::vector<std::string> _to_vector_string(MethodResponse&& response)   {
    if ( response.isOK() )   {
      json_h resp = response.value();
      //std::cout << resp.dump() << std::endl;
      std::vector<std::string> res;
      if ( resp.is_array() )    {
	res.reserve( resp.size() );
	for ( std::size_t i = 0; i < resp.size(); ++i )    {
	  std::string value = resp.at(i);
	  res.emplace_back(value);
	}
	return res;
      }
      throw std::runtime_error("RPC call received a not expected data structure: "+ resp.dump());
    }
    throw std::runtime_error("RPC fault: "+response.fault().dump());
  }

  /// Convert task counters to std::vector
  rpc_relay::client_t::counters_t _convert_counters(MethodResponse&& response)  {
    if ( response.isOK() )   {
      json_h& resp = response.value();
      if ( resp.is_structured() )   {
	auto it_counters = resp.find("counters");
	if ( it_counters != resp.end() )   {
	  json_h& cntrs = *it_counters;
	  if ( cntrs.is_array() )    {
	    rpc_relay::client_t::counters_t counters;
	    for( std::size_t i=0; i < cntrs.size(); ++i )   {
	      rpc_relay::client_t::counter_t cnt;
	      json_h&  itm  = cntrs[i];
	      json_h&  val  = itm["value"];
	      cnt.name      = itm["name"].get<std::string>();
	      cnt.type      = itm["type"].get<int>();
	      cnt.data_type = val["type"].get<std::string>();
	      cnt.value     = val["data"].get<double>();
	      counters.emplace_back(cnt);
	    }
	    return counters;
	  }
	  std::cout << cntrs.dump(2) << std::endl;
	}
      }
      std::cout << resp.dump(2) << std::endl;
      throw std::runtime_error("RPC call received a not expected data structure!");
    }
    throw std::runtime_error("RPC fault: "+response.fault().dump());
  }

  /// Convert single task counter
  rpc_relay::client_t::counter_t _convert_counter(MethodResponse&& response) {
    //std::cout << response.str() << std::endl;
    rpc_relay::client_t::counter_t cnt;
    json_h&  itm  = response.value();
    json_h&  val  = itm["value"];
    cnt.name      = itm["name"].get<std::string>();
    cnt.type      = itm["type"].get<int>();
    cnt.data_type = val["type"].get<std::string>();
    cnt.value     = val["data"].get<double>();
    return cnt;
  }

  /// Convert task counters to std::vector
  rpc_relay::client_t::histograms_t _convert_histograms(MethodResponse&& response)  {
    if ( response.isOK() )   {
      json_h& histos = response.value();
      if ( histos.is_array() )    {
	rpc_relay::client_t::histograms_t histograms;
	for( std::size_t i=0; i < histos.size(); ++i )   {
	  rpc_relay::client_t::histogram_t hist(std::move(histos[i]));
	  histograms.emplace_back(std::move(hist));
	}
	return histograms;
      }
      std::cout << histos.dump(2) << std::endl;
      throw std::runtime_error("RPC call received a not expected data structure!");
    }
    throw std::runtime_error("RPC fault: "+response.fault().dump());
  }

  /// Convert single task counter
  rpc_relay::client_t::histogram_t _convert_histogram(MethodResponse&& response) {
    auto hists = _convert_histograms(std::move(response));
    if ( hists.size() == 1 )
      return hists[0];
    throw std::runtime_error("No such counter known.");
  }
  
  bool _check_response(const MethodCall& call, const MethodResponse& response)   {
    if ( !response.isOK() )   {
      std::stringstream str;
      json_h params = call.params();
      str << "MethodResponse " << call.method() << "(";
      for( std::size_t i=0; i <params.size(); ++i)   {
	std::string v = params.at(i);
	str << v << ",";
      }
      str << ") > " << response.str();
      throw std::runtime_error(str.str());
    }
    return true;
  }

  
  /// Invoke RPC call and formally check response
  MethodResponse _invoke(rpc_relay::client_t& client, const MethodCall& call)   {
    MethodResponse response = client.handler.call(call);
    _check_response(call, response);
    ++client._call_count;
    if ( response.isOK() )   {
      return response;
    }

    std::stringstream str;
    json_h params = call.params();

    str << "MethodResponse " << call.method() << "(";
    for( std::size_t i=0; i <params.size(); ++i)   {
      std::string v = params.at(i);
      str << v << ",";
    }
    str << ") > " << response.str();
    str << "Fault: " << response.faultCode()
	<< "[" << response.faultString() << "]";
    throw std::runtime_error(str.str());
  }
}


std::string rpc_relay::client_t::counter_t::to_string()  const   {
  std::stringstream str;
  str << "{ "
      << "name: '"      << this->name << "', "
      << "type: "       << this->type << ", "
      << "data_type: '" << this->data_type << "', "
      << "value: "      << value
      << "}";
  return str.str();
}

std::string rpc_relay::client_t::histogram_t::name()   const   {
  return data["name"].get<std::string>();
}

std::string rpc_relay::client_t::histogram_t::title()  const   {
  return data["title"].get<std::string>();
}

int         rpc_relay::client_t::histogram_t::type()   const   {
  return data["type"].get<int>();
}

int         rpc_relay::client_t::histogram_t::dimension()   const   {
  return data["dimension"].get<int>();
}

std::string rpc_relay::client_t::histogram_t::to_string()  const   {
  std::stringstream str;
  str << "{ "
      << "name: '"      << this->name()      << "', "
      << "title: '"     << this->title()     << "', "
      << "type: "       << this->type()      << "', "
      << "dimension: "  << this->dimension()
      << "}";
  return str.str();
}

rpc_relay::client_t::client_t(rpc::JsonRpcClientH&& h) : handler(std::move(h))  {
}

rpc_relay::client_t rpc_relay::client_t::create(const std::string& host, const std::string& mount, int port)   {
  int tmo = 10000;
  rpc_relay::client_t client( rpc::client<http::HttpClient>(host, mount, port, tmo) );
  return client;
}

/// Access call counter
std::size_t rpc_relay::client_t::call_count()  const   {
  return this->_call_count;
}

/// Invoke printout of server statistics
void rpc_relay::client_t::output_server_stats()   {
  _invoke(*this, MethodCall().setMethod("sys_output_stats"));
}

/// Access matching counter tasks of a given dns
rpc_relay::client_t::string_list_t
rpc_relay::client_t::counter_tasks(const std::string& dns)    {
  auto resp = _invoke(*this, MethodCall().setMethod("counter_tasks").addParam(dns));
  return _to_vector_string(std::move(resp));
}

/// Emit the counter directory of a specified task
rpc_relay::client_t::string_list_t
rpc_relay::client_t::task_counter_directory(const std::string& dns, const std::string& task)   {
  auto resp = _invoke(*this, MethodCall().setMethod("task_counter_directory").addParam(dns).addParam(task));
  return _to_vector_string(std::move(resp));
}

/// Access counter list by regular expression
rpc_relay::client_t::counter_t
rpc_relay::client_t::task_counter(const std::string& dns, const std::string& task, const std::string& sel)   {
  auto resp = _invoke(*this, MethodCall().setMethod("task_counter").addParam(dns).addParam(task).addParam(sel));
  return _convert_counter(std::move(resp));
}

/// Access all taskcounter
rpc_relay::client_t::counters_t
rpc_relay::client_t::task_counters(const std::string& dns, const std::string& task)  {
  auto resp = _invoke(*this, MethodCall().setMethod("task_counters").addParam(dns).addParam(task));
  return _convert_counters(std::move(resp));
}

/// Access counter list by regular expression
rpc_relay::client_t::counters_t
rpc_relay::client_t::task_counters_regex(const std::string& dns, const std::string& task, const std::string& sel)   {
  auto resp = _invoke(*this, MethodCall().setMethod("task_counters_regex").addParam(dns).addParam(task).addParam(sel));
  return _convert_counters(std::move(resp));
}
    
/// Access matching histogram tasks of a given dns
rpc_relay::client_t::string_list_t
rpc_relay::client_t::histogram_tasks(const std::string& dns)   {
  auto resp = _invoke(*this, MethodCall().setMethod("histogram_tasks").addParam(dns));
  return _to_vector_string(std::move(resp));
}

/// Emit the histogram directory of a specified task
rpc_relay::client_t::string_list_t
rpc_relay::client_t::task_histogram_directory(const std::string& dns, const std::string& task)   {
  auto resp = _invoke(*this, MethodCall().setMethod("task_histogram_directory").addParam(dns).addParam(task));
  return _to_vector_string(std::move(resp));
}

/// Access histogram list by regular expression
rpc_relay::client_t::histogram_t
rpc_relay::client_t::task_histogram(const std::string& dns, const std::string& task, const std::string& selection)   {
  auto resp = _invoke(*this, MethodCall().setMethod("task_histogram").addParam(dns).addParam(task).addParam(selection));
  return _convert_histogram(std::move(resp));
}

/// Access all task histograms
rpc_relay::client_t::histograms_t
rpc_relay::client_t::task_histograms(const std::string& dns, const std::string& task)   {
  auto resp = _invoke(*this, MethodCall().setMethod("task_histograms").addParam(dns).addParam(task));
  return _convert_histograms(std::move(resp));
}

/// Access histogram list by regular expression
rpc_relay::client_t::histograms_t
rpc_relay::client_t::task_histograms_regex(const std::string& dns, const std::string& task, const std::string& selection)   {
  auto resp = _invoke(*this, MethodCall().setMethod("task_histograms_regex").addParam(dns).addParam(task).addParam(selection));
  return _convert_histograms(std::move(resp));
}

rpc_relay::menu_t::menu_t(client_t& cl) : client(cl)
{
}

void rpc_relay::menu_t::show_menu()  {
  ::printf("rpc_relay::menu >\n");
  ::printf("  100: call counter_tasks(%s)                   \n", dns.c_str());
  ::printf("  101: call task_counter(%s, %s, %s)            \n", dns.c_str(), task.c_str(), selection.c_str());
  ::printf("  102: call task_counters(%s, %s)               \n", dns.c_str(), task.c_str());
  ::printf("  103: call task_counters_regex(%s, %s, %s)     \n", dns.c_str(), task.c_str(), selection.c_str());

  ::printf("  200: call histogram_tasks(%s)   \n", dns.c_str());
  ::printf("  201: call task_histogram(%s, %s, %s)          \n", dns.c_str(), task.c_str(), selection.c_str());
  ::printf("  202: call task_histograms(%s, %s)             \n", dns.c_str(), task.c_str());
  ::printf("  203: call task_histograms_regex(%s, %s, %s)   \n", dns.c_str(), task.c_str(), selection.c_str());
  ::printf("                                  \n");
  ::printf("    1: exit.                      \n");
  std::string line;
  int value = 0;
  while( 1 )   {
    ::printf(" Enter choice.\n");
    if ( 1 == ::scanf("%d", &value) )    {
      ::printf(" Handling request: %d\n", value);
      IocSensor::instance().send(this, value, this);
      return;
    }
  }
}

/// Interactor interrupt handler callback
void rpc_relay::menu_t::handle(const Event& event)     {
  switch(event.eventtype) {
  case IocEvent:   {
    switch(event.type) {
    case CMD_MENU_SHOW:
      this->show_menu();
      break;
    case CMD_COUNTER_TASKS:
      this->tasks_in_dns = client.counter_tasks(this->dns);
      break;
    case CMD_TASK_COUNTER:
      client.task_counter(this->dns, this->task, this->selection);
      break;
    case CMD_TASK_COUNTERS:
      client.task_counters(this->dns, this->task);
      break;
    case CMD_TASK_COUNTERS_REGEX:
      client.task_counters_regex(this->dns, this->task, this->selection);
      break;

    case CMD_HISTOGRAM_TASKS:
      this->tasks_in_dns = client.histogram_tasks(this->dns);
      break;
    case CMD_TASK_HISTOGRAM:
      client.task_histogram(this->dns, this->task, this->selection);
      break;
    case CMD_TASK_HISTOGRAMS:
      client.task_histograms(this->dns, this->task);
      break;
    case CMD_TASK_HISTOGRAMS_REGEX:
      client.task_histograms_regex(this->dns, this->task, this->selection);
      break;

    case CMD_MENU_EXIT:
      ::exit(0);
      break;

    default:
      break;
    }
    IocSensor::instance().send(this, CMD_MENU_SHOW, this);
    break;
  }

  case TimeEvent:
    break;

  case UpiEvent:
    break;

  default:
    break;
  }
}

using namespace my_tests;
using namespace jsonrpc;

/// =========================================================================
static void help_server(const char* msg = 0, const char* argv = 0) {
  std::cout << "run_rpc_relay_client -opt <value> [ -opt <value> ]                          " << std::endl
	    << "     -port    <number>    Listening port (default: 8000)                    " << std::endl
	    << "     -debug               Enable debug flag (default: off)                  " << std::endl
	    << "     -help                Print this help.                                  " << std::endl;
  if ( msg )
    std::cout << "Error cause:    " << msg << std::endl;
  if ( argv )
    std::cout << "Argument given: " << argv << std::endl;
  std::cout << std::endl;
  ::exit(EINVAL);
}

extern "C" int run_rpc_relay_client(int argc, char** argv)    {
  int port = 8100, debug = 0, tmo = 1000000;
  std::string host = "0.0.0.0", mount = "/JSONRPC", dns, function, task, selection;
  for(int i = 1; i < argc && argv[i]; ++i)  {
    if ( 0 == std::strncmp("-port",argv[i],4) )
      port  = ::atol(argv[++i]);
    else if ( 0 == std::strncmp("-host",argv[i],4) )
      host  = (++i < argc) ? argv[i] : "localhost";
    else if ( 0 == std::strncmp("-debug",argv[i],4) )
      debug = (++i < argc) ? ::atol(argv[i]) : 3;
    else if ( 0 == std::strncmp("-timeout",argv[i],4) )
      tmo   = (++i < argc) ? ::atol(argv[i]) : 3;
    else if ( 0 == std::strncmp("-mount",argv[i],4) )
      mount = (++i < argc) ? argv[i] : "";
    else if ( 0 == std::strncmp("-func",argv[i],4) )
      function = (++i < argc) ? argv[i] : "";
    else if ( 0 == std::strncmp("-dns",argv[i],4) )
      dns   = (++i < argc) ? argv[i] : "None";
    else if ( 0 == std::strncmp("-task",argv[i],4) )
      task  = (++i < argc) ? argv[i] : "None";
    else if ( 0 == std::strncmp("-selection",argv[i],4) )
      selection  = (++i < argc) ? argv[i] : "None";
    else if ( 0 == std::strncmp("-help",argv[i],2) )
      help_server();
    else
      help_server("Invalid argument given", argv[i]);
  }

  rpc_relay::client_t client( rpc::client<http::HttpClient>(host, mount, port, tmo) );
  rpc_relay::menu_t   menu  (client);

  menu.dns = dns;
  menu.task = task;
  menu.selection = selection;
  menu.debug = debug;
  client.debug = debug;
  client.handler.setDebug(debug != 0);
  
  std::cout << "RPC relay Client> " << "Starting test XMLRPC client with URI:"
	    << host << ":" << port << mount << std::endl;
  
  //return call_client(argc, argv, client, loop);
  IocSensor::instance().send(&menu, menu.CMD_MENU_SHOW, &menu);
  CPP::IocSensor::instance().run();
  return 0x0;
}

