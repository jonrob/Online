//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================

// Framework includes
#include <GauchoServer/gaucho_server.h>

/// =========================================================================
/// Main entry point to start the application
extern "C" int run_gaucho_server(int argc, char** argv) {
  gaucho::gaucho_server server;

  server.configure(argc, argv);
  return server.run();
}
