//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================

// Framework includes
#include <GauchoServer/gaucho_ioc_adapter.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

//using namespace rpc_relay;

/// gaucho namespace declaration
namespace gaucho   {
  
  /// Helper: Load counter directory
  void ioc_server_adapter::get_counters(rpc_t* rpc_ptr)  {
    if ( rpc_ptr )   {
      std::lock_guard<std::mutex> lock(rpc_ptr->lock_mutex);
      const auto& srv = rpc_ptr->server;
      const auto& dns = srv->dns;
      const auto& con = srv->connections;
      for(const auto& c : con)   {
	if ( c.second->service.find("/Counter/") != std::string::npos )   {
	  auto tname = RTL::str_split(c.second->service,"/")[0];
	  auto res = this->task_counter_directory(dns->name, tname.c_str()+4);
	  std::cout << res << std::endl;
	}
      }
    }
  }

  /// Interactor overload: interrupt handler callback
  void ioc_server_adapter::handle(const Event& event)   {
    switch(event.eventtype) {
    case IocEvent:   {
      switch(event.type) {
      case CMD_GETCOUNTERS:
	this->get_counters(event.iocPtr<rpc_t>());
      default:
	break;
      }
      break;
    }
    case TimeEvent:
      IocSensor::instance().send(this, CMD_GETCOUNTERS, event.timer_data);
      break;
    case UpiEvent:
      break;
    default:
      break;
    }
  }
}  
