# ==========================================================================
#   LHCb Online software suite
# --------------------------------------------------------------------------
#   Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
#   All rights reserved.
# 
#   For the licensing terms see OnlineSys/LICENSE.
# 
# =========================================================================
# 
#   Author     : M.Frank
#   Created    : 05/7/2023
# 
# =========================================================================

# =========================================================================
gauchosrv_init()
{
    cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r21;
    . setup.x86_64_v2-el9-gcc12-do0.vars;
}
# =========================================================================
gauchosrv_command()
{
    echo "`which gentest.exe` libGauchoServer.so run_gaucho_server -port 8100 -threads 1";
}
# =========================================================================
gauchoclient_command()
{
    echo "`which gentest.exe` libGauchoServer.so run_rpc_relay_client $*";
}
# =========================================================================
gauchosrv_debug()
{
    gdb --args `gauchosrv_command` $*;
}
# =========================================================================
gauchoclient_run()
{
    `gauchoclient_command` $*;
}
# =========================================================================
gauchoclient_debug()
{
    gdb --args `gauchoclient_command` $*;
}
# =========================================================================

