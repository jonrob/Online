//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================
#ifndef ONLINE_GAUCHOSERVER_RPC_RELAY_SERVER_H
#define ONLINE_GAUCHOSERVER_RPC_RELAY_SERVER_H

// C++ include files
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <string>
#include <mutex>
#include <regex>
#include <map>

// Framework includes

namespace rpc_relay  {

  class server_t   {
  public:
    struct rpc_t;
    struct dns_t;
    struct server_stub_t;
    struct server_adapter_t;

    typedef std::map<std::string,std::shared_ptr<server_stub_t> > srv_map_t;

    /// User interface
    /**
     *   \author  M.Frank
     *   \version 1.0
     */
    struct rpc_t   {
    public:
      unsigned int    magic           { 0xFEEDBABE };
      std::string     service         { };
      std::string     command         { };
      std::mutex      lock_mutex      { };
      server_stub_t*  server          { nullptr };
      unsigned long   rpc_type        { 0 };

    private:
      int             connected       { 0 };
      int             response_svc_id { 0 };

    private:
      /// DIC command callback
      static void rpc_submission_response(void* tag, int* success);
      /// DIS service handler
      static void rpc_response_call(void* tag, void* buffer, int* size);

    public:
      /// Initializing constructor
      rpc_t(const std::string& svc, unsigned long type, server_stub_t* srv);
      /// Inhibit copy constructor
      rpc_t(const rpc_t& copy) = delete;
      /// Inhibit move constructor
      rpc_t(rpc_t&& copy) = delete;
      /// Inhibit default constructor
      rpc_t() = delete;
      /// Default destructor
      virtual ~rpc_t();
      
      /// Inhibit move assignment
      rpc_t& operator= (rpc_t&& copy) = delete;
      /// Inhibit copy assignment
      rpc_t& operator= (const rpc_t& copy) = delete;

      /// Check if the rpc is connected
      bool is_connected()  const   {   return this->connected != 0; }
      /// Invoke RPC call
      void call(const void* data, size_t len);
      /// Invoke RPC call
      void call_with_feedback(const void* data, size_t len);
      /// Receive time stamp of last call (from dim)
      void get_time_stamp(int& secs, int& milli);

      virtual void on_submission_response(bool succeeded);
      /// Handle RPC response from server
      virtual void on_response(const void* data, size_t len);
      /// Handle no-link RPC response
      void on_nolink();
      /// Lock RPC handler
      bool lock();
      /// Unlock RPC handler
      bool unlock();
    };
    typedef std::map<std::string,std::shared_ptr<rpc_t> > rpc_map_t;

    /// User interface
    /**
     *   \author  M.Frank
     *   \version 1.0
     */
    struct dns_t    {
    public:
      unsigned int      magic    { 0xFEEDBABE };
      std::string       name     { };
      srv_map_t         servers  { };
      server_t*         listener { nullptr };
      long              dns_id   { 0 };
      int               info_id  { 0 };

    public:
      /// Initializing constructor
      dns_t(const std::string& n, server_t* l, long d, int i)
	: name(n), listener(l), dns_id(d), info_id(i) {}
      /// Inhibit copy constructor
      dns_t(const dns_t& copy) = delete;
      /// Inhibit move constructor
      dns_t(dns_t&& copy) = delete;
      /// Inhibit default constructor
      dns_t() = delete;
      /// Default destructor
      virtual ~dns_t();
      /// Inhibit move assignment
      dns_t& operator= (dns_t&& copy) = delete;
      /// Inhibit copy assignment
      dns_t& operator= (const dns_t& copy) = delete;
    };

    /// User interface
    /**
     *   \author  M.Frank
     *   \version 1.0
     */
    struct server_stub_t    {
    public:
      unsigned int           magic       { 0xFEEDBABE };
      std::string            name        { };
      std::string            node        { };
      rpc_map_t              connections { };
      std::mutex             lock        { };
      std::shared_ptr<dns_t> dns         { };
      server_t*              listener    { nullptr };
      int                    info_id     { 0 };

    public:
      /// Initializing constructor
      server_stub_t(const std::string& nam, const std::string& nod, std::shared_ptr<dns_t>& d, server_t* l);
      /// Inhibit copy constructor
      server_stub_t(const server_stub_t& copy) = delete;
      /// Inhibit move constructor
      server_stub_t(server_stub_t&& copy) = delete;
      /// Inhibit default constructor
      server_stub_t() = delete;
      /// Default destructor
      virtual ~server_stub_t();
      /// Inhibit move assignment
      server_stub_t& operator= (server_stub_t&& copy) = delete;
      /// Inhibit copy assignment
      server_stub_t& operator= (const server_stub_t& copy) = delete;

      /// Handle RPC response
      void on_response(rpc_t* rpc, const void* data, size_t len);
      /// Handle no-link RPC response
      void on_nolink(rpc_t* rpc);

      void clear_all_rpc();
      bool remove_rpc(const std::string& service);
      bool add_rpc(const std::string& service, unsigned long cookie);
      /// Invoke RPC call
      bool call_rpc(const std::string& service, const void* data, size_t len);
    };

    /// User server interface
    /**
     *   \author  M.Frank
     *   \version 1.0
     */
    struct server_adapter_t   {
    public:
      unsigned int magic { 0xFEEDBABE };
      using rpc_t = server_t::rpc_t;
    public:

      /// Default constructor
      server_adapter_t() = default;
      /// Inhibit copy constructor
      server_adapter_t(const server_adapter_t& copy) = delete;
      /// Inhibit move constructor
      server_adapter_t(server_adapter_t&& copy) = delete;
      /// Default destructor
      virtual ~server_adapter_t();
      /// Inhibit move assignment
      server_adapter_t& operator= (server_adapter_t&& copy) = delete;
      /// Inhibit copy assignment
      server_adapter_t& operator= (const server_adapter_t& copy) = delete;

      /// Handle RPC response from server
      virtual void on_response  (rpc_t* rpc, const void* data, size_t length) = 0;
      /// Informational callback when a rpc no-link
      virtual void on_nolink    (rpc_t* /* rpc */)  {}
      /// Informational callback when a rpc service appears
      virtual void on_connect_rpc   (rpc_t* /* rpc */)  {}
      /// Informational callback when a rpc service disappears
      virtual void on_disconnect_rpc(rpc_t* /* rpc */)  {}
    };

    typedef std::map<std::string, std::shared_ptr<server_stub_t> > server_map_t;
    typedef std::map<std::string, std::shared_ptr<dns_t> >         dns_map_t;
    typedef std::map<unsigned long, std::regex>                    rpc_matches_t;

    unsigned int       magic              { 0xFEEDBABE };
    rpc_matches_t      service_matches    { };
    dns_map_t          known_dns_servers  { };
    server_adapter_t*  server_adapter     { nullptr };
    int                timeout            { 0 };

  private:
    /// DimInfo overload to process messages
    static void dnsinfo_callback(void* tag, void* address, int* size);
    /// DimInfo overload to process messages
    static void taskinfo_callback(void* tag, void* address, int* size);
    /// Process task information from DNS
    void analyze_tasks(const char* input, std::shared_ptr<dns_t>& dns);
    /// Process service information from task
    void analyze_services(const char* input, server_stub_t* dns);

  public:
    /// Inhibit copy constructor
    server_t(const server_t& copy) = delete;
    /// Inhibit move constructor
    server_t(server_t&& copy) = delete;
    /// Default constructor
    server_t();
    /// Default destructor
    virtual ~server_t();

    /// Inhibit move assignment
    server_t& operator= (server_t&& copy) = delete;
    /// Inhibit copy assignment
    server_t& operator= (const server_t& copy) = delete;

    /// Handle RPC response
    void on_response(rpc_t* rpc, const void* data, size_t len);
    /// Handle no-link RPC response
    void on_nolink(rpc_t* rpc);
    /// Informational callback when a rpc service appears
    void on_connect_rpc     (rpc_t* rpc);
    /// Informational callback when a rpc service disappears
    void on_disconnect_rpc  (rpc_t* rpc);

    /// Add new DNS server list 
    bool add_dns(const std::string& dns);
    /// Add new server to list 
    bool add_server(const std::string& server_node, std::shared_ptr<dns_t>& dns);
    /// Remove server from list 
    bool remove_server(const std::string& server_node, std::shared_ptr<dns_t>& dns);

    /// Access all servers from a given DNS node
    std::vector<server_stub_t*> get_servers(const std::string& dns, const std::regex& match)  const;
    std::shared_ptr<rpc_t> get_server_rpc(const std::string& dns, const std::string& server, const std::regex& rpc_match)  const;
    std::shared_ptr<rpc_t> get_server_rpc(const std::string& dns, const std::regex& server_match, const std::regex& rpc_match)  const;
    std::vector<rpc_t*>    get_servers_rpc(const std::string& dns, const std::regex& server_match, const std::regex& rpc_match)  const;
  };
}
#endif /* ONLINE_GAUCHOSERVER_RPC_RELAY_SERVER_H  */
