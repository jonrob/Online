//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================
#ifndef ONLINE_GAUCHOSERVER_RPC_RELAY_CLIENT_H
#define ONLINE_GAUCHOSERVER_RPC_RELAY_CLIENT_H

// C++ include files
#include <condition_variable>
#include <cstdint>
#include <memory>
#include <string>
#include <mutex>
#include <regex>
#include <map>

// Framework includes
#include <RPC/RpcClientHandle.h>
#include <Gaucho/RPCdefs.h>
#include <nlohmann/json.hpp>

/// rpc_relay namespace declaration
namespace rpc_relay  {

  /// User interface
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
  class client_t    {
  public:
    typedef nlohmann::json json;

    rpc::JsonRpcClientH handler;
    int debug  { 0 };
    std::size_t _call_count { 0 };
  public:

    /// Counter type in C++
    /**
     *   \author  M.Frank
     *   \version 1.0
     */
    class counter_t   {
    public:
      std::string name;
      int         type  = -1;
      std::string data_type;
      double      value = 0e0;
      counter_t() = default;
      counter_t(counter_t&& copy) = default;
      counter_t(const counter_t& copy) = default;
      counter_t& operator=(counter_t&& copy) = default;
      counter_t& operator=(const counter_t& copy) = default;
      std::string to_string()  const;
    };

    class histogram_t   {
    public:
      json  data;
    public:
      histogram_t(json&& obj) : data(std::move(obj)) {}
      histogram_t() = default;
      histogram_t(histogram_t&& copy) = default;
      histogram_t(const histogram_t& copy) = default;
      histogram_t& operator=(histogram_t&& copy) = default;
      histogram_t& operator=(const histogram_t& copy) = default;
      std::string name()   const;
      std::string title()  const;
      int         type()   const;
      int         dimension()   const;
      std::string to_string()  const;
    };
    
    using counters_t = std::vector<counter_t>;
    using histograms_t = std::vector<histogram_t>;
    using string_list_t = std::vector<std::string>;
    
  public:
    /// Initializing constructor
    client_t(rpc::JsonRpcClientH&& handler);
    /// No move constructor
    client_t(client_t&& menu) = default;
    /// No copy constructor
    client_t(const client_t& menu) = delete;
    /// Default destructor
    virtual ~client_t() = default;
    /// No move assignment
    client_t& operator = (client_t&& menu) = default;
    /// No copy assignment
    client_t& operator = (const client_t& menu) = delete;

    /// Creation function
    static client_t create(const std::string& host, const std::string& mount, int port);

    /// Statistics: overall call counter
    std::size_t call_count()  const;
    /// Invoke printout of server statistics
    void output_server_stats();
    
    /// Access matching counter tasks of a given dns
    string_list_t counter_tasks(const std::string& dns);
    /// Emit the counter directory of a specified task
    string_list_t task_counter_directory(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    counter_t     task_counter(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access all taskcounter
    counters_t    task_counters(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    counters_t    task_counters_regex(const std::string& dns, const std::string& task, const std::string& selection);
    
    /// Access matching histogram tasks of a given dns
    string_list_t histogram_tasks(const std::string& dns);
    /// Emit the histogram directory of a specified task
    string_list_t task_histogram_directory(const std::string& dns, const std::string& task);
    /// Access histogram list by regular expression
    histogram_t   task_histogram(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access all task histograms
    histograms_t  task_histograms(const std::string& dns, const std::string& task);
    /// Access histogram list by regular expression
    histograms_t  task_histograms_regex(const std::string& dns, const std::string& task, const std::string& selection);
  };
}      // End namespace rpc_relay
#endif /* ONLINE_GAUCHOSERVER_RPC_RELAY_CLIENT_H  */

