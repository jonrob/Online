//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================
#ifndef ONLINE_GAUCHOSERVER_RPC_RELAY_CLIENT_MENU_H
#define ONLINE_GAUCHOSERVER_RPC_RELAY_CLIENT_MENU_H

// C++ include files

// Framework includes
#include <GauchoServer/rpc_relay_client.h>
#include <CPP/Interactor.h>

/// rpc_relay namespace declaration
namespace rpc_relay  {

  /// User interface
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
 class menu_t : public CPP::Interactor   {
  public:
    client_t&   client;
    std::string dns  {};
    std::string task  {};
    std::string selection {};
    std::vector<std::string> tasks_in_dns;

   int debug { 0 };

   enum menu_commands   {
      CMD_MENU_SHOW = 0,
      CMD_MENU_EXIT = 1,

      CMD_COUNTER_TASKS = 100,
      CMD_TASK_COUNTER,
      CMD_TASK_COUNTERS,
      CMD_TASK_COUNTERS_REGEX,

      CMD_HISTOGRAM_TASKS = 200,
      CMD_TASK_HISTOGRAM,
      CMD_TASK_HISTOGRAMS,
      CMD_TASK_HISTOGRAMS_REGEX,

      LAST
    };
  
  public:
    /// Initializing constructor
    menu_t(client_t& client);
    /// No move constructor
    menu_t(menu_t&& menu) = delete;
    /// No copy constructor
    menu_t(const menu_t& menu) = delete;
    /// Default destructor
    virtual ~menu_t() = default;
    /// No move assignment
    menu_t& operator = (menu_t&& menu) = delete;
    /// No copy assignment
    menu_t& operator = (const menu_t& menu) = delete;

    void show_menu();
    /// Interactor interrupt handler callback
    void handle(const Event& event)  override;
  };
}      // End namespace rpc_relay
#endif /* ONLINE_GAUCHOSERVER_RPC_RELAY_CLIENT_MENU_H  */
