/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <Tell1Data/Tell1Decoder.h>
#include <EventData/run3_detector_bits.h>
#include <EventData/raw_bank_offline_t.h>
#include <EventData/raw_bank_online_t.h>
#include <EventData/odin_t.h>

/// C/C++ include files
#include <iostream>
#include <iomanip>
#include <sstream>
#include <netinet/in.h>

using namespace std;
using Online::raw_bank_offline_t;
static const char* s_checkLabel = "BankCheck    ERROR  ";


/// one-at-time hash function
unsigned int Online::hash32Checksum(const void* ptr, size_t len) {
  unsigned int hash = 0;
  const char* k = (const char*)ptr;
  for (size_t i=0; i<len; ++i, ++k) {
    hash += *k;
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash;
}

/* ========================================================================= */
unsigned int Online::adler32Checksum(unsigned int adler,
				     const char* buf,
				     size_t len)
{
#define DO1(buf,i)  {s1 +=(unsigned char)buf[i]; s2 += s1;}
#define DO2(buf,i)  DO1(buf,i); DO1(buf,i+1);
#define DO4(buf,i)  DO2(buf,i); DO2(buf,i+2);
#define DO8(buf,i)  DO4(buf,i); DO4(buf,i+4);
#define DO16(buf)   DO8(buf,0); DO8(buf,8);

  static const unsigned int BASE = 65521;    /* largest prime smaller than 65536 */
  /* NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1 */
  static const unsigned int NMAX = 5550;
  unsigned int s1 = adler & 0xffff;
  unsigned int s2 = (adler >> 16) & 0xffff;
  int k;

  if (buf == NULL) return 1;

  while (len > 0) {
    k = len < NMAX ? (int)len : NMAX;
    len -= k;
    while (k >= 16) {
      DO16(buf);
      buf += 16;
      k -= 16;
    }
    if (k != 0) do {
	s1 += (unsigned char)*buf++;
	s2 += s1;
      } while (--k);
    s1 %= BASE;
    s2 %= BASE;
  }
  unsigned int result = (s2 << 16) | s1;
  return result;
}
/* ========================================================================= */

static unsigned int xorChecksum(const int* ptr, size_t len)  {
  unsigned int checksum = 0;
  len = len/sizeof(int) + (len%sizeof(int) ? 1 : 0);
  for(const int *p=ptr, *end=p+len; p<end; ++p)  {
    checksum ^= *p;
  }
  return checksum;
}

#define QUOTIENT  0x04c11db7
class CRC32Table  {
public:
  unsigned int m_data[256];
  CRC32Table()  {
    unsigned int crc;
    for (int i = 0; i < 256; i++)    {
      crc = i << 24;
      for (int j = 0; j < 8; j++)   {
        if (crc & 0x80000000)
          crc = (crc << 1) ^ QUOTIENT;
        else
          crc = crc << 1;
      }
      m_data[i] = htonl(crc);
    }
  }
  const unsigned int* data() const { return m_data; }
};

// Only works for word aligned data and assumes that the data is an exact number of words
// Copyright  1993 Richard Black. All rights are reserved.
static unsigned int crc32Checksum(const char *data, size_t len)    {
  static CRC32Table table;
  const unsigned int *crctab = table.data();
  const unsigned int *p = (const unsigned int *)data;
  const unsigned int *e = (const unsigned int *)(data + len);
  if ( len < 4 || (size_t(data)%sizeof(unsigned int)) != 0 ) return ~0x0;
  unsigned int result = ~*p++;
  while( p < e )  {
#if defined(LITTLE_ENDIAN)
    result = crctab[result & 0xff] ^ result >> 8;
    result = crctab[result & 0xff] ^ result >> 8;
    result = crctab[result & 0xff] ^ result >> 8;
    result = crctab[result & 0xff] ^ result >> 8;
    result ^= *p++;
#else
    result = crctab[result >> 24] ^ result << 8;
    result = crctab[result >> 24] ^ result << 8;
    result = crctab[result >> 24] ^ result << 8;
    result = crctab[result >> 24] ^ result << 8;
    result ^= *p++;
#endif
  }

  return ~result;
}

static unsigned short crc16Checksum (const char *data, size_t len) {
  static const unsigned short wCRCTable[] =
    { 0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
      0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
      0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
      0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
      0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
      0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
      0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
      0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
      0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
      0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
      0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
      0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
      0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
      0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
      0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
      0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
      0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
      0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
      0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
      0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
      0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
      0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
      0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
      0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
      0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
      0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
      0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
      0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
      0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
      0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
      0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
      0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };
  unsigned int nTemp;
  unsigned short wCRCWord = 0xFFFF;
  while (len--)  {
    nTemp = *data++ ^ wCRCWord;
    wCRCWord >>= 8;
    wCRCWord = (unsigned short)(wCRCWord^wCRCTable[nTemp]);
  }
  return wCRCWord;
}

static char crc8Checksum(const char *data, int len) {
  static unsigned char crc8_table[] =
    { 0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
      157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
      35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
      190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
      70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
      219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
      101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
      248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
      140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
      17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
      175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
      50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
      202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
      87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
      233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
      116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
    };
  const char *s = data;
  char c = 0;
  while (len--) c = crc8_table[c ^ *s++];
  return c;
}

/// Generate  Checksum
unsigned int Online::genChecksum(int flag,const void* ptr, size_t len)  {
  switch(flag)  {
  case 0:
    return xorChecksum((const int*)ptr, len);
  case 1:
    return hash32Checksum(ptr, len);
  case 2:
    len = (len/sizeof(int))*sizeof(int);
    return crc32Checksum((const char*)ptr, len);
  case 3:
    len = (len/sizeof(short))*sizeof(short);
    return crc16Checksum((const char*)ptr, len);
  case 4:
    return crc8Checksum((const char*)ptr, len);
  case 5:
    len = (len/sizeof(int))*sizeof(int);
    return adler32Checksum(1, (const char*)ptr, len);
  case 22:  // Old CRC32 (fixed by now)
    return crc32Checksum((const char*)ptr, len);
  default:
    return ~0x0;
  }
}

static void print_previous_bank(const raw_bank_offline_t* prev) {
  char txt[255];
  if ( prev == 0 )
    ::snprintf(txt,sizeof(txt),"%s Bad bank is the first bank in the MEP fragment.",s_checkLabel);
  else
    ::snprintf(txt,sizeof(txt),"%s Previous (good) bank [%p]: %s",s_checkLabel,
	       (void*)prev,Online::event_print::bankHeader(prev).c_str());
  cout << txt << endl;
}

/// Check sanity of raw bank structure
bool Online::checkRawBank(const raw_bank_offline_t* b, bool throw_exc, bool print_cout)  {
  using event_print::bankHeader;
  // Check bank's magic word: Either Tell1 magic word or PCIE40 magic word
  if ( b->magic() == raw_bank_offline_t::MagicPattern || b->magic() == 0xFACE )  {
    // Crude check on the bank type
    if ( b->type() < raw_bank_offline_t::LastType )  {
      // Crude check on the bank length
      if ( b->size() >= 0 )  // Zero bank length is apparently legal....
	{
	  // Now check source ID range:
	  //// TBD !!
	  return true;
	}
      char txt2[255];
      ::snprintf(txt2, sizeof(txt2), "%s Bad bank length found in Tell1 bank %p: %s", 
		 s_checkLabel, reinterpret_cast<const void*>(b), bankHeader(b).c_str());
      if ( print_cout ) cout << txt2 << endl;
      if ( throw_exc  ) throw runtime_error(txt2);
      return false;
    }
    char txt1[255];
    ::snprintf(txt1, sizeof(txt1), "%s Unknown Bank type in Tell1 bank %p: %s", 
	       s_checkLabel, reinterpret_cast<const void*>(b), bankHeader(b).c_str());
    if ( print_cout ) cout << txt1 << endl;
    if ( throw_exc  ) throw runtime_error(txt1);
    return false;
  }
  // Error: Bad magic pattern; needs handling
  char txt0[255];
  ::snprintf(txt0, sizeof(txt0), "%s Bad magic pattern in Tell1 bank %p: %s",
	     s_checkLabel, reinterpret_cast<const void*>(b), bankHeader(b).c_str());
  if ( print_cout ) cout << txt0 << endl;
  if ( throw_exc  ) throw runtime_error(txt0);
  return false;
}

/// Check consistency of MEP fragment using magic bank patterns.
bool Online::checkRawBanks(const void* start, const void* end, bool exc,bool prt)  {
  char txt[255];
  raw_bank_offline_t* prev = 0;
  if ( end >= start )  {
    for(raw_bank_offline_t* b=(raw_bank_offline_t*)start, *e=(raw_bank_offline_t*)end; b < e; )  {
      if ( !checkRawBank(b,false,true) ) goto Error;  // Check bank sanity
      b = (raw_bank_offline_t*)(((const char*)b) + b->totalSize());
      prev = b;
    }
    return true;
  }
 Error:  // Anyhow only end up here if no exception was thrown...
  ::snprintf(txt, sizeof(txt), "%s Error in multi raw bank buffer start:%p end:%p",
	     s_checkLabel,reinterpret_cast<const void*>(start),reinterpret_cast<const void*>(end));
  if ( prt ) {
    cout << txt << endl;
    print_previous_bank(prev);
  }
  if ( exc ) throw runtime_error(txt);
  return false;
}

/// Check consistency of MEP multi event fragment
bool Online::checkRecord(const event_header_t* h, int opt_len, bool exc,bool prt)    {
  if ( h )  {
    int  compress;
    char txt[255];
    const char *start, *end;
    if ( h->size0() != h->size1() || h->size0() != h->size2() )  {
      ::snprintf(txt,sizeof(txt),"%s Inconsistent MDF header size: %u <-> %u <-> %u at %p",
		 s_checkLabel,h->size0(),h->size1(),h->size2(),reinterpret_cast<const void*>(h));
      goto Error;
    }
    if ( opt_len != ~0x0 && size_t(opt_len) != h->size0() )  {
      ::snprintf(txt,sizeof(txt),"%s Wrong MDF header size: %u <-> %d at %p",
		 s_checkLabel,h->size0(),opt_len,reinterpret_cast<const void*>(h));
      goto Error;
    }
    compress  = h->compression()&0xF;
    if ( compress )  {
      // No uncompressing here! Assume everything is OK.
      return true;
    }
    start = ((char*)h) + sizeof(event_header_t) + h->subheaderLength();
    end   = ((char*)h) + h->size0();
    if ( !checkRawBanks(start,end,exc,prt) )  {
      ::snprintf(txt,sizeof(txt),"%s Error in multi raw bank buffer start:%p end:%p",
		 s_checkLabel,(const void*)start, (const void*)end);
      goto Error;
    }
    return true;

  Error:  // Anyhow only end up here if no exception was thrown...
    if ( prt ) cout << txt << endl;
    if ( exc ) throw runtime_error(txt);
  }
  return false;
}

/// Conditional decoding of raw buffer from MDF to vector of raw banks
int Online::decodeRawBanks(const void* start, const void* end, vector<raw_bank_offline_t*>& banks) {
  const char* s = (const char*)start;
  raw_bank_offline_t *prev = 0, *bank = (raw_bank_offline_t*)s;
  try {
    while (s < end)  {
      bank = (raw_bank_offline_t*)s;
      checkRawBank(bank,true,true);  // Check bank sanity
      banks.push_back(bank);
      s += bank->totalSize();
      prev = bank;
    }
    return 1;
  }
  catch(const exception& e) {
    print_previous_bank(prev);
  }
  catch(...) {
    print_previous_bank(prev);
  }
  throw runtime_error("Error decoding raw banks!");
}

/// Copy RawEvent data from bank vectors to sequential buffer
int Online::encodeRawBank(const raw_bank_offline_t* b, char* const data, size_t size, size_t* length)   {
  size_t s = b->totalSize();
  if ( size >= s )  {
    ::memcpy(data, b, s);
    if ( length ) *length = s;
    return 1;
  }
  return 0;
}

/// Returns the prefix on TES according to bx number, - is previous, + is next
string Online::rootFromBxOffset(int bxOffset) {
  if ( 0 == bxOffset )
    return "/Event";
  if ( 0 < bxOffset )
    return string("/Event/Next") + char('0'+bxOffset);
  return string("/Event/Prev") + char('0'-bxOffset);
}

/// Access to the TAE bank (if present)
raw_bank_offline_t* Online::getTAEBank(const char* start) {
  raw_bank_offline_t* b = (raw_bank_offline_t*)start;            // Get the first bank in the buffer
  if ( b->type() == raw_bank_offline_t::TAEHeader ) {   // Is it the TAE bank?
    return b;
  }
  if ( b->type() == raw_bank_offline_t::DAQ ) {         // Is it the TAE bank?
    start += b->totalSize();                   //
    b = (raw_bank_offline_t*)start;                     // If the first bank is a MDF (DAQ) bank,
  }                                            // then the second bank must be the TAE header
  if ( b->type() == raw_bank_offline_t::TAEHeader ) {   // Is it the TAE bank?
    return b;
  }
  return nullptr;
}

/// Returns the offset of the TAE with respect to the central bx
int Online::bxOffsetTAE(const string& root) {
  size_t idx = string::npos;
  if ( (idx=root.find("/Prev")) != string::npos )
    return -(root[idx+5]-'0');
  if ( (idx=root.find("/Next")) != string::npos )
    return root[idx+5]-'0';
  return 0;
}

/// Return vector of TAE event names
vector<string> Online::buffersTAE(const char* start) {
  vector<string> result;
  raw_bank_offline_t* b = getTAEBank(start);
  if ( b && b->type() == raw_bank_offline_t::TAEHeader ) {   // Is it the TAE bank?
    int nBlocks = b->size()/sizeof(int)/3;          // The TAE bank is a vector of triplets
    const int* block  = b->begin<int>();
    for(int nbl = 0; nBlocks > nbl; ++nbl, block +=3)
      result.push_back(rootFromBxOffset(*block));
  }
  return result;
}

/// Set the detector ID
void Online::bank_header_manip::setDetectorID(bank_header_t* header, unsigned int detector_id)   {
  uint16_t det_id  = uint16_t(detector_id&0xF) << 11;
  header->setSourceID(det_id + (0xFFFF & header->sourceID()));
}

/// Retrieve detector name from the source ID / detector ID
std::string Online::event_print::detectorName(unsigned int detector_id)  {
  if ( detector_id > 16 )  {
    unsigned short the_det_id  = (detector_id&0xFFFF) >> 11;
    return detectorName(the_det_id);
  }
  switch(detector_id)    {
  case run3::RO_BIT_ODIN:    return "ODIN";
  case run3::RO_BIT_VELOA:   return "VELOA";
  case run3::RO_BIT_VELOC:   return "VELOC";
  case run3::RO_BIT_UTA:     return "UTA";
  case run3::RO_BIT_UTC:     return "UTC";
  case run3::RO_BIT_SFA:     return "SFA";
  case run3::RO_BIT_SFC:     return "SFC";
  case run3::RO_BIT_RICH1:   return "RICH1";
  case run3::RO_BIT_RICH2:   return "RICH2";
  case run3::RO_BIT_PLUME:   return "PLUME";
  case run3::RO_BIT_ECAL:    return "ECAL";
  case run3::RO_BIT_HCAL:    return "HCAL";
  case run3::RO_BIT_MUONA:   return "MUONA";
  case run3::RO_BIT_MUONC:   return "MUONC";
  case run3::RO_BIT_TDET:    return "TDET";
  default:  return "UNKNOWN";
  }
}

/// Retrieve detector name from the raw bank
std::string Online::event_print::detectorName(const bank_header_t* r)  {
  return r ? detectorName(r->detectorID()) : std::string("UNKNOWN");
}

/// Retrieve description of the raw bank hader
std::string Online::event_print::bankHeader(const bank_header_t* r)  {
  std::stringstream s; 
  unsigned short det_id  = r->detectorID();
  unsigned short channel = r->channelID();
  s << "Type:"      << std::setw(5) << bankType(r->type())
    << " / "        << std::setw(2) << int(r->type())
    << " Size:"     << std::setw(4) << int(r->size()) 
    << " Source:"   << std::setw(5) << int(r->sourceID())
    << " ["   << std::setw(2) << int(det_id) 
    << "(" << detectorName(det_id) << ")," << std::setw(3) << channel << "]"
    << " Vsn:"      << std::setw(2) << int(r->version()) 
    << " Magic: 0x" << std::setw(4) << std::hex << r->magic();
  return s.str();
}

/// Retrieve bank type name from the raw bank 
std::string Online::event_print::bankType(const bank_header_t* r)  {
  if ( r ) return bankType(r->type());
  return "BAD_BANK";
}

/// Retrieve bank type name from the raw bank type
std::string Online::event_print::bankType(int i)  {
#define PRINT(x)  case bank_types_t::x : return #x;
  switch(i)  {
    PRINT(L0Calo);              //  0
    PRINT(L0DU);                //  1
    PRINT(PrsE);                //  2
    PRINT(EcalE);               //  3
    PRINT(HcalE);               //  4
    PRINT(PrsTrig);             //  5
    PRINT(EcalTrig);            //  6
    PRINT(HcalTrig);            //  7
    PRINT(Velo);                //  8
    PRINT(Rich);                //  9
    PRINT(TT);                  // 10
    PRINT(IT);                  // 11
    PRINT(OT);                  // 12
    PRINT(Muon);                // 13
    PRINT(L0PU);                // 14
    PRINT(DAQ);                 // 15
    PRINT(ODIN);                // 16
    PRINT(HltDecReports);       // 17
    PRINT(VeloFull);            // 18
    PRINT(TTFull);              // 19
    PRINT(ITFull);              // 20
    PRINT(EcalPacked);          // 21
    PRINT(HcalPacked);          // 22
    PRINT(PrsPacked);           // 23
    PRINT(L0Muon);              // 24
    PRINT(ITError);             // 25
    PRINT(TTError);             // 26
    PRINT(ITPedestal);          // 27
    PRINT(TTPedestal);          // 28
    PRINT(VeloError);           // 29
    PRINT(VeloPedestal);        // 30
    PRINT(VeloProcFull);        // 31
    PRINT(OTRaw);               // 32
    PRINT(OTError);             // 33
    PRINT(EcalPackedError);     // 34
    PRINT(HcalPackedError);     // 35  
    PRINT(PrsPackedError);      // 36
    PRINT(L0CaloFull);          // 37
    PRINT(L0CaloError);         // 38
    PRINT(L0MuonCtrlAll);       // 39
    PRINT(L0MuonProcCand);      // 40
    PRINT(L0MuonProcData);      // 41
    PRINT(L0MuonRaw);           // 42
    PRINT(L0MuonError);         // 43
    PRINT(GaudiSerialize);      // 44
    PRINT(GaudiHeader);         // 45
    PRINT(TTProcFull);          // 46
    PRINT(ITProcFull);          // 47
    PRINT(TAEHeader);           // 48
    PRINT(MuonFull);            // 49
    PRINT(MuonError);           // 50
    PRINT(TestDet);             // 51
    PRINT(L0DUError);           // 52
    PRINT(HltRoutingBits);      // 53
    PRINT(HltSelReports);       // 54
    PRINT(HltVertexReports);    // 55
    PRINT(HltLumiSummary);      // 56
    PRINT(L0PUFull);            // 57
    PRINT(L0PUError);           // 58
    PRINT(DstBank);             // 59
    PRINT(DstData);             // 60
    PRINT(DstAddress);          // 61
    PRINT(FileID);              // 62
    PRINT(VP);                  // 63   
    PRINT(FTCluster);           // 64
    PRINT(VL);                  // 65
    PRINT(UT);                  // 66
    PRINT(UTFull);              // 67
    PRINT(UTError);             // 68
    PRINT(UTPedestal);          // 69
    PRINT(HC);                  // 70
    PRINT(HltTrackReports);     // 71
    PRINT(HCError);             // 72
    PRINT(VPRetinaCluster);     // 73
    PRINT(FTGeneric);           // 74
    PRINT(FTCalibration);       // 75
    PRINT(FTNZS);               // 76
    PRINT(Calo);                // 77
    PRINT(CaloError);           // 78
    PRINT(MuonSpecial);         // 79
    PRINT(RichCommissioning);   // 80
    PRINT(RichError);           // 81
    PRINT(FTSpecial);           // 82
    PRINT(CaloSpecial);         // 83
    PRINT(Plume);               // 84
    PRINT(PlumeSpecial);        // 85
    PRINT(PlumeError);          // 86
    PRINT(VeloThresholdScan);   // 87
    PRINT(FTError);             // 88
    /// DAQ errors:
    PRINT(DaqErrorFragmentThrottled);   // 89
    PRINT(DaqErrorBXIDCorrupted);       // 90 
    PRINT(DaqErrorSyncBXIDCorrupted);   // 91
    PRINT(DaqErrorFragmentMissing);     // 92
    PRINT(DaqErrorFragmentTruncated);   // 93
    PRINT(DaqErrorIdleBXIDCorrupted);   // 94
    PRINT(DaqErrorFragmentMalformed);   // 95
    PRINT(DaqErrorEVIDJumped);          // 96
    PRINT(VeloSPPandCluster);           // 97
    PRINT(UTNZS);                       // 98
    PRINT(UTSpecial);                   // 99

  default:
    return "UNKNOWN";
#undef PRINT
  }
}


/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::bankData( const void* data, std::size_t len, std::size_t words_per_line)   {
  std::size_t cnt = 0, num_words = (len+sizeof(int)-1)/sizeof(int);
  std::stringstream s;
  std::vector<std::string> lines;
  for(const int *p=(const int*)data, *e=p + num_words; p < e; ++p)  {
    s << std::hex << std::setw(8) << std::setfill('0') << std::hex << *p << " ";
    if ( ++cnt == words_per_line ) {
      lines.emplace_back(s.str());
      s.str("");
      cnt = 0;
    }
  }
  if ( cnt > 0 ) lines.emplace_back(s.str());
  return lines;
}

template <Online::bank_types_t::BankType i> std::vector<std::string>
Online::event_print::bank_data(const void* bank,
				     std::size_t len,
				     std::size_t words_per_line)
{
  return event_print::bankData(bank, len, words_per_line);
}

template <> std::vector<std::string>
Online::event_print::bank_data<Online::bank_types_t::ODIN>(const void* bank,
								 std::size_t /* len */,
								 std::size_t /* words_per_line */)
{
  auto* s = (Online::run3_odin_t*)bank;
  std::vector<std::string> lines;
  char text[512];
  ::snprintf(text, sizeof(text), "Run:%7d EID:%16ld Orbit:%8d Bunch:%5d Step:%4d",
	     s->run_number(), s->event_id(), s->orbit_id(), s->bunch_id(), s->step_number());
  lines.push_back(text);

  ::snprintf(text, sizeof(text), "Time:%16lX TCK:%08X NZS:%s TAE-frst:%s TAE-central:%s TAE-win:%3d",
	     s->gps_time(), s->tck(),
	     s->nzs_mode()    ? "YES" : "NO ",
	     s->tae_first()   ? "YES" : "NO ",
	     s->tae_central() ? "YES" : "NO ",
	     s->tae_window());
  lines.push_back(text);

  ::snprintf(text, sizeof(text), "Partition:%08X e_typ:%04X, bx_typ:%04X cal_typ:%04X trg_typ:%04X",
	     s->partition_id(), s->event_type(), s->bx_type(), s->calib_type(), s->trigger_type());
  lines.push_back(text);
  return lines;
}

/// Retrieve dump of bank data in line format
std::vector<std::string>
Online::event_print::bankData(const bank_header_t* header, const void* bank, std::size_t words_per_line)
{
  if ( header->type() == bank_types_t::ODIN && header->version() == 7 )    {
    return bank_data<bank_types_t::ODIN>(bank, header->size(), words_per_line);
  }
  return event_print::bankData(bank, header->size(), words_per_line);
}

/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::bankData( const raw_bank_offline_t* bank, std::size_t words_per_line )   {
  return bankData(bank, bank->data(), words_per_line);
}

/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::bankData( const raw_bank_online_t* bank, std::size_t words_per_line )   {
  return bankData(bank, bank->data(), words_per_line);
}

/// Format MDF header data to a set of lines for printing
std::vector<std::string> Online::event_print::headerData( const event_header_t* hdr )   {
  std::vector<std::string> lines;
  std::stringstream str;
  char text[256];

  ::snprintf(text, sizeof(text), "+----> TELL1  header: @ %p", (void*)hdr);
  lines.emplace_back(text);
  ::snprintf(text, sizeof(text),"| Size: %5d %5d %5d  Checksum: 0x%08X compression: %3d Vsn: %3d",
	     hdr->size0(), hdr->size1(), hdr->size2(), hdr->checkSum(), hdr->compression(), hdr->headerVersion());
  lines.emplace_back(text);
  if ( hdr->headerVersion() == 3 )   {
    const auto* h = hdr->subHeader().H1;
    const auto& m = h->triggerMask();
    ::snprintf(text, sizeof(text),"| Run: %6d  Orbit: 0x%08X Bunch: 0x%08X Mask: %08X %08X %08X %08X",
	       h->runNumber(), h->orbitNumber(), h->bunchID(), m[0], m[1], m[2], m[3]);
    lines.emplace_back(text);
  }
  return lines;
}


