//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  odin_t.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef EVENTDATA_POSIX_H
#define EVENTDATA_POSIX_H

#include <cstdio>
#include <cstdint>
#include <cstring>

struct stat;
struct stat64;
struct dirent;
#include <dirent.h>

/// Online namespace declaration
namespace Online {

  /// Generic posix I/O support
  /** @class posix_t
   *
   * @author:  M.Frank
   * @version: 1.0
   */
  class posix_t {
  public:
    enum { NONE, PARTIAL, COMPLETE };
    posix_t() { ::memset( this, 0, sizeof( posix_t ) ); }

    int unbuffered;
    int     ( *open )( const char* filepath, int flags, ...);
    int     ( *close )( int s );
    int     ( *access )( const char* filepath, int mode );
    int     ( *unlink )( const char* filepath );
    ssize_t ( *read )( int s, void* ptr, size_t size );
    ssize_t ( *write )( int s, const void* ptr, size_t size );
    long    ( *lseek )( int s, long offset, int how );
    int64_t ( *lseek64 )( int s, int64_t offset, int how );
    int     ( *stat )( const char* path, struct stat* statbuf );
    int     ( *stat64 )( const char* path, struct stat64* statbuf );
    int     ( *fstat )( int s, struct stat* statbuf );
    int     ( *fstat64 )( int s, struct stat64* statbuf );

    int buffered;
    FILE*   ( *fopen )( const char*, const char* );
    int     ( *fclose )( FILE* );
    size_t  ( *fwrite )( const void*, size_t, size_t, FILE* );
    size_t  ( *fread )( void*, size_t, size_t, FILE* );
    long    ( *ftell )( FILE* );
    int     ( *fseek )( FILE*, long, int );

    int directory;
    int ( *mkdir )( const char* path, unsigned int mode );
    int ( *rmdir )( const char* path );
    DIR* ( *opendir )( const char* dirpath );
    int ( *closedir )( DIR* dirp );
    dirent* ( *readdir )( DIR* dirp );
  };
} // End namespace LHCb
#endif // EVENTDATA_POSIX_H
