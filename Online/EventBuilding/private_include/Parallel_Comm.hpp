#ifndef PARALLEL_COMM_H
#define PARALLEL_COMM_H 1

#include <vector>
#include <queue>
#ifdef EB_USE_SHARP
#include <api/sharp.h>
#endif

#include "timer.hpp"
#include "logger.hpp"
#include "infiniband_net/ib.hpp"
#include "infiniband_net/sock.hpp"
#include "infiniband_net/parser.hpp"
#include "Dataflow/DataflowContext.h"

namespace EB {

  enum comm_op_status { INIT = 0, PENDING, COMPLETED, ERROR };

  struct comm_op {
    int count;
    size_t datatype;

    virtual ~comm_op() = default;

    virtual std::ostream& print(std::ostream& os) const;
    friend std::ostream& operator<<(std::ostream& os, const comm_op& op);
  };

  struct comm_send : public comm_op {
    const void* snd_buff;
    int destination;

    std::ostream& print(std::ostream& os) const override;
    friend std::ostream& operator<<(std::ostream& os, const comm_send& send);
  };

  struct comm_recv : public comm_op {
    void* recv_buff;
    int source;

    std::ostream& print(std::ostream& os) const override;
    friend std::ostream& operator<<(std::ostream& os, const comm_recv& recv);
  };

#ifdef EB_USE_SHARP
  int oob_barrier(void* ctx);
  int oob_bcast(void* comm_context, void* buff, int size, int root);
  int oob_gather(void* comm_context, int root, void* sbuff, void* rbuff, int len);
#endif

  class Parallel_comm {
  public:
    Parallel_comm(
      IB_verbs::Parser* prs,
      std::string logName,
      Online::PrintLevel logLevel,
      const int connection_timeout,
      unsigned int tree_barrier_group_size,
      bool use_sharp);
    Parallel_comm(const Parallel_comm& src) = delete;
    Parallel_comm& operator=(const Parallel_comm& src) = delete;
    Parallel_comm(Parallel_comm&& src) = default;
    Parallel_comm& operator=(Parallel_comm&& src) = default;
    ~Parallel_comm() = default;
    void ibKillBlocking();
    int ibInit(IB_verbs::Parser* ibprs);
    int getTotalProcesses();
    int getProcessID();
    int ibDeregMRs();
    int send(const std::vector<comm_send>& sends);
    int receive(const std::vector<comm_recv>& recvs);
    int addMR(
      char* bufPtr,
      size_t bufSize,
      int access_flags = IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE);

#ifdef IB_DMABUF_SUPPORT
    int addMRDMABuf(
      char* bufPtr,
      size_t bufSize,
      int dma_buf_fd,
      uint64_t offset,
      int access_flags = IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE);
#endif

    int ibBarrier(IB_verbs::IB::barrier_t type, IB_verbs::IB::syncmode_t mode, bool use_sharp = true);
    int ibScatterV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts);
    int ibBroadcastV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts);
    int ibBroadcast(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts);
    int ibGatherBlockV(
      char* data,
      unsigned int* data_sizes,
      unsigned int* data_idxs,
      size_t elem_size,
      std::vector<int>& remotes);
    std::vector<uint32_t> ibGatherV(
      char* data,
      unsigned int* data_sizes,
      unsigned int* data_idxs,
      size_t elem_size,
      std::vector<int>& remotes,
      int& ret_val);
    int ibTestRecvs(std::vector<uint32_t>& recvs);
    Log_stream logger;

  private:
    std::unique_ptr<IB_verbs::IB> _ibObj;
    int _ibInit(IB_verbs::Parser* ibprs, int connection_timeout, unsigned int tree_barrier_group_size);
    int _sendV(const std::vector<comm_send>& sends);
    std::vector<uint32_t> _receiveV(const std::vector<comm_recv>& recvs);
    int _waitSends(std::vector<uint32_t>& sends);
    int _waitRecvs(std::vector<uint32_t>& recvs);
    int _syncSend(int dest);
    uint32_t _syncRecv(int src);

    // sharp code
    bool _enable_sharp;
#ifdef EB_USE_SHARP
    int _initSharp(IB_verbs::Parser* ibprs);
    // sharp data structure
    typedef std::unique_ptr<struct sharp_coll_context, std::function<void(struct sharp_coll_context*)>>
      sharp_coll_ctx_uniq_ptr_t;
    typedef std::unique_ptr<struct sharp_coll_comm, std::function<void(struct sharp_coll_comm*)>>
      sharp_coll_comm_uniq_ptr_t;
    sharp_coll_ctx_uniq_ptr_t _sharp_coll_ctx;
    sharp_coll_comm_uniq_ptr_t _sharp_coll_comm;
#endif
  };
} // namespace EB

#endif // PARALLEL_COMM_H
