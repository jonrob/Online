#ifndef BUILDERDUMMYUNIT_HPP
#define BUILDERDUMMYUNIT_HPP

#include "fbuff_api.hpp"
#include "Unit.hpp"
#include "common.hpp"
#include <assert.h>
#include <ctime>
#include <malloc.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "shmem_buffer_writer.hpp"
#include "EventBuilding/MEP_tools.hpp"
#include "Dataflow/DataflowComponent.h"
#include "MBM/Producer.h"
#include "mbm_writer.hpp"
#include "buffer_interface.hpp"

/// C/C++ include files
#include <memory>
#include <mutex>

/// Forward declarations

///  Online namespace declaration
namespace Online {

  class Builder_dummy_unit : public DataflowComponent, public Unit {

  public:
    /// Initializing constructor
    Builder_dummy_unit(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~Builder_dummy_unit() = default;

    // constructor/destructor & methods
  protected:
    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;
    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

    // methods
  private:
    void generate_meps();
    bool time_elapsed_check();

    // variables
  private:
    int64_t _now;
    int64_t _timer_start;
    int64_t _delay_timer;
    int64_t _probe_global_start;
    size_t _all_completed_bytes;
    size_t _current_probe_completed_bytes;
    int64_t _next_probing;
    std::string _mbm_name;
    EB::Buffer_writer<EB::MEP>* _buff_writer;
    bool _m_receiveEvts = false;
    int _buffer_type;
  };
} // end namespace Online

#endif
