#ifndef UNIT_HPP
#define UNIT_HPP

namespace Online {

  class Unit {
  public:
    Unit() = default;

  protected:
    int _rank;
    int _worldSize;
  };
} // namespace Online
#endif
