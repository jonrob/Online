#ifndef FBUFF_HPP
#define FBUFF_HPP

#include <vector>
#include "common.hpp"
#include "buffer_interface.hpp"
#include "EventBuilding/MEP_tools.hpp"

#include "Dataflow/Incidents.h"
#include "Dataflow/DataflowComponent.h"
#include "buffer_interface.hpp"
#include <sstream>

namespace FBUFF {

  // Get metadata on all MEP entries (pointer to data and size) given buffer
  // tail, head and its size
  template<class T>
  std::vector<DISPATCH::mep_entry> get_mep(EB::Buffer_reader<T>* _recv_buff);
  template<class T>
  void sync_data_read(EB::Buffer_reader<T>* _recv_buff);
  template<class T>
  void sync_data_write(EB::Buffer_writer<T>* _write_buff);
  // TODO
  //  char* get_buffer_data(shmem_buff& buff);
  template<class T>
  void generate_data(EB::Buffer_writer<T>* _write_buff, int rank, int amount_in_gib);

} // namespace FBUFF

#include "fbuff_api_impl.hpp"
#endif
