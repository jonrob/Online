#ifndef BW_MON_H
#define BW_MON_H 1
#include <ctime>
#include "timer.hpp"

namespace EB {
  class Bw_mon {
  public:
    Bw_mon(clockid_t clk_id = CLOCK_MONOTONIC);
    // resets the bw and the number of bytes sent
    void reset();
    // returns the bw in Gb/s since the reset
    double get_bw() const;
    // returns the bw as in get_bw and resets
    double get_bw_and_reset();
    // returns the bytes sent between the reset and the last add_sent_bytes
    size_t get_bytes() const;
    // returns the bytes sent as in get_bytes and resets
    size_t get_bytes_and_reset();
    // adds n_bytes to the counter
    void add_sent_bytes(size_t n_bytes);

    // setter and getter for the clockid used the setter resets the time to avoid inconsistency
    void set_clock_id(clockid_t clk_id);
    clockid_t get_clock_id() const;

    // returns elapsed time since the last reset in seconds
    double get_elapsed_time();

  protected:
    size_t _bytes_sent;

  private:
    Timer _timer;
  };
} // namespace EB

#endif // BW_MON_H
