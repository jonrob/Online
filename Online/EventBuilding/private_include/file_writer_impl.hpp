#include "file_writer.hpp"

#include <sys/stat.h>
#include <fcntl.h>
#include <sstream>
#include <stdexcept>

template<class T>
EB::File_writer<T>::File_writer(const std::string& file_name) :
  _file_name(file_name), _file(fopen(file_name.c_str(), "wb"), [](FILE* file) { fclose(file); })
{
  if (!is_set()) {
    std::ostringstream err_mess;
    err_mess << __FUNCTION__ << " " << file_name << ": " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }

  // disabling lib buffering
  ::setbuf(_file.get(), NULL);
}

template<class T>
int EB::File_writer<T>::write(const T* data)
{
  int ret_val = 0;
  if (data == NULL) {
    std::ostringstream err_mess;
    err_mess << __FUNCTION__ << " data pointer is NULL.";
    throw std::runtime_error(err_mess.str());
  }

  if (!is_set()) {
    std::ostringstream err_mess;
    err_mess << __FUNCTION__ << " data pointer is NULL.";
    throw std::logic_error(err_mess.str());
  }

  ret_val = fwrite(data, 1, data->bytes(), _file.get());

  return ret_val;
}

template<class T>
EB::File_writer<T>::operator bool() const noexcept
{
  return is_set();
}

template<class T>
bool EB::File_writer<T>::is_set() const noexcept
{
  return static_cast<bool>(_file);
}