#ifndef SHMEM_BUFFER_WRITER_H
#define SHMEM_BUFFER_WRITER_H 1

#include "circular_buffer_writer.hpp"
#include "shared_mem_buffer_backend.hpp"
#include <cstddef>
#include <cstdint>
#include <deque>
#include <iostream>
#include <string>
#include <tuple>

namespace EB {
  template<class T>
  class Shmem_buffer_writer : public Circular_buffer_writer<T, Shared_mem_buffer_backend> {
  public:
    // alignment is explesser in 2^alignment min value 1
    Shmem_buffer_writer() {}
    // Options for unlinking and reconnection are available via the full API of Shared_mem_buffer_backend
    Shmem_buffer_writer(const std::string& shm_name, size_t size, uint8_t alignment = 12, int id = 0) :
      Circular_buffer_writer<T, Shared_mem_buffer_backend>(
        std::move(Shared_mem_buffer_backend(shm_name, true, true, true, size, alignment, id)))
    {}

    virtual ~Shmem_buffer_writer() {}
  };
} // namespace EB
#endif // SHMEM_BUFFER_WRITER_H
