#ifndef EB_LOGGER_H_
#define EB_LOGGER_H_ 1
#include <streambuf>
#include <syslog.h>
#include <cstring>
#include <ostream>
#include <iostream>
#include <memory>
#include "DD4hep/Printout.h"
#include "RTL/rtl.h"

namespace EB {
  enum class Log_priority {
    kLogEmerg = LOG_EMERG,     // system is unusable
    kLogAlert = LOG_ALERT,     // action must be taken immediately
    kLogCrit = LOG_CRIT,       // critical conditions
    kLogErr = LOG_ERR,         // error conditions
    kLogWarning = LOG_WARNING, // warning conditions
    kLogNotice = LOG_NOTICE,   // normal, but significant, condition
    kLogInfo = LOG_INFO,       // informational message
    kLogDebug = LOG_DEBUG,     // debug-level message
    kLogNoLog,                 // unprinted message
  };

  std::string log_priority_to_string(const Log_priority& elem);
  Log_priority online_print_level_to_log_priority(const Online::PrintLevel& elem);
  Online::PrintLevel log_priority_to_online_print_level(const Log_priority& elem);

  // TODO convert Online debug levels to Log_priority

  constexpr Log_priority default_log = Log_priority::kLogInfo;

  constexpr char default_ident[] = "dataflow";
  constexpr int default_facility = LOG_LOCAL0;

  // Meyers Singleton that guarantees single initialization of the syslog infrastructure
  class Log_initializer {
  public:
    static void init_log() { static Log_initializer instance; }

    Log_initializer(const Log_initializer&) = delete;
    Log_initializer& operator=(const Log_initializer&) = delete;
    Log_initializer(const Log_initializer&&) = delete;
    Log_initializer& operator=(const Log_initializer&&) = delete;

  private:
    Log_initializer() : _ident(default_ident) { openlog(_ident.c_str(), LOG_PID, default_facility); }

    ~Log_initializer() { closelog(); }

    std::string _ident;
  };

  class Log : public std::stringbuf {
  public:
    explicit Log(
      std::string name = "",
      Log_priority log_level = default_log,
      bool enable_syslog = true,
      bool enable_online = true,
      int facility = 0);

    void set_log_level(const Log_priority& log_level);
    Log_priority get_log_level() const;

    void set_name(const std::string& name);
    std::string get_name() const;

    void set_facility(int facility);
    int get_facility() const;

    void enable_syslog(bool flag = true);
    void enable_online(bool flag = true);
    bool syslog_enabled() const;
    bool online_enabled() const;

  protected:
    int sync() override;
    int overflow(int c) override;

  private:
    // friend Log_stream& operator<<(Log_stream& os, const Log_priority& log_priority);
    std::string _buffer;
    int _facility;
    Log_priority _log_level;
    std::string _name;
    bool _syslog_ena;
    bool _online_ena;
    std::string _UTGID;
  };

  class Log_stream : public std::ostream {
  public:
    Log_stream(
      std::string name = "",
      Log_priority out_level = default_log,
      bool enable_syslog = true,
      bool enable_online = true,
      int facility = 0);

    Log_stream(Log_stream&&);
    Log_stream& operator=(Log_stream&&);

    Log_stream(Log_stream&) = delete;
    Log_stream& operator=(Log_stream&) = delete;

    virtual ~Log_stream() = default;

    void set_output_level(const Log_priority& output_level);
    void set_output_level(const Online::PrintLevel& output_level);
    Log_priority get_output_level() const;

    Log_stream& set_log_level(const Log_priority& log_level);
    Log_stream& set_log_level(const Online::PrintLevel& log_level);
    Log_priority get_log_level() const;

    bool is_active(const Log_priority& priority) const;
    bool is_active(const Online::PrintLevel& priority) const;

    void set_name(const std::string& name);
    std::string get_name() const;

    void enable_syslog(bool flag = true);
    void enable_online(bool flag = true);
    bool syslog_enabled() const;
    bool online_enabled() const;

    // always is there for backwasrds compatibility
    Log_stream& always() { return set_log_level(Log_priority::kLogEmerg); }
    Log_stream& emergency() { return set_log_level(Log_priority::kLogEmerg); }
    Log_stream& alert() { return set_log_level(Log_priority::kLogAlert); }
    Log_stream& critical() { return set_log_level(Log_priority::kLogCrit); }
    Log_stream& error() { return set_log_level(Log_priority::kLogErr); }
    Log_stream& warning() { return set_log_level(Log_priority::kLogWarning); }
    Log_stream& notice() { return set_log_level(Log_priority::kLogNotice); }
    Log_stream& info() { return set_log_level(Log_priority::kLogInfo); }
    Log_stream& debug() { return set_log_level(Log_priority::kLogDebug); }
    Log_stream& nolog() { return set_log_level(Log_priority::kLogNoLog); }

    Log_stream& operator<<(std::ostream& (*pf)(std::ostream&) )
    {
      auto priority = static_cast<Log*>(rdbuf())->get_log_level();
      if (priority <= _output_level) {
        pf(*(dynamic_cast<std::ostream*>(this)));
      }
      return *this;
    }

    template<typename T>
    Log_stream& operator<<(const T& data)
    {
      auto priority = static_cast<Log*>(rdbuf())->get_log_level();
      if (priority <= _output_level) {
        *(dynamic_cast<std::ostream*>(this)) << data;
      }

      return *this;
    }

  private:
    Log_priority _output_level;
    std::unique_ptr<std::streambuf> _buff;
  };

} // namespace EB

std::ostream& operator<<(std::ostream& os, EB::Log_priority const& log_priority);
EB::Log_stream& operator<<(EB::Log_stream& os, EB::Log_priority const& log_priority);

#endif // EB_LOGGER_H_