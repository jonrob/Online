#ifndef EB_WATCHDOG_H
#define EB_WATCHDOG_H 1
#include <mutex>
#include <chrono>
#include <functional>

namespace EB {
  template<class Rep, class Period, typename... Args>
  class Watchdog {
  public:
    Watchdog(
      std::timed_mutex& mtx,
      std::chrono::duration<Rep, Period> timeout,
      std::function<void(Args...)> callback,
      Args... args) :
      _thread(_watchdog_thread, std::ref(mtx), timeout, callback, args...)
    {}

  private:
    static void _watchdog_thread(
      std::timed_mutex& mtx,
      std::chrono::duration<Rep, Period> timeout,
      std::function<void(Args...)> callback,
      Args... args)
    {
      std::unique_lock<std::timed_mutex> lk(mtx, timeout);
      // a timeout of 0 will be ignored
      if (!lk && (timeout != std::chrono::seconds(0))) {
        callback(args...);
      }
    }

    std::jthread _thread;
  };
} // namespace EB
#endif // EB_WATCHDOG_H