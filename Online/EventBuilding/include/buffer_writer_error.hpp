#ifndef BUFFER_WRITER_ERROR_H
#define BUFFER_WRITER_ERROR_H 1
#include <system_error>
#include <string>

namespace EB {
  enum class Buffer_writer_error {
    ALLOC_FAILED = 1,
    WRITE_FULL,
    BUFFER_OVERFLOW,
    NOT_INIT,
  };

  std::error_code make_error_code(Buffer_writer_error ec);

} // namespace EB

namespace std {
  template<>
  struct is_error_code_enum<EB::Buffer_writer_error> : true_type {
  };

} // namespace std

#endif // BUFFER_WRITER_ERROR_H
