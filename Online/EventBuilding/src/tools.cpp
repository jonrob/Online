#include "EventBuilding/tools.hpp"
#include <ctime>
#include <iostream>
#include <unistd.h>
#include <string>
#include <cstring>
#include <iostream>
#include <sstream>
#include <errno.h>
#include <algorithm>
#include <cctype>

timespec EB::timespec_diff(timespec lhs, timespec rhs)
{
  /* Perform the carry for the later subtraction by updating y. */
  if (lhs.tv_nsec < rhs.tv_nsec) {
    int nsec = (rhs.tv_nsec - lhs.tv_nsec) / 1000000000UL + 1;
    rhs.tv_nsec -= 1000000000UL * nsec;
    rhs.tv_sec += nsec;
  }

  if (lhs.tv_nsec - rhs.tv_nsec > 1000000000L) {
    int nsec = (lhs.tv_nsec - rhs.tv_nsec) / 1000000000UL;
    rhs.tv_nsec += 1000000000UL * nsec;
    rhs.tv_sec -= nsec;
  }

  /* Compute the time remaining to wait.
     tv_nsec is certainly positive. */
  lhs.tv_sec -= rhs.tv_sec;
  lhs.tv_nsec -= rhs.tv_nsec;

  return lhs;
}

timespec EB::timespec_plus(timespec lhs, timespec rhs)
{
  if (lhs.tv_nsec + rhs.tv_nsec > 1000000000L) {
    int nsec = (lhs.tv_nsec + rhs.tv_nsec) / 1000000000UL;
    rhs.tv_nsec -= 1000000000UL * nsec;
    rhs.tv_sec += nsec;
  }

  lhs.tv_sec += rhs.tv_sec;
  lhs.tv_nsec += rhs.tv_nsec;

  return lhs;
}

clock_t EB::timespec_to_ns(const timespec& lhs) { return lhs.tv_sec * 1000000000L + lhs.tv_nsec; }

clock_t EB::timespec_diff_ns(const timespec& lhs, const timespec& rhs)
{
  return timespec_to_ns(lhs) - timespec_to_ns(rhs);
}

clock_t EB::timespec_plus_ns(const timespec& lhs, const timespec& rhs)
{
  return timespec_to_ns(lhs) + timespec_to_ns(rhs);
}

std::vector<std::string> EB::str_split(const std::string& string, const std::string& delim)
{
  std::vector<std::string> ret_val;
  std::string tmp_string = string;
  size_t pos = 0;
  std::string token;

  while ((pos = tmp_string.find(delim)) != std::string::npos) {
    token = tmp_string.substr(0, pos);
    ret_val.emplace_back(token);
    tmp_string.erase(0, pos + delim.length());
  }
  ret_val.emplace_back(tmp_string);

  return ret_val;
}

// TODO do a better implementation of this
int EB::string_icompare(const std::string& lhs, const std::string& rhs)
{
  std::string local_lhs(lhs);
  std::string local_rhs(rhs);

  string_tolower(local_lhs);
  string_tolower(local_rhs);

  return local_lhs.compare(local_rhs);
}

void EB::string_toupper(std::string& str)
{
  std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c) { return std::toupper(c); });
}

void EB::string_tolower(std::string& str)
{
  std::transform(str.begin(), str.end(), str.begin(), [](unsigned char c) { return std::tolower(c); });
}

std::string EB::get_hostname_no_domain()
{
  // hostname should be smaller than 253 char
  constexpr int hostname_length = 256;
  char buff[hostname_length];
  int hostname_err;

  hostname_err = gethostname(buff, hostname_length);

  if (hostname_err != 0) {
    std::ostringstream err_mess;
    err_mess << "Unable to get hostname " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }

  return str_split(std::string(buff), ".")[0];
}

int EB::verify_UTGID(const std::string& UTGID, const std::string& unit_name, const std::string& hostname)
{
  int ret_val = 0;
  auto utgid_tokens = str_split(UTGID, "_");

  if (utgid_tokens.size() != 4) {
    ret_val = 1;
    return ret_val;
  }

  if (utgid_tokens[2] != unit_name) {
    ret_val |= 1 << 2;
  }

  if (string_icompare(utgid_tokens[1], std::string(hostname)) != 0) {
    ret_val |= 1 << 3;
  }

  return ret_val;
}

std::string EB::get_sub_detector_UTGID(const std::string& UTGID, const std::string& unit_name)
{
  auto utgid_tokens = str_split(UTGID, "_");

  std::string hostname = get_hostname_no_domain();

  int UTGID_err = verify_UTGID(UTGID, unit_name, hostname);

  if (UTGID_err != 0) {
    std::ostringstream err_mess;
    err_mess << "Invalid UTGID: the UTGID should be EB_" << hostname << "_" << unit_name << "_<idx>. UTGID: " << UTGID;
    throw std::runtime_error(err_mess.str());
  }

  return utgid_tokens[1].substr(0, 2);
}

int EB::get_idx_UTGID(const std::string& UTGID, const std::string& unit_name)
{
  int ret_val;
  auto utgid_tokens = str_split(UTGID, "_");

  std::string hostname = get_hostname_no_domain();

  int UTGID_err = verify_UTGID(UTGID, unit_name, hostname);

  if (UTGID_err != 0) {
    std::ostringstream err_mess;
    err_mess << "Invalid UTGID: the UTGID should be EB_" << hostname << "_" << unit_name << "_<idx>. UTGID: " << UTGID;
    throw std::runtime_error(err_mess.str());
  }

  try {
    ret_val = stoi(utgid_tokens.back());
  } catch (const std::invalid_argument& e) {
    std::ostringstream err_mess;
    err_mess << "Invalid UTGID: invalid multiplicity index " << e.what();
    throw std::runtime_error(err_mess.str());
  } catch (const std::out_of_range& e) {
    std::ostringstream err_mess;
    err_mess << "Invalid UTGID: multiplicity index out of range " << e.what();
    throw std::runtime_error(err_mess.str());
  } catch (...) {
    std::ostringstream err_mess;
    err_mess << __FUNCTION__ << " unexpected exception: Invalid UTGID " << UTGID;
    throw std::runtime_error(err_mess.str());
  }

  return ret_val;
}

int EB::get_node_idx_UTGID(const std::string& UTGID)
{
  auto utgid_tokens = str_split(UTGID, "_");

  return get_node_idx_host(utgid_tokens[1]);
}

int EB::get_node_idx_host(const std::string& host)
{
  // this makes a strong assumption on the hostname
  return std::stoi(host.substr(4, 2));
}