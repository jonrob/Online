/**
 * @file IB.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */

#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <chrono>
#include <cmath>
#include <cerrno>
#include <cstring>
#include <sstream>
#include <iostream>
#include <netdb.h>
#include "timer.hpp"
#include "watchdog.hpp"

using namespace std;
#ifndef P_INFO
#define P_INFO "Rank " << this->_procNum << ":   "
#endif

IB_verbs::IB::IB()
{
  memset(&this->_port_attr, 0, sizeof(_port_attr));
  memset(&this->_dev_attr, 0, sizeof(_dev_attr));
}

IB_verbs::IB::IB(
  const char* filename,
  const int nProc,
  int sock_connection_timeout,
  unsigned int tree_barrier_group_size)
{
  memset(&this->_port_attr, 0, sizeof(_port_attr));
  memset(&this->_dev_attr, 0, sizeof(_dev_attr));
  this->_ibParseHosts(filename, nProc);
  this->_ibInit(sock_connection_timeout);
}

IB_verbs::IB::IB(const char* filename, int sock_connection_timeout, unsigned int tree_barrier_group_size)
{
  memset(&this->_port_attr, 0, sizeof(_port_attr));
  memset(&this->_dev_attr, 0, sizeof(_dev_attr));
  this->_ibParseHosts(filename);
  this->_ibInit(sock_connection_timeout);
}

IB_verbs::IB::IB(const Parser* prs, int sock_connection_timeout, unsigned int tree_barrier_group_size)
{
  memset(&this->_port_attr, 0, sizeof(_port_attr));
  memset(&this->_dev_attr, 0, sizeof(_dev_attr));
  this->_ibSetHostList(*prs);
  this->_ibInit(sock_connection_timeout);
}

int IB_verbs::IB::getProcessID() const { return this->_procNum; }

int IB_verbs::IB::ibGetDevNum() const { return this->_devNum; }

int IB_verbs::IB::ibGetNumaNode() const { return this->_numaNode; }

int IB_verbs::IB::getTotalProcesses() const { return this->_totalProcs; }

void IB_verbs::IB::_destroyQP(ibv_qp* qp)
{

  if (qp) {
    // transition qp to ERROR
    ibv_qp_attr qp_attr;
    memset(&qp_attr, 0, sizeof(qp_attr));
    qp_attr.qp_state = IBV_QPS_ERR;
    int ret = ibv_modify_qp(qp, &qp_attr, IBV_QP_STATE);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to ERROR");
    }
    // destroy qp
    ret = ibv_destroy_qp(qp);
    if (ret != 0) {
      throw runtime_error("Failed to destroy qp");
    }
    qp = NULL;
  }
}

int IB_verbs::IB::_ibSetQP(Proc& host)
{
  // DATA QPs
  {
    int ret = 0;
    {

      ibv_qp_attr qp_attr = {};
      qp_attr.qp_state = IBV_QPS_INIT;
      qp_attr.qp_access_flags =
        IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_ATOMIC;
      qp_attr.pkey_index = 0;
      qp_attr.port_num = this->_portNum;
      ret =
        ibv_modify_qp(host.qp.get(), &qp_attr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
      if (ret != 0) {
        throw runtime_error("Failed to modify qp to INIT");
      }
    }
    {
      ibv_ah_attr ah_attr = {};
      ah_attr.dlid = host.remote_lid;
      ah_attr.sl = 0;
      ah_attr.src_path_bits = 0;
      ah_attr.is_global = 0;
      ah_attr.port_num = this->_portNum;

      ibv_qp_attr qp_attr = {};
      qp_attr.qp_state = IBV_QPS_RTR;
      qp_attr.path_mtu = IBV_MTU_4096;
      qp_attr.rq_psn = 0;
      qp_attr.dest_qp_num = host.remote_qp_num;
      qp_attr.ah_attr = ah_attr;
      qp_attr.max_dest_rd_atomic = 2;
      qp_attr.min_rnr_timer = 12;

      ret = ibv_modify_qp(
        host.qp.get(),
        &qp_attr,
        IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC |
          IBV_QP_MIN_RNR_TIMER);
      if (ret != 0) {
        throw runtime_error("Failed to modify qp to RTR\n");
      }
    }
    {
      ibv_qp_attr qp_attr = {};
      qp_attr.qp_state = IBV_QPS_RTS;
      qp_attr.sq_psn = 0;
      qp_attr.max_rd_atomic = 2;
      qp_attr.timeout = 14;
      qp_attr.retry_cnt = 7;
      qp_attr.rnr_retry = 7;

      ret = ibv_modify_qp(
        host.qp.get(),
        &qp_attr,
        IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT | IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC);
      if (ret != 0) {
        throw runtime_error("Failed to modify qp to RTS\n");
      }
    }
  }
  // SYNC QP
  int ret = 0;
  {

    ibv_qp_attr qp_attr = {};
    qp_attr.qp_state = IBV_QPS_INIT;
    qp_attr.qp_access_flags =
      IBV_ACCESS_REMOTE_READ | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_ATOMIC;
    qp_attr.pkey_index = 0;
    qp_attr.port_num = this->_portNum;

    ret =
      ibv_modify_qp(host.sync_qp.get(), &qp_attr, IBV_QP_STATE | IBV_QP_PKEY_INDEX | IBV_QP_PORT | IBV_QP_ACCESS_FLAGS);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to INIT\n");
    }
  }
  {
    ibv_ah_attr ah_attr = {};
    ah_attr.dlid = host.remote_lid;
    ah_attr.sl = 0;
    ah_attr.src_path_bits = 0;
    ah_attr.is_global = 0;
    ah_attr.port_num = this->_portNum;

    ibv_qp_attr qp_attr = {};
    qp_attr.qp_state = IBV_QPS_RTR;
    qp_attr.path_mtu = IBV_MTU_4096;
    qp_attr.rq_psn = 0;
    qp_attr.dest_qp_num = host.remote_sync_qp_num;
    qp_attr.ah_attr = ah_attr;
    qp_attr.max_dest_rd_atomic = 2;
    qp_attr.min_rnr_timer = 12;

    ret = ibv_modify_qp(
      host.sync_qp.get(),
      &qp_attr,
      IBV_QP_STATE | IBV_QP_AV | IBV_QP_PATH_MTU | IBV_QP_DEST_QPN | IBV_QP_RQ_PSN | IBV_QP_MAX_DEST_RD_ATOMIC |
        IBV_QP_MIN_RNR_TIMER);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to RTR\n");
    }
  }
  {
    ibv_qp_attr qp_attr = {};
    qp_attr.qp_state = IBV_QPS_RTS;
    qp_attr.sq_psn = 0;
    qp_attr.max_rd_atomic = 2;
    qp_attr.timeout = 14;
    qp_attr.retry_cnt = 7;
    qp_attr.rnr_retry = 7;

    ret = ibv_modify_qp(
      host.sync_qp.get(),
      &qp_attr,
      IBV_QP_STATE | IBV_QP_TIMEOUT | IBV_QP_RETRY_CNT | IBV_QP_RNR_RETRY | IBV_QP_SQ_PSN | IBV_QP_MAX_QP_RD_ATOMIC);
    if (ret != 0) {
      throw runtime_error("Failed to modify qp to RTS\n");
    }
  }
  return 0;
}

ibv_mr* IB_verbs::IB::ibAddMR(char* bufPtr, size_t bufSize, int access_flags)
{
  // FIXME uninitialized return value
  if (bufPtr == NULL && bufSize == 0) {
    return NULL;
  }
  if (_memMap.size() > 0) {
    for (size_t i = 0; i < _memMap.size(); i++) {
      if (_memMap.at(i)._bufPtr == bufPtr) {
        // same pointer
        if (_memMap.at(i)._bufSize >= bufSize) {
          return _memMap.at(i)._mr.get();
        }
      } else if (
        bufPtr > _memMap.at(i)._bufPtr && bufPtr < (_memMap.at(i)._bufPtr + _memMap.at(i)._bufSize) &&
        (_memMap.at(i)._bufPtr + _memMap.at(i)._bufSize) >= (bufSize + bufPtr)) {
        return _memMap.at(i)._mr.get();
      }
    }
  }
  memreg item = {};
  item._bufPtr = bufPtr;
  item._bufSize = bufSize;
  item._mr = unique_mr_t(
    ibv_reg_mr(this->_pd.get(), (void*) bufPtr, bufSize, access_flags | IBV_ACCESS_RELAXED_ORDERING),
    [](ibv_mr* elem) { ibv_dereg_mr(elem); });
  if (item._mr.get() == NULL) {
    return NULL;
  }

  _memMap.push_back(move(item));
  return _memMap.back()._mr.get();
}

#ifdef IB_DMABUF_SUPPORT
ibv_mr* IB_verbs::IB::ibAddMRDMABuf(char* bufPtr, size_t bufSize, int dma_buf_fd, uint64_t offset, int access_flags)
{
  // FIXME uninitialized return value
  if (bufPtr == NULL && bufSize == 0) {
    return NULL;
  }
  if (_memMap.size() > 0) {
    for (size_t i = 0; i < _memMap.size(); i++) {
      if (_memMap.at(i)._bufPtr == bufPtr) {
        // same pointer
        if (_memMap.at(i)._bufSize >= bufSize) {
          return _memMap.at(i)._mr.get();
        }
      } else if (
        bufPtr > _memMap.at(i)._bufPtr && bufPtr < (_memMap.at(i)._bufPtr + _memMap.at(i)._bufSize) &&
        (_memMap.at(i)._bufPtr + _memMap.at(i)._bufSize) >= (bufSize + bufPtr)) {
        return _memMap.at(i)._mr.get();
      }
    }
  }
  memreg item = {};
  item._bufPtr = bufPtr;
  item._bufSize = bufSize;
  item._mr = unique_mr_t(
    ibv_reg_dmabuf_mr(
      this->_pd.get(),
      offset,
      bufSize,
      reinterpret_cast<uint64_t>(bufPtr),
      dma_buf_fd,
      access_flags | IBV_ACCESS_RELAXED_ORDERING),
    [](ibv_mr* elem) { ibv_dereg_mr(elem); });
  if (item._mr.get() == NULL) {
    return NULL;
  }

  _memMap.push_back(move(item));
  return _memMap.back()._mr.get();
}
#endif

int IB_verbs::IB::ibRemoveMR(char* bufPtr, size_t bufSize)
{
  for (size_t i = 0; i < _memMap.size(); i++) {
    if (_memMap.at(i)._bufPtr == bufPtr && _memMap.at(i)._bufSize == bufSize) {
      _memMap.at(i) = move(_memMap.back());
      _memMap.pop_back();
      return 0;
    }
  }
  return -1;
}

void IB_verbs::IB::_ibParseHosts(const char* filename, const int nProc)
{
  Parser prs(filename, nProc);
  _ibSetHostList(prs);
}

void IB_verbs::IB::_ibParseHosts(const char* filename)
{
  Parser prs(filename);
  _ibSetHostList(prs);
}

void IB_verbs::IB::_ibSetHostList(const Parser& hostParser)
{
  auto list = hostParser.getHostList();
  this->_totalProcs = hostParser.getTotalProcesses();
  this->_procNum = hostParser.getProcessId();
  this->_devName = hostParser.getIbDevString();
  this->_devNum = hostParser.getIbDevNum();
  this->_numaNode = hostParser.getIbNumaNode();
  _hosts.resize(list.size());
  for (size_t i = 0; i < list.size(); i++) {
    _hosts[i].set_addrInfo(list[i].hostname, std::to_string(list[i].port));
  }
}

void IB_verbs::IB::_getDevInfo()
{
  int num_devs = 0;
  this->_dev_list = unique_dev_t(ibv_get_device_list(&num_devs), [](ibv_device** elem) { ibv_free_device_list(elem); });
  if (this->_dev_list == NULL) {
    throw runtime_error("cannot find IB devices");
  }
  this->_devNum = -1;
  for (int i = 0; i < num_devs; i++) {
    string tmp(ibv_get_device_name(this->_dev_list.get()[i]));
    if (this->_devName == tmp) {
      _devNum = i;
      break;
    }
  }
  if (_devNum == -1) {
    throw runtime_error("cannot find the device");
  }
  string path(this->_dev_list.get()[this->_devNum]->ibdev_path);
  string numastr;
  ifstream numafile(path + "/device/numa_node");
  if (numafile.is_open()) {
    numafile >> numastr;
    this->_numaNode = stoi(numastr);
    numafile.close();
  } else {
    this->_numaNode = -1;
  }
}

void IB_verbs::IB::_ibInit(int sock_connection_timeout, unsigned int tree_barrier_group_size)
{
#ifdef IB_DUMP_FILE
  std::stringstream base_dump_filename;
  char* utgid = getenv("UTGID");
  base_dump_filename << _dump_file_path << "/" << utgid;
  _wc_dump_file = std::ofstream(base_dump_filename.str() + "_wc_dump.txt");
  _wr_dump_file = std::ofstream(base_dump_filename.str() + "_wr_dump.txt");
  if (!_wc_dump_file.is_open() or !_wr_dump_file.is_open()) {
    throw runtime_error("cannot open IB dump files " + base_dump_filename.str());
  }
#endif

  // init atomic
  _wrId.store(0);
  // if no dev list (can happen if ibInit is recalled)
  int num_devs;
  // TODO remove this check _ibInit should be called only by the constructor
  if (this->_dev_list.get() == NULL) {
    this->_dev_list =
      unique_dev_t(ibv_get_device_list(&num_devs), [](ibv_device** elem) { ibv_free_device_list(elem); });
    if (this->_dev_list == NULL) {
      string err_mess("Error while opening IB devices: ");
      err_mess += strerror(errno);
      throw runtime_error(err_mess);
    } else if (num_devs == 0) {
      throw runtime_error("cannot find IB devices");
    }
  }
  // open the device
  this->_ctx = unique_ctx_t(
    ibv_open_device(this->_dev_list.get()[this->_devNum]), [](ibv_context* elem) { ibv_close_device(elem); });
  if (this->_ctx.get() == NULL) {
    throw runtime_error("cannot open IB device");
  }
  // allocate protection domain
  this->_pd = unique_pd_t(ibv_alloc_pd(this->_ctx.get()), [](ibv_pd* elem) { ibv_dealloc_pd(elem); });
  if (this->_pd == NULL) {
    throw runtime_error("cannot allocate protection domain " + std::string(std::strerror(errno)));
  }
  // query dev port
  int ret = ibv_query_port(this->_ctx.get(), this->_portNum, &this->_port_attr);
  if (ret < 0) {
    throw runtime_error("cannot query IB port on device");
  }
  if (this->_port_attr.state != IBV_PORT_ACTIVE) {
    throw runtime_error("IB port is not active, check cable");
  }
  if (this->_port_attr.phys_state != 5 /*LINKUP*/) {
    throw runtime_error("IB port physical link error, check SM");
  }
  // get dev info
  ret = ibv_query_device(this->_ctx.get(), &(this->_dev_attr));
  if (ret < 0) {
    throw runtime_error("cannot query device for attributes " + std::string(std::strerror(errno)));
  }
  // create completion queues
  this->_sendcq =
    unique_cq_t(ibv_create_cq(this->_ctx.get(), (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0), [](ibv_cq* elem) {
      ibv_destroy_cq(elem);
    });
  if (this->_sendcq == NULL) {
    throw runtime_error("cannot create data send completion queue " + std::string(std::strerror(errno)));
  }
  this->_recvcq =
    unique_cq_t(ibv_create_cq(this->_ctx.get(), (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0), [](ibv_cq* elem) {
      ibv_destroy_cq(elem);
    });
  if (this->_recvcq == NULL) {
    throw runtime_error("cannot create data receive completion queue " + std::string(std::strerror(errno)));
  }
  this->_syncScq =
    unique_cq_t(ibv_create_cq(this->_ctx.get(), (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0), [](ibv_cq* elem) {
      ibv_destroy_cq(elem);
    });
  if (this->_syncScq == NULL) {
    throw runtime_error("cannot create sync send completion queue " + std::string(std::strerror(errno)));
  }
  this->_syncRcq =
    unique_cq_t(ibv_create_cq(this->_ctx.get(), (this->_dev_attr.max_cqe) / 3, NULL, NULL, 0), [](ibv_cq* elem) {
      ibv_destroy_cq(elem);
    });
  if (this->_syncRcq == NULL) {
    throw runtime_error("cannot create sync receive completion queue " + std::string(std::strerror(errno)));
  }

  // CREATING QUEUE PAIRS FOR EVERY REMOTE
  ibv_qp_cap cap = {};
  cap.max_send_wr = 8192;
  cap.max_recv_wr = (unsigned int) this->_dev_attr.max_qp_wr;
  cap.max_send_sge = 1;
  cap.max_recv_sge = 1;

  ibv_qp_init_attr qp_init_attr = {};
  qp_init_attr.send_cq = this->_sendcq.get();
  qp_init_attr.recv_cq = this->_recvcq.get();
  qp_init_attr.cap = cap;
  qp_init_attr.qp_type = IBV_QPT_RC;

  ibv_qp_init_attr qp_sync_init_attr = {};
  qp_sync_init_attr.send_cq = this->_syncScq.get();
  qp_sync_init_attr.recv_cq = this->_syncRcq.get();
  qp_sync_init_attr.cap = cap;
  qp_sync_init_attr.qp_type = IBV_QPT_RC;

  // creating queue pairs
  for (size_t i = 0; i < _hosts.size(); i++) {
    // data qp
    _hosts.at(i).qp = Proc::unique_qp_t(ibv_create_qp(this->_pd.get(), &qp_init_attr), [](ibv_qp* elem) {
      // The deleter should be nothrow
      try {
        IB::_destroyQP(elem);
      } catch (...) {
      }
    });
    if (_hosts.at(i).qp.get() == NULL) {
      throw runtime_error("cannot create data queue pair " + std::string(std::strerror(errno)));
    }
    // sync qp
    _hosts.at(i).sync_qp = Proc::unique_qp_t(ibv_create_qp(this->_pd.get(), &qp_sync_init_attr), [](ibv_qp* elem) {
      try {
        // The deleter should be nothrow
        IB::_destroyQP(elem);
      } catch (...) {
      }
    });
    if (_hosts.at(i).sync_qp.get() == NULL) {
      throw runtime_error("cannot query sync queue pair " + std::string(std::strerror(errno)));
    }
  }
  // adding thread for recv qp data over tcp
  std::atomic<int> retsocket = {0};
  // TODO this is in principle not needed, check if this is necessary or not
  this->_totalProcs = this->_hosts.size();
  // _QPrecv = std::jthread(Sock::recvQPs, sock_connection_timeout + 300, _procNum, std::ref(_hosts),
  // std::ref(retsocket));
  _QPrecv = std::jthread(Sock::recvQPs, 0, _procNum, std::ref(_hosts), std::ref(retsocket));
  // std::atomic<bool> timeout(false);
  // std::timed_mutex watchdog_mtx;
  // {
  // EB::Watchdog<int, std::ratio<1>> watchdog(
  // watchdog_mtx, std::chrono::duration<int>(sock_connection_timeout), [&timeout, this]() {
  // timeout = true;
  // this->_QPrecv.request_stop();
  // });
  EB::Timer client_timer;
  client_timer.start();
  // sending qp data over tcp
  for (size_t i = 0; i < _hosts.size(); i++) {
    client_timer.reset();
    _hosts.at(i).local_lid = this->_port_attr.lid;
    while (_sockObj.sendQP(this->_procNum, _hosts.at(i)) < 0) {
      using namespace std::chrono_literals;
      std::this_thread::sleep_for(500ms);
      if ((sock_connection_timeout != 0) && (client_timer.get_elapsed_time_s() > sock_connection_timeout)) {
        // if (timeout) {
        std::stringstream err;
        err << "timeout sending qp info to remote " << i << " " << _hosts[i].get_addrInfo()->ai_canonname
            << " server err value " << retsocket;
        // TODO maybe we shouldn't throw here this mutes the server error
        throw runtime_error(err.str());
      }
    }
  }
  if (!_QPrecv.joinable()) {
    throw logic_error("cannot join socket thread");
  }

  // TODO add timeout and stop request
  _QPrecv.join();
  // watchdog_mtx.unlock();
  // }

  switch (retsocket.load()) {
  case -1: throw runtime_error("socket bind error"); break;
  case -2: throw runtime_error("socket listen error"); break;
  case -3: throw runtime_error("exchange accept error"); break;
  case -4: throw runtime_error("exchange receive error"); break;
  case -5: throw runtime_error("exchange shutdown error"); break;
  case -6: throw runtime_error("exchange close error"); break;
  case -7: throw runtime_error("socket close error"); break;
  case -8: throw runtime_error("server timeout receiving QP info"); break;
  }

  // setting up the qps
  for (size_t i = 0; i < _hosts.size(); i++) {
    this->_ibSetQP(_hosts.at(i));
  }
  // FILLING REVERSE LUT
  for (size_t i = 0; i < _hosts.size(); i++) {
    uint32_t qp_n = _hosts.at(i).sync_qp->qp_num;
    _reverse_hosts[qp_n] = i;
  }
  // PRIMING SYNC RECVS
  if (_syncInit() != 0) {
    throw runtime_error("sync init failed");
  }
  // STARTING SYNC THREAD
  _syncThread = std::jthread([this](std::stop_token stoken) { this->_syncFunc(stoken); });

  // COMPUTE LUT FOR BARRIER
  _initTournament();
  _initCentral();
  _initTree(tree_barrier_group_size);
  _initMCS();
  // SEND SYNC MR
  // allocate and register sync MR
  _syncBuf =
    unique_char_t(reinterpret_cast<char*>(memalign(4096, _totalProcs * sizeof(char))), [](char* elem) { free(elem); });
  if (_syncBuf.get() == NULL) {
    throw runtime_error("failed to allocate sync MR buffer");
  }
  _syncMR._bufPtr = _syncBuf.get();
  _syncMR._bufSize = _totalProcs * sizeof(char);
  // set buffer to 0 except for the byte corresponding to this process
  memset(_syncBuf.get(), 0, _syncMR._bufSize);
  _syncMR._bufPtr[_procNum] = 1;

  _syncMR._mr = unique_mr_t(
    ibv_reg_mr(
      this->_pd.get(),
      (void*) _syncBuf.get(),
      _syncMR._bufSize,
      IBV_ACCESS_LOCAL_WRITE | IBV_ACCESS_REMOTE_WRITE | IBV_ACCESS_RELAXED_ORDERING),
    [](ibv_mr* elem) { ibv_dereg_mr(elem); });
  if (_syncMR._mr == NULL) {
    throw runtime_error("failed to register sync MR");
  }

  // this section can deadlock we put a watchdog
  std::timed_mutex watchdog_mtx;
  {
    std::lock_guard lk(watchdog_mtx);
    EB::Watchdog<int, std::ratio<1>> watchdog(
      watchdog_mtx, std::chrono::duration<int>(sock_connection_timeout), [this]() {
        std::cout << "Watchdog kill" << std::endl;
        this->ibKillBlocking();
      });
    // exchange sync MR RDMA keys and address
    vector<uint32_t> wrs;
    for (int i = 0; i < _hosts.size(); i++) {
      if (i != _procNum) {
        wrs.push_back(_ibRecvRDMAsyncMR(i));
      }
    }
    for (int i = 0; i < _hosts.size(); i++) {
      if (i != _procNum) {
        ret = _ibSendRDMAsyncMR(_syncMR._mr.get(), i);
        if (ret != 0) {
          throw runtime_error("failed to send sync mr");
        }
      }
    }
    ret = ibWaitAllRecvs(wrs);
    if (ret != 0) {
      throw runtime_error("failed to receive sync mr");
    }
    // we unlock explicitly to prevent destructors races between the watchdog and the lock_guard
    watchdog_mtx.unlock();
  }
}

void IB_verbs::IB::ibDeregMRs() { _memMap.clear(); }

void IB_verbs::IB::ibKillBlocking() { _kill_blocking = true; }

uint32_t IB_verbs::IB::_ibGetNextWrId()
{
  uint32_t ret_val = _wrId.fetch_add(1);
  // 0 is not a valid ID
  if (ret_val == 0) {
    return _ibGetNextWrId();
  }
  return ret_val;
}

#ifdef IB_DUMP_FILE
void IB_verbs::IB::_dump_wc(const struct ibv_wc& wc)
{
  std::lock_guard<std::mutex> file_guard(_wc_dump_file_mutex);
  _wc_dump_file << wc.qp_num;
  _wc_dump_file << "," << wc.status << "," << wc.opcode << "," << wc.wr_id << std::endl;
}

void IB_verbs::IB::_dump_wr(const IB_verbs::IB::ib_wr& wr)
{
  std::lock_guard<std::mutex> file_guard(_wr_dump_file_mutex);
  _wr_dump_file << wr.qp_num << "," << wr.src_qp;
  if (!wr.receive) {
    _wr_dump_file << "," << wr.opcode;
  } else {
    _wr_dump_file << ",recv";
  }
  _wr_dump_file << std::endl;
}
#endif
