/**
 * @file ib_collective.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library - Collective Methods
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <random>
#include <chrono>
#include <cmath>

using namespace std;

#ifndef P_INFO
#define P_INFO "Rank " << _procNum << ":   "
#endif

int IB_verbs::IB::ibBarrier(barrier_t type, syncmode_t mode)
{
  switch (type) {
  case TOURNAMENT: return this->_runTournament(mode); break;
  case CENTRAL: return this->_runCentral(mode); break;
  case TREE: return this->_runTree(mode); break;
  case DUMMY: return this->_runDummy(); break;
  case MCS: return this->_runMCS(mode); break;
  default: return this->_runTournament(mode); break;
  }
}

int IB_verbs::IB::_initMCS()
{
  for (int i = 0; i < 4; i++) {
    // for each fan-in node
    if (_totalProcs > ((4 * _procNum) + i + 1)) {
      _self_node.child_ids_in.push_back((4 * _procNum) + i + 1);
    }
  }
  if (_procNum != 0) {
    _self_node.parent_id_in = (_procNum - 1) / 4;  // calculate parent node
    _self_node.parent_id_out = (_procNum - 1) / 2; // calculate parent node
  }
  for (size_t i = 0; i < 2; i++) {
    if (_totalProcs > ((2 * _procNum) + i + 1)) {
      _self_node.child_ids_out.push_back((2 * _procNum) + i + 1);
    }
  }
  return 0;
}

int IB_verbs::IB::_runMCS(syncmode_t mode)
{
  int res = 0;
  if (mode == SENDRECV) {
    if (!_self_node.child_ids_in.empty()) {
      _barrierMasterRecv(_self_node.child_ids_in.begin(), _self_node.child_ids_in.end());
    }
    if (_procNum != 0) {
      // cout << P_INFO << "send to parent"<< endl;
      res = _barrierMinionSnd(_self_node.parent_id_in);
      if (res != 0) {
        return res;
      }
      // release phase
      // cout << P_INFO << "wait for release"<< endl;
      res = _barrierMinionRecv(_self_node.parent_id_out);
      if (res != 0) {
        return res;
      }
    }
    if (!_self_node.child_ids_out.empty()) {
      res = _barrierMasterSnd(_self_node.child_ids_out.begin(), _self_node.child_ids_out.end());
    }
  } else {

    if (!_self_node.child_ids_in.empty()) {
      _barrierMasterRecvRW(_self_node.child_ids_in.begin(), _self_node.child_ids_in.end());
    }
    if (_procNum != 0) {
      // cout << P_INFO << "send to parent"<< endl;
      res = _barrierMinionSndRW(_self_node.parent_id_in);
      if (res != 0) {
        return res;
      }
      // release phase
      // cout << P_INFO << "wait for release"<< endl;
      res = _barrierMinionRecvRW(_self_node.parent_id_out);
      if (res != 0) {
        return res;
      }
    }
    if (!_self_node.child_ids_out.empty()) {
      res = _barrierMasterSndRW(_self_node.child_ids_out.begin(), _self_node.child_ids_out.end());
    }
  }
  return res;
}

int IB_verbs::IB::_initTournament()
{
  int rounds = static_cast<int>(log2(_totalProcs));
  int tmpTotalProcs = _totalProcs;
  for (int i = 1; i <= rounds; i++) {
    if (_procNum % (1 << i) == 0) {
      if (_procNum <= (tmpTotalProcs - (1 << i))) {
        int child = (_procNum + (1 << (i - 1))); // calculate child
        // cout << P_INFO << "S recv " << src << endl;
        _tourNode.child_ids.push_back(child);
        int mod = tmpTotalProcs % ((1 << i));
        if (mod != 0) { // add receive for _tournament_lonely processes
          if (_procNum == (tmpTotalProcs - 3 * (1 << (i - 1)))) {
            int child_spare = (tmpTotalProcs - (1 << (i - 1)));
            // cout << P_INFO << "D recv " << src << endl;
            _tourNode.child_ids.push_back(child_spare);
          }
          tmpTotalProcs -= mod;
        }
      } else {
        int parent = (_procNum - (1 << i));
        //_tourNode.child_ids_in.push_back(src); // recv release
        _tourNode.parent_node = parent;
        break;
      }
    } else {
      int parent = (_procNum - (1 << (i - 1)));
      // cout << P_INFO << "R recv " << src << endl;
      _tourNode.parent_node = parent;
      break;
    }
  }
  return 0;
}

int IB_verbs::IB::_runTournament(syncmode_t mode)
{
  int res = 0;
  if (mode == SENDRECV) {
    if (!_tourNode.child_ids.empty()) {
      res = this->_barrierMasterRecv(_tourNode.child_ids.begin(), _tourNode.child_ids.end());
      if (res != 0) {
        return res;
      }
    }
    if (_tourNode.parent_node > -1) {
      res = _barrierMinionSnd(_tourNode.parent_node);
      if (res != 0) {
        return res;
      }
      res = _barrierMinionRecv(_tourNode.parent_node);
      if (res != 0) {
        return res;
      }
    }
    if (!_tourNode.child_ids.empty()) {
      res = this->_barrierMasterSnd(_tourNode.child_ids.begin(), _tourNode.child_ids.end());
      if (res != 0) {
        return res;
      }
    }
  } else {
    if (!_tourNode.child_ids.empty()) {
      res = this->_barrierMasterRecvRW(_tourNode.child_ids.begin(), _tourNode.child_ids.end());
      if (res != 0) {
        return res;
      }
    }
    if (_tourNode.parent_node > -1) {
      res = _barrierMinionSndRW(_tourNode.parent_node);
      if (res != 0) {
        return res;
      }
      res = _barrierMinionRecvRW(_tourNode.parent_node);
      if (res != 0) {
        return res;
      }
    }
    if (!_tourNode.child_ids.empty()) {
      res = this->_barrierMasterSndRW(_tourNode.child_ids.begin(), _tourNode.child_ids.end());
      if (res != 0) {
        return res;
      }
    }
  }
  return 0;
}

int IB_verbs::IB::_initCentral()
{
  this->_central_master_node = 0;
  this->_central_minion_nodes.resize(this->_totalProcs - 1);
  std::iota(this->_central_minion_nodes.begin(), this->_central_minion_nodes.end(), 1);
  return 0;
}

int IB_verbs::IB::_runCentral(syncmode_t mode)
{
  int res;
  if (mode == SENDRECV) {
    if (_procNum == _central_master_node) {
      res = _barrierMasterRecv(_central_minion_nodes.begin(), _central_minion_nodes.end());
      if (res != 0) {
        return res;
      }
      res = _barrierMasterSnd(_central_minion_nodes.begin(), _central_minion_nodes.end());
      if (res != 0) {
        return res;
      }
    } else {
      res = _barrierMinionSnd(_central_master_node);
      if (res != 0) {
        return res;
      }
      res = _barrierMinionRecv(_central_master_node);
      if (res != 0) {
        return res;
      }
    }
  } else {
    if (_procNum == _central_master_node) {
      res = _barrierMasterRecvRW(_central_minion_nodes.begin(), _central_minion_nodes.end());

      if (res != 0) {
        return res;
      }
      res = _barrierMasterSndRW(_central_minion_nodes.begin(), _central_minion_nodes.end());
      if (res != 0) {
        return res;
      }
    } else {
      res = _barrierMinionSndRW(_central_master_node);
      if (res != 0) {
        return res;
      }
      res = _barrierMinionRecvRW(_central_master_node);
      if (res != 0) {
        return res;
      }
    }
  }
  return 0;
}

int IB_verbs::IB::_runDummy() { return 0; }

int IB_verbs::IB::_initTree(unsigned int group_size)
{
  _tree_group_size = group_size;
  size_t n_groups = _totalProcs / group_size + 1;
  _tree_master_nodes.resize(n_groups);
  _tree_minion_nodes.resize(n_groups);
  // TODO probably calculate all the groups on all the nodes is an overkill
  for (size_t k = 0; k < _totalProcs; k++) {
    size_t group_num = k / group_size;
    if (k % group_size == 0) {
      _tree_master_nodes[group_num] = k;
    } else {
      _tree_minion_nodes[group_num].push_back(k);
    }
  }

  return 0;
}

int IB_verbs::IB::_runTree(syncmode_t mode)
{
  int res = 0;
  bool is_master = (_procNum % _tree_group_size) == 0;
  size_t group_num = _procNum / _tree_group_size;
  if (mode == SENDRECV) {
    if (is_master) {
      // receive from all minions
      res = _barrierMasterRecv(_tree_minion_nodes[group_num].begin(), _tree_minion_nodes[group_num].end());
      if (res != 0) {
        return res;
      }
      // master of the masters
      if (_procNum == _tree_master_nodes[0]) {
        // the first node is self
        // receive from first layer masters
        res = _barrierMasterRecv(_tree_master_nodes.begin() + 1, _tree_master_nodes.end());
        if (res != 0) {
          return res;
        }

        // send to first layer masters
        res = _barrierMasterSnd(_tree_master_nodes.begin() + 1, _tree_master_nodes.end());
        if (res != 0) {
          return res;
        }
      } else {
        // send to master of masters
        res = _barrierMinionSnd(_tree_master_nodes[0]);
        if (res != 0) {
          return res;
        }

        // receive from master of masters
        res = _barrierMinionRecv(_tree_master_nodes[0]);
        if (res != 0) {
          return res;
        }
      }

      // send to all minions
      res = _barrierMasterSnd(_tree_minion_nodes[group_num].begin(), _tree_minion_nodes[group_num].end());
      if (res != 0) {
        return res;
      }
    } else {
      // send to local master
      res = _barrierMinionSnd(_tree_master_nodes[group_num]);
      if (res != 0) {
        return res;
      }

      // receive from local master
      res = _barrierMinionRecv(_tree_master_nodes[group_num]);
      if (res != 0) {
        return res;
      }
    }
  } else {
    if (is_master) {
      // receive from all minions
      res = _barrierMasterRecvRW(_tree_minion_nodes[group_num].begin(), _tree_minion_nodes[group_num].end());
      if (res != 0) {
        return res;
      }
      // master of the masters
      if (_procNum == _tree_master_nodes[0]) {
        // the first node is self
        // receive from first layer masters
        res = _barrierMasterRecvRW(_tree_master_nodes.begin() + 1, _tree_master_nodes.end());
        if (res != 0) {
          return res;
        }

        // send to first layer masters
        res = _barrierMasterSndRW(_tree_master_nodes.begin() + 1, _tree_master_nodes.end());
        if (res != 0) {
          return res;
        }
      } else {
        // send to master of masters
        res = _barrierMinionSndRW(_tree_master_nodes[0]);
        if (res != 0) {
          return res;
        }

        // receive from master of masters
        res = _barrierMinionRecvRW(_tree_master_nodes[0]);
        if (res != 0) {
          return res;
        }
      }

      // send to all minions
      res = _barrierMasterSndRW(_tree_minion_nodes[group_num].begin(), _tree_minion_nodes[group_num].end());
      if (res != 0) {
        return res;
      }
    } else {
      // send to local master
      res = _barrierMinionSndRW(_tree_master_nodes[group_num]);
      if (res != 0) {
        return res;
      }

      // receive from local master
      res = _barrierMinionRecvRW(_tree_master_nodes[group_num]);
      if (res != 0) {
        return res;
      }
    }
  }
  return res;
}

int IB_verbs::IB::_barrierMasterRecv(
  std::vector<uint32_t>::iterator minions_it_start,
  std::vector<uint32_t>::iterator minions_it_end)
{
  int res;
  vector<array<uint32_t, 2>> recvs_ids;
  recvs_ids.reserve(std::distance(minions_it_start, minions_it_end));
  for (auto it = minions_it_start; it != minions_it_end; it++) {
    recvs_ids.push_back({{BARRIER, *it}});
  }
  res = this->ibWaitAllSyncRecvs(recvs_ids);
  if (res != 0) {
    return res;
  }
  return 0;
}

int IB_verbs::IB::_barrierMasterSnd(
  std::vector<uint32_t>::iterator minions_it_start,
  std::vector<uint32_t>::iterator minions_it_end)
{
  int res;
  vector<uint32_t> send_ids;
  uint32_t temp_wr;
  send_ids.reserve(std::distance(minions_it_start, minions_it_end));
  for (auto it = minions_it_start; it != minions_it_end; it++) {
    temp_wr = this->ibSendSync(BARRIER, *it);
    if (temp_wr == 0) {
      return -1;
    }
    send_ids.push_back(temp_wr);
  }
  res = this->ibWaitAllSyncSends(send_ids);
  if (res != 0) {
    return res;
  }
  return 0;
}

int IB_verbs::IB::_barrierMinionSnd(uint32_t master)
{
  int res = 0;
  uint32_t wrid = this->ibSendSync(BARRIER, master);
  if (wrid == 0) {
    return -1;
  }
  res = this->ibWaitSyncSend(wrid);
  if (res != 0) {
    return res;
  }
  return res;
}

int IB_verbs::IB::_barrierMinionRecv(uint32_t master)
{
  int res = 0;
  res = this->ibWaitSyncRecv(BARRIER, master);
  if (res != 0) {
    return res;
  }

  return res;
}

int IB_verbs::IB::_barrierMasterRecvRW(
  std::vector<uint32_t>::iterator minions_it_start,
  std::vector<uint32_t>::iterator minions_it_end)
{
  int res;
  vector<array<uint32_t, 2>> recvs_ids;
  int n_minions = std::distance(minions_it_start, minions_it_end);
  recvs_ids.reserve(n_minions);
  while (res < n_minions) {
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
    res = 0;
    for (auto it = minions_it_start; it != minions_it_end; it++) {
      res += *(_syncBuf.get() + (*it));
    }
  }
  // reset flags
  for (auto it = minions_it_start; it != minions_it_end; it++) {
    *(_syncBuf.get() + (*it)) = 0;
  }
  return 0;
}

int IB_verbs::IB::_barrierMasterSndRW(
  std::vector<uint32_t>::iterator minions_it_start,
  std::vector<uint32_t>::iterator minions_it_end)
{
  int res;
  vector<uint32_t> send_ids;
  uint32_t temp_wr;
  send_ids.reserve(std::distance(minions_it_start, minions_it_end));
  for (auto it = minions_it_start; it != minions_it_end; it++) {
    temp_wr = _writeSync(*it);
    if (temp_wr == 0) {
      return -1;
    }
    send_ids.push_back(temp_wr);
  }
  res = this->ibWaitAllSyncSends(send_ids);
  if (res != 0) {
    return res;
  }
  return 0;
}

int IB_verbs::IB::_barrierMinionSndRW(uint32_t master)
{
  int res = 0;
  uint32_t wrid = _writeSync(master);
  if (wrid == 0) {
    return -1;
  }
  res = ibWaitSyncSend(wrid);
  if (res != 0) {
    return res;
  }
  return res;
}

int IB_verbs::IB::_barrierMinionRecvRW(uint32_t master)
{
  int res = 0;
  while (res == 0) {
    res = this->_readSync(master);
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
  }
  return 0;
}