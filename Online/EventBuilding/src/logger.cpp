#include "logger.hpp"
#include <exception>
#include <system_error>

std::string EB::log_priority_to_string(const EB::Log_priority& elem)
{
  switch (elem) {
  case Log_priority::kLogEmerg: return "EMERGENCY";
  case Log_priority::kLogAlert: return "ALERT";
  case Log_priority::kLogCrit: return "CRITICAL";
  case Log_priority::kLogErr: return "ERROR";
  case Log_priority::kLogWarning: return "WARNING";
  case Log_priority::kLogNotice: return "NOTICE";
  case Log_priority::kLogInfo: return "INFO";
  case Log_priority::kLogDebug: return "DEBUG";
  default: return "Unsupported log priority";
  }
}

EB::Log_priority EB::online_print_level_to_log_priority(const Online::PrintLevel& elem)
{
  switch (elem) {
  case Online::PrintLevel::VERBOSE: return Log_priority::kLogDebug;
  case Online::PrintLevel::DEBUG: return Log_priority::kLogDebug;
  case Online::PrintLevel::INFO: return Log_priority::kLogInfo;
  case Online::PrintLevel::WARNING: return Log_priority::kLogWarning;
  case Online::PrintLevel::ERROR: return Log_priority::kLogErr;
  case Online::PrintLevel::FATAL: return Log_priority::kLogAlert;
  case Online::PrintLevel::ALWAYS: return Log_priority::kLogEmerg;
  case Online::PrintLevel::NOLOG: return Log_priority::kLogNoLog;
  default: return Log_priority::kLogNoLog;
  }
}

Online::PrintLevel EB::log_priority_to_online_print_level(const EB::Log_priority& elem)
{
  switch (elem) {
  case Log_priority::kLogEmerg: return Online::PrintLevel::ALWAYS;
  case Log_priority::kLogAlert: return Online::PrintLevel::FATAL;
  case Log_priority::kLogCrit: return Online::PrintLevel::ERROR;
  case Log_priority::kLogErr: return Online::PrintLevel::ERROR;
  case Log_priority::kLogWarning: return Online::PrintLevel::WARNING;
  case Log_priority::kLogNotice: return Online::PrintLevel::INFO;
  case Log_priority::kLogInfo: return Online::PrintLevel::INFO;
  case Log_priority::kLogDebug: return Online::PrintLevel::DEBUG;
  case Log_priority::kLogNoLog: return Online::PrintLevel::NOLOG;
  default: return Online::PrintLevel::NOLOG;
  }
}

EB::Log::Log(std::string name, Log_priority log_level, bool enable_syslog, bool enable_online, int facility)
{
  Log_initializer::init_log();
  _log_level = log_level;
  _name = name;
  _facility = facility;
  _online_ena = enable_online;
  _syslog_ena = enable_syslog;
  _UTGID = RTL::processName();
}

void EB::Log::set_log_level(const Log_priority& log_level) { _log_level = log_level; }
EB::Log_priority EB::Log::get_log_level() const { return _log_level; }

void EB::Log::set_name(const std::string& name) { _name = name; }
std::string EB::Log::get_name() const { return _name; }

void EB::Log::set_facility(int facility) { _facility = facility; }
int EB::Log::get_facility() const { return _facility; }

void EB::Log::enable_syslog(bool flag) { _syslog_ena = flag; }
void EB::Log::enable_online(bool flag) { _online_ena = flag; }
bool EB::Log::syslog_enabled() const { return _syslog_ena; }
bool EB::Log::online_enabled() const { return _online_ena; }

int EB::Log::sync()
{
  if (_buffer.length()) {
    // the facility can be ored with the log level, the default value will leave it unchanged
    if (_syslog_ena) {
      syslog(static_cast<int>(_log_level) | _facility, "%s %s: %s", _UTGID.c_str(), _name.c_str(), _buffer.c_str());
    }
    if (_online_ena) {
      Online::printout(log_priority_to_online_print_level(_log_level), _name.c_str(), _buffer.c_str());
    }
    _buffer.erase();
  }
  return 0;
}

int EB::Log::overflow(int c)
{
  if (c != EOF) {
    _buffer += static_cast<char>(c);
  } else {
    sync();
  }
  return c;
}

EB::Log_stream::Log_stream(
  std::string name,
  Log_priority out_level,
  bool enable_syslog,
  bool enable_online,
  int facility) :
  std::ostream(new Log(name, EB::default_log, enable_syslog, enable_online, facility))
{
  set_output_level(out_level);
  _buff.reset(rdbuf());
}

EB::Log_stream::Log_stream(Log_stream&& other) :
  std::ostream(std::move(other)), _output_level(std::move(other._output_level)), _buff(std::move(other._buff))
{
  // the rdbuf is not changed by the move operator
  rdbuf(other.rdbuf());
  other.rdbuf(NULL);
}

EB::Log_stream& EB::Log_stream::operator=(Log_stream&& rhs)
{
  // protection against self move
  if (&rhs != this) {
    std::ostream::operator=(std::move(rhs));
    // the rdbuf is not changed by the move operator
    rdbuf(rhs.rdbuf());
    rhs.rdbuf(NULL);
    _output_level = rhs._output_level;
    _buff = std::move(rhs._buff);
  }
  return *this;
}

void EB::Log_stream::set_output_level(const EB::Log_priority& output_level)
{
  // NoLog can't be printed out
  if (output_level != Log_priority::kLogNoLog) {
    _output_level = output_level;
  } else {
    throw std::system_error(
      static_cast<int>(std::errc::invalid_argument), std::generic_category(), "Requesting invalid output level");
  }
}
void EB::Log_stream::set_output_level(const Online::PrintLevel& output_level)
{
  set_output_level(online_print_level_to_log_priority(output_level));
}

EB::Log_priority EB::Log_stream::get_output_level() const { return _output_level; }

void EB::Log_stream::set_name(const std::string& name) { reinterpret_cast<Log*>(rdbuf())->set_name(name); }
std::string EB::Log_stream::get_name() const { return reinterpret_cast<Log*>(rdbuf())->get_name(); }

void EB::Log_stream::enable_syslog(bool flag) { static_cast<Log*>(rdbuf())->enable_syslog(flag); }
void EB::Log_stream::enable_online(bool flag) { static_cast<Log*>(rdbuf())->enable_online(flag); }
bool EB::Log_stream::syslog_enabled() const { return static_cast<Log*>(rdbuf())->syslog_enabled(); }
bool EB::Log_stream::online_enabled() const { return static_cast<Log*>(rdbuf())->online_enabled(); }

EB::Log_stream& EB::Log_stream::set_log_level(const EB::Log_priority& log_level)
{
  static_cast<Log*>(rdbuf())->set_log_level(log_level);
  return *this;
}
EB::Log_stream& EB::Log_stream::set_log_level(const Online::PrintLevel& log_level)
{
  return set_log_level(online_print_level_to_log_priority(log_level));
}

EB::Log_priority EB::Log_stream::get_log_level() const { return static_cast<Log*>(rdbuf())->get_log_level(); }

bool EB::Log_stream::is_active(const EB::Log_priority& priority) const { return priority <= _output_level; }
bool EB::Log_stream::is_active(const Online::PrintLevel& priority) const
{
  return is_active(online_print_level_to_log_priority(priority));
}

std::ostream& operator<<(std::ostream& os, EB::Log_priority const& log_priority)
{
  os << EB::log_priority_to_string(log_priority);
  return os;
}

EB::Log_stream& operator<<(EB::Log_stream& os, EB::Log_priority const& log_priority)
{
  static_cast<EB::Log*>(os.rdbuf())->set_log_level(log_priority);
  return os;
}
