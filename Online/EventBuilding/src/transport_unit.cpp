#include "transport_unit.hpp"
#include "EventBuilding/tools.hpp"
#include <algorithm>
#include <functional>
#include <numeric>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <type_traits>
#include "infiniband_net/sock.hpp"
#include "infiniband_net/parser.hpp"

bool EB::is_bu(int rank, const std::vector<int>& n_rus_nic)
{
  bool ret_val = false;
  int idx = 0;
  int k = 0;
  while (idx <= rank) {
    ret_val = (rank - idx) == n_rus_nic[k];
    idx += n_rus_nic[k] + 1; // +1 for the BU
    k++;
  }

  return ret_val;
}

bool EB::is_ru(int rank, const std::vector<int>& n_rus_nic)
{
  bool ret_val = false;
  int idx = 0;
  int k = 0;
  while (idx <= rank) {
    ret_val = (rank - idx) < n_rus_nic[k];
    idx += n_rus_nic[k] + 1; // +1 for the BU
    k++;
  }

  return ret_val;
}

EB::Transport_unit::Transport_unit(const std::string& nam, Context& ctxt) : DataflowComponent(nam, ctxt)
{
  declareProperty("RUs_per_nic", _n_rus_per_nic);
  // TODO check if this property is needed
  declareProperty("n_par_mess", _n_par_mess = 1);
  declareProperty("n_sources_per_ru", _prop_n_sources_per_ru);
  // the default value is the self assignment
  declareProperty("RU_ranks", _ru_ranks);
  declareProperty("BU_ranks", _bu_ranks);
  declareProperty("shift_pattern", _shift_pattern);
  declareProperty("RU_shift_pattern", _RU_shift_pattern);
  declareProperty("BU_shift_pattern", _BU_shift_pattern);
  declareProperty("ib_config_file", _ib_config_file);
  // This property is deprecated
  declareProperty("MPI_errors_return", _MPI_errors_return = false);
  declareProperty("barrier_type", _prop_barrier_type = IB_verbs::IB::CENTRAL);
  declareProperty("sync_mode", _prop_sync_mode = IB_verbs::IB::SENDRECV);
  declareProperty("tree_barrier_size", _tree_barrier_size = 20);

  declareProperty("profiling_update_interval", _profiling_update_interval = 5);
  declareProperty("enable_profiling", _enable_profiling = false);

  declareProperty("IB_connection_timeout", _IB_connection_timeout = 30);
  // every how many phases the sync function should sync
  declareProperty("sync_phase_freq", _sync_phase_freq = 1);
  declareProperty("use_sharp", _use_sharp = false);

  // DF monitoring counters
  declareMonitor("Events/IN", _DF_events_in, "Number events received in the RUN");
  declareMonitor("Events/OUT", _DF_events_out, "Number events processed and sent in RUN");
  declareMonitor("Events/ERROR", _DF_events_err, "Number events with errors in the RUN");

  // net monitoring counters
  declareMonitor("bytes_in", _bytes_in, "Number of received bytes in the RUN");
  declareMonitor("bytes_out", _bytes_out, "Number of bytes sent in the RUN");

  // DEBUG counters
  declareMonitor("barrier_count", _barrier_count, "Number of barriers completed in the RUN");
  declareMonitor("pre_barrier_count", _pre_barrier_count, "Number of barriers reached in the RUN");
  // profiling counters
  declareMonitor("sync_time_counter", _sync_time_counter);
  declareMonitor("total_time_counter", _total_time_counter);
}

EB::Transport_unit::~Transport_unit() {}

int EB::Transport_unit::init_ranks()
{
  if (_n_rus_per_nic.size() == 0) {
    // if 0 size self assign 1 RU per NIC
    _n_rus_per_nic.resize(_world_size, 1);
    logger.warning() << __FUNCTION__ << " no number of RUs per NIC provided setting to default value " << 1
                     << std::flush;
  } else if (_n_rus_per_nic.size() == 1) {
    _n_rus_per_nic.resize(_world_size, _n_rus_per_nic[0]);
    logger.warning() << __FUNCTION__ << " Single number of RUs per NIC provided setting to default value "
                     << _n_rus_per_nic[0] << std::flush;
  }

  if ((_bu_ranks.size() == 0) && (_ru_ranks.size() == 0)) {
    // if 0 size self assign
    logger.info() << "Self assigning RU and BU ranks" << std::flush;
    _ru_ranks.reserve(_world_size);
    _bu_ranks.reserve(_world_size);
    for (int k = 0; k < _world_size; k++) {
      // TODO this is not implemented in the fastest way, in any case this should not be used in the real system
      if (is_bu(k, _n_rus_per_nic)) {
        _bu_ranks.push_back(k);
      } else if (is_ru(k, _n_rus_per_nic)) {
        _ru_ranks.push_back(k);
      } else {
        logger.error() << "rank " << k << " cannot be assigned to any unit" << std::flush;
      }
    }
  }

  // resizing n_rus_per_nic after ranks assignment
  // TODO add checks when shrinking
  _n_rus_per_nic.resize(_ru_ranks.size());

  if (logger.is_active(PrintLevel::DEBUG)) {
    logger.debug() << "RU ranks: ";
    for (const auto& elem : _ru_ranks) {
      logger.debug() << elem << " ";
    }
    logger.debug() << std::flush;

    logger.debug() << "BU ranks: ";
    for (const auto& elem : _bu_ranks) {
      logger.debug() << elem << " ";
    }
    logger.debug() << std::flush;
  }

  return check_ranks();
}

int EB::Transport_unit::check_ranks()
{
  std::vector<int> all_ranks(_world_size);
  std::iota(all_ranks.begin(), all_ranks.end(), 0);
  logger.debug() << "all ranks: ";
  for (const auto& elem : all_ranks) {
    logger.debug() << elem << " ";
  }
  logger.debug() << std::flush;

  std::vector<int> configured_ranks;
  configured_ranks.reserve(_world_size);
  configured_ranks.insert(configured_ranks.end(), _ru_ranks.begin(), _ru_ranks.end());
  configured_ranks.insert(configured_ranks.end(), _bu_ranks.begin(), _bu_ranks.end());
  std::sort(configured_ranks.begin(), configured_ranks.end());

  // if this condition is met all the ranks are properly set
  if (all_ranks != configured_ranks) {
    // if there is a mismatch we need to go deeper
    std::vector<int> error_ranks(std::max(configured_ranks.size(), all_ranks.size()));
    std::vector<int>::iterator rank_error_it;

    // search for duplicated ranks
    rank_error_it = find_all_rep(configured_ranks.begin(), configured_ranks.end(), error_ranks.begin());
    if (rank_error_it != error_ranks.begin()) {
      logger.error() << __FUNCTION__ << " Invalid RU & BU rank configuration, repeated ranks: ";
      for (auto it = error_ranks.begin(); it != rank_error_it; it++) {
        logger.error() << *it << " ";
      }
      logger.error() << std::flush;
    }

    // after reporting duplicates uniq on the configured ranks
    rank_error_it = std::unique(configured_ranks.begin(), configured_ranks.end());
    configured_ranks.erase(rank_error_it, configured_ranks.end());

    // search for missing ranks
    rank_error_it = std::set_difference(
      all_ranks.begin(), all_ranks.end(), configured_ranks.begin(), configured_ranks.end(), error_ranks.begin());

    if (rank_error_it != error_ranks.begin()) {
      logger.error() << __FUNCTION__ << " Invalid RU & BU rank configuration, missing ranks: ";
      for (auto it = error_ranks.begin(); it != rank_error_it; it++) {
        logger.error() << *it << " ";
      }
      logger.error() << std::flush;
    }

    // search for non existing ranks
    rank_error_it = std::set_difference(
      configured_ranks.begin(), configured_ranks.end(), all_ranks.begin(), all_ranks.end(), error_ranks.begin());

    if (rank_error_it != error_ranks.begin()) {
      logger.error() << __FUNCTION__ << " Invalid RU & BU rank configuration, non existing ranks: ";
      for (auto it = error_ranks.begin(); it != rank_error_it; it++) {
        logger.error() << *it << " ";
      }
      logger.error() << std::flush;
    }
    return DF_ERROR;
  }
  // TODO find a way to check the n_rus_per_nic. This is not urgent since this parameter is not used.

  size_t num_rus = std::accumulate(_n_rus_per_nic.begin(), _n_rus_per_nic.end(), 0);

  if (_ru_ranks.size() != num_rus) {
    logger.error() << __FUNCTION__ << " Invalid configuration: the number of processes and units does not match. Got "
                   << _bu_ranks.size() << " BUs instead of " << _world_size - num_rus << " and " << _ru_ranks.size()
                   << " RUs instead of " << num_rus << std::flush;
    return DF_ERROR;
  }

  // Check if the configuration is the same on all the nodes

  return DF_SUCCESS;
}

void EB::Transport_unit::set_rank(int rank)
{
  // TODO this may not be the best sanity check
  if (!logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.always() << __FILE__ << ":" << __LINE__ << ": WARNING this function is for DEBUG only" << std::endl;
  }
  _my_rank = rank;
}

int EB::Transport_unit::initialize()
{
  int ret_val;
  ret_val = DataflowComponent::initialize();
  if (ret_val != DF_SUCCESS) {
    return error("Failed to initialize service base class.");
  }
  logger.set_output_level(online_print_level_to_log_priority(Online::PrintLevel(outputLevel)));

  _barrier_type = static_cast<IB_verbs::IB::barrier_t>(_prop_barrier_type);
  _sync_mode = static_cast<IB_verbs::IB::syncmode_t>(_prop_sync_mode);

  try {
    _ibParser = std::make_unique<IB_verbs::Parser>(_ib_config_file.c_str());
  } catch (std::exception& e) {
    logger.error() << "IB file parser: " << _ib_config_file << ": " << e.what() << std::flush;
    return DF_ERROR;
  }
  _my_rank = _ibParser->getProcessId();
  logger.debug() << "my rank is " << _my_rank << std::flush;

  _world_size = _ibParser->getTotalProcesses();
  logger.debug() << "world size is " << _world_size << std::flush;
  if (_world_size == 0) {
    return DF_ERROR;
  }
  ret_val = init_ranks();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  _numa_node = _ibParser->getIbNumaNode();

  ret_val = init_n_sources();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  ret_val = init_pattern();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  return ret_val;
}

int EB::Transport_unit::start()
{
  if (_IB_connection_timeout == 0) {
    logger.warning() << __FUNCTION__ << " IB_connection_timeout is set to 0, the timeout will be ignored" << std::flush;
  }
  try {
    _ibComm = std::make_unique<Parallel_comm>(
      _ibParser.get(),
      logger.get_name(),
      log_priority_to_online_print_level(logger.get_output_level()),
      _IB_connection_timeout,
      _tree_barrier_size,
      _use_sharp);
  } catch (std::exception& e) {
    logger.error() << "Parallel Comm Constructor: " << e.what() << std::flush;
    return DF_ERROR;
  }
  logger.debug() << "verbs started" << std::flush;
  _sync_count = 0;
  return DF_SUCCESS;
}

int EB::Transport_unit::cancel()
{
  _ibComm->ibKillBlocking();
  return DF_SUCCESS;
}

int EB::Transport_unit::stop()
{
  _ibComm.reset(nullptr);
  logger.debug() << "verbs stopped" << std::flush;
  return 1;
}

int EB::Transport_unit::finalize()
{
  // TODO maybe we want a barrier here
  _ibParser.reset(nullptr);
  return DataflowComponent::finalize();
}

int EB::Transport_unit::sync(bool ignore_sync_freq)
{
  _sync_timer.start();
  int ret_val = DF_SUCCESS;
  _pre_barrier_count++;
  if (ignore_sync_freq || ((_sync_count % _sync_phase_freq) == 0)) {
    ret_val = _ibComm->ibBarrier(_barrier_type, _sync_mode, _use_sharp);
    _sync_count++;
  }
  _barrier_count++;
  _sync_timer.stop();
  return ret_val;
}

int EB::Transport_unit::init_pattern()
{
  int ret_val = DF_SUCCESS;

  if ((_RU_shift_pattern.size() == 0) && (_shift_pattern.size() == 0)) {
    _RU_shift_pattern = init_shift_pattern(_ru_ranks);
  } else {
    // enabling symmetric shift pattern
    if (_shift_pattern.size() != 0) {
      logger.warning() << "Enabling symmetric shift pattern" << std::flush;
      _RU_shift_pattern = _shift_pattern;
    }
    ret_val = check_shift_pattern(_ru_ranks, _RU_shift_pattern);
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    }
  }

  if ((_BU_shift_pattern.size() == 0) && (_shift_pattern.size() == 0)) {
    _BU_shift_pattern = init_shift_pattern(_bu_ranks);
  } else {
    // enabling symmetric shift pattern
    if (_shift_pattern.size() != 0) {
      logger.warning() << "Enabling symmetric shift pattern" << std::flush;
      _BU_shift_pattern = _shift_pattern;
    }
    ret_val = check_shift_pattern(_bu_ranks, _BU_shift_pattern);
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    }
  }

  if (_BU_shift_pattern.size() != _RU_shift_pattern.size()) {
    logger.error() << "Invalid shift pattern: BUs and RUs have shift pattern of different length. BU:"
                   << _BU_shift_pattern.size() << ", RU:" << _RU_shift_pattern.size() << std::flush;
    return DF_ERROR;
  }

  return ret_val;
}

// TODO refactor errors and use exceptions
int EB::Transport_unit::check_shift_pattern(const std::vector<int>& ranks, const std::vector<int>& pattern)
{
  int ret_val = DF_SUCCESS;
  std::vector<int> nodes;

  nodes.reserve(pattern.size());

  // copy of the vector without the ghost nodes
  std::copy_if(pattern.begin(), pattern.end(), std::back_inserter(nodes), [](int val) { return (val >= 0); });

  auto num_ghost = pattern.size() - nodes.size();

  // this function is not called if the shift pattern is auto generated
  auto base_pattern = init_shift_pattern(ranks);

  std::sort(nodes.begin(), nodes.end());

  if (pattern.size() < ranks.size()) {
    logger.error() << "Invalid shift pattern: the shift pattern should have at least " << ranks.size()
                   << " nodes, got only " << pattern.size() << " nodes" << std::flush;
    ret_val = DF_ERROR;
  } else if (pattern.size() - num_ghost != ranks.size()) {
    logger.error() << "Invalid shift pattern: expecting " << pattern.size() - ranks.size() << " ghost nodes, got "
                   << num_ghost << " ghost nodes" << std::flush;
    ret_val = DF_ERROR;
  } else if (nodes != base_pattern) {
    logger.error() << "Invalid shift pattern:";
    for (const auto& elem : pattern) {
      logger.error() << " " << elem;
    }
    logger.error() << std::flush;
    ret_val = DF_ERROR;
  }

  return ret_val;
}

std::vector<int> EB::Transport_unit::init_shift_pattern(const std::vector<int>& ranks)
{
  std::vector<int> base_pattern(ranks.size());
  std::iota(base_pattern.begin(), base_pattern.end(), 0);
  return base_pattern;
}

int EB::Transport_unit::init_n_sources()
{
  int ret_val = DF_SUCCESS;
  _n_sources_per_ru.resize(_prop_n_sources_per_ru.size());
  for (size_t k = 0; k < _prop_n_sources_per_ru.size(); k++) {
    if (_prop_n_sources_per_ru[k] >= 0) {
      _n_sources_per_ru[k] = _prop_n_sources_per_ru[k];
    } else {
      ret_val = DF_ERROR;
      logger.error() << __FUNCTION__ << " configuration error: got negative number of sources per ru "
                     << _prop_n_sources_per_ru[k] << std::flush;
      return ret_val;
    }
  }

  if (_n_sources_per_ru.size() == 0) {
    _n_sources_per_ru.resize(_ru_ranks.size(), 1);
    logger.warning() << __FUNCTION__ << " no number of sources per RU provided setting to default value: " << 1
                     << std::flush;
  } else if (_n_sources_per_ru.size() == 1) {
    _n_sources_per_ru.resize(_ru_ranks.size(), _n_sources_per_ru[0]);
    logger.warning() << __FUNCTION__ << " Single number of sources per RU provided: " << _n_sources_per_ru[0]
                     << std::flush;
  }

  else if (_n_sources_per_ru.size() != _ru_ranks.size()) {
    logger.error() << __FUNCTION__ << " configuration error: incorrect number of sources per RU. Got "
                   << _n_sources_per_ru.size() << " values for " << _ru_ranks.size() << " RUs" << std::flush;
    ret_val = DF_ERROR;
  }

  auto zero_src_it = std::find(_n_sources_per_ru.begin(), _n_sources_per_ru.end(), 0);
  if (zero_src_it != _n_sources_per_ru.end()) {
    auto ru_idx = std::distance(_n_sources_per_ru.begin(), zero_src_it);
    logger.error() << __FUNCTION__ << " configuration error: RU id " << ru_idx << " got 0 sources." << std::flush;
    ret_val = DF_ERROR;
  }

  // the last element is the used for the MPI gather and represents the number of sources of the BU so it is set to 0
  _n_sources_per_ru.emplace_back(0);

  // n RU + 2 elements the first element is 0 and the last two the number total number of sources (the last element is
  // the used for the MPI gather and represents the number of sources of the BU)
  _prefix_n_sources_per_ru.resize(_n_sources_per_ru.size() + 1);
  _prefix_n_sources_per_ru[0] = 0;
  std::partial_sum(_n_sources_per_ru.begin(), _n_sources_per_ru.end(), _prefix_n_sources_per_ru.begin() + 1);

  return ret_val;
}

void EB::Transport_unit::reset_counters()
{

  _DF_events_out = 0;
  _DF_events_in = 0;
  _DF_events_err = 0;

  _bytes_out = 0;
  _bytes_in = 0;

  _pre_barrier_count = 0;
  _barrier_count = 0;
  _total_time_counter = 0;
  _sync_time_counter = 0;
}

void EB::Transport_unit::update_profiling()
{
  if (_enable_profiling) {
    double total_time = _total_timer.get_elapsed_time_s();
    if (total_time > _profiling_update_interval) {
      double sync_time = _sync_timer.get_elapsed_time_s();
      _total_time_counter += total_time;
      _sync_time_counter += sync_time;
      if (logger.is_active(Online::PrintLevel::INFO)) {
        logger.info() << "Profiling counters" << std::flush;
        logger.info() << " sync " << sync_time << " s " << sync_time / total_time * 100 << " %" << std::flush;
      }

      _total_timer.reset();
      _sync_timer.reset();
    }
  }
}

void EB::Transport_unit::init_profiling()
{
  if (_enable_profiling) {
    _total_timer.stop();
    _total_timer.reset();
    _sync_timer.stop();
    _sync_timer.reset();
    // start the total timer
    _total_timer.start();
  } else {
    _total_timer.disable();
    _sync_timer.disable();
  }
}