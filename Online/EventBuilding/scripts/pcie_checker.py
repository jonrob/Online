import os
import pwd
import sys
import signal
import subprocess
import shlex
import argparse
import time
import re
import distutils.core
import itertools
import json
import multiprocessing
import functools


def get_pcie_ids(device_grep, host):
    device_id_cmd = f'ssh {host} /usr/sbin/lspci | grep -i {device_grep}'
    pipe = subprocess.Popen(shlex.split(device_id_cmd), stdout=subprocess.PIPE)
    output = pipe.communicate()[0].decode("utf-8")
    pcie_ids = [
        line.split(' ')[0] for line in output.split('\n') if len(line) > 0
    ]
    return pcie_ids


def get_pcie_status(device_grep, host):
    pcie_ids = get_pcie_ids(device_grep, host)
    device_status_cmds = [
        f'ssh {host} sudo /usr/sbin/lspci -s {dev_id}  -vvv'
        for dev_id in pcie_ids
    ]
    outs = [
        subprocess.Popen(
            shlex.split(cmd),
            stdout=subprocess.PIPE).communicate()[0].decode("utf-8")
        for cmd in device_status_cmds
    ]

    match = re.compile(
        'LnkSta:\s+Speed\s+(?P<link_speed>[0-9]+)GT/s,\s+Width\s+x(?P<link_width>[0-9]+)'
    )

    matches = [(dev_id, match.search(out))
               for dev_id, out in zip(pcie_ids, outs)]

    speed_widths = [(dev_id, int(out_match.group('link_speed')),
                     int(out_match.group('link_width')))
                    for dev_id, out_match in matches if out_match != None]

    return speed_widths


def check_pcie_status(curr_width, curr_speed, target_width, target_speed):
    return (curr_width == target_width) and (curr_speed == target_speed)


def main():
    parser = argparse.ArgumentParser(
        description='EB startup script',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-H",
                        "--hostfile",
                        help='Host/device file.'
                        'Every line must contain an hostname.',
                        type=str,
                        required=True)

    parser.add_argument("-p",
                        "--parallel",
                        help='Number of parallel ssh connections',
                        type=int,
                        default=16,
                        required=False)

    parser.add_argument("-s",
                        "--speed",
                        help='Link speed in GT/s',
                        type=int,
                        default=16,
                        required=False)

    parser.add_argument("-w",
                        "--width",
                        help='Link width',
                        type=int,
                        default=16,
                        required=False)

    parser.add_argument(
        "-v",
        "--vendor",
        help='Device vendor string to parse (case insensitive)',
        type=str,
        default='mellanox',
        required=False)

    args = parser.parse_args()

    with open(args.hostfile, 'r') as host_file:
        hosts = set(
            [line.strip().split(' ')[0] for line in host_file.readlines()])

    pool = multiprocessing.Pool(processes=args.parallel)

    status_funct = functools.partial(get_pcie_status, args.vendor)

    pcie_statuses = pool.map(status_funct, hosts)

    for host, devs in zip(hosts, pcie_statuses):
        for dev_id, speed, width in devs:
            if (not check_pcie_status(width, speed, args.width, args.speed)):
                print(host, dev_id, f'{speed} GT/s x{width}')


if __name__ == "__main__":
    main()
