//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo Logger implementation
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ONLINE_DEBUGSERVER_H
#define ONLINE_DEBUGSERVER_H

/// C/C++ include files
#include <memory>

/// Debugger namespace declaration
namespace debugger  {

  class DebugServer   {
  public:
    class implementation;
    std::unique_ptr<implementation> imp;
  public:
    /// Default constructor
    DebugServer();
    /// Default destructor
    ~DebugServer();
    /// Configure object
    void start(bool start_gdb);
  };
}

extern "C"  {
  /// Initialize the debug server
  void debug_server_initialize();
  /// Shutdown the debug server
  void debug_server_finalize();
}

#endif // ONLINE_DEBUGSERVER_H
