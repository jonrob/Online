//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <PCIE40Data/pcie40encoder.h>
#include <EventData/event_header_t.h>
#include <EventData/RawFile.h>
#include <RTL/Logger.h>
#include <RTL/rtl.h>

// C/C++ include files

using namespace Online;

namespace {
  void help ()    {
    lib_rtl_output(LIB_RTL_ALWAYS,"pcie_encode_file -option [-option]");
    lib_rtl_output(LIB_RTL_ALWAYS,"  -input=<file>       input file name   ");
    lib_rtl_output(LIB_RTL_ALWAYS,"  -output=<file>      output file name  ");
    lib_rtl_output(LIB_RTL_ALWAYS,"  -events=<number>    number of events to be processed");
    lib_rtl_output(LIB_RTL_ALWAYS,"  -packing=<number>   packing factor for MEP creation");
  }
  class file_writer : public pcie40::encoder_t::output_t  {
    RawFile* file = 0;
  public:
    file_writer(RawFile* f) : file(f) {}
    virtual size_t output(const void* buffer, size_t len)  override
    {  return file->write(buffer, len);  }
  };
}

extern "C" int pcie_encode_file(int argc, char* argv[])    {
  pcie40::encoder_t encoder;
  RTL::CLI cli(argc, argv, help);
  std::string input, output;
  size_t packing_factor = 1, events = 99999999999UL;

  RTL::Logger::install_rtl_printer(LIB_RTL_INFO);
  cli.getopt("input",   1, input);
  cli.getopt("output",  1, output);
  cli.getopt("events",  1, events);
  cli.getopt("packing", 1, packing_factor);

  if ( input.empty() || output.empty() )   {
    help();
    return ENOENT;
  }

  RawFile in(input), out(output);
  if ( in.open() == -1 )    {
    lib_rtl_output(LIB_RTL_ERROR,"Invalid input file name.");
    return ENOENT;
  }
  if ( out.openWrite() == -1 )   {
    lib_rtl_output(LIB_RTL_ERROR,"Invalid output file name.");
    return ENOENT;
  }

  size_t num_evt = 0;
  size_t eid = 0;
  size_t buff_len = 10*1024*1024;
  unsigned char* buffer = new unsigned char[buff_len];
  file_writer writer {&out};

  for (; events; --events )    {
    RawFile::EventType expected = RawFile::MDF_INPUT_TYPE;
    RawFile::EventType found    = RawFile::AUTO_INPUT_TYPE;
    long rec_len = in.read_event(expected, found, buffer, buff_len);
    if ( rec_len > 0 )   {
      const event_header_t* hdr = (const event_header_t*)buffer;
      size_t hdr_len = hdr->sizeOf(hdr->headerVersion());
      encoder.append(++eid, buffer + hdr_len, rec_len - hdr_len);
      if ( encoder.packingFactor() == packing_factor )  {
	auto ret = encoder.write_record(writer);
	::lib_rtl_output(LIB_RTL_INFO,"++ Wrote %ld sources %ld bytes. Packing:%ld",
			 ret.first, ret.second, packing_factor);
	num_evt += packing_factor;
	encoder.reset();
      }
    }
    if ( !in.isOpen() ) break;
  }
  if ( encoder.packingFactor() > 0 )  {
    num_evt += packing_factor;
    encoder.write_record(writer);
    encoder.reset();
  }
  out.close();
  ::lib_rtl_output(LIB_RTL_INFO,"++ Closed file after encoding %ld events",num_evt);
  delete [] buffer;
  return 1;
}
