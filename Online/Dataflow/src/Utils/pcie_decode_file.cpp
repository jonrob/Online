//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <PCIE40Data/pcie40decoder.h>
#include <EventData/RawFile.h>

// C/C++ include files
#include <filesystem>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <vector>
#include <set>

namespace {
  const char* get_arg(const char* arg)   {
    const char* p = ::strchr(arg,'=');
    if ( p ) return p+1;
    throw std::runtime_error(std::string("Invalid argument: ")+arg);
  }

  bool prompt()   {
  re_check:
    std::cout << "====>  Continue to dump the next event  [q,Q to quit]: " << std::flush;
  re_prompt:
    int c = std::getchar();
    if ( c == -1  ) goto re_prompt;
    if ( c == 10  ) return true;  // <ENTER>
    if ( c == 'q' ) return false;
    if ( c == 'Q' ) return false;
    goto re_check;
  }
}

extern "C" int pcie_decode_file(int argc, char* argv[])    {
  namespace fs = std::filesystem;
  namespace pcie40 = Online::pcie40;

  class help  {
  public:
    static void show()  {
      std::cout <<
	"Usage: pcie_decode_file -option [-option]                           \n"
	"  --input=<file>      input file name  (multiple entries allowed)   \n"
	"  -i <file>           dto.                                          \n"
	"  --directory=<file>  use directory for input files                 \n"
	"  -D <file>           dto.                                          \n"
	"  --dump=mep          Dump MEP headers                              \n"
	"  --dump=mfp          Dump MEP headers                              \n"
	"  --dump=events       Dump Event headers                            \n"
	"  --dump=collections  Dump bank collection summaries                \n"
	"  --dump=banks        Dump bank headers                             \n"
	"  -d <item>           dto.                                          \n"
	"  --break             Break before next chunk of printout           \n"
	"  -b                  dto.                                          \n"
		<< std::endl;
    }
  };
  bool do_break = false;
  bool dump_mep = false;
  bool dump_mfp = false;
  bool dump_events = false;
  bool dump_bank_headers = false;
  bool dump_bank_collections = false;
  std::vector<std::string> inputs;
  std::vector<std::string> directories;
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )   {
	inputs.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcasecmp("-i",argv[i]) )   {
	inputs.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--directory",argv[i],4) )   {
	directories.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcmp("-D",argv[i]) )   {
	directories.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--break",argv[i],4) )   {
	do_break = true;
      }
      else if ( 0 == ::strcasecmp("-b",argv[i]) )   {
	do_break = true;
      }
      else if ( 0 == ::strncasecmp("--dump",argv[i],4) )    {
	if ( ::strncmp(get_arg(argv[i]), "mep", 3) == 0 )   {
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "mfp", 3) == 0 )   {
	  dump_mfp = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "events", 3) == 0 )   {
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "collections", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "banks", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	  dump_events = true;
	  dump_mep = true;
	}
      }
      else if ( 0 == ::strncasecmp("-d",argv[i],4) )  {
	const char* a2 = argv[++i];
	if ( ::strncmp(a2, "mep", 3) == 0 )   {
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "mfp", 3) == 0 )   {
	  dump_mfp = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "events", 3) == 0 )   {
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "collections", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "banks", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	  dump_events = true;
	  dump_mep = true;
	}
      }
      else   {
	throw std::runtime_error(std::string("Invalid argument: ")+argv[i]);
      }
    }
  }
  catch(const std::exception& e)   {
    std::cout << "Exception: " << e.what() << std::endl << std::endl;
    help::show();
    return EINVAL;
  }

  if ( !directories.empty() )   {
    for( const auto& dir : directories )   {
      std::error_code ec;
      fs::path path = dir;
      if ( !fs::exists(path, ec) || !fs::is_directory(path, ec) )   {
	std::cout << "+++ Ignore directory: " << path.string() 
		  << " [" << ec.message() << "]" << std::endl;
	continue;
      }
      for(auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it)   {
	fs::path e = *it;
	if ( fs::exists(e, ec) )   {
	  inputs.push_back(e.string());
	}
      }
    }
  }

  if ( inputs.empty() )   {
    std::cout << "No input files given!" << std::endl << std::endl;
    help::show();
    return ENOENT;
  }

  pcie40::printer_t printer;
  pcie40::decoder_t decoder;
  std::unique_ptr<pcie40::event_collection_t> coll;

  clock_t start = clock();
  clock_t end   = start;
  std::size_t  n_evt = 0;
  std::size_t  n_mep = 0;
  std::size_t  bufflen = 0;
  unsigned char* buff = nullptr;
  for(std::size_t i=0; i< inputs.size(); ++i)   {
    std::size_t total = 0;
    std::error_code ec;
    fs::path path = inputs[i];
    if ( !fs::exists(path, ec) )   {
      std::cout << "+++ Ignore input: " << path.string() 
		<< " [" << ec.message() << "]" << std::endl;
      continue;
    }
    Online::RawFile input(inputs[i]);
    if ( input.open() < 0 )  {
      std::cout << "FAILED to open file: " << inputs[i] << std::endl;
      continue;
    }
    std::cout << "+++ Successfully opened file: " << inputs[i] << std::endl;
    while(1)   {
      pcie40::mep_header_t header;
      std::size_t len = input.read(&header, sizeof(header));
      if ( len == sizeof(header) )   {
	std::size_t rec_len = header.size*sizeof(uint32_t);
	total += len;
	if ( bufflen < rec_len )  {
	  bufflen = rec_len;
	  if ( buff ) delete [] buff;
	  buff = new unsigned char[bufflen];
	}
	::memcpy(buff, &header, sizeof(header));
	len = input.read(buff + sizeof(header), rec_len - sizeof(header));
	if ( len == rec_len - sizeof(header) )   {
	  char text[512];
	  total += len;
	  const pcie40::mep_header_t* mep = (pcie40::mep_header_t*)buff;
	  if ( !mep->is_valid() )   {
	    break;
	  }
	  else   {
	    const pcie40::multi_fragment_t *mfp = mep->multi_fragment(0);
	    uint16_t packing = mfp->header.packing;
	    n_evt += packing;
	    if ( dump_events || (dump_mep&&dump_mfp) )   {
	      std::cout << "+=========================================================================+" << std::endl;
	    }
	    if ( dump_mep )   {
	      ::snprintf(text, sizeof(text),
			 "|   Reading mep[%04ld] at %p length:%7d end: %p  packing:%8d  sources:%d magic:%04X total events:%7ld", 
			 n_mep, (void*)mep, mep->size, (void*)mep->next(), int(packing), 
			 int(mep->num_source), int(mep->magic), n_evt);
	      std::cout << text << std::endl;
	    }
	  }
	  if ( dump_mfp )   {
	    std::size_t num_fragments = mep->num_source;
	    for( uint32_t imfp = 0; imfp < num_fragments; ++imfp )    {
	      const auto* mfp = mep->multi_fragment(imfp);
	      const auto& hdr = mfp->header;
	      const char* tag = mfp->is_valid() ? "" : "CORRUPTED ";
	      ::snprintf(text, sizeof(text),
			 "|   -> %sMFP[%4d]  [%p,%p] magic:%04X "
			 "pack:%5d eid:%8ld srcid:%5d len:%8d vsn:%d align:%d",
			 tag, imfp, (void*)mfp, pcie40::add_ptr<void>(mfp,hdr.size),
			 int(hdr.magic), int(hdr.packing), long(hdr.event_id),
			 int(hdr.source_id), int(hdr.size), int(hdr.version),
			 int(hdr.alignment));
	      std::cout << text << std::endl;
	    }
	  }
	  coll.reset();

	  //   coll_reset ? coll.reset() : (void)decoder.initialize(coll, packing);
	  decoder.decode(coll, mep);
	  if ( dump_events )    {
	    std::size_t count = 0;
	    if ( dump_mep )   {
	      std::cout << "+ Got MEP with " << coll->size() << " events." << std::endl;
	    }
	    for(const auto* e=coll->begin(); e != coll->end(); e = coll->next(e), ++count)   {
	      if ( dump_bank_collections )   {
		std::cout << "+ ==>  ===================================================================+" << std::endl;
	      }
	      std::size_t normal_banks_num  = 0;
	      std::size_t special_banks_num = 0;
	      std::size_t normal_data_len  = 0;
	      std::size_t special_data_len = 0;
	      for(std::size_t ie=0, ne=e->num_bank_collections(); ie<ne; ++ie)   {
		std::map<int,std::size_t> normal_types, special_types;
		const auto* bc = e->bank_collection(ie);
		for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))  {
		  normal_data_len += b->size();
		  normal_banks_num++;
		}
		for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))  {
		  special_data_len += b->size();
		  special_banks_num++;
		}
	      }
	      ::snprintf(text, sizeof(text), "|   -> Event No. %6ld %6ld bytes. %4ld collections  "
			 "banks:%4ld [%6ld bytes] specials:%4ld [%6ld bytes]",
			 count, e->total_length(), e->num_bank_collections(),
			 normal_banks_num, normal_data_len, special_banks_num, special_data_len);
	      std::cout << text << std::endl;

	      for(std::size_t ie=0, ne=e->num_bank_collections(); ie<ne; ++ie)   {
		std::map<int,std::size_t> normal_types, special_types;
		const auto* bc = e->bank_collection(ie);
		normal_data_len  = 0;
		special_data_len = 0;
		for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))  {
		  normal_data_len += b->size();
		  normal_types[b->type()] += 1;
		}
		for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))  {
		  special_data_len += b->size();
		  special_types[b->type()] += 1;
		}
		if ( dump_bank_collections )   {
		  std::cout << "|   -->  Bank collection: "  << ie << "  "
		       << std::setw(3) << bc->num_banks()    << " / "
		       << std::setw(3) << bc->num_special()  << " banks  "
		       << std::setw(5) << bc->total_length() << " bytes  "
		       << std::endl;
		  if ( bc->num_banks() > 0 )   {
		    std::cout << "|   ---> Data:   "  << std::setw(6) << normal_data_len << " bytes  Types: ";
		    for(const auto& in : normal_types) std::cout << in.first << ":" << in.second << "  ";
		    std::cout << std::endl;
		    if ( dump_bank_headers )   {
		      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))
			std::cout << "|   ----> " << *b << std::endl;
		    }
		  }
		  if ( bc->num_special() > 0 )   {
		    std::cout << "|   ---> Special:" << std::setw(6) << special_data_len << " bytes  Types: ";
		    for(const auto& is : special_types) std::cout << is.first << ":" << is.second << "  ";
		    std::cout << std::endl;
		    if ( dump_bank_headers )   {
		      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))
			std::cout << "|   ----> " << *b << std::endl;
		    }
		  }
		}
		if ( dump_bank_headers && do_break )		/// Break in interactive running
		  if ( !prompt() ) goto Done;
	      }
	      if ( dump_bank_collections && do_break )		/// Break in interactive running
		if ( !prompt() ) goto Done;
	    }
	  }
	  if ( dump_events && do_break )		/// Break in interactive running
	    if ( !prompt() ) goto Done;
	  n_mep++;
	  continue;
	}
      }
    Done:
      std::cout << "+++ End of file " << inputs[i] << " [" << total << " bytes read]" << std::endl;
      break;
    }
  }
  if ( buff ) delete [] buff;
  end = clock();
  std::printf(" %ld Events Processed: Fill all banks      %8ld %8ld ticks/event\n",
	      n_evt, long(end-start), long(end-start)/std::max(1UL,n_evt));
  printer.print_summary(coll);
  return 0;
}
