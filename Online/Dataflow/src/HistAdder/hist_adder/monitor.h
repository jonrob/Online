//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_DATAFLOW_MONITOR_H
#define ONLINE_DATAFLOW_MONITOR_H

/// Framework include files
#include <RTL/Logger.h>

/// C/C++ include files
#include <mutex>
#include <memory>
#include <string>

/// Online namespace declaration
namespace Online   {

  /// Histograms sub-namespace
  namespace histos  {

    /// Forward declarations
    class monitor;
    
    /// Progress monitor for histogram merger
    /**
     *  Note: Locking is a bit difficult here:
     *
     *  It has by all means to be avoided that somone else than the forking thread
     *  holds the dim lock (or any other mutex). After the fork any other thread
     *  than the forking thread shall be non-existing and any held lock shall
     *  not released otherwise!
     *
     *  \author  M.Frank
     *  \version 1.0
     */
    class monitor   {
    public:
      std::mutex  log_lock;
      std::string log_info;
      int32_t     log_id  { 0 };
      int32_t     run { 0 };
      int32_t     run_id { 0 };
    public:
      /// Default constructor
      monitor();
      /// Default destructor
      virtual ~monitor();

      /// Lock monitor (and DIM!)
      void lock();
      /// Unlock monitor (and DIM!)
      void unlock();
      
      /// Update logger info
      void updateLog(int severity, const char* format, ...);
      void updateLogMsg(int severity, const char* format, ...);
      /// Update logger info
      void updateLog(int severity, const std::string& msg);
      /// Update dim network logger
      void update(int svc_id, const char* msg);
      /// Update run info
      void updateRun(int32_t run);
      /// DIS data feed callback
      static void feedLog(void* tag, void** buf, int* size, int* );
      /// DIS data feed callback
      static void feedRun(void* tag, void** buf, int* size, int* );
    };
    std::shared_ptr<RTL::Logger::LogDevice>& logger();

  }    // End namespace histos
}      // End namespace Online

#endif // ONLINE_DATAFLOW_MONITOR_H
