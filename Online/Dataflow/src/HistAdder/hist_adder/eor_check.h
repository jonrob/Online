//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_DATAFLOW_EOR_CHECK_H
#define ONLINE_DATAFLOW_EOR_CHECK_H

/// Framework include files
#include <hist_adder/monitor.h>
#include <RTL/Logger.h>

/// C/C++ include files
#include <map>
#include <memory>
#include <vector>
#include <string>
#include <cstdint>
#include <filesystem>

// Forward declarations

/// Online namespace declaration
namespace Online   {

  /// Hists sub-namespace
  namespace histos  {

    /// Generic adder interface
    /**
     *  \author  M.Frank
     *  \version 1.0
     */
    class eor_check  {
    public:
      std::string base_directory = "/hist/Savesets/2023/LHCb/Allen";

      /// List of histogram files to be merged.
      std::map<int32_t, std::size_t> histogram_files;
      std::map<int32_t, std::size_t> eor_files;
      int32_t last_run_number { 0 };
      monitor& mon;
      
    public:
      /// Initializing constructor
      eor_check(monitor& m);
      /// Copy constructor
      eor_check(const eor_check& copy) = default;
      /// Move constructor
      eor_check(eor_check&& copy) = default;
      /// Default destructor
      virtual ~eor_check() = default;
      /// Copy assignment
      eor_check& operator= (const eor_check& copy) = delete;
      /// Move assignment
      eor_check& operator= (eor_check&& copy) = delete;
      /// Verify that this run has histogram files
      std::size_t check_disk();
      std::size_t check_file(const std::filesystem::path& path);
      std::size_t check_directory(const std::filesystem::path& path);
    };
  }    // End namespace histos
}      // End namespace Online

#endif // ONLINE_DATAFLOW_EOR_CHECK_H
