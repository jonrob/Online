//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_DATAFLOW_HISTADDER_HIST_INFO_H
#define ONLINE_DATAFLOW_HISTADDER_HIST_INFO_H

/// C/C++ include files
#include <string>
#include <vector>
#include <filesystem>

// Forward declarations
class TH1;
class TDirectory;

/// Online namespace declaration
namespace Online   {

  /// Histograms sub-namespace
  namespace histos  {

    /// Forward declarations
    class hist_info;

    class hist_info  {
    public:
      std::string task;
      std::string run;
      std::string time;
      int         runno;
      bool        eor;
    public:
      hist_info(const std::filesystem::path& path);
      hist_info(const hist_info& copy) = default;
      hist_info(hist_info&& copy) = default;
      hist_info() = default;
      virtual ~hist_info() = default;
      hist_info& operator= (const hist_info& copy) = default;
      hist_info& operator= (hist_info&& copy) = default;      
      operator std::vector<std::pair<std::string, std::string> > () const;
    };
  }
}
#endif // ONLINE_DATAFLOW_HISTADDER_HIST_INFO_H
