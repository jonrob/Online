//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: April 28, 2023
//==========================================================================

/// Framework include files
#include <hist_adder/eor_check_app.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/rtl.h>

#include <iostream>

using namespace Online::histos;

/// Default constructor
eor_check_app::eor_check_app()
  : checker(mon)
{
}

/// Initialize database access
void eor_check_app::init()  {
  rundb  = std::make_unique<rpc::run_db>("rundb11.lbdaq.cern.ch", 8080);
}

void eor_check_app::check_disk()  {
  /// First update data cache
  checker.check_disk();
  /// We only arrive here if we have new runs older than 15 minutes.
  /// All these should have a histogram file
  auto rtodo = this->runs_todo;
  for( const auto& r : rtodo )  {
    int32_t run = r.first;
    auto rit1 = checker.histogram_files.find(run);
    auto rit2 = checker.eor_files.find(run);
    if ( rit1 == checker.histogram_files.end() &&
	 rit2 == checker.eor_files.end() )  {
      missing_run(run, r.second);
    }
    else if ( rit1 == checker.histogram_files.end() ||
	      rit2 == checker.eor_files.end() )  {
      missing_run(run, r.second);
    }
    else if ( run > checker.last_run_number )  {
      missing_run(run, r.second);
    }
  }
}

void eor_check_app::missing_run(int32_t run, const run_entry& entry)   {
  auto rit1 = checker.histogram_files.find(run);
  auto rit2 = checker.eor_files.find(run);
  std::string start = ::ctime(&entry.start);
  std::string end = ::ctime(&entry.end);
  start = start.substr(0, start.length()-1);
  end   = end.substr(0, end.length()-1);
  mon.updateLogMsg(LIB_RTL_ALWAYS,
		   "Missing run %8d start: %s end: %s duration:%5ld sec state: %d  Histos: %s EOR: %s\n",
		   run, start.c_str(), end.c_str(), entry.end-entry.start, entry.state,
		   rit1 == checker.histogram_files.end() ? "NO" : "YES",
		   rit2 == checker.eor_files.end() ? "NO" : "YES");
  auto rit = this->runs_todo.find(run);
  if ( rit != this->runs_todo.end() )  {
    this->runs_todo.erase(rit);
  }
}

void eor_check_app::check_rundb()  {
  using namespace xmlrpc;
  char start_time[128];
  std::vector<int> states { 1, 2, 3, 4, 5, 6 };
  std::vector<std::string> destinations { "OFFLINE", "LOCAL", "EOS" };
  std::vector<std::string> fields { "runID", "state", "startTime", "endTime" };
  ::time_t    since = ::time(0) - rundb_days_to_scan*24*3600;
  struct tm  *tm  = ::localtime(&since);

  ::strftime(start_time, sizeof(start_time), "%Y-%m-%dT%H:%M:%S", tm);
  try  {
    Tuple ret = rundb->get_runs_start(fields, destinations, states, this->partition, start_time);
    if ( ret.is<Array>() )  {
      auto elements  = ret.get<Array>().elements();
      Tuple code(elements[0]);
      if ( code.get<int>() == 1 )  {
	Array runs = Tuple(elements[1]).get<Array>();
	auto  run_set = runs.elements();
	std::size_t new_runs = 0;

	mon.updateLogMsg(LIB_RTL_ALWAYS, "====> New scan: Size of run set: %ld\n", run_set.size());
	for(std::size_t i=0; i < run_set.size(); ++i)  {
	  Tuple run(run_set[i]);
	  if ( run.is<Array>() )  {
	    struct timeval start { 0, 0 }, end { 0, 0 };
	    int runID = 0, state = 0;
	    auto params = run.get<Array>().elements();
	    for( std::size_t j=0; j<params.size(); ++j )  {
	      Array::Entry f(params[j]);
	      switch ( j )  {
	      case 0:
		runID = f.get<int>();
		break;
	      case 1:
		state = f.get<int>();
		break;
	      case 2:
		start = f.get<struct timeval>();
		break;
	      case 3:
		end = f.get<struct timeval>();
		break;
	      default:
		mon.updateLogMsg(LIB_RTL_ALWAYS, "====> UNKNOWN DATA FIELD\n");
		break;
	      }
	    }

	    /// Do not take any runs younger than 15 minutes
	    /// (first histogram file creation)
	    std::time_t now = ::time(0);
	    if ( now - start.tv_sec < 15*60 )   {
	      continue;
	    }
	    /// Really small runs: we do not care
	    if ( (end.tv_sec - start.tv_sec) < 2*60 )  {
	      continue;
	    }
	    /// If the run was already see: update data - that's it
	    bool present = this->runs_on_disk.find(runID) != this->runs_on_disk.end();
	    this->runs_on_disk[runID] = { start.tv_sec, end.tv_sec, state };
	    if ( present )  {
	      continue;
	    }
	    /// This run we need to have a closer look
	    this->runs_todo[runID] = { start.tv_sec, end.tv_sec, state };
	    ++new_runs;
	  }
	}
	if ( new_runs > 0 )   {
	  IocSensor::instance().send(this, SCAN_DISK, this);
	}
      }
      return;
    }
  }
  catch(const std::exception& e)   {
    mon.updateLogMsg(LIB_RTL_ALWAYS, "+++ Exception occurred durin RPC call: %s\n", e.what());
  }
  mon.updateLogMsg(LIB_RTL_ERROR,"Failed to access the run database.\n");
}

/// Interactor interrupt handler callback
void eor_check_app::handle(const CPP::Event& ev)  {
  try  {
    switch(ev.eventtype) {
    case IocEvent: {
      switch(ev.type) {
      case SCAN_DISK:
	check_disk();
	//TimeSensor::instance().add(this, disk_scan_interval, SCAN_DISK);
	break;
      case SCAN_RUNDB:
	check_rundb();
	TimeSensor::instance().add(this, rundb_scan_interval, SCAN_RUNDB);
	break;
      default:
	break;
      };
      break;
    }
    
    case TimeEvent:  {
      long cmd = long(ev.timer_data);
      switch(cmd) {
      case SCAN_DISK:
      case SCAN_RUNDB:
	IocSensor::instance().send(this, cmd, this);
	break;
      default:
	break;
      }
      break;
    }

    default:
      break;
    }
  }
  catch(const std::exception& e)   {
    mon.updateLogMsg(LIB_RTL_ALWAYS,"+++ EXCEPTION: Scanning run database: %s.\n", e.what());
  }
  catch(...)   {
    mon.updateLogMsg(LIB_RTL_ALWAYS,"+++ EXCEPTION: Scanning run database: UNKNOWN EXCEPTION.\n");
  }
}
