//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  SocketReceiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data receiver 
//               using proprietory socket based data I/O.
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS DataTransfer
#include "NET/Transfer.h"
#define DataReceiver Dataflow_SocketReceiver
#include "Dataflow/DataReceiver.h"
