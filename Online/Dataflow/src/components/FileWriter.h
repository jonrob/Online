//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  FileWriter.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Beat Jost
//==========================================================================
#ifndef ONLINE_DATAFLOW_FILEWRITER_H
#define ONLINE_DATAFLOW_FILEWRITER_H

/// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/FileWriterMgr.h>
#include <MBM/Requirement.h>

/// C/C++ include files
#include <map>
#include <set>
#include <list>
#include <mutex>
#include <thread>
#include <memory>

// Forward declarations
namespace MBM
{
  class Consumer;
  class EventDesc;
}

///  Online namespace declaration
namespace Online   {

  class FileWriter : public FileWriterMgr::Processor  {
  public:
    typedef std::vector<std::string>      Requirements;
    typedef std::vector<MBM::Requirement> BinRequirements;
    enum    {
      C_OPEN=0,
      C_CLOSEWAIT=1,
      C_CLOSED
    };
    class RunDesc;
    class FileDescr;
    class SteeringInfo;
  protected:
    /// Property: Request specification
    Requirements                       m_req;
    ///File Prefix String. Filename wil be m_filePrefix + Runnumber + _FileWriter+.mdf
    std::string                        m_filePrefix;
    /// Limit on the filesize in MB=1024*1024 Bytes
    std::vector<std::string>           m_deviceList;
    /// Property: Partition name string
    std::string                        m_partitionName;
    /// Property: Run-type string
    std::string                        m_activity;
    /// Property: Stream identifier
    std::string                        m_stream;
    /// Property:
    std::string                        m_nodePattern;
    /// Property: Maximum number of events to be written per run
    long                               m_maxEvents           { 0 };
    /// Property: Maximal event limit of an open file
    long                               m_eventLimit          { 9999999999 };
    /// Property: Maximal size of an open file
    long                               m_sizeLimit           { 0 };
    /// Property: Maximal number of open files
    int                                m_maxFilesOpen        { 0 };
    /// Property:
    int                                m_fileCloseDelay      { 0 };
    /// Property: Delay time to close if file is idle
    int                                m_fileIdleDelay       { 10 };
    /// Property:
    float                              m_evfrac              { 1e0 };
    /// Property:
    float                              m_evfreq              {-1e0 };
    /// Property:
    int                                m_dimSteering         { 0 };
    /// Property:
    int                                m_dimMonitoring       { 0 };
    /// Property: Flag if a sample should be extracted for every run
    int                                m_use_all_runs        { 0 };
    /// Property: Flag if a sample should be extracted for every run
    int                                m_checkFreeSpace      { 1 };

    /// Monitoring quantity: Bytes Written to file
    long                               m_BytesOut            { 0L };
    /// Monitoring quantity: Number of Event in
    long                               m_events_in           { 0L };
    /// Monitoring quantity: Number of Event out
    long                               m_events_out          { 0L };

    /// Lock for run lists
    std::mutex                         m_runlock             { };
    /// Lock for file lists
    std::mutex                         m_listlock            { };
    /// Binary requirements from properties
    BinRequirements                    m_breq                { };

    std::unique_ptr<std::thread>       m_thread;
    std::unique_ptr<SteeringInfo>      m_steeringSvc;

    ///List of runs and descriptors;
    std::map<unsigned int,RunDesc*>    m_runList;
    std::list<FileDescr*>              m_fileCloseList;

    unsigned int                       m_curr_run            { 0 };
    long                               m_evt_number          { 0 };
    long                               m_filesOpen           { 0 };
    long                               m_filesClosed         { 0 };
    int                                m_steeringData        { 0 };
    int                                m_num_bad_frames      { 0 };
    bool                               m_ExitOnError;
    bool                               m_writeEnabled        { false };
    bool                               m_texit               { false };
    bool                               m_disabled            { false };
    bool                               m_stopped             { false };

    void close_files();

    int  getDevice(std::vector<std::string> &devlist);
    void getRODevices();

  public:
    ///Clear Counters
    void clearCounters();
    /// Standard Constructor
    FileWriter(const std::string& name, Context& context);
    /// Standard Destructor
    virtual ~FileWriter();
    /// Service overload: initialize()
    virtual int initialize()  override;
    /// Service overload: start()
    virtual int start()  override;
    /// Service overload: stop()
    virtual int stop()  override;
    /// Service overload: finalize()
    virtual int finalize()  override;
    /// Process single event. Input buffer is ALWAYS in mdf bank format.
    virtual int execute(const Context::EventData& e)  override;

    /// Write single event to disk. Input buffer is ALWAYS in mdf bank format.
    virtual int writeEvent(RunDesc* run_desc, const event_header_t* mdf);
    /// Open a new file
    virtual FileDescr* openFile(RunDesc* run_desc);
    /// Declare Monitoring information on a run
    virtual void       createMonitoringInfo(unsigned int runn);
    /// get the file time string
    std::string        fileTime();
    /// Handle writer error
    virtual void       handleFileWriteError();
    /// Write data to file
    virtual ssize_t    write(FileDescr* d, const void *buf, size_t n);
    /// Mark file for close
    virtual void       markClose(FileDescr* d, int delay);
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_FILEWRITER_H
