//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  SocketSender.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data sender 
//               using proprietory socket based data I/O.
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS DataTransfer
#include "NET/Transfer.h"
#define DataSender Dataflow_SocketSender
#include "Dataflow/DataSender.h"
