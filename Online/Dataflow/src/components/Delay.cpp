//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Delay.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Delay.h"
#include <Dataflow/Incidents.h>
#include <RTL/rtl.h>

using namespace Online;

/// Initializing constructor
Delay::Delay(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  declareProperty("Initialize",m_initDelay     = 0.0);
  declareProperty("Start",     m_startDelay    = 0.0);
  declareProperty("Stop",      m_stopDelay     = 0.0);
  declareProperty("Finalize",  m_finiDelay     = 0.0);
  declareProperty("Pause",     m_pauseDelay    = 0.0);
  declareProperty("Cancel",    m_cancelDelay   = 0.0);
  declareProperty("Continue",  m_continueDelay = 0.0);
  declareProperty("Event",     m_eventDelay    = 0.0);
}

/// Default destructor
Delay::~Delay()   {
}

/// Sleep function
void Delay::delay(double timeout)  const   {
  if ( timeout > 1E-6 )  {
    long useconds = long(timeout*1E6);
    if ( useconds > 0 ) ::lib_rtl_usleep(useconds);
  }
}

/// Initialize the delay component
int Delay::initialize()  {
  int sc = Component::initialize();
  subscribeIncident("DAQ_PAUSE");
  subscribeIncident("DAQ_CANCEL");
  if ( sc == DF_SUCCESS )   {
    delay(m_initDelay);
  }
  return sc;
}

/// Start the delay component
int Delay::start()  {
  int sc = Component::start();
  if ( sc == DF_SUCCESS )   {
    delay(m_startDelay);
  }
  return sc;
}

/// Finalize the delay component
int Delay::finalize()  {
  delay(m_finiDelay);
  unsubscribeIncidents();
  return Component::finalize();
}

/// Stop the delay component
int Delay::stop()  {
  delay(m_stopDelay);
  return Component::stop();
}

/// Pause the data flow component. Default implementation is empty.
int Delay::pause()  {
  delay(m_pauseDelay);
  return DataflowComponent::pause();
}

/// Continuing the data flow component. Default implementation is empty.
int Delay::continuing()  {
  delay(m_continueDelay);
  return DataflowComponent::continuing();
}

/// Data processing overload: Send data record to network client
int Delay::execute(const Context::EventData& /* event */)  {
  delay(m_eventDelay);
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Delay::handle(const DataflowIncident& inc)    {
  info("Got incident: %s ot type:%s",inc.name.c_str(), inc.type.c_str());
  if ( inc.type == "DAQ_PAUSE" )  {
    delay(m_pauseDelay);
  }
  else if ( inc.type == "DAQ_CANCEL" )  {
    delay(m_cancelDelay);
  }
}
