//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Component.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_PARSERS_H
#define ONLINE_DATAFLOW_PARSERS_H

#include <XML/Evaluator.h>
namespace dd4hep  {
  namespace XmlTools { class Evaluator; }
  XmlTools::Evaluator& evaluator(); 
}
namespace Online {
  inline dd4hep::XmlTools::Evaluator& g4Evaluator() { return dd4hep::evaluator(); }
}

// Standard Dataflow parser handling
#include <Parsers/spirit/Parsers.h>
#include <Parsers/spirit/ToStream.h>

#endif //  ONLINE_DATAFLOW_PARSERS_H
