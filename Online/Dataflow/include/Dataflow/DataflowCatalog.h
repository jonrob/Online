//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowCatalog.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATAFLOWCATALOG_H
#define ONLINE_DATAFLOW_DATAFLOWCATALOG_H

// C/C++ include files
#include <map>

///  Online namespace declaration
namespace Online  {

  /// Monitor service for the dataflow
  /** @class DataflowCatalog DataflowCatalog.h Dataflow/DataflowCatalog.h
   *
   *
   * @author  Markus Frank
   * @version 1.0
   */
  template <typename K, typename T>
    class DataflowCatalog : public std::map<K,T>  {
    typedef std::map<K,T> base_t;
  public:
    /// Initializing constructor
    DataflowCatalog()           {                        }
    /// Default destructor
    virtual ~DataflowCatalog()  { this->base_t::clear(); }
    /// Clear data content
    void clear()                { this->base_t::clear(); }
  };

#define DATAFLOW_MAKE_CATALOG(type,key,value)	\
  namespace Online {					\
    class type : public DataflowCatalog<key,value> {	\
    public:						\
    type() : DataflowCatalog<key,value>() {}		\
      virtual ~type() {}				\
    };}
  
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_DATAFLOWCATALOG_H
