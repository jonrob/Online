###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import platform
import xml.etree.ElementTree as ET
from string import Template


def node_name():
    return platform.node().split('.', 1)[0].upper()


def rinterp(obj, mapping):
    """Recursively interpolate object with a dict of values."""
    return _rinterp(obj, mapping)


def _rinterp(obj, mapping):
    try:
        return {k: _rinterp(v, mapping) for k, v in obj.items()}
    except AttributeError:
        pass
    try:
        return Template(obj).safe_substitute(mapping)
    except TypeError:
        pass
    try:
        return [_rinterp(v, mapping) for v in obj]
    except TypeError:
        return obj


def parse_task_tree(task):
    """Parse task tree into name, n_instances, subprocess.Popen arguments."""

    name = task.attrib["name"]
    try:
        n_instances = int(task.attrib.get("instances", 1))
    except ValueError:  # instances = "NUMBER_OF_INSTANCES"
        n_instances = 1

    params = {"args": [], "env": {}, "cwd": "/"}
    # The controller passes populates `-instances`` as the instances argument
    # in the architecture minus one. `createEnvironment.sh` then populates
    # the NBOFSLAVES variable directly from `-instances`.
    params["args"].append(f"-instances={n_instances-1}")
    command = task.find("command").text
    for argument in task.findall("argument"):
        arg = argument.attrib["name"]
        if "value" in argument.attrib:
            arg += "=" + argument.attrib["value"]
        params["args"].append(arg)
    for fmcparam in task.findall("fmcparam"):
        if fmcparam.attrib["name"] == "wd":
            params["cwd"] = fmcparam.attrib["value"]
        elif fmcparam.attrib["name"] == "utgid":
            task_name = fmcparam.attrib["value"]
        elif fmcparam.attrib["name"] == "define":
            k, v = fmcparam.attrib["value"].split("=", 1)
            params["env"][k] = v

    params["executable"] = os.path.join(params["cwd"], command)
    params["args"].insert(0, task_name)
    return name, n_instances, params


def read_xml(path):
    """"Parse architecture file into a list of task specs."""
    tree = ET.parse(path)
    tasks_inventory = tree.getroot()
    return [parse_task_tree(task) for task in tasks_inventory]


def instance_args(tasks, replacements):
    """Return Popen arguments for each task instance."""
    result = []
    for name, n_instances, args in tasks:
        for instance in range(n_instances):
            result.append(
                rinterp(
                    args, replacements | {
                        "NODE": node_name(),
                        "NAME": name,
                        "INSTANCE": instance,
                    }))
    return result
