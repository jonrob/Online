###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import asyncio
import logging
from TestBeam.emulator import (
    tasks_load,
    tasks_wait_for_status,
    tasks_send_command,
    tasks_wait_for_exit,
    tasks_measure_throughput,
)

log = logging.getLogger(__name__)

def call_handler(handler, name):
  if hasattr(handler, name):
    getattr(handler, name)()
  elif hasattr(handler, 'onStateChanged'):
    getattr(handler, 'onStateChanged')(name)

async def run(tasks, args, producer, handler):
    await tasks_load(tasks)
    # TODO for some reason HLT2 publishes OFFLINE before NOT_READY, but only sometimes
    call_handler(handler, 'onCreated')
    await tasks_wait_for_status(tasks, "NOT_READY", skip=["OFFLINE"])

    await tasks_send_command(tasks, "configure")
    await tasks_wait_for_status(tasks, "READY")
    call_handler(handler, 'onReady')

    await tasks_send_command(tasks, "start")
    await tasks_wait_for_status(tasks, "RUNNING")
    call_handler(handler, 'onRunning')

    if producer:
        prod_task = [t for t in tasks if producer in t.utgid]
        await tasks_wait_for_status(prod_task, "PAUSED")
    elif args.measure_throughput > 0:
        # wait a bit for things to settle and measure throughput
        await asyncio.sleep(args.measure_throughput / 8)
        await tasks_measure_throughput(
            tasks, max_duration=args.measure_throughput)
    else:
        prod_task = [t for t in tasks if producer in t.utgid]
        await tasks_wait_for_status(prod_task, "PAUSED")
    call_handler(handler, 'onPaused')

    await tasks_send_command(tasks, "stop")
    await tasks_wait_for_status(tasks, "READY")
    call_handler(handler, 'onStopped')

    await tasks_send_command(tasks, "reset")
    await tasks_wait_for_status(tasks, "NOT_READY")
    call_handler(handler, 'onNotReady')

    # FIXME: the following is hack
    for task in tasks:
        task._exit_task.cancel()

    await tasks_send_command(tasks, "unload")
    await tasks_wait_for_status(tasks, "OFFLINE")
    call_handler(handler, 'onOffline')

    # Wait for the tasks to close
    exit_codes = await tasks_wait_for_exit(tasks)
    if set(exit_codes) != {0}:
        for t, ec in zip(tasks, exit_codes):
            if ec != 0:
                log.error(f"{t.utgid} exited with non-zero code {ec}")
        return 102
    log.info(f"Scenario processing finished!")
    call_handler(handler, 'onDone')
    return 0

