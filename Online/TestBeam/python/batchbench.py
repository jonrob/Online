#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import asyncio
import logging
import os
import pathlib
from TestBeam import architecture, emulator

global online_env_module
online_env_module = None

log = logging.getLogger(__name__)

# -------------------------------------------------------------------------------------
class handler:
  def __init__(self):
    pass
  def onStateChanged(self, state):
    print('scene: onStateChanged: '+state)
    
# -------------------------------------------------------------------------------------
def _expand_path(path):
  #if hasattr(path,'resolve'):
  #  path = path.resolve()
  spath = str(path)
  while spath.find('${') >= 0:
    idx   = spath.find('${')
    idq   = spath.find('}')
    pre   = spath[:idx]
    post  = spath[idq+1:]
    env   = spath[idx+2:idq]
    rep   = os.getenv(env,'Unknown-path-environment-'+env)
    spath = pre + rep + post
  path = pathlib.Path(spath).resolve()
  return path

# -------------------------------------------------------------------------------------
def _import_online_environment(path, replacements):
  import importlib.util
  from string import Template
  global online_env_module
  
  spec = importlib.util.spec_from_file_location('online_env_object', path)
  online_env_module = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(online_env_module)
  items = {}
  for i in dir(online_env_module):
    if len(str(i))>2 and str(i)[:2] != '__':
      val = eval('online_env_module.'+str(i))
      if isinstance(val,str):
        val = Template(val).safe_substitute(replacements)
      items[i] = val
  return items

# -------------------------------------------------------------------------------------
def _import_scenario(path):
  import importlib.util
  spec = importlib.util.spec_from_file_location('scenario', path)
  s = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(s)
  return s

# -------------------------------------------------------------------------------------
def _generate_environment( ):
  import subprocess
  from datetime import datetime

  subprocess.check_call([
    "/bin/bash", "-c", "declare -px > " + str(args.working_dir / "setup.vars")
  ])

  if args.output_level:
    online_env["OutputLevel"] = args.output_level

  online_opts = online_env.copy()
  online_opts |= {
    'Reader_Rescan': args.measure_throughput > 0,
    'Reader_Directories': [str(args.working_dir.resolve())],
    'Reader_FilePrefix': '',
    'Reader_Preload': args.measure_throughput > 0,
  }

  with open( args.working_dir / "runTask.sh" , "w") as f:
    f.write("#!/bin/bash\n")
    f.write("cd "+str(args.working_dir)+";\n")
    f.write(". ./setup.vars;\n")
    f.write(". ${FARMCONFIGROOT}/job/createEnvironment.sh $*;\n")
    f.write(". ${COMMAND};\n")
    os.chmod(f.name, 0o777)

  with open(args.working_dir / "OnlineEnv.opts", "w") as f:
    f.write("//  Auto generated RunInfo options for " +
            f"partition:{args.partition} activity:{{Activity}}  " +
            f"{datetime.now():%Y.%m.%d %H:%M:%S.000}\n")
    emulator.dump_opts(online_opts, f)

  with open(args.working_dir / "OnlineEnvBase.py", "w") as f:
    f.write("#  Auto generated OnlineEnvBase for " +
            f"partition:{args.partition} activity:{{Activity}}  " +
            f"{datetime.now():%Y.%m.%d %H:%M:%S.000}\n")
    for k, v in online_env.items():
        f.write(f"{k} = {v!r}\n")

  with open(args.working_dir / "OnlineEnv.py", "w") as f:
    f.write("#  Auto generated OnlineEnv for " +
            f"partition:{args.partition} activity:{{Activity}}  " +
            f"{datetime.now():%Y.%m.%d %H:%M:%S.000}\n")
    f.write("from OnlineEnvBase import *\n\n")

# -------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(allow_abbrev=False)

parser.add_argument(
    "--architecture",
    type=pathlib.Path,
    dest='architecture',
    help="Path to task architecture XML",
)

parser.add_argument(
    "--working-dir",
    type=pathlib.Path,
    dest='working_dir',
    default=pathlib.Path.cwd(),
    help="Working directory (created if needed)",
)
parser.add_argument(
    "--task-script",
    type=pathlib.Path,
    dest='task_script',
    default="Invalid-startup-script",
    help="Path to common task startup script",
)
parser.add_argument(
    "--dim-dns-port",
    type=int,
    help="DIM DNS port (must be unused)",
)
parser.add_argument(
    "--partition",
    type=str,
    default='0x%04X'%(os.getpid()&0xFFFF,),
    help="Partition name to use",
)
parser.add_argument(
    "--scenario",
    type=pathlib.Path,
    default=None,
    help="Scenario path",
)
parser.add_argument(
    "--producer",
    type=str,
    dest='producer',
    default=None,
    help="Producer task to wait for PAUSE",
)
parser.add_argument(
    "--runinfo",
    type=pathlib.Path,
    default=None,
    help="Online environment path (runingo)",
)
parser.add_argument(
    "--output-level",
    type=int,
    default=None,
    dest='output_level',
    choices=[1, 2, 3, 4, 5],
    help="Gaudi OutputLevel",
)

parser.add_argument(
    "-d",
    "--debug",
    action="store_const",
    dest="log_level",
    const=logging.DEBUG,
    default=logging.INFO,
    help="Enable debug messages from the testbench",
)
parser.add_argument(
    "--log-file",
    default="emu.log",
    help="Location of logfile (if relative, with respect to --working-dir).",
)
parser.add_argument(
    "--measure-throughput",
    default=10,
    type=float,
    help="How long to measure throughput for.",
)
parser.add_argument(
    "--dim_dns_node",
    type=str,
    dest='dim_dns_node',
    default='localhost',
    help="DIM DNS node (default: localhost)",
)

args = parser.parse_args()

os.putenv('DIM_DNS_NODE', args.dim_dns_node)
os.environ['DIM_DNS_NODE'] = args.dim_dns_node

debug = False
if debug:
  import pdb
  pdb.set_trace()

args.runinfo      = _expand_path(args.runinfo)
args.scenario     = _expand_path(args.scenario)
args.architecture = _expand_path(args.architecture)
args.working_dir  = _expand_path(args.working_dir.resolve())
if wd := args.working_dir:
  if not wd.exists():
    wd.mkdir()

if args.log_level == logging.DEBUG:
  logging.basicConfig(level=logging.DEBUG)
  logging.getLogger("asyncio").setLevel(logging.DEBUG)
emulator.setup_logging( args.working_dir / args.log_file, console_level=args.log_level)

# TODO filter out some variables as done in cmsetup.py
replacements = {
  'PARTITION':    args.partition,
  'RUNINFO':      args.working_dir.resolve() / 'OnlineEnvBase.py',
  'BINARY_TAG':   os.environ['BINARY_TAG'],
  'WORKING_DIR':  args.working_dir.resolve(),
}

arch = architecture.read_xml(args.architecture)
scenario = _import_scenario(args.scenario)
online_env = _import_online_environment(args.runinfo, replacements)
task_instance_args = architecture.instance_args(arch, replacements)

emulator.check_for_orphans([a['args'][0] for a in task_instance_args])

async def main():
  import contextlib

  os.chdir(args.working_dir)
  try:
    async with contextlib.AsyncExitStack() as stack:
      kwargs = {}
      if args.dim_dns_port:
        kwargs = { "ports": [args.dim_dns_port] }
      await stack.enter_async_context( emulator.start_dim_dns(**kwargs) )

      _generate_environment()
      tasks = []
      for task_args in task_instance_args:
        short_utgid = "_".join(task_args["args"][0].split("_")[1:])
        task_log_server = emulator.logs.LogServerThread(
          name=short_utgid,
          console=False,
          fifo_path=args.working_dir,
          output_path=args.working_dir / f"{short_utgid}.log")
        task_args['cwd'] = str(args.working_dir)
        task_args['executable'] = './runTask.sh'
        stack.enter_context(task_log_server)
        # print(str(task_args))
        task = emulator.Task( task_args, task_log_server )
        await stack.enter_async_context( task )
        tasks.append( task )
      return await scenario.run( tasks, args, args.producer, handler() )

  except asyncio.CancelledError:
    log.warning( "Event loop was cancelled" )
    return 101

exit( asyncio.run(main(), debug=True) )
