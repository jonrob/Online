//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <TestBeam/gui/InfrastructurePanel.h>
#include <TestBeam/gui/GuiException.h>
#include <TestBeam/gui/TaskManager.h>
#include <TestBeam/gui/GuiCommand.h>
#include <TestBeam/gui/GuiMsg.h>

#include <CPP/IocSensor.h>
#include <CPP/Event.h>

/// ROOT include files
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGComboBox.h>
#include <TGTableLayout.h>

ClassImp(testbeam::InfrastructurePanel)

/// Standard initializing constructor
testbeam::InfrastructurePanel::InfrastructurePanel(TGFrame* pParent, CPP::Interactor* g, TaskManager& tm)
  : TGCompositeFrame(pParent, 100, 100, kVerticalFrame), gui(g), taskManager(tm), taskHandler(tm)
{
  gClient->GetColorByName("#c0c0c0", disabled);
  gClient->GetColorByName("white",   enabled);
  gClient->GetColorByName("#30B030", green);
  gClient->GetColorByName("#C04040", red);
  gClient->GetColorByName("#4040F0", blue);
  gClient->GetColorByName("#F0F000", yellow);
  gClient->GetColorByName("#C0C000", executing);
  gClient->GetColorByName("#000000", black);
  white = enabled;
}

void testbeam::InfrastructurePanel::init()   {
  TGGroupFrame* para_group   = new TGGroupFrame(this, "Processing infrastructure");
  para_group->SetLayoutManager(new TGTableLayout(para_group, 11, 9));

  maindns.label  = new TGLabel(    para_group,  "Main DNS:",            MAINDNS_LABEL);
  maindns.input  = new TGTextEntry(para_group,  taskManager.mainDNS().c_str(),      MAINDNS_INPUT);
  maindns.input->SetMaxWidth(100);

  dns.label      = new TGLabel(    para_group,  "Local DNS:",           DNS_LABEL);
  dns.input      = new TGTextEntry(para_group,  taskManager.dnsNode().c_str(),      DNS_INPUT);
  dns.input->SetMaxWidth(100);

  node.label     = new TGLabel(    para_group,  "Node",                 NODE_LABEL);
  node.input     = new TGTextEntry(para_group,  taskManager.nodeName().c_str(),     NODE_INPUT);

  part.label     = new TGLabel(    para_group,  "Partition",            PART_LABEL);
  part.input     = new TGTextEntry(para_group,  taskManager.partition().c_str(),    PART_INPUT);

  dnsSrv.label   = new TGLabel(     para_group, "dnsSrv",       DNSSRV_LABEL);
  dnsSrv.utgid   = new TGTextEntry( para_group, taskManager.dnsSrvName().c_str());
  dnsSrv.start   = new TGTextButton(para_group, "Start",        DNSSRV_START);
  dnsSrv.status  = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  dnsSrv.kill    = new TGTextButton(para_group, "Kill",         DNSSRV_KILL);
  dnsSrv.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "dnsSrv_start()");
  dnsSrv.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "dnsSrv_kill()");

  tmSrv.label    = new TGLabel(     para_group, "tmSrv",        TMSRV_LABEL);
  tmSrv.utgid    = new TGTextEntry( para_group, taskManager.tmSrvName().c_str());
  tmSrv.start    = new TGTextButton(para_group, "Start",        TMSRV_START);
  tmSrv.status   = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  tmSrv.kill     = new TGTextButton(para_group, "Kill",         TMSRV_KILL);
  tmSrv.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "tmSrv_start()");
  tmSrv.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "tmSrv_kill()");

  tanSrv.label   = new TGLabel(     para_group, "tanSrv",    TANSRV_LABEL);
  tanSrv.utgid   = new TGTextEntry( para_group, taskManager.tanSrvName().c_str());
  tanSrv.start   = new TGTextButton(para_group, "Start",     TANSRV_START);
  tanSrv.status  = new TGTextEntry( para_group,  TaskManager::TASK_DEAD);
  tanSrv.kill    = new TGTextButton(para_group, "Kill",      TANSRV_KILL);
  tanSrv.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "tanSrv_start()");
  tanSrv.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "tanSrv_kill()");

  logSrv.label   = new TGLabel(     para_group, "logSrv",       LOGSRV_LABEL);
  logSrv.utgid   = new TGTextEntry( para_group, taskManager.logSrvName().c_str());
  logSrv.start   = new TGTextButton(para_group, "Start",        LOGSRV_START);
  logSrv.status  = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  logSrv.kill    = new TGTextButton(para_group, "Kill",         LOGSRV_KILL);
  logSrv.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "logSrv_start()");
  logSrv.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "logSrv_kill()");

  logViewer.label   = new TGLabel(     para_group, "logViewer", LOGVIEWER_LABEL);
  logViewer.utgid   = new TGTextEntry( para_group, taskManager.logViewName().c_str());
  logViewer.start   = new TGTextButton(para_group, "Start",     LOGVIEWER_START);
  logViewer.status  = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  logViewer.kill    = new TGTextButton(para_group, "Kill",      LOGVIEWER_KILL);
  logViewer.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "logViewer_start()");
  logViewer.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "logViewer_kill()");

  mbmmon.label      = new TGLabel(     para_group, "mbmmon",    MBMMON_LABEL);
  mbmmon.utgid      = new TGTextEntry( para_group, taskManager.mbmmonName().c_str());
  mbmmon.start      = new TGTextButton(para_group, "Start",     MBMMON_START);
  mbmmon.status     = new TGTextEntry( para_group,  TaskManager::TASK_DEAD);
  mbmmon.kill       = new TGTextButton(para_group, "Kill",      MBMMON_KILL);
  mbmmon.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "mbmmon_start()");
  mbmmon.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "mbmmon_kill()");

  mbmdmp.label      = new TGLabel(     para_group, "mbmdmp",    MBMDMP_LABEL);
  mbmdmp.utgid      = new TGTextEntry( para_group, taskManager.mbmdmpName().c_str());
  mbmdmp.start      = new TGTextButton(para_group, "Start",     MBMDMP_START);
  mbmdmp.status     = new TGTextEntry( para_group,  TaskManager::TASK_DEAD);
  mbmdmp.kill       = new TGTextButton(para_group, "Kill",      MBMDMP_KILL);
  mbmdmp.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "mbmdmp_start()");
  mbmdmp.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "mbmdmp_kill()");

  storage.label     = new TGLabel(     para_group, "storage",   STORAGE_LABEL);
  storage.utgid     = new TGTextEntry( para_group, taskManager.storageName().c_str());
  storage.start     = new TGTextButton(para_group, "Start",     STORAGE_START);
  storage.status    = new TGTextEntry( para_group,  TaskManager::TASK_DEAD);
  storage.kill      = new TGTextButton(para_group, "Kill",      STORAGE_KILL);
  storage.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "storage_start()");
  storage.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "storage_kill()");

  did.label      = new TGLabel(para_group,      "Local DID",            DID_LABEL);
  did.utgid      = new TGTextEntry(para_group,  taskManager.didName().c_str(),      PART_INPUT);
  did.start      = new TGTextButton(para_group, "Start",        DID_START);
  did.status     = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  did.kill       = new TGTextButton(para_group, "Kill",         DID_KILL);
  did.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "did_start()");
  did.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "did_kill()");

#define _PAD 0
#define _PADL 15
#define _PADY  5
  para_group->AddFrame(maindns.label,    new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(maindns.input,    new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(dns.label,        new TGTableLayoutHints(0, 1, 1, 2, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(dns.input,        new TGTableLayoutHints(1, 2, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(node.label,       new TGTableLayoutHints(0, 1, 2, 3, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(node.input,       new TGTableLayoutHints(1, 2, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(part.label,       new TGTableLayoutHints(0, 1, 3, 4, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(part.input,       new TGTableLayoutHints(1, 2, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));

  int row = 0;
  para_group->AddFrame(dnsSrv.label,     new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(dnsSrv.utgid,     new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(dnsSrv.start,     new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(dnsSrv.kill,      new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(dnsSrv.status,    new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(tmSrv.label,      new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tmSrv.utgid,      new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tmSrv.start,      new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tmSrv.kill,       new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(tmSrv.status,     new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(tanSrv.label,     new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tanSrv.utgid,     new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tanSrv.start,     new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tanSrv.kill,      new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(tanSrv.status,    new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(logSrv.label,     new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logSrv.utgid,     new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logSrv.start,     new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logSrv.kill,      new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(logSrv.status,    new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(logViewer.label,  new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logViewer.utgid,  new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logViewer.start,  new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logViewer.kill,   new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(logViewer.status, new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(mbmmon.label,     new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmmon.utgid,     new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmmon.start,     new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmmon.kill,      new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(mbmmon.status,    new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(mbmdmp.label,     new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmdmp.utgid,     new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmdmp.start,     new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmdmp.kill,      new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(mbmdmp.status,    new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(storage.label,    new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(storage.utgid,    new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(storage.start,    new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(storage.kill,     new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 
  para_group->AddFrame(storage.status,   new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  ++row;
  para_group->AddFrame(did.label,        new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(did.utgid,        new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(did.start,        new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(did.kill,         new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(did.status,       new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD)); 

  AddFrame(para_group, new TGLayoutHints(kLHintsRight|kLHintsTop|kLHintsExpandX, 1,1,1,1));

  maindns.input->Resize(250,   maindns.input->GetDefaultHeight());
  dns.input->Resize(250,       dns.input->GetDefaultHeight());
  node.input->Resize(250,      node.input->GetDefaultHeight());
  part.input->Resize(250,      part.input->GetDefaultHeight());
  maindns.input->SetEnabled(kFALSE);
  dns.input->SetEnabled(kFALSE);
  node.input->SetEnabled(kFALSE);
  part.input->SetEnabled(kFALSE);

  _configTask(did);
  _configTask(tmSrv);
  _configTask(dnsSrv);
  _configTask(tanSrv);
  _configTask(logSrv);
  _configTask(logViewer);
  _configTask(mbmmon);
  _configTask(mbmdmp);
  _configTask(storage);

  taskManager.subscribe(TaskManager::TMSRV_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::TANSRV_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::LOGSRV_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::DNSSRV_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::LOGVIEWER_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::MBMMON_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::MBMDMP_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::STORAGE_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::DID_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::CMD_UPDATE_DEPENDENT_VALUES, this, nullptr);
}

/// Default destructor
testbeam::InfrastructurePanel::~InfrastructurePanel()   {
}

void testbeam::InfrastructurePanel::_configTask(TaskEntry& entry)   {
  double height = entry.kill->GetDefaultHeight();
  entry.kill  ->Resize(120, height);
  entry.start ->Resize(220, height);
  entry.utgid ->Resize(270, height);
  entry.status->Resize(100, height);
  entry.utgid ->SetEnabled(kFALSE);
  entry.start ->SetEnabled(kTRUE);
  entry.kill  ->SetEnabled(kTRUE);
  entry.status->SetEnabled(kFALSE);
  entry.status->SetBackgroundColor(red);
  entry.status->SetForegroundColor(white);
}

void testbeam::InfrastructurePanel::updateDependentValues()  {
  if ( maindns.input )   {
    maindns.input->SetText(taskManager.mainDNS().c_str());
    gClient->NeedRedraw(maindns.input);
  }
  if ( dns.input )   {
    dns.input->SetText(taskManager.dnsNode().c_str());
    gClient->NeedRedraw(dns.input);
  }
  if ( part.input )  {
    part.input->SetText(taskManager.partition().c_str());
    gClient->NeedRedraw(part.input);
  }
  if ( tmSrv.utgid )  {
    tmSrv.utgid->SetText(taskManager.tmSrvName().c_str());
    gClient->NeedRedraw(tmSrv.utgid);
  }
  if ( dnsSrv.utgid )  {
    dnsSrv.utgid->SetText(taskManager.dnsSrvName().c_str());
    gClient->NeedRedraw(dnsSrv.utgid);
  }
  if ( logSrv.utgid )  {
    logSrv.utgid->SetText(taskManager.logSrvName().c_str());
    gClient->NeedRedraw(logSrv.utgid);
  }
  if ( tanSrv.utgid )  {
    tanSrv.utgid->SetText(taskManager.tanSrvName().c_str());
    gClient->NeedRedraw(tanSrv.utgid);
  }
  if ( did.utgid )   {
    did.utgid->SetText(taskManager.didName().c_str());
    gClient->NeedRedraw(did.utgid);
  }
  if ( logViewer.utgid )  {
    logViewer.utgid->SetText(taskManager.logViewName().c_str());
    gClient->NeedRedraw(logViewer.utgid);
  }
  if ( mbmmon.utgid )  {
    mbmmon.utgid->SetText(taskManager.mbmmonName().c_str());
    gClient->NeedRedraw(mbmmon.utgid);
  }
  if ( mbmdmp.utgid )  {
    mbmdmp.utgid->SetText(taskManager.mbmdmpName().c_str());
    gClient->NeedRedraw(mbmdmp.utgid);
  }
  if ( storage.utgid )  {
    storage.utgid->SetText(taskManager.storageName().c_str());
    gClient->NeedRedraw(storage.utgid);
  }
}

/// Update tmSrv status pane
void testbeam::InfrastructurePanel::updateTaskStatus(TaskEntry& entry, const std::string& state)    {
  if ( !state.empty() )   {
    if ( state == TaskManager::TASK_DEAD )   {
      entry.start->SetEnabled(kTRUE);
      entry.kill->SetEnabled(kFALSE);
      entry.status->SetText(TaskManager::TASK_DEAD);
      entry.status->SetBackgroundColor(red);
    }
    else  {
      entry.start->SetEnabled(kFALSE);
      entry.kill->SetEnabled(kTRUE);
      entry.status->SetText(TaskManager::TASK_RUNNING);
      entry.status->SetBackgroundColor(green);
    }
  }
  gClient->NeedRedraw(entry.status);
}

/// Interactor interrupt handler callback
void testbeam::InfrastructurePanel::handle(const CPP::Event& ev)   {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case UPDATE_DEPENDENT_VALUES:
      updateDependentValues();
      break;

    case TaskManager::DID_STATUS:
      updateTaskStatus(did, taskManager.didState());
      break;

    case TaskManager::DNSSRV_STATUS:
      updateTaskStatus(dnsSrv, taskManager.dnsSrvState());
      break;

    case TaskManager::TANSRV_STATUS:
      updateTaskStatus(tanSrv, taskManager.tanSrvState());
      break;

    case TaskManager::LOGSRV_STATUS:
      updateTaskStatus(logSrv, taskManager.logSrvState());
      break;

    case TaskManager::TMSRV_STATUS:
      updateTaskStatus(tmSrv, taskManager.tmSrvState());
      break;

    case TaskManager::MBMMON_STATUS:
      updateTaskStatus(mbmmon, taskManager.mbmmonState());
      break;

    case TaskManager::MBMDMP_STATUS:
      updateTaskStatus(mbmdmp, taskManager.mbmdmpState());
      break;

    case TaskManager::STORAGE_STATUS:
      updateTaskStatus(storage, taskManager.storageState());
      break;

    case TaskManager::LOGVIEWER_STATUS:
      updateTaskStatus(logViewer, taskManager.logViewState());
      break;

    case TaskManager::CMD_UPDATE_DEPENDENT_VALUES:
      updateDependentValues();
      break;

    default:
      break;
    }
    break;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}
