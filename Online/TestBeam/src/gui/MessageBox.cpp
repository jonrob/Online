//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

#include <TestBeam/gui/MessageBox.h>
#include <TestBeam/gui/MessageBoxImp.h>

/// Create the dialog box
testbeam::MessageBox::MessageBox(const TGWindow *p, const TGWindow *main,
				 const char *title, const char *msg, const TGPicture *icon,
				 Int_t buttons, Int_t *ret_code, UInt_t options,
				 Int_t text_align)
{
  MessageBoxImp* imp = new MessageBoxImp(p,main,title,msg,icon,buttons,ret_code,options,text_align);
  imp->Wait();
}

/// Create a message dialog box
testbeam::MessageBox::MessageBox(const TGWindow *p, const TGWindow *main,
				 const char *title, const char *msg, EMsgBoxIcon icon,
				 Int_t buttons, Int_t *ret_code, UInt_t options,
				 Int_t text_align)
{
  MessageBoxImp* imp = new MessageBoxImp(p,main,title,msg,icon,buttons,ret_code,options,text_align);
  imp->Wait();
}

testbeam::MessageBox::~MessageBox()   {
}
