#!/bin/bash
# =========================================================================
#
#  Default script to start the passthrough process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
export PYTHONPATH=${TESTBEAMROOT}/options:${PYTHONPATH};
exec -a ${UTGID} `which python` `which gaudirun.py` /home/frankm/upgrade_sw/Online/gaudionline.py --application=Online::OnlineEventApp;


