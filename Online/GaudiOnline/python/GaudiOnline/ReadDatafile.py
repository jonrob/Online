"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from .OnlineApplication import *

class ReadDatafile(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)
    self.config.numEventThreads  = 1
    self.config.numStatusThreads = 1
    self.config.events_HighMark  = 0
    self.config.events_LowMark   = 0
    self.config.logDeviceType    = 'RTL::Logger::LogDevice'
    self.config.logDeviceFormat  = '%-9LEVEL%-24SOURCE'
    self.interactive = True

  def setup_input(self):
    import Gaudi.Configuration as Gaudi
    import Configurables

    input                        = Configurables.Online__InputAlg('EventInput')
    input.DeclareData            = True
    input.DeclareEvent           = True
    input.MakeRawEvent           = True
    self.input                   = input
    algs                         = [input]
    self.app.TopAlg              = algs
    self.broker.DataProducers    = self.app.TopAlg
    return self
    
  def setup_algorithms(self):
    import Gaudi.Configuration as Gaudi
    import Configurables

    self.setup_input()
    explorer                     = store_explorer(load=1, print_freq=1.0)
    ask                          = Configurables.Online__InteractiveAlg('DumpHandler')
    ask.Prompt                   = "Press <ENTER> to dump banks, q or Q to quit :"
    dump                         = bank_dump(name='Dump', raw_data='Banks/RawData', dump_data=True, full_dump=True, check_data=False)
    hdr                          = header_dump(name='Headers', raw_data='Banks/RawDataGuard')
    ctrl                         = Configurables.Online__InteractiveAlg('CommandHandler')
    ctrl.Prompt                  = "Press <ENTER> to continue, q or Q to quit :"
    algs                         = [explorer]
    if self.interactive:
      algs.append(ask)
    algs.append(hdr)
    algs.append(dump)
    if self.interactive:
      algs.append(ctrl)
    for alg in algs:  
      self.app.TopAlg.append(alg)
    self.broker.DataProducers = self.app.TopAlg
    return self

def readDatafile(file, interactive=True, output_level=MSG_VERBOSE, algs=None):
  app = ReadDatafile(outputLevel=output_level)
  app.interactive = interactive
  if isinstance(file,list):
    app.setup_file_access(file)
  else:
    app.setup_file_access([file])
  app.config.autoStart = True
  app.config.autoStop  = True
  app.setup_hive(FlowManager("EventLoop"), 1)
  if algs:
    app.setup_input()
    for alg in algs:  
      app.app.TopAlg.append(alg)
    app.broker.DataProducers = app.app.TopAlg
  else:
    app.setup_algorithms()
  return app

