"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from .OnlineApplication import *

class DumpODIN(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)
    self.config.numEventThreads  = 1
    self.config.numStatusThreads = 1
    self.config.events_HighMark  = 0
    self.config.events_LowMark   = 0
    self.config.logDeviceType    = 'RTL::Logger::LogDevice'
    self.config.logDeviceFormat  = '%-9LEVEL%-24SOURCE'
    self.interactive = True

  def setup_algorithms(self):
    import Gaudi.Configuration as Gaudi
    import Configurables

    input                        = Configurables.Online__InputAlg('EventInput')
    input.DeclareData            = True
    input.DeclareEvent           = True
    input.MakeRawEvent           = True
    self.input                   = input
    explorer                     = Configurables.StoreExplorerAlg('Explorer')
    explorer.Load                = 1
    explorer.PrintFreq           = 1.0
    explorer.OutputLevel         = 1
    self.explorer                = explorer
    hdr                          = Configurables.Online__Tell1HeaderDump('Headers')
    hdr.RawGuard                 = 'Banks/RawDataGuard'
    self.header_dump             = hdr
    dump                         = Configurables.Online__TAEBankDump('ODIN-Dump')
    dump.RawGuard                = 'Banks/RawDataGuard'
    dump.BankType                = 16
    dump.OutputLevel             = 1
    self.dump                    = dump
    ctrl                         = Configurables.Online__InteractiveAlg('CommandHandler')
    ctrl.Prompt                  = "Press <ENTER> to continue, q or Q to quit :"
    algs                         = [input]
    algs.append(explorer)
    algs.append(hdr)
    algs.append(dump)
    if self.interactive:
      algs.append(ctrl)
    self.app.TopAlg              = algs
    self.broker.DataProducers    = self.app.TopAlg
    return self

def dumpODIN(file):
  app = DumpODIN(outputLevel=MSG_VERBOSE)
  if isinstance(file,list):
    app.setup_file_access(file)
  else:
    app.setup_file_access([file])
  app.config.autoStart = True
  app.config.autoStop  = True
  app.setup_hive(FlowManager("EventLoop"), 1)
  app.setup_algorithms()
  return app

