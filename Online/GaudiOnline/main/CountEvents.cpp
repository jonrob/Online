/*
 * CountEvents.cpp
 *
 *  Created on: Nov 11, 2016
 *      Author: beat
 */
#include <string>
#include <map>
#include "stdio.h"
#include "string.h"
#include <dirent.h>
#include <vector>
using namespace std;
class RunPar
{
  public:
    int m_runno;
    int m_fillno;
    int m_collbu;
    float m_avmu;
    float m_lumir;
    long m_numlumi[4];
    RunPar(int fillno, int runno, int collbu, float avMu, float lumirate)
    {
      m_runno = runno;
      m_fillno = fillno;
      m_collbu = collbu;
      m_avmu = avMu;
      m_lumir = lumirate;
      m_numlumi[0] = 0;
      m_numlumi[1] = 0;
      m_numlumi[2] = 0;
      m_numlumi[3] = 0;
    }
};
typedef map<int, RunPar*> RunMap;
void setup(string & /* runlist */, string & /*rundb*/, string &/*conditions*/)
{

}
int main(int argc, char **argv)
{
  RunMap rmap;
  unsigned long ODIN_Header = 0x001006100030cbcb;
  unsigned int Event[1000000];
  int MDFHdLen = 0x30;
  string fileprefix =
      "/eos/lhcb/grid/prod/lhcb/freezer/lhcb/data/2016/RAW/LUMI/LHCb/COLLISION16/";
  if (argc >2)
  {
    fileprefix =argv[2];
  }

  string runpars = argv[1];
  FILE *rpfil;
  rpfil = fopen(runpars.c_str(), "r");
  int fillno, runno, collbunch;
  float avMu, lumirate;
  while (1)
  {
    int stat;
    stat = fscanf(rpfil, "%d %d %f %d %f", &fillno, &runno, &avMu, &collbunch,
        &lumirate);
    if (stat == EOF)
    {
      break;
    }
    rmap[runno] = new RunPar(fillno, runno, collbunch, avMu, lumirate);
  }
  fclose(rpfil);
  FILE *f,*f1;
  f1=fopen("LumiEvents.txt","a");
  for (auto run : rmap)
  {
    struct dirent **namelist;
    int n;
    string dirname = (fileprefix + to_string(run.first) + "/");
    n = scandir(dirname.c_str(), &namelist, 0, alphasort);
    if (n < 0)
    {
      char line[128];
      ::snprintf(line,sizeof(line),"scandir %d",run.first);
      perror(line);
      run.second->m_numlumi[0]=-1;
      run.second->m_numlumi[1]=-1;
      run.second->m_numlumi[2]=-1;
      run.second->m_numlumi[3]=-1;
    }
    else
    {
      string filename;
      long nev[4] = {0,0,0,0};
      while (--n)
      {
        if (strcmp(namelist[n]->d_name,"..") ==0)
        {
          free (namelist[n]);
          continue;
        }
        filename = dirname + namelist[n]->d_name;
//        printf("Filename: %s\n",namelist[n]->d_name);
        free(namelist[n]);
        f = fopen(filename.c_str(), "rb");
        //int elen;
        //int cpos = 0;
        //int eend;
        int stat;
        //unsigned long fevpos=0;
        //unsigned long odin_bank[9];
        int rev=0;
        while (1)
        {
          stat = fread(Event, 4, 1, f);
          int ell = Event[0]/4;
          if (stat != 1)
          {
            break;
          }
          rev++;
          //int status;
          int evstart = MDFHdLen/4;
          int evend = ell;
          int bstart = evstart;
          stat = fread(Event+1,Event[0]-4,1,f);
          bool odinfnd = false;
          while (bstart < evend)
          {
            unsigned long bhdr = Event[bstart];
            if (bhdr == ODIN_Header)
            {
              odinfnd=true;
              break;
            }
            int blenb = ((Event[bstart]&0xffff0000)>>16);
            bstart += (blenb %4)==0 ?blenb/4:(blenb/4)+1;//((Event[bstart]&0xffff0000)>>16)/4;
            continue;
          }
          if (odinfnd)
          {
            int bxtype = (Event[bstart+2+8]>>22)&3;
            nev[bxtype]++;
          }
//          cpos+=4;
//          eend = elen;
//          stat = fseek(f,MDFHdLen-4,SEEK_CUR);
//          if (stat != 0)
//          {
//            break;
//          }
//          cpos += MDFHdLen;
//          unsigned long bhdr;
//          bool odinfnd = false;
//          while (cpos <elen)
//          {
//            stat = fread(&bhdr,8,1,f);
//            if (stat != 1)
//            {
//              break;
//            }
//            if (bhdr == ODIN_Header)
//            {
//              odinfnd = true;
//              cpos+=8;
//              stat = fread(odin_bank,sizeof(odin_bank),1,f);
//              break;
//            }
//          }
//          fevpos += elen;
//          nev++;
//          stat = fseek(f, fevpos, SEEK_SET);
//          if (stat != 0)
//          {
//            break;
//          }
        }
        fclose(f);
      }
      printf("Number of events in run %i: %ld %ld %ld %ld\n", run.first, nev[0], nev[1], nev[2], nev[3]);
      run.second->m_numlumi[0]=nev[0];
      run.second->m_numlumi[1]=nev[1];
      run.second->m_numlumi[2]=nev[2];
      run.second->m_numlumi[3]=nev[3];
    }
    fprintf(f1,"%d %d %d %ld %ld %ld %ld %f %f\n",run.second->m_fillno,run.second->m_runno,
        run.second->m_collbu,run.second->m_numlumi[0],run.second->m_numlumi[1],run.second->m_numlumi[2],run.second->m_numlumi[3],run.second->m_avmu,run.second->m_lumir);
    fflush(f1);
  }
//  f=fopen("LumiEvents.txt","w");
//  for (auto run:rmap)
//  {
//    fprintf(f,"%d %d %d %ld %f %f",run.second->m_fillno,run.second->m_runno,
//        run.second->m_collbu,run.second->m_numlumi,run.second->m_avmu,run.second->m_lumir);
//  }
  fclose(f1);
}

