//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_EVENTPROCESSOR_H
#define GAUDIONLINE_EVENTPROCESSOR_H

/// Framework include files
#include <EventHandling/EventHandler.h>
#include <GaudiKernel/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>

/// Online namespace declaration
namespace Online  {
  
  /// Online algorithm to feed the TES with TAE events from the basic raw event.
  /** 
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class EventProcessor : public Gaudi::Algorithm, public EventHandler  {
  protected:
    /// Put bank container to specified location in the TES
    virtual void put_banks(const std::string& loc, evt_data_t&& event)  const  override  final;

    /// Monitoring quantity: Number of events processed
    mutable std::atomic<long>   m_numEventsProcessed;
    /// Monitoring quantity: Number of generic exceptions encountered
    mutable std::atomic<long>   m_numGenericException;
    /// Monitoring quantity: Number of events passing filter criteria
    mutable std::atomic<long>   m_numFilterPassed;
    
  public:
    using Algorithm::Algorithm;

    /// Initialize the algorithm
    virtual StatusCode initialize()   override;

    /// Start the algorithm
    virtual StatusCode start()   override;

    /// Stop the algorithm
    virtual StatusCode stop()   override;

    /// Finalize the algorithm
    virtual StatusCode finalize()   override;

    /// Execute single event
    virtual StatusCode execute(EventContext const& ctxt)  const  override  final;

    /// Execute single event: To be overloaded by sub-classes
    virtual StatusCode process(EventContext const& ctxt)  const  = 0;
  };
}
#endif  // GAUDIONLINE_EVENTPROCESSOR_H
