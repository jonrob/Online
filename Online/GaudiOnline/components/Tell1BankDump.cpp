//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <EventData/bank_types_t.h>
#include <EventData/bank_header_t.h>
#include <Tell1Data/Tell1Decoder.h>

// C/C++ include files
#include <atomic>

/// Forward declarations
namespace LHCb { class RawBank;  }

/// Online namespace declaration
namespace Online  {

/** @class Tell1BankDump Tell1BankDump.cpp
  *  Creates and fills dummy RawEvent
  *
  *  @author Markus Frank
  *  @date   2005-10-13
  */
  class Tell1BankDump : public Gaudi::Algorithm {

    using evt_data_t  = std::vector<std::pair<const bank_header_t*, const void*> >;
    using lhcb_data_t = std::vector<std::pair<const LHCb::RawBank*, const void*> >;

    Gaudi::Property<bool>   m_full{  this, "FullDump",  false, "FullDump:  If true, full bank contents are dumped"};
    Gaudi::Property<bool>   m_dump{  this, "DumpData",  false, "DumpData:  If true, full bank contents are dumped"};
    Gaudi::Property<bool>   m_check{ this, "CheckData", false, "CheckData: If true, full bank contents are checked"};
    Gaudi::Property<int>    m_debug{ this, "Debug",     0,     "Number of events where all dump flags should be considered true"};
    DataObjectReadHandle<lhcb_data_t>  m_rawData{this, "RawData", "DAQ/RawData"};

    /// Event counter
    mutable std::atomic<long> m_numEvent;             ///< Event counter

  public:
    using Algorithm::Algorithm;

    /// Algorithm initialization
    StatusCode start() override  {
      m_numEvent = 0;
      return StatusCode::SUCCESS;
    }

    /// Main execution callback
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      static int evt = 0;
      MsgStream info(msgSvc(),name());
      std::map<int, std::vector<std::pair<const bank_header_t*, const void*> > > banks;
      bool dmp = m_numEvent<m_debug || m_dump;
      bool chk = m_numEvent<m_debug || m_check;
      bool ful = m_numEvent<m_debug || m_full;
      DataObject* pObj = nullptr;
      const evt_data_t* raw = nullptr;      // = (evt_data_t*)m_rawData.get();

      if ( eventSvc()->retrieveObject(m_rawData.objKey(), pObj).isSuccess() )   {
	if ( typeid(*pObj) == typeid(AnyDataWrapper<evt_data_t>) )  {
	  const auto* p = (AnyDataWrapper<evt_data_t>*)pObj;
	  raw = &p->getData();
	}
	else if ( typeid(*pObj) == typeid(AnyDataWrapper<lhcb_data_t>) )  {
	  const auto* p = (AnyDataWrapper<lhcb_data_t>*)pObj;
	  raw = (evt_data_t*)&p->getData();
	}
	else   {
	  throw std::runtime_error("Wrong, unexpected datatype: "+
				   std::string(typeid(*pObj).name())+
				   " from "+m_rawData.objKey());
	}
      }
      else   {
	info << MSG::ERROR << "Failed to access object: "
	     << m_rawData.objKey() << " from TES." << endmsg;
	return StatusCode::FAILURE;
      }
      ++evt;
      info << MSG::INFO;
      for ( const auto& dsc : *raw )   {
	banks[dsc.first->type()].emplace_back(dsc.first, dsc.second);
      }
      for ( const auto& dsc : banks )   {
        const auto& b = dsc.second;
	int btyp = dsc.first;
        int cnt, inc = (btyp == bank_types_t::Rich) ? 64 : 32;
        if ( b.size() > 0 )  {
          if ( dmp )  {
            info << "+----> Event No:" << std::left << std::setw(6) << evt
                 << " has " << std::setw(3) << b.size() << " bank(s) of type " << btyp
                 << " (" << event_print::bankType(btyp) << ") "
		 << " Key: " << m_rawData.objKey() << " "
		 << endmsg;
          }
          int k = 0;
          for(const auto& itB : b)  {
            const bank_header_t* r = itB.first;
            if ( dmp )   {
              info << "+ Bank:  " << event_print::bankHeader(r) << endmsg;
            }
            if( ful ) {
	      bool hdr  = true;
	      auto lines = event_print::bankData(itB.second, r->size(), 10);
	      for( const auto& line : lines )   {
		info << (hdr ? "| Data:  " : "|        ") << line << endmsg;
		hdr = false;
	      }
            }
            if( chk ) { // Check the patterns put in by RawEventCreator
              int kc = k;
              int ks = k+1;
              if ( r->type() != bank_types_t::DAQ )  {
		const int *p, *end;
                if ( r->size() != inc*ks )  {
                  info << "!! Bad bank size:" << r->size() << " expected:" << ks*inc << endmsg;
                }
                if ( r->sourceID() != kc )  {
                  info << "!! Bad source ID:" << r->sourceID() << " expected:" << kc << endmsg;
                }
                for(p=(int*)itB.second, end=p+(r->size()/sizeof(int)), cnt=0; p != end; ++p, ++cnt)  {
                  if ( *p != cnt )  {
                    info << "!! Bad BANK DATA:" << *p << endmsg;
                  }
                }
                if ( cnt != (inc*ks)/int(sizeof(int)) )  {
                  info << "!! Bad amount of data in bank:" << cnt << " word" << endmsg;
                }
              }
            }
          }
        }
      }
      m_numEvent++;
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::Tell1BankDump )
