"""
     Minimal Gaudi task in the online environment

export UPGRADE=/home/frankm/upgrade_sw/Online/Online;

${UPGRADE}/FarmConfig/job/runStandalone.sh -script /group/online/dataflow/scripts/runTestBeam.sh -runinfo ${UPGRADE}/TestBeam/options/OnlineEnvBase.py -taskinfo ${UPGRADE}/TestBeam/options -logfifo /run/fmc/logSrv.fifo -auto -type MEPInit -utgid MEPInit


export UPGRADE=/home/frankm/upgrade_sw/Online/Online;

${UPGRADE}/FarmConfig/job/runStandalone.sh -script /group/online/dataflow/scripts/runTestBeam.sh -runinfo ${UPGRADE}/TestBeam/options/OnlineEnvBase.py -taskinfo ${UPGRADE}/TestBeam/options -logfifo /run/fmc/logSrv.fifo -auto -type FileProd -utgid PROD

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys
import Gaudi.Configuration as Gaudi
import Configurables

#from GaudiKernel.ProcessJobOptions import PrintOff, InstallRootLoggingHandler,logging

#GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))

import GaudiOnline
import OnlineEnvBase as OnlineEnv

from Configurables import Online__FlowManager as FlowManager
application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
application.setup_mbm_access('Input', True)
writer = application.setup_mbm_output('EventOutput')
writer.MBM_maxConsumerWait = 10
application.setup_hive(FlowManager("EventLoop"), 40)
application.setup_algorithms(writer, 0.05)
application.setup_monitoring()
application.monSvc.DimUpdateInterval   = 1
application.config.numEventThreads     = 15
application.config.MBM_numConnections  = 8
application.config.MBM_numEventThreads = 5
application.config.MBM_requests = [
    'EvType=3;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
]
## Speed measurement: short-cut gaudi
##application.input.DeclareData = False
##application.app.TopAlg = [application.input]
#
print('Setup complete....')
#print(dir(application.config))
