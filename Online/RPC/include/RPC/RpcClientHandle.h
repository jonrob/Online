//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_RPCCLIENTHANDLE_H
#define RPC_RPCCLIENTHANDLE_H

#include <memory>
#include <string>
#include <vector>

namespace xmlrpc  {
  class MethodResponse;
  class MethodCall;
  struct traits {
    typedef MethodResponse Response;
    typedef MethodCall Call;
  };
}
namespace jsonrpc  {
  class MethodResponse;
  class MethodCall;
  struct traits {
    typedef MethodResponse Response;
    typedef MethodCall Call;
  };
}

/// Namespace for the http based implementation
namespace rpc  {

  ///  Wrap Client base class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class RpcClientBase  {
  public:
    /// Default constructor
    RpcClientBase() = default;
    /// Copy constructor
    RpcClientBase(const RpcClientBase& copy) = default;
    /// assignment operator
    RpcClientBase& operator=(const RpcClientBase& copy) = default;
    /// Default destructor
    virtual ~RpcClientBase() = default;

    /// Modify debug flag
    virtual int setDebug(int /* new_value */ )  {  return 0; }
    /// Access debug flag
    virtual int debug()  const                  {  return 0; }
    /// Access name
    virtual std::string name()  const           {  return ""; }
    /// Connect client to given URI and execute command (Bridge call)
    virtual std::vector<unsigned char> request(const std::string& request_data)  const   {
      return this->request(request_data.c_str(), request_data.length()); 
    }
    /// Connect client to given URI and execute RPC call
    virtual std::vector<unsigned char> request(const void* request_data, size_t len)  const = 0;
  };

  ///  Wrap Client base class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  template <typename IMP> class RpcClient : public RpcClientBase   {
  public:
    IMP imp;
  public:
    template <class... Args>
      RpcClient(Args&&... args) : imp(std::forward<Args>(args)...)  {
    }
    /// Default constructor
    RpcClient() = default;
    /// Copy constructor
    RpcClient(const RpcClient& copy) = default;
    /// assignment operator
    RpcClient& operator=(const RpcClient& copy) = default;
    /// Default destructor
    virtual ~RpcClient() = default;

    /// Access handler directly
    IMP* operator->()                   {   return &this->imp;   }
    /// Access handler directly
    const IMP* operator->()  const      {   return &this->imp;   }

    /// Modify debug flag
    virtual int setDebug(int value) override;
    /// Access debug flag
    virtual int debug()  const override;
    /// Access name
    virtual std::string name()  const override;
    /// Connect client to given URI and execute command (Bridge call)
    virtual std::vector<unsigned char> request(const std::string& request_data)  const override  {
      return this->request(request_data.c_str(), request_data.length()); 
    }
    /// Connect client to given URI and execute RPC call
    virtual std::vector<unsigned char> request(const void* request_data, size_t len)  const override;
  };

  ///  RPC Client handle class for RPC queries
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  template <typename PROTOCOL>
  class ClientHandle  {
  public:
    typedef std::unique_ptr<RpcClientBase> handler_t;

    handler_t handler;

  public:
    /// NO Default constructor
    ClientHandle() = default;
    /// Initializing constructor
    ClientHandle(handler_t&& cl);
    /// Initializing constructor
    template <typename T> ClientHandle(std::unique_ptr<RpcClient<T> >&& cl) : handler(cl.release()) { }
    /// Move constructor
    ClientHandle(ClientHandle&& copy) = default;
    /// Copy constructor
    ClientHandle(const ClientHandle& copy) = delete;
    /// Assignment move operator
    ClientHandle& operator=(ClientHandle&& copy) = default;
    /// Assignment copy operator
    ClientHandle& operator=(const ClientHandle& copy) = delete;
    /// Default destructor
    virtual ~ClientHandle();

    /// Check validity
    operator bool()  const                   {   return this->handler.get() != nullptr;  }
    /// Access handler directly
    RpcClientBase* get()                     {   return this->handler.get();             }
    /// Access handler directly
    const RpcClientBase* get()  const        {   return this->handler.get();             }
    /// Access handler directly
    RpcClientBase* operator->()              {   return this->handler.operator->();      }
    /// Access handler directly
    const RpcClientBase* operator->()  const {   return this->handler.operator->();      }

    /// Modify debug flag
    int setDebug(int value)                  {   return handler->setDebug(value);        }
    /// Access debug flag
    int debug()  const                       {   return handler->debug();                }
    /// Access name
    std::string name()  const                {   return handler->name();                 }
    /// Reset the handle and release the client
    void reset()                             {   this->handler.reset();                  }
    /// Connect client to given URI and execute RPC call
    typename PROTOCOL::Response call(const typename PROTOCOL::Call& call)  const;
  };

  template <class TRANSPORT,class... Args> inline
    std::unique_ptr<RpcClient<TRANSPORT> > client(Args&&... args)   {
    return std::make_unique<RpcClient<TRANSPORT> >(std::forward<Args>(args)...);
  }

  typedef ClientHandle<xmlrpc::traits>   XmlRpcClientH;
  typedef ClientHandle<jsonrpc::traits>  JsonRpcClientH;

}
#endif  // RPC_RPCCLIENTHANDLE_H
