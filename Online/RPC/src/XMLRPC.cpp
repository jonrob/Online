//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/XMLRPC.h>
#include <XML/DocumentHandler.h>

// C/C++ include files
#include <sstream>
#include <cstring>
#include <stdexcept>
#include <typeinfo>

using namespace std;
using namespace xmlrpc;
using namespace dd4hep::xml;
using p_char = char*;

namespace  {

  static constexpr const char TAG_VOID[]   = "nil";
  static constexpr const char TAG_I4[]     = "i4";
  static constexpr const char TAG_INT[]    = "int";
  static constexpr const char TAG_BOOL[]   = "boolean";
  static constexpr const char TAG_DOUBLE[] = "double";
  static constexpr const char TAG_STRING[] = "string";
  static constexpr const char TAG_ARRAY[]  = "array";
  static constexpr const char TAG_STRUCT[] = "struct";
  static constexpr const char TAG_TIME[]   = "dateTime.iso8601";

  static constexpr const char TAG_DATA[]   = "data";
  static constexpr const char TAG_VALUE[]  = "value";
  
  static constexpr const char DATETIME_FORMAT_0[] = "%Y-%m-%dT%H:%M:%S";
  //const char DATETIME_FORMAT_0[] = "%Y-%m-%dT%H:%M:%SZ";
  static constexpr const char DATETIME_FORMAT_1[] = "%Y%m%dT%H:%M:%S";

  /// Default call XML
  static constexpr const char s_call_xml[]     = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n"
    "<methodCall><methodName/><params/></methodCall>";
  //"<methodCall><!--\n\t XML-RPC interface in C++     M.Frank CERN/LHCb  \n--><methodName/><params/></methodCall>";

  /// Default empty response XML
  static constexpr const char s_response_xml[] = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n"
    "<methodResponse></methodResponse>";
  //    "<methodResponse><!--\n\t XML-RPC interface in C++     M.Frank CERN/LHCb  \n--></methodResponse>";

  /// C++ version: replace all occurrences of a string
  string str_rep(const string& str, const string& pattern, const string& replacement) {
    string res = str;
    for(size_t id=res.find(pattern); id != string::npos; id = res.find(pattern) )
      res.replace(id,pattern.length(),replacement);
    return res;
  }

  string string_encode(const string& s)   {
    string res, alt;
    res.reserve(2*s.length());
    for(char c : s)   {
      switch(c)   {
      case '<':
	res += "&lt;";
	break;
      case '>':
	res += "&gt;";
	break;
      case '\'':
	res += "&apos;";
	break;
      case '"':
	res += "&quot;";
	break;
      case '%':
	res += "&#37;";
	break;
      case '&':
	res += "&amp;";
	break;
      default:
	res += c;
	break;
      }
    }
    return res;
  }
  string string_decode(const string& s)   {
    string res = str_rep(s,"&lt;","<");
    res = str_rep(res,"&gt;",">");
    res = str_rep(res,"&quot;","\"");
    res = str_rep(res,"&apos;","'");
    res = str_rep(res,"&#37;","%");
    res = str_rep(res,"&amp;","&");
    return res;
  }
}


/// Namespace for the AIDA detector description toolkit
namespace dd4hep {
  /// Namespace for the AIDA detector description toolkit supporting XML utilities
  namespace xml {

    string _toString(const Structure& p)   {
      return p.str();
    }
    string _toString(const Array& p)   {
      return p.str();
    }
  }    /* End of namespace XML        */
}      /* End namespace dd4hep        */

bool operator<(const tm& a, const tm& b)   {
  if ( a.tm_year < b.tm_year ) return true;
  if ( a.tm_yday < b.tm_yday ) return true;
  if ( a.tm_hour < b.tm_hour ) return true;
  if ( a.tm_min  < b.tm_min  ) return true;
  if ( a.tm_sec  < b.tm_sec  ) return true;
  return false;
}

bool operator<(const timeval& a, const timeval& b)   {
  if ( a.tv_sec  < b.tv_sec  ) return true;
  if ( a.tv_usec < b.tv_usec ) return true;
  return false;
}


XMLRPC_NS_BEGIN

string _toString(const struct tm& p)   {
  char text[64];
  ::strftime(text,sizeof(text), DATETIME_FORMAT_0, &p);
  return text;
}

struct tm _toTime(const string& s)   {
  struct tm tm;
  if ( ::strptime(s.c_str(), DATETIME_FORMAT_0, &tm) == s.c_str()+::strlen(s.c_str()) )
    return tm;
  else if ( ::strptime(s.c_str(), DATETIME_FORMAT_1, &tm) == s.c_str()+::strlen(s.c_str()) )
    return tm;
  else  {
    // Throw exception ?
  }
  return tm;
}

string _toString(const struct timeval& tv)   {
  struct tm p;
  ::localtime_r(&tv.tv_sec, &p);
  return _toString(p);
}

struct timeval _toTimeVal(const string& s)   {
  struct tm tm = _toTime(s);
  return { ::mktime(&tm), 0 };
}

/// Add a new value to the array
template <typename T> inline xml_h Array::add(const T& /* val */)   {
  throw runtime_error("Array: Invalid XML-RPC datatype:"+string(typeid(T).name()));
}

/// Add a new value to the structure object
template <typename T> inline xml_h Structure::add(const string& /* nam */, const T& /* val */)   {
  throw runtime_error("Structure: Invalid XML-RPC datatype:"+string(typeid(T).name()));
}

template <typename T> inline T MethodCall::data(xml_h /* handle */)   const   {
  throw runtime_error("MethodCall: Invalid XML-RPC datatype:"+string(typeid(T).name()));
}
/// Add argument entry <param> to the call stricture
template <typename T> inline MethodCall& addParam(const string& name, const T& /* value */)   {
  throw runtime_error("MethodCall: Invalid XML-RPC parameter:"+name+": "+string(typeid(T).name()));
}

/// Add a new parameter to the response
template <typename T> inline MethodResponse MethodResponse::make(const T& /* param */)  {
  throw runtime_error("MethodResponse: Invalid XML-RPC response datatype:"+string(typeid(T).name()));
}

/// Access return value data element by true type
template <typename T> inline T MethodResponse::data()   const   {
  throw runtime_error("MethodResponse: Invalid XML-RPC response datatype:"+string(typeid(T).name()));
}

template <> long XmlCoder::to() const   {
  xml_h data = element.child(element.hasChild(_rpcU(int)) ? _rpcU(int) : _rpcU(i4));
  return _toLong(data.rawText());
}
template <> unsigned long  XmlCoder::to() const
{  return (unsigned long)this->to<long>();                                }
template <> int            XmlCoder::to() const
{  return (int)this->to<long>();                                          }
template <> unsigned int   XmlCoder::to() const
{  return (unsigned int)this->to<long>();                                 }
template <> short          XmlCoder::to() const
{  return (short)this->to<long>();                                        }
template <> unsigned short XmlCoder::to() const
{  return (unsigned short)this->to<long>();                               }
template <> char           XmlCoder::to() const
{  return (char)this->to<long>();                                         }
template <> unsigned char  XmlCoder::to() const
{  return (unsigned char)this->to<long>();                                }
template <> bool           XmlCoder::to() const
{  return _toInt(  element.child(_rpcU(boolean)).rawText()) != 0;         }
template <> float          XmlCoder::to() const
{  return _toFloat( element.child(_rpcU(double)).rawText());              }
template <> double         XmlCoder::to() const
{  return _toDouble(element.child(_rpcU(double)).rawText());              }
template <> struct tm      XmlCoder::to() const
{  return _toTime(element.child(_rpcU(dateTime)).rawText());              }
template <> struct timeval XmlCoder::to() const
{  return _toTimeVal(element.child(_rpcU(dateTime)).rawText());           }
template <> Array          XmlCoder::to() const
{  return Array(element.child(_rpcU(array)));                             }
template <> Structure      XmlCoder::to() const
{  return Structure(element.child(_rpcU(struct)));                        }
template <> string         XmlCoder::to() const
{  return _toString(element.child(_rpcU(string)).rawText());              }

template <typename T> static const XmlCoder& conv_xml_coder(const XmlCoder& c, const XmlChar* typ, const T& val)  {
  xml_doc_t doc = xml_elt_t(c.element).document();
  xml_elt_t i(doc, typ), v(doc, _rpcU(value));
  i.text(_toString(val));
  return c;
}

template <> const XmlCoder& XmlCoder::from(const long& val) const
{  return conv_xml_coder(*this, _rpcU(int), val);                         }

template <> const XmlCoder& XmlCoder::from(const unsigned long& val) const
{  return conv_xml_coder(*this, _rpcU(int), val);                         }

template <> const XmlCoder& XmlCoder::from(const int& val) const
{  return conv_xml_coder(*this, _rpcU(i4), val);                          }

template <> const XmlCoder& XmlCoder::from(const unsigned int& val) const
{  return conv_xml_coder(*this, _rpcU(i4), val);                          }

template <> const XmlCoder& XmlCoder::from(const short& val) const
{  return conv_xml_coder(*this, _rpcU(i4), val);                          }

template <> const XmlCoder& XmlCoder::from(const unsigned short& val) const
{  return conv_xml_coder(*this, _rpcU(i4), val);                          }

template <> const XmlCoder& XmlCoder::from(const char& val) const
{  return conv_xml_coder(*this, _rpcU(i4), (int)val);                     }

template <> const XmlCoder& XmlCoder::from(const unsigned char& val) const
{  return conv_xml_coder(*this, _rpcU(i4), (int)val);                     }

template <> const XmlCoder& XmlCoder::from(const float& val) const
{  return conv_xml_coder(*this, _rpcU(double), val);                      }

template <> const XmlCoder& XmlCoder::from(const double& val) const
{  return conv_xml_coder(*this, _rpcU(double), val);                      }

template <> const XmlCoder& XmlCoder::from(const std::string& val) const
{  return conv_xml_coder(*this, _rpcU(string), val);                      }

template <> const XmlCoder& XmlCoder::from(const struct tm& val) const
{  return conv_xml_coder(*this, _rpcU(dateTime), val);                    }

template <> const XmlCoder& XmlCoder::from(const struct timeval& val) const
{  return conv_xml_coder(*this, _rpcU(dateTime), val);                    }

template <typename T> inline void primitive_container_to_xmlrpc(xml_h element, const T& elts)  {
  Array a(element);
  for(const auto& e : elts )
    a.add(e);
}

//---------------------------------------------------------------------------------
/// Add a new parameter to the response
template <> xml_h Array::add<bool>(const bool& p)
{  return addValue(_rpcU(int), p ? "1" : "0");                            }
/// Add a new parameter to the response
template <> xml_h Array::add<char>(const char& p)
{  return addValue(_rpcU(int), _toString(p));                             }
/// Add a new parameter to the response
template <> xml_h Array::add<signed char>(const signed char& p)
{  return addValue(_rpcU(int), _toString(int(p)));                        }
/// Add a new parameter to the response
template <> xml_h Array::add<unsigned char>(const unsigned char& p)
{  return addValue(_rpcU(int), _toString(int(p)));                        }
/// Add a new parameter to the response
template <> xml_h Array::add<short>(const short& p)
{  return addValue(_rpcU(int), _toString(int(p)));                        }
/// Add a new parameter to the response
template <> xml_h Array::add<unsigned short>(const unsigned short& p)
{  return addValue(_rpcU(int), _toString(int(p)));                        }
/// Add a new parameter to the response
template <> xml_h Array::add<int>(const int& p)
{  return addValue(_rpcU(int), _toString(p));                             }
/// Add a new parameter to the response
template <> xml_h Array::add<unsigned int>(const unsigned int& p)
{  return addValue(_rpcU(int), _toString(long(p)));                       }
/// Add a new parameter to the response
template <> xml_h Array::add<long>(const long& p)
{  return addValue(_rpcU(int), _toString(p));                             }
/// Add a new parameter to the response
template <> xml_h Array::add<unsigned long>(const unsigned long& p)
{  return addValue(_rpcU(int), _toString(long(p)));                       }
/// Add a new parameter to the response
template <> xml_h Array::add<double>(const double& p)
{  return addValue(_rpcU(double), _toString(p));                          }
/// Add a new parameter to the response
template <> xml_h Array::add<float>(const float& p)
{  return addValue(_rpcU(double), _toString(p));                          }
/// Add a new parameter to the response
template <> xml_h Array::add<string>(const string& p)
{  return addValue(_rpcU(string), string_encode(p));                      }
/// Add a new parameter to the response
template <> xml_h Array::add<p_char>(const p_char& p)
{  return addValue(_rpcU(string), string_encode(p));                      }
/// Add a new parameter to the response
template <> xml_h Array::add<struct tm>(const tm& p)
{  return addValue(_rpcU(dateTime), _toString(p));                        }
/// Add a new parameter to the response
template <> xml_h Array::add<struct timeval>(const timeval& p)
{  return addValue(_rpcU(dateTime), _toString(p));                        }
/// Add a new parameter to the response
template <> xml_h Array::add<Array>(const Array&)
{  return addValue(_rpcU(array), "");                                     }
/// Add a new parameter to the response
template <> xml_h Array::add<Structure>(const Structure&)
{  return addValue(_rpcU(struct), "");                                    }
/// Add a new parameter to the response
template <> xml_h Array::add<xml_elt_t>(const xml_elt_t& elt)  { 
  if ( elt.tag() == TAG_VALUE )  {
    root.append(elt);
    return elt;
  }
  xml_doc_t doc = xml_elt_t(root).document();
  xml_elt_t v(doc, _rpcU(value));
  root.append(v);
  v.append(elt);
  return elt;
}
/// Add a new parameter to the response
template <> xml_h Array::add<xml_h>(const xml_h& handle)
{  return this->add<xml_elt_t>(xml_elt_t(handle));                         }
/// Add a new parameter to the response
template <> xml_h Array::add<XmlCoder>(const XmlCoder& coder)
{  return add(coder.element);                                              }
    
//---------------------------------------------------------------------------------
/// Add a new parameter to the response
template <> xml_h Structure::add<bool>(const string& nam, const bool& p)
{  return addMember(nam, _rpcU(int), p ? "1" : "0");                       }
/// Add a new parameter to the response
template <> xml_h Structure::add<char>(const string& nam, const char& p)
{  return addMember(nam, _rpcU(int), _toString(p));                        }
/// Add a new parameter to the response
template <> xml_h Structure::add<signed char>(const string& nam, const signed char& p)
{  return addMember(nam, _rpcU(int), _toString(int(p)));                   }
/// Add a new parameter to the response
template <> xml_h Structure::add<unsigned char>(const string& nam, const unsigned char& p)
{  return addMember(nam, _rpcU(int), _toString(int(p)));                   }
/// Add a new parameter to the response
template <> xml_h Structure::add<short>(const string& nam, const short& p)
{  return addMember(nam, _rpcU(int), _toString(int(p)));                   }
/// Add a new parameter to the response
template <> xml_h Structure::add<unsigned short>(const string& nam, const unsigned short& p)
{  return addMember(nam, _rpcU(int), _toString(int(p)));                   }
/// Add a new parameter to the response
template <> xml_h Structure::add<int>(const string& nam, const int& p)
{   return addMember(nam, _rpcU(int), _toString(p));                       }
/// Add a new parameter to the response
template <> xml_h Structure::add<unsigned int>(const string& nam, const unsigned int& p)
{   return addMember(nam, _rpcU(int), _toString(long(p)));                 }
/// Add a new parameter to the response
template <> xml_h Structure::add<long>(const string& nam, const long& p)
{   return addMember(nam, _rpcU(int), _toString(p));                       }
/// Add a new parameter to the response
template <> xml_h Structure::add<unsigned long>(const string& nam, const unsigned long& p)
{   return addMember(nam, _rpcU(int), _toString(p));                       }
/// Add a new parameter to the response
template <> xml_h Structure::add<double>(const string& nam, const double& p)
{   return addMember(nam, _rpcU(double), _toString(p));                    }
/// Add a new parameter to the response
template <> xml_h Structure::add<float>(const string& nam, const float& p)
{   return addMember(nam, _rpcU(double), _toString(p));                    }
/// Add a new parameter to the response
template <> xml_h Structure::add<p_char>(const string& nam, const p_char& p)
{   return addMember(nam, _rpcU(string), p ? string_encode(p) : string()); }
/// Add a new parameter to the response
template <> xml_h Structure::add<string>(const string& nam, const string& p)
{   return addMember(nam, _rpcU(string), p.empty() ? p : string_encode(p));}
/// Add a new parameter to the response
template <> xml_h Structure::add<struct tm>(const string& nam, const struct tm& p)
{  return addMember(nam, _rpcU(dateTime), _toString(p));                   }
/// Add a new parameter to the response
template <> xml_h Structure::add<struct timeval>(const string& nam, const struct timeval& p)
{  return addMember(nam, _rpcU(dateTime), _toString(p));                   }
/// Add a new parameter to the response
template <> xml_h Structure::add<Array>(const string& nam, const Array&)
{   return addMember(nam, _rpcU(array), "");                               }
/// Add a new parameter to the response
template <> xml_h Structure::add<Structure>(const string& nam, const Structure&)
{   return addMember(nam, _rpcU(struct), "");                              }

//---------------------------------------------------------------------------------
/// Access data element by true type
template <> string MethodCall::data<string>(xml_h handle)   const
{   return string_decode(_toString(handle.child(_rpcU(string)).rawText()));}
/// Access data element by true type
template <> bool MethodCall::data<bool>(xml_h handle)   const
{   return handle.child(_rpcU(boolean)).text() != "0";                     }
/// Access data element by true type
template <> char MethodCall::data<char>(xml_h handle)   const
{   return (char)intData(handle);                                          }
/// Access data element by true type
template <> signed char MethodCall::data<signed char>(xml_h handle)   const
{   return (signed char)intData(handle);                                   }
/// Access data element by true type
template <> unsigned char MethodCall::data<unsigned char>(xml_h handle)   const
{   return (unsigned char)intData(handle);                                 }
/// Access data element by true type
template <> short MethodCall::data<short>(xml_h handle)   const
{   return (short)intData(handle);                                         }
/// Access data element by true type
template <> unsigned short MethodCall::data<unsigned short>(xml_h handle)   const
{   return (unsigned short)intData(handle);                                }
/// Access data element by true type
template <> int MethodCall::data<int>(xml_h handle)   const
{   return intData(handle);                                                }
/// Access data element by true type
template <> unsigned int MethodCall::data<unsigned int>(xml_h handle)   const
{   return (unsigned int)intData(handle);                                  }
/// Access data element by true type
template <> long MethodCall::data<long>(xml_h handle)   const
{   return intData(handle);                                                }
/// Access data element by true type
template <> unsigned long MethodCall::data<unsigned long>(xml_h handle)   const
{   return (unsigned long)intData(handle);                                 }
/// Access data element by true type
template <> double MethodCall::data<double>(xml_h handle)   const
{   return _toDouble(handle.child(_rpcU(double)).rawText());               }
/// Access data element by true type
template <> float MethodCall::data<float>(xml_h handle)   const
{   return _toFloat(handle.child(_rpcU(double)).rawText());                }
/// Access data element by true type
template <> struct tm MethodCall::data<struct tm>(xml_h handle)   const
{   return _toTime(handle.child(_rpcU(dateTime)).rawText());               }
/// Access data element by true type
template <> struct timeval MethodCall::data<struct timeval>(xml_h handle)   const
{   return _toTimeVal(handle.child(_rpcU(dateTime)).rawText());            }

/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<bool>(const bool& p)  const
{    return addParam(_rpcU(int), p ? "1" : "0");                           }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<char>(const char& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<signed char>(const signed char& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned char>(const unsigned char& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<short>(const short& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned short>(const unsigned short& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<int>(const int& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned int>(const unsigned int& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<long>(const long& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned long>(const unsigned long& p)  const
{    return addParam(_rpcU(int), _toString(long(p)));                      }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<double>(const double& p)  const
{    return addParam(_rpcU(double), _toString(p));                         }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<float>(const float& p)  const
{    return addParam(_rpcU(double), _toString(p));                         }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<struct tm>(const struct tm& p)  const
{    return addParam(_rpcU(dateTime), _toString(p));                       }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<struct timeval>(const struct timeval& p)  const
{    return addParam(_rpcU(dateTime), _toString(p));                       }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<string>(const string& p)  const
{    return addParam(_rpcU(string), string_encode(p));                     }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<p_char>(const p_char& p)  const
{    return addParam(_rpcU(string), string_encode(p ? p : ""));            }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<Structure>(const Structure&)  const
{    return addParam(_rpcU(struct), "");                                   }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<Array>(const Array&)  const
{    return addParam(_rpcU(array), "");                                    }

//---------------------------------------------------------------------------------
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<bool>(const bool& p)
{    return makeValue(_rpcU(boolean), p ? "1" : "0");                      }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<char>(const char& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<signed char>(const signed char& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned char>(const unsigned char& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<short>(const short& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned short>(const unsigned short& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<int>(const int& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned int>(const unsigned int& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<long>(const long& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned long>(const unsigned long& p)
{    return makeValue(_rpcU(int), _toString(long(p)));                     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<double>(const double& p)
{    return makeValue(_rpcU(double), _toString(p));                        }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<float>(const float& p)
{    return makeValue(_rpcU(double), _toString(p));                        }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<string>(const string& p)
{    return makeValue(_rpcU(string), string_decode(p));                    }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<struct tm>(const struct tm& p)
{    return makeValue(_rpcU(dateTime), _toString(p));                      }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<struct timeval>(const struct timeval& p)
{    return makeValue(_rpcU(dateTime), _toString(p));                      }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<p_char>(const p_char& p)
{    return makeValue(_rpcU(string), string_decode(p ? p : ""));           }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<Structure>(const Structure&)
{    return makeValue(_rpcU(struct), "");                                  }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<Array>(const Array&)
{    return makeValue(_rpcU(array), "");                                   }


/// Access data element by true type
template <> string MethodResponse::data<string>()   const
{   return string_decode(checked_handle().child(_rpcU(string)).text());    }
/// Access data element by true type
template <> bool MethodResponse::data<bool>()   const
{   return checked_handle().child(_rpcU(boolean)).text() != "0";           }
/// Access data element by true type
template <> char MethodResponse::data<char>()   const
{   return (char)intData();                                                }
/// Access data element by true type
template <> signed char MethodResponse::data<signed char>()   const
{   return (signed char)intData();                                         }
/// Access data element by true type
template <> unsigned char MethodResponse::data<unsigned char>()   const
{   return (unsigned char)intData();                                       }
/// Access data element by true type
template <> short MethodResponse::data<short>()   const
{   return (short)intData();                                               }
/// Access data element by true type
template <> unsigned short MethodResponse::data<unsigned short>()   const
{   return (unsigned short)intData();                                      }
/// Access data element by true type
template <> int MethodResponse::data<int>()   const
{   return intData();                                                      }
/// Access data element by true type
template <> unsigned int MethodResponse::data<unsigned int>()   const
{   return (unsigned int)intData();                                        }
/// Access data element by true type
template <> long MethodResponse::data<long>()   const
{   return intData();                                                      }
/// Access data element by true type
template <> unsigned long MethodResponse::data<unsigned long>()   const
{   return (unsigned long)intData();                                       }
/// Access data element by true type
template <> double MethodResponse::data<double>()   const
{   return _toDouble(checked_handle().child(_rpcU(double)).rawText());     }
/// Access data element by true type
template <> float MethodResponse::data<float>()   const
{   return _toFloat(checked_handle().child(_rpcU(double)).rawText());      }
/// Access data element by true type
template <> struct tm MethodResponse::data<struct tm>()   const
{   return _toTime(checked_handle().child(_rpcU(dateTime)).rawText());     }
/// Access data element by true type
template <> struct timeval MethodResponse::data<struct timeval>()   const
{   return _toTimeVal(checked_handle().child(_rpcU(dateTime)).rawText());  }
/// Access data element by true type
template <> Array MethodResponse::data<Array>()   const
{   return Array(checked_handle().child(_rpcU(array)));                    }
/// Access data element by true type
template <> Structure MethodResponse::data<Structure>()   const
{   return Structure(checked_handle().child(_rpcU(array)));                }

XMLRPC_NS_END

/// Initializing constructor
XmlCoder::XmlCoder(xml_h a) : element(a)  {
}

/// Initializing constructor
Arg::Arg(xml_h a) : XmlCoder(a)  {
  element = a.child(_rpcU(value));
}

/// Helper: Get integer value from XML handle. Extracts <value><boolean>...</boolean></value>
bool Arg::get_bool(xml_h handle)    {
  return Arg(handle).to<bool>();
}

/// Helper: Get integer value from XML handle. Extracts <value><int>...</int></value>
long Arg::get_int(xml_h handle)    {
  return Arg(handle).to<long>();
}

/// Helper: Get double value from XML handle. Extracts <value><double>...</double></value>
double Arg::get_double(xml_h handle)    {
  return Arg(handle).to<double>();
}

/// Helper: Get double value from XML handle. Extracts <value><string>...</string></value>
string Arg::get_string(xml_h handle)    {
  return string_decode(Arg(handle).to<string>());
}

/// Helper: Get double value from XML handle. Extracts <value><string>...</string></value>
struct tm Arg::get_time(xml_h handle)    {
  return Arg(handle).to<struct tm>();
}

/// Helper: Get double value from XML handle. Extracts <value><string>...</string></value>
struct timeval Arg::get_timeval(xml_h handle)    {
  return Arg(handle).to<struct timeval>();
}

/// Initializing constructor
Array::Array(xml_h handle)   {
  xml_doc_t doc = xml_elt_t(handle).document();
  std::string tag = handle.tag();
  if ( tag == "data" )   {
    root = handle;
  }
  else if ( tag == TAG_VALUE )  {
    auto arr = handle.child(_rpcU(array));
    root = arr.child(_rpcU(data));
  }
  else if ( tag == TAG_ARRAY )  {
    if ( handle.hasChild(_rpcU(data)) )
      root = handle.child(_rpcU(data));
    else
      handle.append(root = xml_elt_t(doc, _rpcU(data)));
  }
  else  {
    throw std::runtime_error("Invalid xmlrpc::Array constructor: bad parent type");
  }
}

/// Default destructor
Array::~Array()  {
}

/// Retrieve string representation of the object
string Array::str() const   {
  stringstream out;
  dump_tree(root,out);
  return out.str();
}

/// Access to the array's root element
xml_h Array::array()  const   {
  return xml_elt_t(root).parent();
}

/// Add a new value to the array
xml_h Array::addValue(const Unicode& typ, const string& val)   {
  xml_elt_t value(xml_elt_t(root).document(), typ);
  value.text(val);
  return add(value);
}

/// Collect elements
std::vector<xml_h> Array::elements()  const    {
  std::vector<xml_h> ret;
  for(xml_coll_t c(root,_rpcU(value)); c; ++c)
    ret.emplace_back(c);
  return ret;
}

/// Initializing constructor
Structure::Structure(xml_h r) : root(r)  {
}

/// Default destructor
Structure::~Structure()  {
}

/// Retrieve string representation of the object
string Structure::str() const   {
  stringstream out;
  dump_tree(root,out);
  return out.str();
}

/// Access to the array's root element
xml_h Structure::structure()  const   {
  return xml_elt_t(root).parent();
}

/// Add a new value to the array
xml_h Structure::addMember(const string& nam, const Unicode& typ, const string& val)   {
  xml_doc_t doc = xml_elt_t(root).document();
  xml_elt_t m(doc, _rpcU(member)), n(doc, _U(name)), v(doc, _rpcU(value)), i(doc, typ);
  root.append(m);
  m.append(n);
  n.text(nam);
  m.append(v);
  v.append(i);
  i.text(val);
  return i;
}

/// Retrieve string representation of the object
string Field::str() const   {
  stringstream out;
  dump_tree(*this,out);
  return out.str();
}

/// Access xml data type in string form
std::string Field::type() const   {
  for(xml_coll_t c(*this,_U(star)); c; ++c)
    return c.tag();
  return "nil";
}

/// Access xml data type as closest C++ data type
const std::type_info& Field::type_info()  const   {
  for(xml_coll_t c(*this,_U(star)); c; ++c)   {
    std::string tag = c.tag();
    if ( tag == TAG_VOID )
      return typeid(void);
    if ( tag == TAG_I4 )
      return typeid(int);
    if ( tag == TAG_INT )
      return typeid(long);
    if ( tag == TAG_DOUBLE )
      return typeid(double);
    if ( tag == TAG_BOOL )
      return typeid(bool);
    if ( tag == TAG_STRING )
      return typeid(std::string);
    if ( tag == TAG_ARRAY )
      return typeid(Array);
    if ( tag == TAG_STRUCT )
      return typeid(Structure);
    if ( tag == TAG_TIME )
      return typeid(struct tm);
  }
  return typeid(void);
}

/// Access data element containing data
static xml_h field_value_element(xml_h e)    {
  return e.tag() == TAG_VALUE ? e : e.child(_rpcU(value));
}

/// Access interpreted field data 
template <typename T> T Field::get()  const   {  throw "error";            }
/// Specialized functions to interprete data fields
template <> short Field::get<short>()  const
{  return XmlCoder(field_value_element(m_element)).to<int>();              }
template <> unsigned short Field::get<unsigned short>()  const
{  return XmlCoder(field_value_element(m_element)).to<int>();              }
template <> int Field::get<int>()  const
{  return XmlCoder(field_value_element(m_element)).to<int>();              }
template <> unsigned int Field::get<unsigned int>()  const
{  return XmlCoder(field_value_element(m_element)).to<int>();              }
template <> long Field::get<long>()  const
{  return XmlCoder(field_value_element(m_element)).to<int>();              }
template <> unsigned long Field::get<unsigned long>()  const
{  return XmlCoder(field_value_element(m_element)).to<int>();              }
template <> float Field::get<float>()  const
{  return XmlCoder(field_value_element(m_element)).to<double>();           }
template <> double Field::get<double>()  const
{  return XmlCoder(field_value_element(m_element)).to<double>();           }
template <> struct tm Field::get<struct tm>()  const
{  return XmlCoder(field_value_element(m_element)).to<struct tm>();        }
template <> struct timeval Field::get<struct timeval>()  const
{  return XmlCoder(field_value_element(m_element)).to<struct timeval>();   }
template <> Array Field::get<Array>()  const
{  return XmlCoder(field_value_element(m_element)).to<Array>();            }
template <> Structure Field::get<Structure>()  const
{  return XmlCoder(field_value_element(m_element)).to<Structure>();        }
template <> xml_h Field::get<xml_h>()  const
{  return field_value_element(m_element);                                  }
template <> xml_elt_t Field::get<xml_elt_t>()  const
{  return { field_value_element(m_element) };                              }
template <> std::string Field::get<std::string>()  const  {
  xml_h e = field_value_element(m_element); 
  if ( e.hasChild(_rpcU(string)) )
    return XmlCoder(e).to<std::string>();
  xml_elt_t elt = (this->tag() == TAG_DATA) ? parent() : e;
  for(xml_coll_t c(elt,_U(star)); c; ++c)   {
    std::stringstream str;
    dump_tree(c, str);
    return str.str();
  }
  return "<nil/>";
}

template <> std::string Field::type_name<short>()
{  return TAG_INT;                                                         }
template <> std::string Field::type_name<unsigned short>()
{  return TAG_INT;                                                         }
template <> std::string Field::type_name<int>()
{  return TAG_INT;                                                         }
template <> std::string Field::type_name<unsigned int>()
{  return TAG_INT;                                                         }
template <> std::string Field::type_name<long>()
{  return TAG_INT;                                                         }
template <> std::string Field::type_name<unsigned long>()
{  return TAG_INT;                                                         }
template <> std::string Field::type_name<float>()
{  return TAG_DOUBLE;                                                      }
template <> std::string Field::type_name<double>()
{  return TAG_DOUBLE;                                                      }
template <> std::string Field::type_name<struct tm>()
{  return TAG_TIME;                                                        }
template <> std::string Field::type_name<struct timeval>()
{  return TAG_TIME;                                                        }
template <> std::string Field::type_name<Array>()
{  return TAG_ARRAY;                                                       }
template <> std::string Field::type_name<Structure>()
{  return TAG_STRUCT;                                                      }
template <> std::string Field::type_name<std::string>()
{  return TAG_STRING;                                                      }


/// Default constructor
Tuple::Tuple() : xml_doc_holder_t(nullptr)
{
}

/// Initializing constructor
Tuple::Tuple(xml_h handle)
  : xml_doc_holder_t(nullptr), root(handle)
{
  auto elts = this->elements();
  fields.reserve(elts.size());
  for( xml_h e : elts )
    fields.emplace_back(Field(e));
}

/// Move constructor
Tuple::Tuple(Tuple&& tuple)
  : xml_doc_holder_t(tuple.m_doc), root(tuple.root)
{
  this->fields = std::move(tuple.fields);
  tuple.m_doc = nullptr;
}

/// Move constructor
Tuple& Tuple::operator=(Tuple&& tuple)  {
  this->m_doc  = tuple.m_doc;
  this->root   = tuple.root;
  this->fields = std::move(tuple.fields);
  tuple.m_doc  = nullptr;
  return *this;
}

/// Move document
void Tuple::set_document(xml_doc_holder_t& doc_holder)   {
  if ( m_doc )    {
    this->assign(nullptr);
  }
  this->assign(doc_holder.m_doc);
  doc_holder.m_doc = nullptr;
}

/// Retrieve string representation of the object
string Tuple::str() const   {
  stringstream out;
  dump_tree(this->root, out);
  return out.str();
}

/// Check if the tuple is an int
template <> bool Tuple::is<int>()  const    {
  return this->root ? this->root.child(_rpcU(int)).ptr() != nullptr : false;
}
/// Check if the tuple is an array
template <> bool Tuple::is<Array>()  const    {
  return this->root ? this->root.child(_rpcU(array)).ptr() != nullptr : false;
}
/// Check if the tuple is a structure
template <> bool Tuple::is<Structure>()  const    {
  return this->root ? this->root.tag() == TAG_STRUCT : false;
}

/// Check if the tuple is an int
template <> int Tuple::get<int>()  const    {
  return Field(this->root).get<int>();
}
/// Check if the tuple is an array
template <> Array Tuple::get<Array>()  const    {
  return Field(this->root).get<Array>();
}
/// Check if the tuple is a structure
template <> Structure Tuple::get<Structure>()  const    {
  return Field(this->root).get<Structure>();
}

/// Collect elements
std::vector<xml_h> Tuple::elements()  const    {
  std::vector<xml_h> ret;
  if ( root.ptr() )   {
    if ( root.tag() == TAG_VALUE )   {
      ret.emplace_back(root);
      return ret;
    }
    for(xml_coll_t c(root,_rpcU(value)); c; ++c)
      ret.emplace_back(c);
  }
  return ret;
}

/// Bind Tuple to string map for names item access
void Tuple::bind(std::map<std::string, xml_h>& binding)  const   {
  if ( fields.size() != binding.size() )   {
    std::size_t i = 0;
    for( auto& b : binding )  {
      b.second = fields[i];
      ++i;
    }
    return;
  }
  std::stringstream err;
  err << "xmlrpc: Tuple::bind: Dimension of fields (" << fields.size()
      << ") does not match number of bindings (" << binding.size() << ")";
  throw std::runtime_error(err.str());
}


/// Initializing constructor
MethodCall::MethodCall()
  : Document(DocumentHandler().parse(s_call_xml,sizeof(s_call_xml)))
{
  params = xml_elt_t(root().child(_rpcU(params)));
}

/// Move constructor
MethodCall::MethodCall(MethodCall&& copy)
  : Document(copy), name(std::move(copy.name)), params(copy.params)
{
  copy.params = 0;
  m_doc = copy.m_doc;
  copy.m_doc = 0;
}

/// Initializing constructor
MethodCall::MethodCall(const char* xml)
  : Document(DocumentHandler().parse(xml,::strlen(xml)+1))
{
  check();
}

/// Initializing constructor
MethodCall::MethodCall(const string& xml)
  : Document(DocumentHandler().parse(xml.c_str(),xml.length()))
{
  check();
}

/// Server interface: Initializing constructor
MethodCall::MethodCall(const void* xml, size_t num_bytes)
  : Document(DocumentHandler().parse((char*)xml, num_bytes))
{
  check();
}

/// Default destructor
MethodCall::~MethodCall()   {
  if ( m_doc )  {
    xml_doc_holder_t holder(*this);
    holder.assign(0);
  }
}

/// Move constructor
MethodCall& MethodCall::operator = (MethodCall&& copy)
{
  name   = std::move(copy.name);
  params = copy.params;
  m_doc  = copy.m_doc;
  copy.m_doc  = 0;
  copy.params = 0;
  return *this;
}

/// Check validity of passed document
void MethodCall::check()      {
  if ( root() )   {
    if ( root().tag() == "methodCall" )  {
      xml_h methodName = root().child(_rpcU(methodName));
      params = xml_elt_t(root().child(_rpcU(params)));
      name = methodName.text();
      return;
    }
    xml_doc_holder_t(*this).assign(0);
    throw runtime_error("Invalid XML-RPC document root tag:"+root().tag());
  }
  throw runtime_error("Invalid XML-RPC document [No ROOT handle]");
}

/// Retrieve string representation of the object
string MethodCall::str() const   {
  stringstream out;
  dump_tree(root(),out);
  return out.str();
}

/// Access the <methodName> field of the call
string MethodCall::method() const    {
  return root().child(_rpcU(methodName)).text();
}

/// Add <methodName> tag to the call stricture
const MethodCall& MethodCall::setMethod(const string& nam)  const {
  root().child(_rpcU(methodName)).setText(nam);
  return *this;
}

/// Number of arguments supplied
vector<xml_h> MethodCall::arguments() const   {
  vector<xml_h> args;
  for(xml_coll_t c(root().child(_rpcU(params)), _rpcU(param)); c; ++c)
    args.push_back(xml_h(c));
  return args;
}

/// Convert anything integer like
long MethodCall::intData(xml_h handle)  const   {
  if ( handle.hasChild(_rpcU(int)) )
    return _toLong(handle.child(_rpcU(int)).rawText());
  return _toLong(handle.child(_rpcU(i4)).rawText());  
}

/// Access parameter data value as string
string MethodCall::text(xml_h handle)  const   {
  for(xml_coll_t c(handle,_U(star)); c; ++c )
    return c.text();
  return "";
}

/// Add a new parameter to the rpc method call
const MethodCall& MethodCall::addNil()  const   {
  createParam(_rpcU(nil));
  return *this;
}

/// Add a new value to the response structure
const MethodCall& MethodCall::addParam(const Unicode& tag, const string& val)  const {
  createParam(tag).text(val);
  return *this;
}

/// Add a new value to the response structure
xml_elt_t MethodCall::createParam(const Unicode& tag)  const {
  xml_elt_t p{0}, v{0}, t{0};
  //Document doc = xml_elt_t(params).document();
  params.append(p = xml_elt_t(*this, _rpcU(param)));
  p.append(v = xml_elt_t(*this, _rpcU(value)));
  v.append(t = xml_elt_t(*this, tag));
  return t;
}

/// Initializing constructor
MethodResponse::MethodResponse() : xml_doc_holder_t(0)
{
}

/// Initializing constructor from existing XML document
MethodResponse::MethodResponse(const string& xml)
  : xml_doc_holder_t(DocumentHandler().parse(xml.c_str(),xml.length()))
{
  check();
}

/// Initializing constructor from existing XML document
MethodResponse::MethodResponse(const void* xml, size_t bytes)
  : xml_doc_holder_t(DocumentHandler().parse((const char*)xml, bytes))
{
  check();
}

/// Move constructor
MethodResponse::MethodResponse(MethodResponse&& copy)
  : xml_doc_holder_t(0)
{
  value = copy.value;
  copy.value = 0;
  m_doc = copy.m_doc;
  copy.m_doc = 0;
}

/// Default destructor
MethodResponse::~MethodResponse()   {
}

/// Check validity of passed document
void MethodResponse::check()  {
  if ( root() )  {
    if ( root().hasChild(_rpcU(fault)) )   {
      xml_h param = root().child(_rpcU(fault));
      value = param.child(_rpcU(value)).child(_rpcU(struct));
      return;
    }
    else if ( root().hasChild(_rpcU(params)) )   {
      xml_h param = root().child(_rpcU(params)).child(_rpcU(param));
      xml_h v = param.child(_rpcU(value));
      for(xml_coll_t type(v,_U(star)); type; ++type)   {
	value = type;
	return;
      }
    }
    throw runtime_error("Invalid XML-RPC response [no well formed <faults> or <params>]");
  }
  throw runtime_error("Invalid XML-RPC document [No ROOT handle]");
}

/// Safe access: Check response value before using it.
xml_elt_t MethodResponse::checked_value()  const  {
  if ( value.ptr() ) return value;
  throw runtime_error("MethodResponse: Attempt to access invalid XML value-handle.");
}

/// Safe access: Check response value before using it.
xml_elt_t MethodResponse::checked_handle()  const  {
  if ( value.ptr() ) return value.parent();
  throw runtime_error("MethodResponse: Attempt to access invalid XML handle.");
}

/// Move assignment operation
MethodResponse& MethodResponse::operator=(MethodResponse&& copy)  {
  value = copy.value;
  copy.value = 0;
  m_doc = copy.m_doc;
  copy.m_doc = 0;
  return *this;
}

/// Check if the response structure contains fault information
bool MethodResponse::isFault()  const  {
  return !this->ptr() || this->root().hasChild(_rpcU(fault));
}

/// Access the faults strcuture from the response
Structure MethodResponse::fault()  const  {
  if ( isFault() )  
    return Structure(value);
  throw runtime_error("Invalid XML-RPC response [no <faults> element found]");
}

/// Access the fault code (requires a fault element in the response)
int MethodResponse::faultCode()  const   {
  xml_h struc = checked_value();
  for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
    string n = c.child(_rpcU(name)).text();
    if ( n == "faultCode" ) return Arg::get_int(c);
  }
  return 0; // Cannot really throw exception here if analysing already an error
}

/// Access the fault message string (requires a fault element in the response)
string MethodResponse::faultString()  const   {
  xml_h struc = checked_value();
  for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
    string n = c.child(_rpcU(name)).text();
    if ( n == "faultString" ) return Arg::get_string(c);
  }
  return "Invalid value"; // Cannot really throw exception here if analysing already an error
}

/// Check if the response structure contains <params>
bool MethodResponse::isOK()  const  {
  return this->ptr() && this->root().hasChild(_rpcU(params));
}

/// Retrieve string representation of the object
string MethodResponse::str() const   {
  stringstream out;
  dump_tree(root(),out);
  return out.str();
}

/// Convert anything integer like
long MethodResponse::intData()  const   {
  xml_elt_t h = checked_handle();
  xml_h e = h.child(_rpcU(int), false);
  if ( e )  {
    return _toLong(e.rawText());
  }
  return _toLong(h.child(_rpcU(i4)).rawText());  
}

/// Set fault information in case of failure. Message is strerror(code).
MethodResponse MethodResponse::makeFault(int code)   {
  return makeFault(code, std::error_condition(code,std::system_category()).message());
}

/// Set fault information in case of failure
MethodResponse MethodResponse::makeFault(int code, const string& reason)   {
  MethodResponse doc;
  xml_h fault, val, structure, item, member, cod;
  doc.assign(DocumentHandler().parse(s_response_xml,sizeof(s_response_xml)));
  doc.root().append(fault=        xml_elt_t(doc, _rpcU(fault)));
  fault.append(     val=          xml_elt_t(doc, _rpcU(value)));
  val.append(       structure=    xml_elt_t(doc, _rpcU(struct)));

  // Add fault code
  structure.append( member=       xml_elt_t(doc, _rpcU(member)));
  member.append(    item=         xml_elt_t(doc, _rpcU(name)));
  item.setText(     "faultCode");
  member.append(    val=          xml_elt_t(doc, _rpcU(value)));
  val.append(       cod=          xml_elt_t(doc, _rpcU(int)));
  cod.setText(     _toString(code));

  // Add fault string
  structure.append( member=       xml_elt_t(doc, _rpcU(member)));
  member.append(    item=         xml_elt_t(doc, _rpcU(name)));
  item.setText(     "faultString");
  member.append(    val=          xml_elt_t(doc, _rpcU(value)));
  val.append(       cod=          xml_elt_t(doc, _rpcU(string)));
  cod.setText(      reason);
  return doc;
}

/// Add a value to the response structure
MethodResponse MethodResponse::make()  {
  //MethodResponse doc;
  //doc.assign(DocumentHandler().parse(s_response_xml,sizeof(s_response_xml)));
  //doc.root().append(xml_elt_t(doc, _rpcU(params)));
  //return doc;
  return MethodResponse::make(int(0));
}

/// Add a value to the response structure
MethodResponse MethodResponse::makeValue(const Unicode& tag, const string& val)  {
  MethodResponse doc;
  xml_elt_t params(0), p(0), v(0);

  doc.assign(DocumentHandler().parse(s_response_xml,sizeof(s_response_xml)));
  doc.root().append(params = xml_elt_t(doc, _rpcU(params)));
  params.append(p = xml_elt_t(doc, _rpcU(param)));
  p.append(     v = xml_elt_t(doc, _rpcU(value)));
  v.append(     doc.value = xml_elt_t(doc, tag));
  doc.value.setText(val);
  return doc;
}

/// Add a value to the response structure
MethodResponse MethodResponse::makeValue(const Unicode& tag)  {
  MethodResponse doc;
  xml_elt_t params(0), p(0), v(0);

  doc.assign(DocumentHandler().parse(s_response_xml,sizeof(s_response_xml)));
  doc.root().append(params = xml_elt_t(doc, _rpcU(params)));
  params.append(p = xml_elt_t(doc, _rpcU(param)));
  p.append(     v = xml_elt_t(doc, _rpcU(value)));
  v.append(     doc.value = xml_elt_t(doc, tag));
  return doc;
}

/// Decode the response of a server
MethodResponse MethodResponse::decode(const string& xml)   {
  return MethodResponse(xml);
}

/// Decode the response of a server
MethodResponse MethodResponse::decode(const std::vector<unsigned char>& xml)   {
  return decode(&xml[0], xml.size());
}

/// Decode the response of a server
MethodResponse MethodResponse::decode(const void* ptr, size_t num_bytes)   {
  return MethodResponse(ptr, num_bytes);
}

/// Access return value data element as string and type
pair<string,xml_h> MethodResponse::return_value()   const    {
  for(xml_coll_t c(value,_U(star)); c; ++c )
    return make_pair(c.tag(),c);
  return make_pair("",xml_h(0));
}

/// Copy constructor
CallSequence::CallSequence(const CallSequence& c) : callbacks(c.callbacks) {
}

/// Execution overload for callbacks with no arguments
void CallSequence::operator()(const MethodCall& c) const {
  if (!callbacks.empty()) {
    for (Calls::const_iterator i = callbacks.begin(); i != callbacks.end(); ++i)
      (*i).execute(c);
  }
}

/// Check the compatibility of two typed objects. The test is the result of a dynamic_cast
void CallSequence::checkTypes(const type_info& typ1, const type_info& typ2, void* test) {
  if (!test) {
    string msg = "The types ";
    msg += typ1.name();
    msg += " and ";
    msg += typ2.name();
    msg += " are unrelated. Cannot install a callback for these 2 types.";
    throw runtime_error(msg);
  }
}

#include <vector>
#include <list>
#include <set>

using tuple_set_t        = std::vector<Tuple>;
using tuple_set_result_t = std::pair<long, tuple_set_t>;

template <> Tuple XmlCoder::to() const {
  Tuple tuple(element.child(_rpcU(array)));
  return tuple;
}
template <> const XmlCoder& XmlCoder::from(const Tuple& val) const {
  Array(val.root); // Only check if xml syntax is correct
  return *this;
}
#if 0
template <>  tuple_set_t XmlCoder::to() const {
  tuple_set_t val;
  // element is of type <value>. Need to get to the <array> to decode
  auto elts  = Array(element.child(_rpcU(array))).elements();
  val.reserve(elts.size());
  for( xml_h e : elts )
    val.emplace_back(XmlCoder(e).to<Tuple>());
  return val;
}

template <> const XmlCoder& XmlCoder::from(const tuple_set_t& val) const {
  Array array(element);
  for(const auto& v : val)
    array.add(XmlCoder().from<Tuple>(v));
  return *this;
}
#endif

#define XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER_CONVERSION(container,type)	XMLRPC_NS_BEGIN	\
  template <> const XmlCoder& XmlCoder::from(const container<type>& elts)   const { primitive_container_to_xmlrpc(element,elts); return *this;  } \
  template <> container<type> XmlCoder::to() const { return container_from_xmlrpc<container<type> >(element); } \
  XMLRPC_NS_END								\
  XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,container<type>)

#define XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(type)  \
  XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER_CONVERSION(vector,type) \
  XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER_CONVERSION(list,type)   \
  XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER_CONVERSION(set,type)

XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(bool)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(char)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(unsigned char)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(short)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(unsigned short)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(int)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(unsigned int)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(long)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(unsigned long)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(float)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(double)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(string)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(struct tm)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(struct timeval)
XMLRPC_IMPLEMENT_PRIMITIVE_CONTAINER(Tuple)
