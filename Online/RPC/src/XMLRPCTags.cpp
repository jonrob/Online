//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <XML/XMLTags.h>

/// Namespace for the AIDA detector description toolkit
namespace dd4hep {
  /// Namespace for the AIDA detector description toolkit supporting XML utilities 
  namespace xml {    
    /// Namespace of conditions unicode tags
    namespace cxxrpc  {
      extern const ::dd4hep::xml::Tag_t Unicode_dateTime("dateTime.iso8601");
    }
  }
}

// Define unicode tags
#define UNICODE(x) extern const ::dd4hep::xml::Tag_t Unicode_##x ( #x )
#include "RPC/XMLRPCTags.h"
