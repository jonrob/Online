//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_ONLINEALGIN_FITTERFCN_H_
#define ONLINE_ONLINEALGIN_FITTERFCN_H_

/// Framework include files
#include <GaudiKernel/AlgTool.h>
#include <OnlineAlign/IAlignUser.h>

/// Online namespace declaration
namespace Online   {

  class FitterFcn : public extends<AlgTool, IAlignFcn>  {

  protected:
    std::string          m_dataFileName;
    std::string          m_paramFileName;
    std::string          m_partitionName;
    std::vector<double>  m_dat_x;
    std::vector<double>  m_dat_y;
    std::vector<double>  m_dat_dy;
    std::vector<double>  m_params;
    SmartIF<IMonitorSvc> m_monitorSvc;
    double               m_result   { 0e0 };

  public:
    /// Tool constructor
    FitterFcn(const std::string& type, const std::string& name, const IInterface* parent);
    /// Default destructor
    virtual ~FitterFcn();
    /// Algtool override: initialize the tool
    virtual StatusCode initialize() override;
    /// Algtool override: finalize the tool
    virtual StatusCode finalize() override;

    /// IAlignFcn overrides
    virtual void publishResult(long reference) override;
    virtual StatusCode run()  override;

    /// Implementation
    virtual void analyze();
    virtual void readParams();
    virtual void setPartitionName(const std::string& partition);
  };
}      // End namespace Online
#endif /* ONLINE_ONLINEALGIN_FITTERFCN_H_ */
