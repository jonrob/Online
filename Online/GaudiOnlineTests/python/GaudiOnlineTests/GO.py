#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
"""
     Gaudi online tests

     \author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from GaudiOnline.OnlineApplication import *

def setup_app_basic(self):
  self.config.numEventThreads  = 1
  self.config.numStatusThreads = 0
  self.config.events_HighMark  = 0
  self.config.events_LowMark   = 0
  self.config.logDeviceType    = 'RTL::Logger::LogDevice'
  self.config.logDeviceFormat  = '%-9LEVEL%-24SOURCE'
  self.config.expandTAE        = True
  self.config.autoStart        = True
  self.config.autoStop         = True
  self.config.execMode         = 2
  return self

class TestApplication(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF, interactive=False, dump=False):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)
    self.interactive = interactive
    self.dump        = dump
    setup_app_basic(self)
    
  def setup_generator(self, type, factory, buffer_size=10000000, half_window=0, packing=1):
    self.config.inputType             = type
    self.config.GENERIC_factory       = factory
    self.config.GENERIC_bufferSize    = buffer_size
    self.config.GENERIC_halfWindow    = half_window
    self.config.GENERIC_packingFactor = packing
    self.config.GENERIC_maxEventsIn   = 0
    return self

  def exec_events(self, algs, num_events):
    self.setup_hive(FlowManager("EventLoop"), 1)
    self.config.GENERIC_maxEventsIn = num_events
    self.app.TopAlg                 = algs
    self.broker.DataProducers       = self.app.TopAlg
    return self

def test_generator(app, half_window=0, packing=1, num_events=10, algorithm=None):
  app.setup_generator(type='Generic', factory='Online__GenerateEventAccess', half_window=half_window, packing=packing)
  input = app.setup_event_input()
  algs  = [input]
  if app.dump:
    algs.append(store_explorer(print_freq=1.0))
    algs.append(header_dump('Headers', 'Banks/RawDataGuard'))
    if half_window == 0:
      algs.append(bank_dump('Dump', raw_data='Banks/RawData'))
    else:
      for i in range(2*half_window+1):
        alg = bank_dump('Dump', raw_data=None, bx=i-half_window)
        alg.FullDump = False
        algs.append(alg)
        
  # Add algorithms if required
  if isinstance(algorithm, (list, tuple)):
    for item in algorithm:
      algs.append(item)
  elif algorithm:
      algs.append(algorithm)
  # Add command prompt if requested
  if app.interactive:
    algs.append(command_handler())
  app.exec_events(algs, num_events=num_events)
  return app

def test_create_banks(num_events, output_level=MSG_INFO, dump=True, half_window=0):
  app = TestApplication(outputLevel=output_level, dump=dump)
  return test_generator(app, packing=1, half_window=half_window, num_events=num_events)

def test_create_mdf(num_events, output_level=MSG_INFO, dump=True, half_window=0, output='Output.mdf'):
  app = TestApplication(outputLevel=output_level, dump=dump)
  alg = app.setup_file_output(type='MDF', name_format=output)
  return test_generator(app, packing=1, half_window=half_window, num_events=num_events, algorithm=alg)

def test_create_mdf_tae(num_events, output_level=MSG_INFO, dump=True, half_window=7, output='Output.tae'):
  app = TestApplication(outputLevel=output_level, dump=dump)
  alg = app.setup_file_output(type='MDF', name_format=output)
  alg.UseRawGuard = True
  return test_generator(app, packing=1, half_window=half_window, num_events=num_events, algorithm=alg)

def test_read_mdf(num_events=9999999999, output_level=MSG_INFO, dump=True, input='Input.mdf'):
  import GaudiOnline
  app = GaudiOnline.readDatafile(input, interactive=False, output_level=output_level)
  return setup_app_basic(app)

def test_bank_dump_tae(num_events, output_level=MSG_INFO, input='Input.mdf'):
  import GaudiOnline
  import Configurables
  hdr = header_dump(name='Headers', raw_data='Banks/RawDataGuard')
  dmp = Configurables.Online__TAEBankDump('TAE-dump')
  dmp.BankType = 16
  app = GaudiOnline.readDatafile(input, interactive=False, output_level=output_level, algs=[hdr, dmp])  
  return setup_app_basic(app)
