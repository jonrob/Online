#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"


from dataflow import *

global online_environment
online_environment = None

def mbm_flags(which):
  if isinstance(which,(list,tuple)):
    flgs = ''
    for i in which:
      flgs = flgs + ' ' + mbm_flags(i)
    return flg[1:]
  elif isinstance(which, str) and which == 'Events':
    return '-s=1000000 -e=50 -u=30 -b=15 -t=1 -y -i=Events -f -c '
  elif isinstance(which, str) and which == 'Output':
    return '-s=100000 -e=50 -u=15 -b=12 -t=1 -y -i=Output -f -c'
  return '-s=100000 -e=50 -u=15 -b=12 -t=1 -y -i='+which+' -f -c'

class online_env:
  def __init__(self):
    import os
    self.PartitionID      = os.getpid()&0xFFFF
    self.PartitionIDName  = '%04X'%(self.PartitionID,)
    self.PartitionName    = '0x%04X'%(self.PartitionID,)
    self.PartitionBuffers = True
    self.Activity         = "PHYSICS"
    self.TAE              = 0
    self.OutputLevel      = 3
    self.AcceptRate       = 0.0001
    self.EnableWriting    = 1
    self.HasAlignment     = 0
    self.HasMonitoring    = 1
    self.HasStorage       = 0
    self.passThroughDelay = 0
    self.RunType          = "PHYSICS"
    self.MBM_setup        = mbm_flags('Events')+mbm_flags('Output')

class dataflow_task:
  def __init__(self, clazz, output_level=1):
    self.output_level = output_level
    self.app = Application(clazz)
    self.app.OutputLevel = output_level
    self.manager = Component('DataflowManager/Manager')
    self.exit_component = None
    self.runable = None
    self.algorithms = []
    self.services = []
    self.setup = []

  def create_component(self, type, name, output_level=None):
    """ Create arbitrary component and set its name and output level
    """
    comp = Component(type+'/'+name)
    if output_level:
      comp.OutputLevel = output_level
    else:
      comp.OutputLevel = self.output_level
    return comp

  def setup_logger(self, type=Logger.type, name=Logger.name):
    """ Setup output logging for the dataflow task
    """
    print('INFO:   +++ Setting up output logger component')
    log = self.create_component(type=type, name=name)
    log.LogDeviceType = "RTL::Logger::LogDevice"
    log.Format        = '%-20SOURCE %-8LEVEL'
    log.OutputLevel   = self.output_level
    self.logger = log
    return self

  def setup_ui(self, type='Dataflow_UI', name='UI'):
    """ Setup UI service for the dataflow task.
    """
    ui = self.create_component(type=type, name=name)
    self.services.append(ui)
    self.ui = ui
    return self

  def set_automatic(self, auto_startup, auto_finalize):
    """ Setup AutoStart and AutoStop machinery
    """
    self.app.Automatic = auto_startup
    self.app.Terminate = auto_finalize
    self.app.PrintOptions = True
    return self

  def setup_exit(self, when="(start)"):
    print('INFO:   +++ Setting up exit component')
    self.exit_component = Component('Dataflow_ExitPlugin/Exit')
    self.exit_component.PrintPID = False    
    self.exit_component.When = when
    return self

  def setup_monitoring(self, type=Monitoring.type, name=Monitoring.name):
    mon = self.create_component(type=type, name=name)
    mon.PartitionName = online_environment.PartitionName
    mon.ExpandNameInfix       = "<proc>/"
    mon.UniqueServiceNames    = 1
    mon.ExpandCounterServices = 1
    mon.CounterUpdateInterval = 10
    mon.DimUpdateInterval     = 30
    self.app.Monitoring = type
    self.monitoring = mon
    return self

  def setup_basics(self, auto_startup=False, auto_finalize=False, exit=False, ui=True, monitoring=True):
    """ Do basic task setup
    """
    self.setup_logger()
    if ui:
      self.setup_ui();

    self.set_automatic(auto_startup=auto_startup, auto_finalize=auto_finalize)
    if monitoring:
      self.setup_monitoring()
    if isinstance(exit,str):
      self.setup_exit(exit)
    elif exit:
      self.setup_exit()
    return self    

  def setup_numa(self, type='Dataflow_NumaControl', name='Numa'):
    print('INFO:   +++ Setting up numa component')
    numa            = self.create_component(type=type, name=name)
    numa.When       = "(initialize)"
    numa.BindCPU    = True
    numa.BindMemory = True
    numa.PrintPID   = False
    numa.CPUMask    = [1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    numa.Print      = "(all)(result)"
    self.services.append(numa)
    return numa

  def run(self):
    import sys
    # Basic infrastructure setup
    if len(self.setup):
      self.manager.Setup = self.setup
    # Setup the dataflow services
    if self.exit_component:
      self.services.append(self.exit_component)
    if len(self.services):
      self.manager.Services = self.services
    # setup the dataflow algorithms
    if len(self.algorithms):
      self.manager.Algorithms = self.algorithms
    # Setup runable if defined
    if self.runable:
      self.manager.Runable = self.runable

    print('INFO:   +++ Starting dataflow process')
    sys.stdout.flush()
    sys.stderr.flush()
    self.app.run()
    sys.stdout.flush()
    sys.stderr.flush()
    return self

  def setup_mbm_server(self, flags):
    global online_environment
    if not flags:
      raise KeyError('setup_mbm_server: No MBM server flags provided!')
    mbm = self.create_component(type='Dataflow_MBMServer', name='MBM')
    mbm.PartitionBuffers = online_environment.PartitionBuffers
    mbm.PartitionName    = online_environment.PartitionName
    mbm.PartitionID      = online_environment.PartitionID
    mbm.InitFlags        = flags
    self.services.append(mbm)
    self.mbm = mbm
    return self

  def setup_mbm_client(self, name, buffers):
    global online_environment
    client = self.create_component(type='Dataflow_MBMClient', name=name)
    client.PartitionBuffers = True
    client.PartitionName    = online_environment.PartitionName
    client.PartitionID      = online_environment.PartitionID
    client.Buffers          = buffers
    self.services.append(client)
    return client

  def setup_mbm_writer(self, buffer, type='Dataflow_MBMWriter', name='Writer'):
    writer = self.create_component(type=type, name=name)
    writer.Buffer = buffer
    self.algorithms.append(writer)
    return writer

  def setup_mbm_selector(self, name='EventSelector', type='Dataflow_MBMSelector'):
    selector = self.create_component(type=type, name=name)
    self.services.append(selector)
    return selector
    
  
  def setup_mbm_summary(self, regex='', type='Dataflow_MBMSummary', name='Summary', when=None):
    sum = self.create_component(type=type, name=name)
    sum.Regex = regex
    if when:
      sum.When = when
    self.services.append(sum)
    return sum

  def setup_basic_reader(self, name, buffer):
    reader  = self.create_component(type='Dataflow_BurstReader', name=name)
    reader.Buffer               = buffer
    reader.BrokenHosts          = ''
    reader.AllowedRuns          = ['*']
    reader.MuDelay              = 0
    reader.DeleteFiles          = 0
    reader.SaveRest             = 0
    reader.PauseSleep           = 2  # Optional wait time until 'Output' event queue is empty
    reader.InitialSleep         = 0
    reader.MaxPauseWait         = 1
    reader.GoService            = ''
    reader.Rescan               = 0
    reader.Rescan               = 1
    reader.RequireConsumers     = 0
    reader.PatchOdin            = 5000000
    return reader

  def setup_reader(self, name, buffer):
    client  = self.setup_mbm_client('MBM', [buffer])
    reader  = self.setup_basic_reader(name, buffer)
    self.services.append(reader)

    wrapper = self.create_component(type='Dataflow_RunableWrapper', name='Wrap')
    wrapper.Callable = reader
    self.runable = wrapper
    self.services.append(wrapper)
    return reader

  def setup_event_generator(self, name, half_window=0, packing=1):
    runable = self.create_component(type='Dataflow_MDFGenerator', name=name)
    runable.HalfWindow = half_window
    runable.PackingFactor = packing
    self.runable = runable
    self.services.append(runable)
    return runable

  def setup_generator(self, name, run, bursts, buffer, half_window, packing):
    client = self.setup_mbm_client('MBM', [buffer])
    gene   = self.setup_event_generator(name=name, half_window=half_window, packing=packing)
    gene.RunNumber = run
    gene.MaxBursts = bursts
    writer = self.setup_mbm_writer(buffer=buffer, type='Dataflow_MBMWriter', name='Writer')
    return gene

  def setup_passthrough(self, name='EventSelector', inbuffer='Events', outbuffer='Output'):
    client = self.setup_mbm_client('MBM', [inbuffer, outbuffer])
    select = self.setup_mbm_selector(name=name, type='Dataflow_MBMSelector')
    select.Input = inbuffer;
    select.REQ1  = 'EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;'+\
      'VetoMask=0,0,0,0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0'
    writer = self.setup_mbm_writer(buffer=outbuffer, type='Dataflow_MBMWriter', name='Writer')
    return select

def df_01_numa():
  global online_environment
  online_environment = online_env()
  task = dataflow_task('Class1', output_level=online_environment.OutputLevel)
  task.setup_basics(auto_startup=True, auto_finalize=True, ui=None, monitoring=None, exit=True).setup_numa()
  return task.run()

def df_02_install_mbm():
  global online_environment
  online_environment = online_env()
  task = dataflow_task('Class0', output_level=online_environment.OutputLevel)
  task.setup_basics(auto_startup=True, auto_finalize=True, exit=None) \
      .setup_mbm_server(flags=online_environment.MBM_setup) \
      .setup_mbm_summary(regex='.(.*)_'+online_environment.PartitionName, when=['(start)'])
  return task.run()

def MBM():
  global online_environment
  import OnlineEnv as online_environment
  task = dataflow_task('Class0', output_level=online_environment.OutputLevel)
  task.setup_basics()\
      .setup_mbm_server(flags=mbm_flags('Events') + ' ' + mbm_flags('Output'))
  return task.run()

def Reader():
  global online_environment
  import OnlineEnv as online_environment
  task = dataflow_task('Class2', output_level=online_environment.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  reader = task.setup_reader(name='Reader', buffer='Events')
  reader.Directories = ['.']
  reader.FilePrefix = '*'
  return task.run()

def MDFGen(run=12345, bursts=10000, half_window=0, packing=1):
  global online_environment
  import OnlineEnv as online_environment
  task = dataflow_task('Class2', output_level=online_environment.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  task.setup_generator(name='Generator',
                       run=run,
                       bursts=bursts,
                       buffer='Events',
                       half_window=half_window,
                       packing=packing)
  task.setup_mbm_summary(regex='.(.*)_'+online_environment.PartitionName, when=['(stop)'])
  return task.run()

def TAEGen(run=12345, bursts=100, half_window=7, packing=1000):
  return MDFGen(run=run, bursts=bursts, half_window=half_window, packing=packing)

def Passthrough():
  global online_environment
  import OnlineEnv as online_environment
  task = dataflow_task('Class1', output_level=online_environment.OutputLevel)
  task.setup_basics().setup_passthrough(name='Passthrough', inbuffer='Events', outbuffer='Output')
  return task.run()


## df_01_numa()
## df_02_install_mbm()
