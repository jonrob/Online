//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <Dataflow/DataflowComponent.h>
#include <MBM/Summary.h>

/// Online namespace declaration
namespace Online    {

  /// Generic MDF File I/O class to be used by the OnlineEvtApp
  /**
   *
   * \version 1.0
   * \author  M.Frank
   */
  class MBMSummary : public DataflowComponent  {
    /// Property: Define when to make snapshots
    std::vector<std::string> snapshots;
    /// Property: Regular expression for selective buffer summaries
    std::string expression;
    
    /// Sleep function
    void make_snapshot(const char* state);

  public:
    /// Default constructor
    MBMSummary(const std::string& name, Context& ctxt);

    /// Standard destructor
    virtual ~MBMSummary() = default;

    /// Initialize the delay component
    virtual int initialize()  override;
    /// Start the delay component
    virtual int start()  override;
    /// Stop the delay component
    virtual int stop()  override;
    /// Finalize the delay component
    virtual int finalize()  override;
    /// Continuing the data flow component.
    virtual int continuing()  override;
    /// Pause the data flow component.
    virtual int pause()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
  };
}    // End namespace Online

// Include files from the framework
#include <Dataflow/Plugins.h>
#include <Dataflow/Incidents.h>
#include <cstring>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MBMSummary,MBMSummary)

using namespace Online;

/// Initializing constructor
MBMSummary::MBMSummary(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt)
{
  declareProperty("Regex", this->expression);
  declareProperty("When",  this->snapshots);
}

    /// Sleep function
void MBMSummary::make_snapshot(const char* state)   {
  for(const auto& st : snapshots)   {
    if ( 0 == ::strcmp(st.c_str(), state) )   {
      MBM::Summary sum(expression);
      auto [status,lines] = sum.get_info(true);
      for(const auto& line : lines)
	always("%-10s %s\n", state, line.c_str());
    }
  }
}

/// Initialize the component
int MBMSummary::initialize()  {
  int sc = Component::initialize();
  if ( snapshots.empty() )   {
    snapshots.emplace_back("(start)");
    snapshots.emplace_back("(stop)");
  }
  make_snapshot("(initialize)");
  return sc;
}

/// Start the delay component
int MBMSummary::start()  {
  int sc = Component::start();
  make_snapshot("(start)");
  return sc;
}

/// Finalize the delay component
int MBMSummary::finalize()  {
  make_snapshot("(finalize)");
  return Component::finalize();
}

/// Stop the delay component
int MBMSummary::stop()  {
  make_snapshot("(stop)");
  return Component::stop();
}

/// Pause the data flow component. Default implementation is empty.
int MBMSummary::pause()  {
  make_snapshot("(pause)");
  return DataflowComponent::pause();
}

/// Continuing the data flow component. Default implementation is empty.
int MBMSummary::continuing()  {
  make_snapshot("(continue)");
  return DataflowComponent::continuing();
}

/// Data processing overload: Send data record to network client
int MBMSummary::execute(const Context::EventData& /* event */)  {
  make_snapshot("(execute)");
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void MBMSummary::handle(const DataflowIncident& inc)    {
  if ( inc.type == "MBM_SNAPSHOT" )  {
    make_snapshot("(snapshot)");
  }
  else if ( inc.type == "DAQ_CANCEL" )  {
    make_snapshot("(cancel)");
  }
}
