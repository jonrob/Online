#!/bin/bash
if test -n "${DEBUG}"; then
    echo "+++ Executing run_test.sh";
    echo "+++ Working directory: `pwd`";
    echo "+++ Command: ${COMMAND}";
    echo "+++ Info options: ${INFO_OPTIONS}";
fi;
. ./setup.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh $*;
#    f.write(". ${GAUDIONLINETESTSROOT}/scripts/run_test.sh \"import GaudiOnlineTests.DF as df; ${TASK_TYPE}()\";\n")

setup_rootsys()   {
    if test -z "${ROOTSYS}"; then
	ROOTSYS=`which root`;
	if test -n "${ROOTSYS}"; then
	    ROOTSYS=`dirname ${ROOTSYS}`;
	    ROOTSYS=`dirname ${ROOTSYS}`;
	fi;
	export ROOTSYS;
    else
	echo "ERROR Cannot locate root executable. ROOTSYS cannot be defined!";
    fi;
}

setup_rootsys;
export PYTHONPATH=`dirname ${INFO_OPTIONS}`:${PYTHONPATH};
## echo `which python` -c "import ${OPTIONS} as module; module.${TASK_TYPE}()";
exec -a ${UTGID} `which python` -c "import ${OPTIONS} as module; module.${TASK_TYPE}()";
