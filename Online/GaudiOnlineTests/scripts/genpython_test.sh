#!/bin/bash
shift;

setup_rootsys()   {
    if test -z "${ROOTSYS}"; then
	ROOTSYS=`which root`;
	if test -n "${ROOTSYS}"; then
	    ROOTSYS=`dirname ${ROOTSYS}`;
	    ROOTSYS=`dirname ${ROOTSYS}`;
	fi;
	# echo "ROOTSYS=${ROOTSYS}";
	export ROOTSYS;
    else
	echo "ERROR Cannot locate root executable. ROOTSYS cannot be defined!";
    fi;
}

setup_rootsys;
export DIM_DNS_NODE=127.0.0.1;
temp_file=/tmp/0xfeed$$_commands.py;
echo $* >${temp_file}
`which python` `which gaudirun.py` ${temp_file} --application=Online::OnlineEventApp 2>&1 | grep -v "Server Connecting to DIM_DNS";
rm -f ${temp_file};
