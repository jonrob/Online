//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_RATESVC_H
#define ONLINE_GAUCHO_RATESVC_H

#include "PubSvc.h"
#include <GaudiKernel/Service.h>
#include <GaudiKernel/IToolSvc.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GauchoAppl/COutService.h>

#include <vector>

// Forward declarations
class IMonitorSvc;
class IIncidentSvc;
class IGauchoMonitorSvc;
class IHistogramSvc;

class RateService;
namespace Online   {
  class MonAdder;
  class AdderSys;
  class SaveTimer;
  class DimBuffBase;
}

class OUTServiceDescr   {
public:
  std::string m_serviceName;
  long long last_update;
  RateService *m_svc;
  void *m_buffer;
  int m_buffersize;
  OUTServiceDescr(const std::string& name, RateService *srvc)  {
    m_serviceName = name;
    m_svc = srvc;
  }
  ~OUTServiceDescr() = default;
};

class RateSvc : public PubSvc   {
  typedef std::map<std::string, OUTServiceDescr*>  OUTServiceMap;
  template <typename T> void update_rate(const Online::DimBuffBase* b);

public:
  RateSvc(const std::string& name, ISvcLocator* sl);
  virtual ~RateSvc() = default;
  StatusCode start() override;
  StatusCode stop() override;
  void analyze(mem_buff& buffer, Online::MonitorItems* mmap)  override;

  void makerate(Online::MonitorItems* mmap);
  void makecounters(Online::MonitorItems* mmap);

  void *m_oldProf { nullptr };
  OUTServiceMap m_outputServicemap;
  OUTServiceDescr *findOUTService(std::string servc);
  std::string m_NamePrefix;
  COUTServiceMap m_outmap;
};
#endif // ONLINE_GAUCHO_RATESVC_H
