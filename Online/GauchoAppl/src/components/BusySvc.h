//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_BUSYSVC_H
#define ONLINE_GAUCHO_BUSYSVC_H

#include <GaudiKernel/Service.h>
class IMonitorSvc;

/// Online namespace declaration
namespace Online   {
  class BusySvc : public Service    {
  private:
    class IdleTimer;

    SmartIF<IMonitorSvc>       m_monitorSvc;
    std::unique_ptr<IdleTimer> m_timer;

    double       m_bogus         { 0e0 };
    bool         m_extendedMonitoring { false };

    long         m_times_old[7];

    /// Monitoring quantities
    double       m_idlebogus     { 0e0 };
    double       m_busybogus     { 0e0 };
    double       m_mybogus       { 0e0 };
    double       m_busyFraction  { 0e0 };
    long         m_memtot        { 0 };
    long         m_memfree       { 0 };
    long         m_membuff       { 0 };
    long         m_memSwaptot    { 0 };
    long         m_memSwapfree   { 0 };
    long         m_memAvail      { 0 };
    int          m_numCores      { 0 };
    bool         m_first         { true };

    void calcIdle();
    double getBogus();
    std::string nodeClass(const std::string& node_name)   const;
  public:
    BusySvc(const std::string& name, ISvcLocator* sl);
    virtual ~BusySvc() = default;
    virtual StatusCode start()       override;
    virtual StatusCode initialize()  override;
    virtual StatusCode finalize()    override;
  };
}
#endif // ONLINE_GAUCHO_BUSYSVC_H
