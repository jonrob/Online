//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "Gaucho/RPCRec.h"
#include "stdio.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TApplication.h"
#include "Gaucho/MonHist.h"
#include "TH1D.h"
#include "TProfile.h"
#include "TH2D.h"
#include "TTimer.h"
#include "Gaucho/HistSerDes.h"

using namespace Online;

/// Local definitions in anonymous namespace
namespace   {
  
  class MyTTimer : public TTimer  {
  public:
    RPCRec      *rec {nullptr};
    RPCCommRead *rrd {nullptr};
    int          rdlen {0};
    MyTTimer(RPCRec* r, RPCCommRead *rdc, int rlen) 
      :  TTimer(5000,true), rec(r), rrd(rdc), rdlen(rlen)
    {
    }
    Bool_t Notify() override    {
      std::printf("Timer Fired\n");
      rec->setData(rrd,rdlen);
      Start(5000, true);
      return true;
    };
  };

  TCanvas *canv;
  std::vector <std::string*> hnames;
  static TObject *o=0;
  void DirCB(const RPCRec::NAMEVEC &nam)  {
    hnames.clear();
    for (auto i=nam.begin();i!=nam.end();i++)  {
      hnames.push_back(new std::string(*i));
    }
  }
  void DatCB(const RPCRec::PTRMAP &hists)  {
    for (auto i=hists.begin();i!=hists.end();i++)   {
      //        printf("Looping over Histos...\n");
      DimBuffBase *hist = (DimBuffBase *)i->second;
      if (o) delete o;
      o = (TObject*)HistSerDes::de_serialize(hist);
      o->Draw();
    }
    canv->Update();
  }
}

int main(int argc, char *argv[])  {
  const char *nam = (argc <= 1) ? "plus16_MON_Adder_MON_Moore_0/Histos/Data/Command" : argv[1];
  RPCComm    *rda = new RPCComm;
  RPCRec      rpcCB(nam, 1000);

  rpcCB.declareDirectoryCallback(DirCB);
  rpcCB.declareDataCallback(DatCB);
  rda->comm = RPCCReadAll;
  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TProfile::SetDefaultSumw2();
  int dat = RPCCDirectory;
  rpcCB.setData(dat);

  while ( hnames.size() == 0 )
    ::sleep(1);

  for (int i=0;i<(int)hnames.size();i++)
    std::printf("%d %s\n",i,hnames[i]->c_str());

  TApplication *app=new TApplication("Shit",&argc,argv);
  canv =  gROOT->MakeDefCanvas();
  gROOT->Draw();
  canv->Show();
  canv->Draw();
  int rdlen = sizeof(RPCCommRead)+std::strlen(hnames[1023]->c_str());
  RPCCommRead *rrd = (RPCCommRead*)malloc(rdlen);
  ::memset(rrd,0,rdlen);
  std::strcpy(rrd->which,hnames[1023]->c_str());
  std::printf("asking for histogram %s\n",hnames[1023]->c_str());
  rrd->comm = RPCCRead;
  MyTTimer *timr = new MyTTimer(&rpcCB, rrd,rdlen);
  timr->TurnOn();
  timr->Start(5000,true);
  while (1)  {
    rpcCB.setData(rrd,rdlen);
    app->Run(true);
  }
  return 0;
}
