//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * TypeTst.cpp
 *
 *  Created on: Oct 14, 2019
 *      Author: beat
 */

#include <atomic>
#include <utility>
#include <typeinfo>
#include <stdio.h>
#include <Gaudi/Accumulators.h>

using namespace std;

int main()
{
  atomic<int> a;
  atomic<long> b;
  atomic<float> c;
  atomic<double> d;
  std::pair<int*,int*> e;
  printf("%s %s\n",typeid(a).name(),typeid(a.load()).name());
  printf("%s %s\n",typeid(b).name(),typeid(b.load()).name());
  printf("%s %s\n",typeid(c).name(),typeid(c.load()).name());
  printf("%s %s\n",typeid(d).name(),typeid(d.load()).name());
  printf("%s\n",typeid(e.first).name());
  Gaudi::Accumulators::Counter<Gaudi::Accumulators::atomicity::none> *aa;
  Gaudi::Accumulators::AveragingCounter<long, Gaudi::Accumulators::atomicity::none> *bb;
  Gaudi::Accumulators::AveragingCounter<float, Gaudi::Accumulators::atomicity::none> *cc;
  Gaudi::Accumulators::AveragingCounter<double, Gaudi::Accumulators::atomicity::none> *dd;
  printf("%s %s\n",typeid(aa).name() ,typeid(aa->value()).name());
  printf("%s %s\n",typeid(bb).name() ,typeid(bb->value()).name());
  printf("%s %s\n",typeid(cc).name() ,typeid(cc->value()).name());
  printf("%s %s\n",typeid(dd).name() ,typeid(dd->value()).name());
  return 0;
}



