//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_ADDERSYS_H
#define ONLINE_GAUCHO_ADDERSYS_H

/// Framework include files
#include <dim/dis.hxx>

/// C/C++ include files
#include <string>
#include <memory>
#include <vector>
#include <map>
#include <set>

/// Forward declarations
class DimServerDns;

/// Online namespace declaration
namespace Online   {

  /// Forward declarations
  class MonAdder;

  /// AdderSys class definition
  class AdderSys   {
  private:
    /// Internal class to handle client's service lists
    class ServiceHandler  {
    protected:
      int id            { 0 };
      int iv            { 0 };
      std::string task  {   };

      /// Service analyzing function
      void analyze_services(const char* services, bool may_remove);
      void start();

      /// DTQ callback handlers to process timeout(s)
      static void service_tmo_handler(void* tag);

      /// DimInfo overload to process messages
      static void callback(void* tag, void* address, int* size);
      static void interval(void* tag, void* address, int* size);

    public:
      ServiceHandler(const std::string& server, int delay);
      virtual ~ServiceHandler();
    };

    typedef std::set<std::string>  SourceSet;
    typedef std::set<std::string>  ServerSet;
    typedef std::vector<MonAdder*> AdderList;
    typedef std::map<std::string, std::shared_ptr<DimServerDns> >   DNSMap;
    typedef std::map<std::string, std::shared_ptr<ServiceHandler> > TaskMap;

    DNSMap       dnsMap    {};
    TaskMap      taskMap   {};
    ServerSet    servers   {};
    SourceSet    sources   {};
    AdderList    adderList {};
    std::string  dns_node  {};
    std::string  source_buffer {};
    int          sourceSvc_id  { 0 };
    int          taskInfo_id   { 0 };

    /// DTQ callback handlers to process timeout(s)
    static void task_tmo_handler(void* tag);
    /// DIS/DIC callback handlers
    static void feedSources(void* tag, void** address, int* size, int* first);
    static void taskinfo_callback(void* tag, void* address, int* size);

    void analyze_tasks(const char* input);
    void publishSources();

    /// Default constructor
    AdderSys();
    /// Default destructor
    virtual ~AdderSys();

  public:
    /// Monitoring quantities
    /// Number of known adders in this system
    int                       numAdders            { 0 };
    /// Number of sources known
    int                       numSources           { 0 };
    /// Number of services analysed
    int                       numServices          { 0 };
    /// Number of service callbacks from clients
    int                       numServicesCalls     { 0 };
    /// Number of invalid service callbacks from clients
    int                       numServicesFails     { 0 };
    /// Number of processed service items from service callbacks from clients
    int                       numServicesProcessed { 0 };

  public:
    /// Setup parameters
    /// Delay time in seconds to start connections to client's service list
    int                       service_delay { 3 };
    /// Debug flag
    bool                      debugOn       { false };

  public:
    /// Instance accessor
    static AdderSys& instance();
    /// Get DNS by node name
    std::shared_ptr<DimServerDns> getDNS(std::string& dns);
    /// Get DNS by node and task
    std::shared_ptr<DimServerDns> getDNS(std::string& dns, std::string& name);
    /// Set the default client DNS node
    void setClientDns(const std::string& dns);

    /// Add an adder instance
    void add(MonAdder *adder);
    /// Remove an adder instance
    void remove(MonAdder *adder);

    void start();
    void stop();
    void setDebugOn(bool dbg);
    void addSource(const std::string& src);
    void removeSource(const std::string& src);
    void trackSources();
    bool is_tracking()  const    {   return this->sourceSvc_id != 0;  }
  };
}
#endif /* ONLINE_GAUCHO_ADDERSYS_H */
