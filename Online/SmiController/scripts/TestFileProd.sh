#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
export LOGFIFO=/run/fmc/logSrv.fifo;
`dataflow_task Class2` `dataflow_default_options ${TASK_TYPE}`

