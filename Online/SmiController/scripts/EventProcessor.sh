#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
exec -a ${UTGID} genPython.exe `which gaudirun.py` EventProcessor.py --application=OnlineEvents;
