//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
#ifndef ONLINE_SMICONTROLLER_SMITASKCONFIGURATION_H
#define ONLINE_SMICONTROLLER_SMITASKCONFIGURATION_H

// C/C++ include files
#include <string>
#include <map>

/// FiniteStateMachine namespace declaration
namespace FiniteStateMachine   {

  class SmiTask;

  /**@class SmiTaskConfiguration  SmiTaskConfiguration.h FiniteStateMachine/SmiTaskConfiguration.h
   *
   * @author  M.Frank
   * @date    01/03/2013
   * @version 0.1
   */
  struct SmiTaskConfiguration  {
  public:
    typedef std::map<std::string, std::string> replacements_t;

  protected:
    /// Replacement item list
    replacements_t  m_replacements;
    /// String with the partition name
    std::string     m_partition;
    /// String with the name of the configuration file
    std::string     m_config;
    /// String with the location of the run-info python file
    std::string     m_runinfo;
    /// stdin and stderr output files
    std::string     m_stdout_file, m_stderr_file;

  public:
    /// Standard constructor
    SmiTaskConfiguration(const replacements_t& replacements,
			 const std::string& partition, 
			 const std::string& config, 
			 const std::string& runinfo,
			 const std::string& out,
			 const std::string& err);
    /// Default destructor
    virtual ~SmiTaskConfiguration();
    /// Analyse the configuration file and attach the corresponding tasks to the FSM machine
    std::map<std::string,SmiTask*> taskList(int bind_cpu,
					    int output_level);
  };   //  End class SmiTaskConfiguration
}      //  End namespace 
#endif //  ONLINE_SMICONTROLLER_SMITASKCONFIGURATION_H
