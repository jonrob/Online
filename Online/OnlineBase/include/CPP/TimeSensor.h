/*
**++
**  FACILITY:  DEXPERT  (TimeSensor.h)
**
**  MODULE DESCRIPTION:
**
**      TimeSensor class definition.
**
**  AUTHORS:
**
**      P. Mato
**
**  CREATION DATE:  16-Nov-1990
**
**  DESIGN ISSUES:
**
**      The TimeSensor behaves as similar way that the AmsSensor or
**      UpiSensor all of them are derivated from a unique Sensor class.
**
**
**  MODIFICATION HISTORY:
**
**      16-Nov-1990 Derivated from the existing Timer.h
**--
*/

#ifndef CPP_TIMESENSOR_H
#define CPP_TIMESENSOR_H

/// Framework include files
#include <CPP/Sensor.h>

/// CPP namespace declaration
namespace CPP  {


  class TimeSensor : public Sensor {
    /// Standard constructor
    TimeSensor();
  public:

    /// Standard destructor
    virtual ~TimeSensor();
    /// Add interactor
    void add( Interactor*, void* ) override;
    /// Add interactor
    virtual void  add(Interactor*, const char* period, void* param);
    /// Add interactor
    virtual void  add(Interactor*, int seconds, void* param = 0 );
    /// Add interactor
    virtual void  add(Interactor*, int seconds, int param );
    /// Remove interactor
    void  remove(Interactor*, void* = 0) override;
    /// Timer event dispatching routine
    void  dispatch(void*) override;
    /// Timer rearm callback
    void  rearm() override;
    /// Singleton instantiation
    static TimeSensor& instance();
  };
}
using CPP::TimeSensor;

#define TIMESENSOR (::CPP::TimeSensor::instance())

#endif // CPP_TIMESENSOR_H
