//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINEBASE_RTL_FCMLOGDEVICE_H
#define ONLINEBASE_RTL_FCMLOGDEVICE_H

/// Framework include files
#include "RTL/Logger.h"

/// RTL namespace declaration
namespace RTL   {

  /// Basic output logger device. Prints to stdout
  /** @class Logger::LogDevice
   *
   * @author M.Frank
   */
  class FmcLogDevice : public Logger::LogDevice  {

  public:
    /// Property: Output device name
    std::string    logDevice;
    /// Property: Retries before dropping
    int            numRetry;
    /// Property: Output level
    int            outputLevel;
    /// Property: allow the output logger to drop bytes if busy
    bool           allowDrop;
    /// Property: reuse STDOUT/STDERR_FILEHANDLE
    bool           reuse_handles {false};
    /// The file descriptor of the putput device
    int            fifoFD;
    /// Nuber of bytes dropped for a given message
    mutable size_t bytesDropped;

    /// Write output .....
    virtual size_t print_record(bool exc, int severity, const char* source, const char* msg)  const;

  public:
    /// Default constructor reusing stdout and stderr file handles
    FmcLogDevice();

    /// Default constructor attaching explicitly to a fifo path
    FmcLogDevice(const std::string& path);

    /// Default destructor
    virtual ~FmcLogDevice();

    /// Compile the format string
    virtual void compileFormat(const std::string& fmt)  override;
      
    /// Open fifo device. Returns file descriptor
    int open(const char* path) const;

    /// Close fifo device
    void close() const;

    /// Calls the display action with a given severity level
    /**
     *  @arg severity   [int,read-only]      Display severity flag (see enum)
     *  @arg msg        [string,read-only]   Formatted message string
     *  @return Status code indicating success or failure
     */
    virtual size_t printmsg(int severity, const char* source, const char* msg)  const  override;

    /// Calls the display action with ERROR and throws an std::runtime_error exception
    /**
     *  @return Status code indicating success or failure
     *  @arg msg        [string,read-only]   Formatted message string
     */
    virtual void exceptmsg(const char* source, const char* msg)  const  override;
  };
}       // End namespace RTL
#endif  // ONLINEBASE_RTL_FCMLOGDEVICE_H
