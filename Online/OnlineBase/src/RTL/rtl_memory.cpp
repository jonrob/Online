//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include "RTL/rtl.h"
#if defined(ONLINE_HAVE_NUMA)
#include "numa.h"
#endif

/// Allocate memory for special purpose (NUMA bound, etc.)
extern "C" void* lib_rtl_allocate(size_t length, int flags)    {
  void* mapping = 0;
#if defined(ONLINE_HAVE_NUMA)
  if ( flags&LIB_RTL_NUMA0 )
    mapping = ::numa_alloc_onnode(length, 0);
  else if ( flags&LIB_RTL_NUMA1 )
    mapping = ::numa_alloc_onnode(length, 1);
  else if ( flags&LIB_RTL_NUMA2 )
    mapping = ::numa_alloc_onnode(length, 2);
  else if ( flags&LIB_RTL_NUMA3 )
    mapping = ::numa_alloc_onnode(length, 3);
  else
#endif
    mapping = ::malloc(length);
  return mapping;
}

/// Reallocate memory for special purpose (NUMA bound, etc.)
extern "C" void* lib_rtl_reallocate(void* pointer, size_t old_length, size_t new_length, int flags)    {
  void* mapping = 0;
#if defined(ONLINE_HAVE_NUMA)
  if ( flags&LIB_RTL_NUMA0 )
    mapping = ::numa_realloc(pointer, old_length, new_length);
  else if ( flags&LIB_RTL_NUMA1 )
    mapping = ::numa_realloc(pointer, old_length, new_length);
  else if ( flags&LIB_RTL_NUMA2 )
    mapping = ::numa_realloc(pointer, old_length, new_length);
  else if ( flags&LIB_RTL_NUMA3 )
    mapping = ::numa_realloc(pointer, old_length, new_length);
  else
#endif
    mapping = ::realloc(pointer, new_length);
  return mapping;
}

/// Free special purpose (NUMA bound, etc.) memory
extern "C" void lib_rtl_free(void* pointer, size_t len, int flags)   {
#if defined(ONLINE_HAVE_NUMA)
  if ( flags&LIB_RTL_NUMA0 )
    ::numa_free(pointer, len);
  else if ( flags&LIB_RTL_NUMA1 )
    ::numa_free(pointer, len);
  else if ( flags&LIB_RTL_NUMA2 )
    ::numa_free(pointer, len);
  else if ( flags&LIB_RTL_NUMA3 )
    ::numa_free(pointer, len);
  else
#endif
    ::free(pointer);
}

/// Move a memory region to a specified CPU slot
extern "C" int lib_rtl_memmove(void* pointer, size_t len, int flags)   {
#if defined(ONLINE_HAVE_NUMA)
  if ( flags&LIB_RTL_NUMA0 )   {
    ::numa_tonode_memory(pointer, len, 0);
    return 1;
  }
  else if ( flags&LIB_RTL_NUMA1 )   {
    ::numa_tonode_memory(pointer, len, 1);
    return 1;
  }
  else if ( flags&LIB_RTL_NUMA2 )   {
    ::numa_tonode_memory(pointer, len, 2);
    return 1;
  }
  else if ( flags&LIB_RTL_NUMA3 )   {
    ::numa_tonode_memory(pointer, len, 3);
    return 1;
  }
#endif
  return 0;
}

