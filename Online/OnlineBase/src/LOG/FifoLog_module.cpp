//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//
//==========================================================================

/// Framework include files
#include "LOG/FifoLog.inl.h"

/// C/C++ include files
#include <cctype>
#include <cstdlib>
#include <cstdio>

extern "C" {
#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif
#ifdef _XOPEN_SOURCE
#undef _XOPEN_SOURCE
#endif
#include <Python.h>
}
#if PY_MAJOR_VERSION >= 3
#define PyString_FromString          PyUnicode_FromString
#endif

namespace  {
  constexpr const char* module_version = "0.1";
}
using namespace std;

static PyObject*
version(PyObject* /* self */, PyObject* /* args */)  {
  /** Return module version   */
  return PyString_FromString(module_version);
}

/// Initialize logger object from python
static PyObject* _initialize_logger(PyObject* /* self */, PyObject* /* args */)  {
  string err;
  Py_BEGIN_ALLOW_THREADS
    try    {
      fifolog_initialize_logger();
    }
    catch(const std::exception& e)    {
      err = e.what();
    }
  Py_END_ALLOW_THREADS
    if ( err.empty() ) {
      Py_RETURN_NONE;
    }
  PyErr_SetString(PyExc_RuntimeError, err.c_str());
  return nullptr;
}

/// Finalize logger object from python
static PyObject* _finalize_logger(PyObject* /* self */, PyObject* /* args */) {
  string err;
  Py_BEGIN_ALLOW_THREADS	
    try    {
      fifolog_finalize_logger();
    }
    catch(const std::exception& e)    {
      err = e.what();
    }
  Py_END_ALLOW_THREADS
    if ( err.empty() ) {
      Py_RETURN_NONE;
    }
  PyErr_SetString(PyExc_RuntimeError, err.c_str());
  return nullptr;
}

/// Set UTGID value
static PyObject* _set_utgid(PyObject* /* self */, PyObject* args) {
  char* utgid = NULL;
  if ( !PyArg_ParseTuple(args, "s", &utgid) ) {
    PyErr_SetString(PyExc_RuntimeError, "Invalid UTGID");
    return nullptr;
  }
  string err;
  Py_BEGIN_ALLOW_THREADS	
    try    {
      fifolog_set_utgid(utgid);
    }
    catch(const std::exception& e)    {
      err = e.what();
    }
  Py_END_ALLOW_THREADS
    if ( err.empty() ) {
      Py_RETURN_NONE;
    }
  PyErr_SetString(PyExc_RuntimeError, err.c_str());
  return nullptr;
}

/// Set TAG value
static PyObject* _set_tag(PyObject* /* self */, PyObject* args) {
  char* tag = NULL;
  if ( !PyArg_ParseTuple(args, "s", &tag) ) {
    PyErr_SetString(PyExc_RuntimeError, "Invalid TAG");
    return NULL;
  }
  string err;
  Py_BEGIN_ALLOW_THREADS	
    try    {
      fifolog_set_tag(tag);
    }
    catch(const std::exception& e)    {
      err = e.what();
    }
  Py_END_ALLOW_THREADS
    if ( err.empty() ) {
      Py_RETURN_NONE;
    }
  PyErr_SetString(PyExc_RuntimeError, err.c_str());
  return nullptr;
}

/// Set TAG value
static PyObject* _print_msg(PyObject* /* self */, PyObject* args) {
  int level = 3;
  char* src = nullptr, *msg = nullptr;
  if ( !PyArg_ParseTuple(args, "iss", &level, &src, &msg) ) {
    PyErr_SetString(PyExc_RuntimeError, "Invalid Argument list");
    return nullptr;
  }
  string err;
  Py_BEGIN_ALLOW_THREADS	
    try    {
      fifolog_print(level, src, msg);
    }
    catch(const std::exception& e)    {
      err = e.what();
    }
  Py_END_ALLOW_THREADS
    if ( err.empty() ) {
      Py_RETURN_NONE;
    }
  PyErr_SetString(PyExc_RuntimeError, err.c_str());
  return nullptr;
}

static PyMethodDef LoggerMethods[] = {
  { "version",
    version,
    METH_VARARGS,
    "Module version"
  },
  {    "logger_start",
       _initialize_logger,
       METH_VARARGS,
       "Start logger"
  },
  {    "logger_stop",
       _finalize_logger,
       METH_VARARGS,
       "Stop logger"
  },
  {    "logger_set_utgid",
       _set_utgid,
       METH_VARARGS,
       "PASS user defined UTGID to module"
  },
  {    "logger_set_tag",
       _set_tag,
       METH_VARARGS,
       "PASS user defined SYSTAG to module"
  },
  {    "logger_print",
       _print_msg,
       METH_VARARGS,
       "Print user defined message"
  },
  {NULL, NULL, 0, NULL}        /* Sentinel */
};
#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef logger_module = {
    PyModuleDef_HEAD_INIT,
    "_fifo_log",               /* name of module */
    "Fifo Logger Methods",    /* module documentation, may be NULL */
    -1,                       /* size of per-interpreter state of the module,
                                 or -1 if the module keeps state in global variables. */
    LoggerMethods,
    NULL,                     /* m_reload */
    NULL,                     /* m_traverse */
    NULL,                     /* m_clear */
    NULL                      /* m_free */
};
  #define PyMODINIT_RETURN(x)   return x
#else
  #define PyMODINIT_RETURN(x)   return
#endif

#if PY_MAJOR_VERSION >= 3
PyMODINIT_FUNC    PyInit__fifo_log(void)
#else
PyMODINIT_FUNC    init_fifo_log(void)
#endif
{
#if PY_MAJOR_VERSION <=2 || (PY_MAJOR_VERSION == 3 && PY_MINOR_VERSION < 7)
  PyEval_InitThreads();
#endif
#if PY_MAJOR_VERSION >= 3
  PyObject *m = ::PyModule_Create(&logger_module);
#else
  PyObject *m = Py_InitModule3("_fifo_log", LoggerMethods, "Logger methods");
#endif
  if (m == NULL)  {
    PyMODINIT_RETURN(m);
  }
  // Add constants for service type definitions
  PyModule_AddIntConstant (m, "INITIALIZED", 1);
  PyMODINIT_RETURN(m);
}
