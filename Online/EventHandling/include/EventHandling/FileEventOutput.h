//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_FILEEVENTOUTPUT_H
#define ONLINE_EVENTDATA_FILEEVENTOUTPUT_H

/// Framework include files
#include <EventHandling/EventOutput.h>

/// C/C++ include files
#include <list>

/// Online namespace declaration
namespace Online   {

  /// Base class to output online event data
  /** @class FileEventOutput
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class FileEventOutput : public EventOutput  {
  public:
    /// Base class to handle one atomic File write operation
    class file_transaction_t;

    /// Pointer to fileplug device
    typedef std::list<file_transaction_t*> transaction_collection_t;

    struct file_config_t : public config_t  {
      using config_t::config_t;
      /// Property: maximum file size
      std::size_t   maxfileSize    { 1024*1024*1024 };
    } config;

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(FileEventOutput);

    /// Default constructor
    FileEventOutput(std::unique_ptr<RTL::Logger>&& logger);

    /// Standard destructor
    virtual ~FileEventOutput();

    /// EventAccess overload: Connect to event data source
    virtual int connect(const std::string& output, size_t instance);

    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  override;
  };
}        // End namespace Online
#endif   // ONLINE_EVENTDATA_FILEEVENTOUTPUT_H
