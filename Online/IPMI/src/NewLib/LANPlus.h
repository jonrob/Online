/*
 * LANPlus.h
 *
 *  Created on: Jul 14, 2021
 *      Author: beat
 */

#ifndef NEWLIB_LANPLUS_H_
#define NEWLIB_LANPLUS_H_

#include "ipmi.h"
#include "lanplus_crypt.h"
//#include "lanplus/lanplus_crypt_impl.h"
//#include "lanplus/lanplus_dump.h"
//#include "lanplus/rmcp.h"
//#include "lanplus/asf.h"
//#include "ipmi_intf.h"
//#include "ipmi_session.h"
//#include "ipmi.h"
//#include "helper.h"
//#include "ipmi_constants.h"
//#include "ipmi_oem.h"
//#include "hpm2.h"
//#include "ipmi_sel.h"
#include "helper.h"
//#include "LANBase.h"
#include <stdio.h>
#include <string.h>
#include "ipmi_oem.h"
#define IPMI_LANPLUS_PORT           0x26f

/*
 * RAKP return codes.  These values come from table 13-15 of the IPMI v2
 * specification.
 */
#define IPMI_RAKP_STATUS_NO_ERRORS                          0x00
#define IPMI_RAKP_STATUS_INSUFFICIENT_RESOURCES_FOR_SESSION 0x01
#define IPMI_RAKP_STATUS_INVALID_SESSION_ID                 0x02
#define IPMI_RAKP_STATUS_INVALID_PAYLOAD_TYPE               0x03
#define IPMI_RAKP_STATUS_INVALID_AUTHENTICATION_ALGORITHM   0x04
#define IPMI_RAKP_STATUS_INVALID_INTEGRITTY_ALGORITHM       0x05
#define IPMI_RAKP_STATUS_NO_MATCHING_AUTHENTICATION_PAYLOAD 0x06
#define IPMI_RAKP_STATUS_NO_MATCHING_INTEGRITY_PAYLOAD      0x07
#define IPMI_RAKP_STATUS_INACTIVE_SESSION_ID                0x08
#define IPMI_RAKP_STATUS_INVALID_ROLE                       0x09
#define IPMI_RAKP_STATUS_UNAUTHORIZED_ROLE_REQUESTED        0x0A
#define IPMI_RAKP_STATUS_INSUFFICIENT_RESOURCES_FOR_ROLE    0x0B
#define IPMI_RAKP_STATUS_INVALID_NAME_LENGTH                0x0C
#define IPMI_RAKP_STATUS_UNAUTHORIZED_NAME                  0x0D
#define IPMI_RAKP_STATUS_UNAUTHORIZED_GUID                  0x0E
#define IPMI_RAKP_STATUS_INVALID_INTEGRITY_CHECK_VALUE      0x0F
#define IPMI_RAKP_STATUS_INVALID_CONFIDENTIALITY_ALGORITHM  0x10
#define IPMI_RAKP_STATUS_NO_CIPHER_SUITE_MATCH              0x11
#define IPMI_RAKP_STATUS_ILLEGAL_PARAMTER                   0x12

#define IPMI_LAN_CHANNEL_1  0x07
#define IPMI_LAN_CHANNEL_2  0x06
#define IPMI_LAN_CHANNEL_E  0x0e

#define IPMI_LAN_TIMEOUT    2
#define IPMI_LAN_RETRY      4

#define IPMI_PRIV_CALLBACK 1
#define IPMI_PRIV_USER     2
#define IPMI_PRIV_OPERATOR 3
#define IPMI_PRIV_ADMIN    4
#define IPMI_PRIV_OEM      5

#define IPMI_CRYPT_AES_CBC_128_BLOCK_SIZE 0x10

/* Session message offsets, from table 13-8 of the v2 specification */
#define IPMI_LANPLUS_OFFSET_AUTHTYPE     0x04
#define IPMI_LANPLUS_OFFSET_PAYLOAD_TYPE 0x05
#define IPMI_LANPLUS_OFFSET_SESSION_ID   0x06
#define IPMI_LANPLUS_OFFSET_SEQUENCE_NUM 0x0A
#define IPMI_LANPLUS_OFFSET_PAYLOAD_SIZE 0x0E
#define IPMI_LANPLUS_OFFSET_PAYLOAD      0x10

#define IPMI_GET_CHANNEL_AUTH_CAP 0x38

/*
 * TODO: these are wild guesses and should be checked
 */
#define IPMI_MAX_CONF_HEADER_SIZE   0x20
#define IPMI_MAX_PAYLOAD_SIZE       0xFFFF /* Includes confidentiality header/trailer */
#define IPMI_MAX_CONF_TRAILER_SIZE  0x20
#define IPMI_MAX_INTEGRITY_PAD_SIZE IPMI_MAX_MD_SIZE
#define IPMI_MAX_AUTH_CODE_SIZE     IPMI_MAX_MD_SIZE

#define IPMI_REQUEST_MESSAGE_SIZE   0x07
#define IPMI_MAX_MAC_SIZE           IPMI_MAX_MD_SIZE /* The largest mac we ever expect to generate */

#define IPMI_SHA1_AUTHCODE_SIZE          12
#define IPMI_HMAC_MD5_AUTHCODE_SIZE      16
#define IPMI_MD5_AUTHCODE_SIZE           16
#define IPMI_HMAC_SHA256_AUTHCODE_SIZE   16

#define IPMI_SHA_DIGEST_LENGTH                20
#define IPMI_MD5_DIGEST_LENGTH                16
#define IPMI_SHA256_DIGEST_LENGTH             32

/*
 *This is accurate, as long as we're only passing 1 auth algorithm,
 * one integrity algorithm, and 1 encyrption alogrithm
 */
#define IPMI_OPEN_SESSION_REQUEST_SIZE 32
#define IPMI_RAKP1_MESSAGE_SIZE        44
#define IPMI_RAKP3_MESSAGE_MAX_SIZE    (8 + IPMI_MAX_MD_SIZE)

#define IPMI_MAX_USER_NAME_LENGTH      16
#if 0
extern const struct valstr ipmi_privlvl_vals[];
extern const struct valstr ipmi_authtype_vals[];
#endif

#define IPMI_LAN_MAX_REQUEST_SIZE 38 /* 45 - 7 */
#define IPMI_LAN_MAX_RESPONSE_SIZE 34 /* 42 - 8 */


extern int verbose;

struct ipmi_rq;
const  valstr ipmi_rakp_return_codes[] = {

    { IPMI_RAKP_STATUS_NO_ERRORS,                          "no errors"                           },
    { IPMI_RAKP_STATUS_INSUFFICIENT_RESOURCES_FOR_SESSION, "insufficient resources for session"  },
    { IPMI_RAKP_STATUS_INVALID_SESSION_ID,                 "invalid session ID"                  },
    { IPMI_RAKP_STATUS_INVALID_PAYLOAD_TYPE,               "invalid payload type"                },
    { IPMI_RAKP_STATUS_INVALID_AUTHENTICATION_ALGORITHM,   "invalid authentication algorithm"    },
    { IPMI_RAKP_STATUS_INVALID_INTEGRITTY_ALGORITHM,       "invalid integrity algorithm"         },
    { IPMI_RAKP_STATUS_NO_MATCHING_AUTHENTICATION_PAYLOAD, "no matching authentication algorithm"},
    { IPMI_RAKP_STATUS_NO_MATCHING_INTEGRITY_PAYLOAD,      "no matching integrity payload"       },
    { IPMI_RAKP_STATUS_INACTIVE_SESSION_ID,                "inactive session ID"                 },
    { IPMI_RAKP_STATUS_INVALID_ROLE,                       "invalid role"                        },
    { IPMI_RAKP_STATUS_UNAUTHORIZED_ROLE_REQUESTED,        "unauthorized role requested"         },
    { IPMI_RAKP_STATUS_INSUFFICIENT_RESOURCES_FOR_ROLE,    "insufficient resources for role"     },
    { IPMI_RAKP_STATUS_INVALID_NAME_LENGTH,                "invalid name length"                 },
    { IPMI_RAKP_STATUS_UNAUTHORIZED_NAME,                  "unauthorized name"                   },
    { IPMI_RAKP_STATUS_UNAUTHORIZED_GUID,                  "unauthorized GUID"                   },
    { IPMI_RAKP_STATUS_INVALID_INTEGRITY_CHECK_VALUE,      "invalid integrity check value"       },
    { IPMI_RAKP_STATUS_INVALID_CONFIDENTIALITY_ALGORITHM,  "invalid confidentiality algorithm"   },
    { IPMI_RAKP_STATUS_NO_CIPHER_SUITE_MATCH,              "no matching cipher suite"            },
    { IPMI_RAKP_STATUS_ILLEGAL_PARAMTER,                   "illegal parameter"                   },
    { 0,                                                   0                                     },
};
#include "auth.h"
#define IPMI_LAN_MAX_REQUEST_SIZE 38 /* 45 - 7 */
#define IPMI_LAN_MAX_RESPONSE_SIZE 34 /* 42 - 8 */

const valstr ipmi_priv_levels[] = {
    { IPMI_PRIV_CALLBACK, "callback" },
    { IPMI_PRIV_USER,     "user"     },
    { IPMI_PRIV_OPERATOR, "operator" },
    { IPMI_PRIV_ADMIN,    "admin"    },
    { IPMI_PRIV_OEM,      "oem"      },
    { 0,                  0          },
};
template <class G> class ipmi_intf;

class LANPlus
{
  public:
    using ipmi_Intf = ipmi_intf<LANPlus>;
    ipmi_Intf *intf;
    ipmi_rq_entry<LANPlus> * ipmi_req_entries;
    ipmi_rq_entry<LANPlus> * ipmi_req_entries_tail;
    uint8_t bridgePossible = 0;
    char name[16];//: "lan"
    char desc[128];//("IPMI v1.5 LAN Interface");
    uint8_t bridge_possible;
    ipmi_auth<LANPlus> *auth=0;
    int sessionsup_fail;
    ipmi_oem<LANPlus> *oemO=0;
    struct ipmi_rs rsp;
    LANPlus(ipmi_Intf *Intf)
    {
      this->intf = Intf;
      strcpy(name, "LANPlus");
      strcpy(desc, "IPMI v2 LAN Interface");
      ipmi_req_entries = 0;
      ipmi_req_entries_tail = 0;
      bridge_possible = 0;
      auth = new ipmi_auth<LANPlus>();
      sessionsup_fail = false;
      oemO = new ipmi_oem<LANPlus>(intf);
      Crypt = new LANPlusCrypt<LANPlus>(oemO);
    }
    ~LANPlus();
    LANPlusCrypt<LANPlus> *Crypt=0;
    int ipmi_send_packet( uint8_t * data, int data_len);
    int ipmi_open();
    int ipmi_setup();
    int ipmi_keepalive();
    void ipmi_set_max_rq_data_size( uint16_t size);
    int ipmi_lan_send_packet( uint8_t * data, int data_len);
    struct ipmi_rs * ipmi_recv_packet();
    struct ipmi_rs * ipmi_poll_recv();
    struct ipmi_rs * ipmi_send_cmd( struct ipmi_rq * req,struct ipmi_rs *);
    struct ipmi_rs * ipmi_send_payload(struct ipmi_v2_payload * payload);
    void getIpmiPayloadWireRep(
                                      struct ipmi_v2_payload * payload,  /* in  */
                                      uint8_t  * out,
                                      struct ipmi_rq * req,
                                      uint8_t    rq_seq,
                                      uint8_t curr_seq);
    void getSolPayloadWireRep(ipmi_Intf       * intf,
                                     uint8_t          * msg,
                                     struct ipmi_v2_payload * payload);
    void read_open_session_response(struct ipmi_rs * rsp, int offset);
    void read_rakp2_message(struct ipmi_rs * rsp, int offset, uint8_t alg);
    void read_rakp4_message(struct ipmi_rs * rsp, int offset, uint8_t alg);
    void read_session_data(struct ipmi_rs * rsp, int * offset, ipmi_session<LANPlus> *s);
    void read_session_data_v15(struct ipmi_rs * rsp, int * offset, ipmi_session<LANPlus> *s);
    void read_session_data_v2x(struct ipmi_rs * rsp, int * offset, ipmi_session<LANPlus> *s);
    void read_ipmi_response(struct ipmi_rs * rsp, int * offset);
    void read_sol_packet(struct ipmi_rs * rsp, int * offset);
    struct ipmi_rs * ipmi_recv_sol();
    struct ipmi_rs * ipmi_send_sol(struct ipmi_v2_payload * payload);
    int check_sol_packet_for_new_data(struct ipmi_rs *rsp);
    void ack_sol_packet(struct ipmi_rs * rsp);
    void ipmi_lanp_set_max_rq_data_size( uint16_t size);
    void ipmi_lanp_set_max_rp_data_size( uint16_t size);
    int get_requested_ciphers(int       cipher_suite_id,
                                      uint8_t * auth_alg,
                                      uint8_t * integrity_alg,
                                      uint8_t * crypt_alg);
    void lanplus_swap(
                      uint8_t * buffer,
                            int             length);
    void ipmi_build_v2x_msg(ipmi_Intf       * intf,     /* in  */
                                struct ipmi_v2_payload * payload,  /* in  */
                                int                    * msg_len,  /* out */
                                uint8_t         ** msg_data, /* out */
                                uint8_t curr_seq);
    ipmi_rq_entry<LANPlus> *ipmi_req_add_entry( struct ipmi_rq * req, uint8_t req_seq);
    ipmi_rq_entry<LANPlus> *ipmi_req_lookup_entry(uint8_t seq, uint8_t cmd);
    void ipmi_req_remove_entry(uint8_t seq, uint8_t cmd);
    void ipmi_req_clear_entries(void);
    int ipmi_handle_pong( struct ipmi_rs * rsp);
    int ipmiv2_ping();
    struct ipmi_rs *ipmi_poll_single();
    ipmi_rq_entry<LANPlus> *ipmi_build_v2x_ipmi_cmd(

                                    struct ipmi_rq * req,
                                    int isRetry);
    ipmi_rq_entry<LANPlus> *ipmi_build_v15_ipmi_cmd(

                                    struct ipmi_rq * req);
    int sol_response_acks_packet(
                             struct ipmi_rs         * rsp,
                             struct ipmi_v2_payload * payload);;

    int is_sol_packet(struct ipmi_rs * rsp);
    int is_sol_partial_ack(
                            struct ipmi_v2_payload * v2_payload,
#define IPMI_LAN_MAX_REQUEST_SIZE 38 /* 45 - 7 */
#define IPMI_LAN_MAX_RESPONSE_SIZE 34 /* 42 - 8 */
                struct ipmi_rs         * rs);
    void ipmi_set_max_rp_data_size(uint16_t size);
    void set_sol_packet_sequence_number(

                                                struct ipmi_v2_payload * v2_payload);
    int ipmi_get_auth_capabilities_cmd(struct get_channel_auth_cap_rsp * auth_cap);
    int ipmi_close_session_cmd();
    int ipmi_open_session();
    int ipmi_rakp1();
    int ipmi_rakp3();
    void ipmi_close();
    int ipmi_set_session_privlvl_cmd();
    void test_crypt1(void);
    void test_crypt2(void);
    int lanplus_get_requested_ciphers(int       cipher_suite_id,
                                      uint8_t * auth_alg,
                                      uint8_t * integrity_alg,
                                      uint8_t * crypt_alg);
    int get_session_setup_code(){return sessionsup_fail;};
  private:
/*    struct ipmi_intf ipmi_lanplus_intf = {
        .name = "lanplus",
        .desc = "IPMI v2.0 RMCP+ LAN Interface",
        .setup = ipmi_lanplus_setup,
        .open = ipmi_lanplus_open,
        .close = ipmi_lanplus_close,
        .sendrecv = ipmi_lanplus_send_ipmi_cmd,
        .recv_sol = ipmi_lanplus_recv_sol,
        .send_sol = ipmi_lanplus_send_sol,
        .keepalive = ipmi_lanplus_keepalive,
        .set_max_request_data_size = ipmi_lanp_set_max_rq_data_size,
        .set_max_response_data_size = ipmi_lanp_set_max_rp_data_size,
        .target_addr = IPMI_BMC_SLAVE_ADDR,
    };
*/
};
#endif /* NEWLIB_LANPLUS_H_ */
