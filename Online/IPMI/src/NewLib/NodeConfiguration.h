//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * NodeThread.h
 *
 *  Created on: Feb 11, 2016
 *      Author: beat
 */

#ifndef SOURCE_DIRECTORY__ONLINE_IPMI_SRC_NEWLIB_NODECONFIGURATION_H_
#define SOURCE_DIRECTORY__ONLINE_IPMI_SRC_NEWLIB_NODECONFIGURATION_H_

#include <string>
#include <mutex>
using namespace std;
class NodeConfiguration
{
  public:
    NodeConfiguration(string &N, string &U, string &P, string *sp,string IF = "",int statepoll=1, int setuppoll=10)
    {
      NodeName = N;
      UserName = U;
      Password = P;
      ServicePrefix = sp;
      Interface = IF;
      statePollDelay = statepoll;
      setupRetryDelay = setuppoll;
    }
    NodeConfiguration()
    {
      NodeName = "";
      UserName = "";
      Password = "";
      optRest = "";
      ServicePrefix = 0;
      Interface = "";
      this->statePollDelay = 1;
      this->setupRetryDelay =10;
    }
    string NodeName;
    string UserName;
    string Password;
    string *ServicePrefix;
    string optRest;
    string Interface;
    int statePollDelay;
    int setupRetryDelay;
    int cipher=0;
};
#endif /* SOURCE_DIRECTORY__ONLINE_IPMI_SRC_NEWLIB_NODECONFIGURATION_H_ */
