// callins.hxx: interface for the CallIns class.
//
//                                                  B. Franek
//                                                September 2020
//
//////////////////////////////////////////////////////////////////////
#ifndef CALLINS_HH
#define CALLINS_HH

#include "name.hxx"
#include "parms.hxx"
#include "instruction.hxx"

class Action;
class SMIObject;
class State;

class CallIns  : public Instruction
{
public:
	CallIns();	

	virtual ~CallIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
		
	int examine();
			
  	Name outString() const;


protected :

	int examineFunction
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction);

	int examineParameters
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction);
	   
	int checkCompatibility(Action* pFunctionAction);
		
//-------------   Data  ----------	
	Name _functionNm;

	Parms _parameters;

};

#endif 
