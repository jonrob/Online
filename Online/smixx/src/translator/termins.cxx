// termins.cxx: implementation of the TermIns class.
//
//                                                B. Franek
//                                           30 September 1999
//////////////////////////////////////////////////////////////////////

#include "termins.hxx"

#include "stdlib.h"
#include "smlline.hxx"
#include "utilities.hxx"
#include "name.hxx"
#include "smiobject.hxx"
#include "errorwarning.hxx"
#include "action.hxx"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TermIns::TermIns()  
{
   _name = "terminate_action";
   _endState = "";
   return;
}

TermIns::~TermIns()
{
    delete _pSMLcode;
}
//=======================================================================
void TermIns::translate() {

	Name token;
	int idel, jdel, ist, jst, inext, jnext;
	SMLline lineBeingTranslated;

	lineBeingTranslated = (*_pSMLcode)[0];
	char del = getNextToken(_pSMLcode,0,0," /",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
//	cout << token << endl; 
//	cout << " " << idel << " " << jdel << " " << inext << " " << jnext << endl;
	if (token == "TERMINATE_ACTION") {
		if ( del != '/' ) {
			ErrorWarning::printHead("ERROR",lineBeingTranslated);
			cout << "Expecting '/' ";
			cout << " but found " << token << endl;
			throw FATAL; 
		}
		ist = inext; jst = jnext;
		
		lineBeingTranslated = (*_pSMLcode)[ist];
		del = getNextToken(_pSMLcode,ist,jst,"=",token,idel,jdel,inext,jnext);
//cout << token << endl;
//cout << " " << idel << " " << jdel << " " << inext << " " << jnext << endl;
		if ( del != '=' ) {
			ErrorWarning::printHead("ERROR",lineBeingTranslated);
			cout << "Expecting '=' ";
			cout << " but found " << token << endl;
			throw FATAL; 
		}
		token.upCase(); token.trim();
		if ( token == "STATE" ) {}
		else {
			ErrorWarning::printHead("ERROR",lineBeingTranslated);
			cout << "Expecting keyword STATE ";
			cout << " but found " << token << endl;
			throw FATAL; 
		}
		ist = inext; jst = jnext;
		
		Name endState("");
		lineBeingTranslated = (*_pSMLcode)[ist];
		del = getNextToken(_pSMLcode,ist,jst," ",endState,idel,jdel,inext,jnext);
		_endState = endState;
	}
	else if (token == "ENDINSTATE" || token == "MOVE_TO") {
		if ( del != ' ' ) {
			ErrorWarning::printHead("ERROR",lineBeingTranslated);
			cout << " Expecting ' ' ";
			cout << " but found " << token << endl;
			throw FATAL; 
		}
		ist = inext; jst = jnext;

		Name endState;		
		lineBeingTranslated = (*_pSMLcode)[ist];
		del = getNextToken(_pSMLcode,ist,jst," ",endState,idel,jdel,inext,jnext);
		_endState = endState;
	}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Expecting TERMINATE_ACTION or ENDINSTATE or MOVE_TO";
		cout << " but found " << token << endl;
		throw FATAL; 
	}

	return;
}

void TermIns::out(const Name offset) const
{

	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn 
							<< "End state " << _endState.outString() << endl;
}
//-------------------------------------  BF April 2000  ---------------
void TermIns::outSobj( ofstream& sobj) const
{
	sobj << "terminate" << endl;

	sobj << _endState.stringForSobj() << endl;

	return;

}

//---------------------------------------------------------------------------
int TermIns::examine()
{
	int retcode = 0;
	SMLline firstLine = (*_pSMLcode)[0];
/*
  cout << endl 
  << " ====================== TermIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
*/
	Name stateName = _endState.name();
	Name parName = _endState.parName();
	
	int dummy;
	SMIObject* pParentObject = (SMIObject*)(_pParentUnit->parentPointer("Object")); 

	Action* pParentAction = (Action*)(_pParentUnit->parentPointer("Action"));
	Parms* pActionParameters = pParentAction->pActionParameters(); 

	if ( stateName != "" )
	{	
		if ( pParentObject->hasState(stateName,dummy) ) {}
		else
		{
		//	retcode = 1; review
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Object " << pParentObject->unitName() 
			<< " does not have state " << stateName <<
			" declared " << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;      
		}
	}
	else
	{
		if ( !_endState.paramAccessible(pActionParameters ) )
		{
		//	retcode = 1; review
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Object " << pParentObject->unitName() 
			<< " parameter" << parName <<
			" is not accessible " << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;  		
		}
	}
	
	int iflg = examineUnits();
	
	return retcode+iflg;
}

//-------------------------------------------------------------------------------------------------------
Name TermIns::outString() 
{
	Name temp;
	
	temp = "MOVE_TO ";
	temp += _endState.outString();
	
	return temp;
}



