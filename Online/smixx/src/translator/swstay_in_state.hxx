// swstay_in_state.hxx: interface for the SWStay_in_State class.
//
//                                                  B. Franek
//                                                 July 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef SWSTAY_IN_STATE_HH
#define SWSTAY_IN_STATE_HH

#include "whenresponse.hxx"
#include "smlunit.hxx"

class SWStay_in_State : public SMLUnit, public WhenResponse
{
public:
	SWStay_in_State();

	~SWStay_in_State() {};

	void translate();
	
	int examine();

	void outSobj(ofstream& sobj) const;

	void out(const Name offset) const 
	             { cout << offset.getString() 
		        << "STAY_IN_STATE" << endl; return; };

protected :

};

#endif 
