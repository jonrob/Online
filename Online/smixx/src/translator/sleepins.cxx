// sleepins.cxx: implementation of the SleepIns class.
//
//                                                B. Franek
//                                                June 2011
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "sleepins.hxx"
#include "registrar.hxx"
#include "errorwarning.hxx"
#include "objectregistrar.hxx"

	extern Registrar allObjectSets;
	extern Registrar allUnits;
	extern ObjectRegistrar allSMIObjects;
	
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SleepIns::SleepIns()  
{
   _name = "sleep";
   return;
}

SleepIns::~SleepIns()
{
    delete _pSMLcode;
}

void SleepIns::translate() {

	Name token; int idel,jdel; int inext,jnext;
	Name inFrom;
	
	SMLline lineBeingTranslated = (*_pSMLcode)[0];
	
	getNextToken(_pSMLcode,0,0," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	Name seconds("");
	if ( token == "SLEEP" )
	{
		lineBeingTranslated = (*_pSMLcode)[inext];
		getNextToken(_pSMLcode,inext,jnext," ",seconds,idel,jdel,inext,jnext);
	
	}
	else
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"unrecognised code encountered while attempting to parse SLEEP instruction");
		throw FATAL;
	}

	_seconds = seconds;
			
    if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "There is some crap following a SLEEP instruction" << endl;
		throw FATAL;
	}
	return;
	
}

//--------------------------------------------------------------------------
void SleepIns::out(const Name offset) const
{
	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn ;
	cout << " sleep " << _seconds.outString() <<  endl;

	return;
}
//------------------------------------------  BF June  2011  -----------
void SleepIns::outSobj(ofstream& sobj) const
{

	sobj << "sleep" << endl;


        sobj << _seconds.stringForSobj() << endl;
	
	return;
}
//---------------------------------------------------------------------------
int SleepIns::examine()
{
	int retCode = 0;
	
	SMLline firstLine = (*_pSMLcode)[0];
	
/*	
//beg debug
  cout << endl 
  << " ====================== DoIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
	
	cout << "Parent Unit Code " << endl;	

	char temp[] = " "; _pSMLcode->out(temp);	
//end debug
*/

	SMIObject* pParentObject = (SMIObject*)(_pParentUnit->parentPointer("Object")); 
	State* pParentState = (State*)(_pParentUnit->parentPointer("State")); 
	Action* pParentAction = (Action*)(_pParentUnit->parentPointer("Action")); 

	Name actualType(""); int err;	
	Name actualValue = _seconds.actualValue(allSMIObjects,
                                            pParentObject,
                                            pParentState,
                                            pParentAction,
                                            actualType,err);

	if ( err != 0 )
	{
		retCode = 1;
		ErrorWarning::printHead("SEVERE WARNING",firstLine);
		cout << " Can not determine the value type in indivalue "
		 << _seconds.outString() <<  "  Error : " << actualValue << endl;
		return retCode;
	}

	if ( actualType != "INT" )
	{
		retCode = 1;
		ErrorWarning::printHead("SEVERE WARNING",firstLine);
		cout << " The type of seconds must be integer and not "
		 << actualType << endl;
	}

	return retCode;

}
