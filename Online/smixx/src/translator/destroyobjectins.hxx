// destroyobjectins.hxx: interface for the DestroyObjectIns class.
//
//                                                  B. Franek
//                                                 January 2020
//
//////////////////////////////////////////////////////////////////////
#ifndef DESTROYOBJECTINS_HH
#define DESTROYOBJECTINS_HH

#include "instruction.hxx"
#include "varelement.hxx"

class DestroyObjectIns  : public Instruction 
{
public:
	DestroyObjectIns();

	virtual ~DestroyObjectIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;


protected :

	VarElement _objectId;
  
};

#endif 
