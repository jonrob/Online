// whenresponse.hxx: interface for the WhenResponce class.
//
//                                                  B. Franek
//                                                 July 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef WHENRESPONSE_HH
#define WHENRESPONSE_HH

class SMLlineVector;

class WhenResponse 
{
public:
	WhenResponse() {return;}

	virtual ~WhenResponse() {return;}

	virtual void translate() =0;
	
	virtual int examine() =0;

	virtual void outSobj(std::ofstream& sobj) const =0;
	
	virtual void out(const Name offset) const =0;
	
	virtual SMLlineVector* getEndStateActionCodePointer() const {return NULL;}


protected :

};

#endif 
