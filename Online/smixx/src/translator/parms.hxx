//----------------------  Class   Parms  -----------------------------------
//
//
//                                                      Author: Boda Franek
//                                                      Date : 10 June 1998
//                                                Revised: BF 26 March 1999
//                                                Revised: BF March 2014
//                                                Revised: BF October 2014
//                                                Revised: BF February 2021
//----------------------------------------------------------------------------
#ifndef PARMS_HH
#define PARMS_HH
#include <vector>
#include "name.hxx"
#include "param.hxx"
#include "parmsbase.hxx"
class SMLlineVector;
//------------------------------------------------------------------
class Parms  : public ParmsBase
{
	public :
		
		Parms();
		
		Parms(const Parms&);
		
		~Parms(); 
		
		Parms& operator=(const Parms&);

								   
//---------------  Methods used by Translator   --------------------

// when something goes wrong, Translator is terminated inside the method
// that means it can be void now.

		void initFromSMLcode(const int declaration, SMLlineVector* pSMLcode,
				const int ist, const int jst,  
				int& inext, int& jnext);
	private :

		void getNextParameter(const int declaration, SMLlineVector* pSMLcode,
							  const int ist, const int jst,
							  int& idel, int& jdel, char& del,
							  int& inext, int& jnext);
};

#endif
