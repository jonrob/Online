//----------------------  Class  Parms  -----------------------------------
// This class is going eventually to replace all the other parameter clases
// this was a dream (BF March 2014)
//
//                                                      Author: Boda Franek
//                                                      Date : 10 June 1998
//----------------------------------------------------------------------------
#include <stdlib.h>
#include "smixx_common.hxx"
#include <string.h>
#include <assert.h>
#include "name.hxx"
#include "smlline.hxx"
#include "smllinevector.hxx"
#include "parms.hxx"
#include "utilities.hxx"
#include "errorwarning.hxx"

//-----------------------   Constructors -------------------------------------
  Parms::Parms() 
  	:ParmsBase()
  {}
//------------------------
  Parms::Parms(const Parms &parms)
  	:ParmsBase(parms)
  {}
//-----------------------------------------------------------------------
  Parms::~Parms() { return; }
//-------------------------------- assignement -------------------------
  Parms& Parms::operator=(const Parms &parms)
  {
       ParmsBase::operator=(parms);
	   return *this;
  }
//------------------------------------------- BF 28-March-2000 ------------------
void Parms::initFromSMLcode(const int declaration,SMLlineVector* pSMLcode,
				const int ist, const int jst,  
				int& inext, int& jnext)
 {
	int inbl,jnbl,iprev,jprev; 
	char item = firstNonBlank(pSMLcode,ist,jst,inbl,jnbl,inext,jnext,iprev,jprev);
	if (item == '\0' || item == '#' || item == '!' ) {
		ErrorWarning::printHead("ERROR",(*pSMLcode)[ist],
		                        "No parameters found");
		throw FATAL;
	}

	char firstChar = item;


	int istt,jstt,idel,jdel; char del;
	if (firstChar == '(' ) {istt = inext; jstt = jnext;}
	else				{istt=inbl; jstt=jnbl;}

//cout << "First char |" << firstChar << "|" << endl;
//pSMLcode->out();

    for (;;) {
		getNextParameter(declaration, pSMLcode, istt,jstt,
			idel,jdel,del,inext,jnext);

//cout << "delimiter |" <<del<<"|"<< " inext jnext " << inext << " " << jnext << endl;
		if (del==')') {
			if (firstChar == '(') {return;}
			else
			{
				ErrorWarning::printHead("ERROR",(*pSMLcode)[istt],
		                "Brackets do not balance while parsing parameters");
				throw FATAL;
			}	
		}
		if (inext<0) {
			if (firstChar == '(')
			{
				ErrorWarning::printHead("ERROR",(*pSMLcode)[istt],
				"Brackets do not balance while parsing parameters");
				throw FATAL;
			}
			else { return; }
		}
		istt = inext; jstt = jnext;
	}
	return;
 }

//------------------------------------------- BF 28-March-2000 ------------------
void Parms::getNextParameter(const int declaration, SMLlineVector* pSMLcode,
							const int ist, const int jst,
					   int& idel, int& jdel, char& del, int& inext, int& jnext)
{
// the next parameter will look like:
//   [type] name indiValue
// If it is a declaration, then type can be STRING, INT or FLOAT and 
// indiValue does not need to be present. If present, it can only be 
// 'direct' value. Examples: PARS or PARS="string" or int PARI=7 or float PARF
// If it is not a declaration, i.e. those found in DO and CALL instructions,
// then type is not present and indiValu must be present
// Example PARS=7.5 or PARS='local-par-name' 

	SMLline line;
	
	SMLline lineToReport;  // in case of errors

//cout << " ist jst " << ist << " " << jst << endl;

	int istt,jstt; int ierr;
	Name token;
	Name declaredType,name,indiValue,typev; Name form;
	
	lineToReport = (*pSMLcode)[ist];
	del = getNextToken(pSMLcode,ist,jst," ,=)",token,idel,jdel,inext,jnext);
	if ( token == "\0" ) {
		ErrorWarning::printHead("ERROR",lineToReport,
		                        "parameters parsing - no parameters found");
		throw FATAL;
	}
//cout << " Getting the next parm: token :" 
//<< token << " delimiter after |" << del << "|" <<endl;
	if (declaration) {
		token.upCase(); token.trim();
		if ( token == "STRING" ||
		     token == "INT" ||
			 token == "FLOAT" ) {
			declaredType = token;
				// now get the 'name'
			istt = inext; jstt = jnext;
			lineToReport = (*pSMLcode)[istt];
			del = getNextToken(pSMLcode,istt,jstt,",=)",
			                       name,idel,jdel,inext,jnext);
			if ( name == "\0" ) // name not found
			{
				ErrorWarning::printHead("ERROR",lineToReport);	
				cout << " parameter name after a valid type not found" << endl;
				throw FATAL;
			}			
		}
		else
		{  // must be the name
			declaredType = "STRING";
			name = token;		
		}	
		name.upCase(); name.trim();
		if(!check_name(name))
		{
			ErrorWarning::printHead("ERROR",lineToReport);
			cout << " String " << name << " is not a name " << endl;
			throw FATAL;
		}	
// we now have declaredType and name
		if ( del == '=' )
		{  // what follows is the direct value
			line = (*pSMLcode)[inext];

			lineToReport = (*pSMLcode)[inext];
			del = getIndiValue(pSMLcode,inext,jnext,",)",indiValue,form,ierr,
				idel,jdel,inext,jnext);
			if (ierr != 0 )
			{
				ErrorWarning::printHead("ERROR",lineToReport);
				cout << " Error parsing parameter indiValue" << endl;
				throw FATAL;
			}
			if ( !(declaredType == form) )
			{
				ErrorWarning::printHead("ERROR",lineToReport);
				cout << "Parameter value inconsistent with type" << endl;
				throw FATAL;
			}
			Param tempPar(name,indiValue,declaredType);
			addParam(tempPar); 		
		}
		else
		{   // no value
			Param tempPar(name,paramcons::noval,declaredType);
			addParam(tempPar);
		}
		return;
	}
//  This is no declaration so we do not have any type and we must have value
// that means that the token is the name
	name = token;

	name.upCase(); name.trim();
	if(!check_name(name))
	{
		ErrorWarning::printHead("ERROR",lineToReport);
		cout << " String " << name << " is not a name " << endl;
		throw FATAL;
	}	
// there now must follow the indiValue. 
    if (del != '=')
	{
		ErrorWarning::printHead("ERROR",lineToReport);
		cout << " Parameter " << name << " is missing the indiValue " << endl;
		throw FATAL;
	}	
	line = (*pSMLcode)[inext];

	lineToReport = (*pSMLcode)[inext];
	del = getIndiValue(pSMLcode,inext,jnext,",)",indiValue,form,ierr,
				idel,jdel,inext,jnext);

	if (ierr != 0 || form == "UNKNOWN")
	{
		ErrorWarning::printHead("ERROR",lineToReport);
		cout << " Error parsing parameter indiValue" << endl;
		throw FATAL;
	}
	if (form == "STRING" ||
	    form == "INT" ||
		form == "FLOAT")
	{
		Param tempPar(name,indiValue,form);
		addParam(tempPar);
	}
	else 
	{
		Param tempPar(name,indiValue,paramcons::undef);
		addParam(tempPar);
	}

	return;
}

