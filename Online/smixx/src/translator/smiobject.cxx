//-----------------------------------------------------------------------
//                         SMIObject  Class
//                                                 B. Franek
//                                                 21 March 1999
//-----------------------------------------------------------------------
//
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "utilities.hxx"
#include "smiobject.hxx"
#include "attributeblock.hxx"
#include "parameterblock.hxx"
#include "state.hxx"
#include "action.hxx"
#include "smlline.hxx"
#include "nmdptnrvector.hxx"
#include "nmdptnr.hxx"
#include "errorwarning.hxx"
#include "smlsectiontype.hxx"

//--------------------------- Constructors -------------------------------

SMIObject::SMIObject
( const Name& name, int cls, int assoc) 
: SMLUnit("Object",100,name),
   _class(cls), _associated(assoc),
  _isOfClass("\0"),
  _pAttributeBlock(NULL), _pParameterBlock(NULL),
  _functions(),
  _states()
{
//	cout << endl << "Object  " << _name << endl;
}

SMIObject::~SMIObject() {
    delete _pSMLcode;
}

Name SMIObject::name() const
{
	return _name;
}

//-----------------------------------------------------------------------------
void SMIObject::translate() 
{
// The code of Object unit is in _pSMLcode begining with line zero.
// It consists of 4 sections:
//
//     1) Attribute Block. This consists of all the lines at the beginning
//        of Object unit starting with line zero plus all the lines up to
//        the PARAMETERS or FUNCTIONS or STATES section.
//
//     2) PARAMETERS section  with all PARAMETERS declarations
//
//     3) FUNCTION section with all the function declarations
//
//     4) STATE section with all the STATES.

	int istart;   // first line number (in _pSMLcode vector) of the section
	              // to be processed.
	sectionType_t nextSection;  // this indicates what type of section is following
	                  // the current (processed) section.
	int next_istart; // first line number (in _pSMLcode vector) of
	                      // the section following the processed section..
//_pSMLcode->out(" ");		
	istart = 0;
	processAttributeBlock(istart,next_istart,nextSection);
	if ( nextSection == SECTION_NULL )   // nothing else is following
	{	
		ErrorWarning::printHead("FATAL",(*_pSMLcode)[next_istart]);
		cout << " Empty Object" << endl;
		throw FATAL;
	}
	  
	
	if ( nextSection == SECTION_PARAMETERS )
	{
		istart = next_istart;
		processParameters(istart,next_istart,nextSection);
		if ( nextSection == SECTION_NULL ) { return;}
	}
	else
	{    // always create parameter block. Need it for historical reasons
		_pParameterBlock = new ParameterBlock();
		assert(_pParameterBlock != 0);
	}
		
	if ( nextSection == SECTION_FUNCTIONS )
	{
		istart = next_istart;
		processFunctions(istart,next_istart,nextSection);
		if ( nextSection == SECTION_NULL ) { return; }
	}
	
	if ( nextSection == SECTION_STATES )
	{
		istart = next_istart;
		processStates(istart,next_istart,nextSection);
		if ( nextSection == SECTION_NULL ) 
		{ // end of successfull translation
//			Name temp(" "); out(temp); 
			return;
		}
	}
	
	ErrorWarning::printHead("FATAL",(*_pSMLcode)[next_istart]);
	cout << " missplaced section" << endl;
	throw FATAL;
	
	return;

}
//---------------------------------------------  BF April 2000  ------------------
void SMIObject::outSobj(ofstream& sobj) const
{
	if ( _class ) {
		sobj << "*CLASS" << endl;
	}
	else {
		sobj << "*OBJECT" << endl;
	}
	char* pStr = _name.getString();
	sobj << pStr << endl;

	int n1,n2,n3;

	n1 = 0;

//	n2 = _pAttributeBlock->numOfAttributes();
	n2 = _pAttributeBlock->_attributes.length();
	n3 = _associated;

	char line[80];

	if (_class) {
		sprintf(line,"%5d%5d",n2,n3);
	}
	else {
		sprintf(line,"%5d%5d%5d",n1,n2,n3);
	}

	sobj << line << endl;

	for (int i=0; i<n2; i++) {
//		sobj << (_pAttributeBlock->attribute(i)).getString() << endl;
		sobj << _pAttributeBlock->_attributes[i].getString() << endl;
	}

//	int numpar = _pParameterBlock->numOfParameters();
	int numpar = _pParameterBlock->_parameters.numOfEntries();
	if (numpar > 0) { 
		sobj << "*PARAMETERS" << endl;
	}

	for (int ip=0; ip<numpar; ip++) {
		Name name, indiValue, type;	

		Param par = _pParameterBlock->_parameters.getParam(ip);
		type =	par.paramDeclType();
		name = par.paramName();
		indiValue = par.paramIndiValue();

		sobj << type.getString() << endl;
		sobj << name.getString() << endl;
		sobj << indiValue.getString() << endl;
	}

	if (numpar > 0) { sobj << "*END_PARAMETERS" << endl;}

// Now Functions  ---------------------------------------------

	int nfun = _functions.length();

	for (int ifun=0; ifun < nfun; ifun++) {
		void* ptnvoid = _functions.gimePointer(ifun) ;
		Action* pFunction;
		pFunction = (Action*)ptnvoid;
		pFunction->outSobj(sobj);
	}
//-------------------------------------------------------------

//  and finally states

	int nStates = _states.length();


	for (int ist=0; ist < nStates; ist++) {
		void* ptnvoid = _states.gimePointer(ist) ;
		State* pState;
		pState = (State*)ptnvoid;
//	cout << ist << pState->unitName() << endl;
//	pState->out(" ");
	
		pState->outSobj(sobj);
	}

	if ( _class ) { sobj << "*END_CLASS" << endl;}
	else { sobj << "*END_OBJECT" << endl; }

	return;

}
//------------------------------------------------------------------------
bool SMIObject::isClass() const
{
	if ( _class == 0 )
	{
		return false;
	}
	else
	{
		return true;
	}
}
//------------------------------------------------------------------------
bool SMIObject::isAssociated() const
{
	if ( _associated == 0 )
	{
		return false;
	}
	else
	{
		return true;
	}
}
//-------------------------------------------------------------------------
int SMIObject::getStates( NameVector& states,int& undeclared) const
{

	int nStates = _states.length();
	undeclared = 0;
	
	for (int ist=0; ist<nStates; ist++) {
		void* ptnvoid = _states.gimePointer(ist);
		State* pState;
		pState = (State*)ptnvoid;
		states+= pState->unitName();
		if ( pState->isUndeclared() == true ) { undeclared = 1; }
	}
	
	return nStates;
}
//-------------------------------------------------------------------------
bool SMIObject::hasState(const Name& stateNm, int& undeclared) const
{
	NameVector states;
	
	int nStates = getStates(states,undeclared);

	if (undeclared) { return true; }

	for (int ist=0; ist<nStates; ist++) {
		if ( stateNm == states[ist] ) { return true; }
	}
	
	return false;
}
//---------------------------------------------------------------------------
int SMIObject::examine()
{
	int retcode = 0;
	
/*
  cout << endl 
  << " ====================== SMIObject::examine() ============= " << endl;
*/

	Name stateNm("DEAD"); // I need a name and this is as good as any
	int undeclared;
	
	hasState(stateNm,undeclared);
	
	if ( undeclared == 1 )
	{
		ErrorWarning::printHead("WARNING",(*_pSMLcode)[0]
		,"undeclared states");
		cout << endl << " There are undeclared states." << endl 
		     << " This makes it more difficult" << endl
		     << " to perform consistency checks " << endl;
	}
	
	int iflg = examineUnits();
	
	return retcode+iflg;
}
//-------------------------------------------------------------------------------------------------------
Name SMIObject::outString() 
{
	Name temp;
	
	if ( _class == 1 ) { temp = "CLASS : "; }
	else               { temp = "OBJECT : "; }
	
	temp += _name;
	
// to do:  add the attributes	
	return temp;
	
}
//----------------------------------------------------------------------------
bool SMIObject::hasAction( const Name& actionNm ) const
{
	int numStates = _states.length();
	
	void* ptnvoid; State* pstate;
	
	for ( int i=0; i<numStates; i++ )
	{
		ptnvoid = _states.gimePointer(i);
		pstate = (State*)ptnvoid;
		
		if ( pstate->hasAction(actionNm) ) { return true; }
	}
	
	return false;
}
//----------------------------------------------------------------------------
bool SMIObject::hasFunction( const Name& functionNm ) const
{
	int numFunctions = _functions.length();
	
	void* ptnvoid; Action* pfunc;
	
	for ( int i=0; i<numFunctions; i++ )
	{
		ptnvoid = _functions.gimePointer(i);
		pfunc = (Action*)ptnvoid;
		
		if ( pfunc->name() == functionNm ) { return true; }
	}
	
	return false;
}
//----------------------------------------------------------------------
Action* SMIObject::gimePointerToFunction(const Name& funNm) const
{
	Action* pFun;
	pFun = static_cast<Action*>(_functions.gimePointer(funNm));
	return pFun;
}
//------------------------------------------------------------------------------
int SMIObject::candidateActions
		      (const Name& actionNm, const int& noPars,
		       NmdPtnrVector& pActions)
{
	int numOfCandidates = 0;
	
	int numStates = _states.length();
	
	void* ptnvoid; State* pstate;
	
	for ( int is=0; is<numStates; is++ )
	{
		ptnvoid = _states.gimePointer(is);
		pstate = (State*)ptnvoid;
		
		int numAct = pstate->numActions();
		Name stateNm = pstate->unitName();
		
		for ( int ia=0; ia<numAct; ia++ )
		{
			Action* pAct = pstate->gimeActionPointer(ia);
			
			if ( pAct->name() == actionNm &&
			     pAct->numParameters() >= noPars )
			{
				NmdPtnr temp(stateNm,pAct);
				pActions += temp;
				numOfCandidates++;
			}
		}
	}
	

	return numOfCandidates;
}
//----------------------------------------------------------------------------
Parms* SMIObject::pObjectParameters()
{
	return &(_pParameterBlock->_parameters);
}
//---------------------------------------------------------------------------------------------
void SMIObject::processAttributeBlock(int& istart, int& next_istart, sectionType_t& nextSection)
{
//  create
	_pAttributeBlock = new AttributeBlock();
	assert (_pAttributeBlock != 0);

	SMLUnit* pUnit = _pAttributeBlock;
	Name unitType = "attribute block";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
//------------------------------

// collect
	SMLlineType_t nextUnit;
	
	std::vector<SMLlineType_t> termTypes;
	termTypes.push_back(PARAMETERS_LINE);
	termTypes.push_back(FUNCTION_LINE);
	termTypes.push_back(STATE_LINE);
	
	collectUnit(pUnit,istart,termTypes,next_istart, nextUnit);
	
	if ( nextUnit == PARAMETERS_LINE ) { nextSection = SECTION_PARAMETERS; }
	else if ( nextUnit == FUNCTION_LINE ) { nextSection = SECTION_FUNCTIONS; }
	else if ( nextUnit == STATE_LINE ) { nextSection = SECTION_STATES; }
	else if ( nextUnit == END_LINE ) { nextSection = SECTION_NULL; }
	else { nextSection = SECTION_UNEXPECTED; }		
// translate
	pUnit->translate();

	return;
}  
//---------------------------------------------------------------------------------------------
void SMIObject::processParameters(int& istart, int& next_istart, sectionType_t& nextSection)
{
//  create
	_pParameterBlock = new ParameterBlock();
	assert (_pParameterBlock != 0);

	SMLUnit* pUnit = _pParameterBlock;
	Name unitType = "parameter block";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
//------------------------------

// collect
	
	SMLlineType_t nextUnit;
	
	std::vector<SMLlineType_t> termTypes;
	termTypes.push_back(FUNCTION_LINE);
	termTypes.push_back(STATE_LINE);
	
	collectUnit(pUnit,istart,termTypes,next_istart, nextUnit);

//	collectUnit(pUnit,istart,STATE_LINE,END_LINE,
//            next_istart, nextUnit);
	
	if ( nextUnit == STATE_LINE ) { nextSection = SECTION_STATES; }
	else if ( nextUnit == FUNCTION_LINE ) { nextSection = SECTION_FUNCTIONS; }
	else if ( nextUnit == END_LINE ) { nextSection = SECTION_NULL; }
	else { nextSection = SECTION_UNEXPECTED; }		
// translate
	pUnit->translate();

	return;
}
//-------------------------------------------------------------------------------------
void SMIObject::createCollectTranslateState
		        (int& istart, int& next_istart, SMLlineType_t& nextUnit)
{
//  create
	SMLline line; Name name;
	line = (*_pSMLcode)[istart];

	line.lineType(name); // the first line is STATE_LINE. Pick up the State name
	
//cout << endl << " createCollectTranslateState istart " << istart
//      << " First line:" << line << endl;
	
	State* pState = new State(name);
	assert (pState != 0);

	SMLUnit* pUnit = pState;
	Name unitType = "State";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
	
	Name registeredName = name; 
	int iflag = _states.add(registeredName,pUnit);
	if (iflag != 1)
	{
		ErrorWarning::printHead("ERROR",line);
		cout << unitType << registeredName 
		<< " has allready been declared" << endl;
		throw FATAL;
	}	
// collect
	
	std::vector<SMLlineType_t> termTypes;
	termTypes.push_back(STATE_LINE);
	
	collectUnit(pUnit,istart,termTypes,next_istart, nextUnit);

//	collectUnit(pUnit, istart, STATE_LINE, END_LINE, next_istart, nextUnit);

// translate
	pUnit->translate();

	return;	

} 
//------------------------------------------------------------------------------------------------
void SMIObject::processStates(int& istart, int& next_istart, sectionType_t& nextSection)
{
	SMLlineType_t nextUnit;
	
	while (1==1)
	{
		createCollectTranslateState( istart, next_istart, nextUnit);
		
		if ( nextUnit != STATE_LINE ) break;
		istart = next_istart;
	}

	if ( nextUnit == END_LINE ) { nextSection = SECTION_NULL; }
	else { nextSection = SECTION_UNEXPECTED; }
	
	//  anything else is misplaced unit. Need error warning!!
}
//-------------------------------------------------------------------------------------
void SMIObject::createCollectTranslateFunction
		        (int& istart, int& next_istart, SMLlineType_t& nextUnit)
{
//  create
	SMLline line; Name name;
	line = (*_pSMLcode)[istart];

	line.lineType(name); // the first line is FUNCTION_LINE. Pick up the Function name
	
//cout << endl << " createCollectTranslateFunction istart " << istart
//      << " First line:" << line << endl;
	
	Action* pFunction = new Action(name);
	assert (pFunction != 0);

	SMLUnit* pUnit = pFunction;
	Name unitType = "Function";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
	
	Name registeredName = name; 
	int iflag = _functions.add(registeredName,pUnit);
	if (iflag != 1)
	{
		ErrorWarning::printHead("ERROR",line);
		cout << unitType << registeredName 
		<< " has allready been declared" << endl;
		throw FATAL;
	}	
// collect
	
	std::vector<SMLlineType_t> termTypes;
	termTypes.push_back(FUNCTION_LINE);
	termTypes.push_back(STATE_LINE);
	
	collectUnit(pUnit,istart,termTypes,next_istart, nextUnit);

//	collectUnit(pUnit, istart, STATE_LINE, END_LINE, next_istart, nextUnit);

// translate
	pUnit->translate();

	return;	

} 
//------------------------------------------------------------------------------------------------
void SMIObject::processFunctions(int& istart, int& next_istart, sectionType_t& nextSection)
{
	SMLlineType_t nextUnit;
	
	while (1==1)
	{
		createCollectTranslateFunction( istart, next_istart, nextUnit);
		
		if ( nextUnit != FUNCTION_LINE ) break;
		istart = next_istart;
	}

	if ( nextUnit == END_LINE ) { nextSection = SECTION_NULL; }
	else if ( nextUnit == STATE_LINE ) { nextSection = SECTION_STATES; }
	else { nextSection = SECTION_UNEXPECTED; }
	
	//  anything else is misplaced unit. Need error warning!!
}
//-------------------------------------------------------------------------------
Name SMIObject::objCurrState() const
{
	Name temp("current state of ");
	temp += name();
	return temp;   // in Translator is meaningless
}
//------------------------------------------------------------------------------
Name SMIObject::objCurrAction() const
{
	Name temp("current action of ");
	temp += name();
	return temp;  // in Translator is meaningless
}
