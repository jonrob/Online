// stack.cxx: implementation of the Stack class.
//
//                                                B. Franek
//                                           23 February 2000
//////////////////////////////////////////////////////////////////////

#include "stackitem.hxx"
#include "stack.hxx"

#include "stdlib.h"
#include "assert.h"
#include "name.hxx"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Stack::Stack(int incr) : _size(0), _incr(incr), _s(NULL), _top(EMPTY)  
{	 
	return;
}

Stack::~Stack()
{
	delete [] _s;
    return;
}
void Stack::push(const char item)
{
	StackItem temp(item);
	push(temp);
	return;
}
void Stack::push(BoolItem* pBoolItem)
{
	StackItem temp(pBoolItem);
	push(temp);
	return;
}


//---------------------------------------------------------------------
void Stack::push(const StackItem& source) {
	if ( (_top+1) == _size )    // no more space for another item
	{
		StackItem* snew = new StackItem[_size+_incr];
		assert(snew != 0);
		
		if (_size > 0)
		{
			for (int i=0; i<_size; i++)
			{
				*(snew+i) = *(_s+i);
			}
			delete [] _s;
		}
		
		_s = snew;
		_size = _size + _incr;
//cout << " Stack size increased to " << _size << endl;
	}

	_top++;
	*(_s+_top) = source;
	return;
}
//----------------------------------------------------------------------
int Stack::getFromStack(const int num, StackItem& sitem) const
{
	if ( (_top+1) < num ) {return 0;}
	sitem = *(_s+_top-num+1);
	return 1;
}
//----------------------------------------------------------------------
void Stack::deleteItem2FromStack()
{
	*(_s+_top-1) = *(_s+_top);
	_top = _top - 1;
}
//----------------------------------------------------------------------
void Stack::deleteTopStack(const int num)
{
	if (num <= 0) {return;}

	_top = _top - num;
	if (_top < 0) _top = EMPTY;
}
//----------------------------------------------------------------------
void Stack::out(const Name offset) const
{
	char* ptn=offset.getString(); cout << ptn << "Stack";
	
	cout << endl; 
	cout << "  _top = " << _top << "  _size = " << _size << endl;
	for (int i=0; i<= _top; i++) {
		cout << " ";
		(_s+i)->out(offset);
	}
	cout << endl;
	return;
}
