//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  March 2017
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------

#include "reservednames.hxx"

#include "smiobject.hxx"
#include "state.hxx"
#include "action.hxx"
extern Name smiDomain;

const char* ReservedNames::rnames[] =
{
	"_DOMAIN_",
	"_OBJECT_",
	"_STATE_",
	"_ACTION_"
};

bool ReservedNames::isReserved(Name& name)
{
	for (int i=0; i<num; i++)
	{
		if (name == rnames[i]) {return true;}
	}
	return false;
}
//-------------------------------------------------------------------------
int ReservedNames::getType(const Name& name,
	                   const SMIObject* pParentObject,
			   const State* pParentState,
			   const Action* pParentAction,
			   Name& type)
{
	type = "STRING";
	if ( name == "_DOMAIN_" ) {return 1;}
	if ( name == "_OBJECT_" && pParentObject != 0 ) {return 1;}
	if ( name == "_STATE_" && pParentState != 0 ) {return 1;}
	if ( name == "_ACTION_" && pParentAction != 0 ) {return 1;}
	
	return 0;
}
//-------------------------------------------------------------------------
int ReservedNames::getValue(const Name& name,
	                   SMIObject* pParentObject,
			   State* pParentState,
			   Action* pParentAction,
			   Name& value)
{
	value = "unknown";
	if ( name == "_DOMAIN_" )
	{
		value = smiDomain;
		return 1;
	}
	if ( name == "_OBJECT_" )
	{
		if ( pParentObject == 0 ) { return 0; }
		value = pParentObject->name();
		return 1;
	}
	if ( name == "_STATE_" )
	{
		if ( pParentState == 0 ) { return 0; }
		value = pParentState->stateName();
		return 1;
	}
	if ( name == "_ACTION_" )
	{
		if ( pParentAction == 0 ) { return 0; }
		value = pParentAction->actionName();
		return 1;
	}

	
	return 0;
}
