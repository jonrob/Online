//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  March 2016
// Copyright Information:
//      Copyright (C) 1996-2016 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifdef _WIN32
#	include <time.h>
#	include <sys/timeb.h>
#else
#	include <sys/time.h>
#endif

#include <stdio.h>	// this is where NULL is defined

#include "timehandling.hxx"
//------------------------------------------------------------------------
double TimeHandling::floatTime()
{
	long seconds;
	int millies;
	double fseconds;

#ifdef WIN32
       struct timeb timebuf;
       
       ftime(&timebuf);
       
       seconds = (long)timebuf.time;
       millies = timebuf.millitm;
#else

       struct timeval tv;

       gettimeofday(&tv, NULL);
       
       seconds = (long)tv.tv_sec;
       millies = (int)((float)tv.tv_usec / 1000. + 0.5);
#endif
	
	fseconds = seconds;
	fseconds = fseconds + double(millies/1000.);
	
	return fseconds;
}
