//
//  options.hxx
//  smiSM-Options
//
//  Created by Bohumil Franek on 28/03/2016.
//  Copyright © 2016 Bohumil Franek. All rights reserved.
//

#ifndef options_hxx
#define options_hxx

#include <stdio.h>
#include <vector>
#include "option.hxx"

class Options
{
public:
    
    static Options* pOptions();
    
    static void printOptions();
    
    static void processCommandLine( int argc, const char* argv[],
                            Name& domain, Name& sobjFile);
    
    static int cValue(const char Id[], char*& pval);
    
    static int iValue(const char Id[], int& val);

    static int fValue(const char Id[], float& val);
    
    static Name gimeExportString();
    
    static int newValue(const char Id[], const char value[]);
                             
private:
    
//  methods
    
    Options();
    
    static void createInstance();
    
    void takeOption( const char Id[],
                    const  char type[],
                    const char defvalue[],
                    const char name[],
                    const char comment[] );
    
    void initialise();
    
    
    void printUsage();
    
    int findId(const Name& Id); // negative answer means not found
    
    void* gimeConvValue(const char Id[], int& err);
    
    Name gimeType(const char Id[], int& err);


// Data
    static Options* _pSinstance;   // Singleton instance;
    
    std::vector<Option> _options;
    
};

#endif /* options_hxx */
