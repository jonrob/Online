
//----------------------------- SMISetMember  Class --------------------------
#ifndef SMISETMEMBER_HH
#define SMISETMEMBER_HH

class SMIObject;

#include "name.hxx"
//
//                                                               July 2018
//                                                               B. Franek
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------

/** @class SMISetMember
  Once an SMI Object becomes a member of a Set, its position in the Set
  remains fixed in the following sense: When the object is 'taken out',
  it is not actually removed from the set container (as used to be the case),
  but 'switched off'. When it is inserted back again, it is just 'switched on'.
  Instantiations of this class hold the necessary information.
  
  @author Boda Franek
  @date July 2018
*/

class SMISetMember
{
public :
	SMISetMember();
	
	SMISetMember(const Name& name, SMIObject* pObj, bool on);
	
	SMISetMember(const SMISetMember& );

	~SMISetMember();
/**
    returns the name of the object
*/	
	Name name() const; 
/**
    returns the pointer to the object
*/
	SMIObject* pointer() const;
/**
    returns 'true' if 'ON'
*/	
	bool isOn() const;
/**
   switches the member 'ON'
*/
	void switchOn();
/**
   switches the member 'OFF'
*/
	void switchOff();

private:
/**
     name of the member object and its pointer
*/
	Name _objName;
	
	SMIObject* _pObj;
/**
    ON/OFF  flag   'true' when ON
*/	
	bool _onFlg;
};

#endif

