//----------------------  Class  Parms  -----------------------------------
// This class is going eventually to replace all the other parameter clases
//
//                                                      Author: Boda Franek
//                                                      Date : 10 June 1998
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------------
#include "smixx_common.hxx"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "parms.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "alarm.hxx"
#include "ut_sm.hxx"
#include "param.hxx"

  const Name typeint = "INT", typefloat = "FLOAT", typestring = "STRING";
//
//
//-----------------------   Constructors -------------------------------------
Parms::Parms()
	: ParmsBase()
{}
//------------------------- Copy constructor  --------------------------
Parms::Parms(const Parms& parms)
	: ParmsBase(parms)
{}
//------------------------  Destructor  BF  Mar 2020  ------------------------
  Parms::~Parms() { return; }
//--------------------------  assignement operator ---------------------------
Parms& Parms::operator=(const Parms& parms)
{
	ParmsBase::operator=(parms);

	return *this;
} 
//-----------------------------------------------------------------------
  void Parms::buildParmString(Name& str) const {
//
// Format:   /PAR1(I)=val1/PAR2(F)=val2..../PARn(S)=valn
//
//*** However at the moment we shall not output (S)
//
   str = "\0";
   Name doubleq,nullstr;
   Name parname, parval, partype;
   
   doubleq = "\"";
   nullstr = "\0";

	int numPar = numOfEntries();
   if ( numPar <= 0 ) { return; }

     for ( int i=0; i< numPar; i++) {
		 parname = getParName(i);	
         parval = getParCurrValue(i);
		 partype = getParDeclType(i);
		 
         str += "/";
         str += parname;
         if ( partype == typeint )    { str += "(I)"; }
         if ( partype == typefloat )  { str += "(F)"; }
         if ( partype == typestring ) { 
//            str += "(S)"; 
// For the moment, if the value does not contain either / or = 
// I remove the double quotes so that DELPHI proxies are happy
            if ( parval.exists("/") || parval.exists("=") 
               || parval == "\"\"") {}
            else { parval.replace(doubleq,nullstr); }   
         }
         
         if ( parval == paramcons::noval ) {}
         else { str += "="; str += parval;}
     } 
  }
//------------------------- initFromParmString --------------------------
// Initialises itself from param string
// Format of the parameter string is:
//  /PAR1(I)=val1/......./PARi=vali/.../PARn(F)=valn
//
//   The string values can or need not
//   be surounded by double quotes. They of course have to be surounded by 
//   double quotes when they contain /  character. Also, the values 
//   are not allowed to contain a double quote(this could be easily allowed
//   by escaping them.
//   The brackets after the name have to be present. If they are not, it is
//   a string
//
  void Parms::initFromParmString(const Name& parmstr) {
//
	 int numPar = numOfEntries();
	 
     if ( numPar > 0 ) {
         clear();
     }
     
     int iel = 1;  
     Name element; 
     Name nametemp, name, value, valmod;
     char type[7];
     int ieqpos; // position of = in the element

     while ( parmstr.elementSp(iel,'/',element) ) {
        ieqpos = element.existsAt("=");
	if ( ieqpos < 0 ) {
           cout << " Parameter string has illegal format\n";
           cout << parmstr << "\n";
           cout.flush();
      	   Name temp = "-"; Alarm::message("FATAL",temp,"parameter problem");
        }
	element.subString(0,ieqpos,nametemp);
	element.subString(ieqpos+1,-1,value);
//	cout << endl << element << nametemp << value << endl;
        char* ptnr = strstr(&nametemp[0],"(");
        if ( ptnr ) {  
           if ( !strcmp(ptnr,"(I)") ) { strcpy(type,"INT");}           
           else if ( !strcmp(ptnr,"(F)") ) { strcpy(type,"FLOAT");}           
           else if ( !strcmp(ptnr,"(S)") ) { strcpy(type,"STRING");}           
           else {
              cout << " Parameter string has illegal format\n";
              cout << parmstr << "\n";
              cout.flush();
      	      Name temp = "-"; Alarm::message("FATAL",temp,"parameter problem");
           }
           *ptnr = '\0';
           name = &nametemp[0];
           *ptnr = '(';
        }   
        else { name=nametemp; strcpy(type,"STRING");} 
//                     name does not have type specifier-> it is a string

        valmod = value;
        if (!strcmp(type,"STRING")) {
           if ( value[0] != '\"' ) { // Strings without quotes are going to get them
              valmod = "\"";
              valmod += value;
              valmod += "\"";
           }
        }
		Param tempPar(name,valmod,type);
		addParam(tempPar);
        iel++;
     }
  }
//--------------------------------------------------------------------------
  int Parms::setFromParmString(const Name& parmstr) {
//
//  1 all parameters found in the string or
//    if not found, parameter has default value
//  0 not true

	 int numPar = numOfEntries();
     if ( numPar == 0 ) {return 1;}

     Parms temp;

     temp.initFromParmString(parmstr);


     if ( temp.numOfEntries() == 0 ) {return 1;}

	int retflg = 1;
	
//--- Loop through the parameters ----
     for ( int i=0; i<numPar; i++) {
        Name value, type;
		Name myName = getParName(i);
		Name myType = getParDeclType(i);
		Name defValue = getParIndiValue(i);
		Name newValue;

// get their value and type from those that came with the string		
		value = temp.getParCurrValue(myName);
		type = temp.getParDeclType(myName);

		if (  ( value == paramcons::notfound ) ||
		     !( myType == type ) )
		{
			if ( defValue == paramcons::noval )
			{
				cout << " Parameter " << myName <<
			       " of the correct type not found in parameter string"
				   << endl << " and default value is absent" << endl;
					newValue = paramcons::noval;
					retflg = 0;
			}
			else
			{
				newValue = defValue;
			}
		}
		else
		{
			newValue = value;
		}
		
		setParCurrValue(myName,newValue);
		
     }
	 
//-----
     return retflg;
  }
