//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/Cache.h>
#include <HTTP/Cache.inl.h>

// C/C++ include files
#include <cstring>

template <>
void http::Cache<http::HttpReply>::_set(http::Cache<http::HttpReply>::entry_type* e,
					http::Cache<http::HttpReply>::key_type    key,
					const struct timeval&  tmo,
					const std::string&     path,
					const std::string&     encoding,
					const http::HttpReply& reply)
{
  ::gettimeofday(&e->timeout, nullptr);
  e->data     = reply.clone();
  e->timeout.tv_sec  += tmo.tv_sec;
  uint64_t usec = e->timeout.tv_usec;
  usec += tmo.tv_usec;
  if ( usec > 1000000 )    {
    e->timeout.tv_sec  += time_t(usec/1000000);
    e->timeout.tv_usec += suseconds_t(usec%1000000);
  }
  e->encoding = encoding;
  e->path     = path;
  e->hash     = key;
}

template class http::Cache<http::HttpReply>;
