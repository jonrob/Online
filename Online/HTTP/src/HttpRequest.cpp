//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with small changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//==========================================================================

// Framework include files
#include <HTTP/HttpRequest.h>

// C/C++ include files
#include <cstring>

using namespace boost;

/// Access header by name
const http::HttpHeader* http::HttpRequest::header(const std::string& name) const {
  for ( const auto& h : headers )   {
    if ( 0 == ::strcasecmp(h.name.c_str(), name.c_str()) )
      return &h;
  }
  return 0;
}

/// Access source host
asio::ip::tcp::endpoint http::HttpRequest::remote()  const    {
  if ( this->socket )   {
    system::error_code ec;
    asio::ip::tcp::endpoint end = this->socket->remote_endpoint(ec);
    if ( !ec )  {
      return end;
    }
    throw std::runtime_error("HttpRequest::remote: Cannot determine remote "
			     "endpoint: "+ec.message());
  }
  throw std::runtime_error("HttpRequest::remote: Invalid socket handle.");
}

/// Access request of remote address
boost::asio::ip::address http::HttpRequest::remote_address()  const   {
  return this->remote().address();
}

/// Access request to remote host name
std::string http::HttpRequest::remote_host_name()  const   {
  return this->remote().address().to_string();
}

/// Access request to remote port
boost::asio::ip::port_type http::HttpRequest::port()  const   {
  return this->remote().port();
}

/// Access request to remote protocol
boost::asio::ip::tcp::endpoint::protocol_type http::HttpRequest::protocol()  const   {
  return this->remote().protocol();
}

