//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef HTTP_HTTP_HTTPCACHEHANDLER_H
#define HTTP_HTTP_HTTPCACHEHANDLER_H

/// Framework include caches
#include <HTTP/HttpReply.h>
#include <HTTP/Cache.h>

/// C/C++ include files
#include <cstdint>
#include <sys/time.h>

/// Namespace for the http server and client apps
namespace http   {

  class HttpRequest;
  
  /// A cachehandler to cache http requests
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpCacheHandler   {
  public:
    typedef Cache<HttpReply>       cache_type;
    typedef cache_type::stats_type stats_type;

    /// Structure holding cache configuration properties
    struct cache_params_t   {
      /// String identifier of this cache
      std::string name          { };
      /// Minimal content size for keeping entries in the cache
      size_t minContentLength   { 128 };
      /// Minimal content size for keeping entries in the cache
      size_t maxContentLength   { 100*MegaBYTE };
      /// Timeout in seconds for cleanup strategies
      struct timeval timeout    { std::numeric_limits<std::time_t>::max(), 0 };
      /// Flag to enable data cache
      bool   enable             { false };
      /// Debug flag
      bool   debug              { false };
    };

    /// Structure steering the cache if enabled
    cache_params_t       cache;
    
  protected:
    std::unique_ptr<cache_type> reply_cache;

    /// Check cache for the existence of a given entry
    std::pair<bool, http::Reply> check_cache(const struct timeval& now,
					     const std::string& path,
					     const std::string& encoding);

    /// Check cache for the existence of a given entry
    std::pair<bool, http::Reply> check_cache(const std::string& path,
					     const HttpRequest& req);

    /// Add a new entry to the data cache
    http::Reply add_cache(const std::string& path,
			  const std::string& encoding,
			  http::HttpReply&&  reply);

    /// Auto enable or disable cache functionality
    bool use_cache(std::mutex& handler_lock);

  public:
    /// Default construction
    HttpCacheHandler() = default;
    /// Inhibit move construction
    HttpCacheHandler(HttpCacheHandler&& copy) = delete;
    /// Inhibit copy construction
    HttpCacheHandler(const HttpCacheHandler& copy) = delete;
    /// Inhibit move assignment
    HttpCacheHandler& operator=(HttpCacheHandler&& copy) = delete;
    /// Inhibit copy assignment
    HttpCacheHandler& operator=(const HttpCacheHandler& copy) = delete;
    /// Default destructor
    virtual ~HttpCacheHandler();

    /// Access number of entries in the cache
    std::size_t size()  const;

    /// Access monitoring counters
    Cache<HttpReply>::stats_type cache_counters()  const;
    
    /// Clean "old" entries from cache [Thread safe, locked]
    std::size_t drop_expired();

    /// Drop cache entries by key  [Thread safe, locked]
    std::size_t drop_key(cache_type::key_type key);

    /// Drop a cache entries by key  [Thread safe, locked]
    std::size_t drop_keys(const std::vector<cache_type::key_type>& key);
  };
}      // End namespace http
#endif // HTTP_HTTP_HTTPCACHEHANDLER_H
