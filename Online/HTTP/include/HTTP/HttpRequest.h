//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with some changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//==========================================================================
#ifndef HTTP_HTTP_HTTPREQUEST_H
#define HTTP_HTTP_HTTPREQUEST_H

// Framework include files
#include <HTTP/HttpHeader.h>
#include <HTTP/Asio.h>

// C/C++ include files
#include <vector>
#include <chrono>

/// Namespace for the http server and client apps
namespace http   {

  /// Forward declarations
  std::vector<unsigned char> to_vector(const std::string& data);
  std::vector<unsigned char> to_vector(const char* data);

  /// A request received from a client.
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpRequest  final   {
  public:
    /// Definition of the header collection
    typedef std::vector<HttpHeader>    reqheaders_t;
    typedef std::vector<unsigned char> content_t;
  public:
    static constexpr size_t invalid_content_length = ~0x0UL;
    /// The request method, e.g. "GET", "POST".
    std::string  method;
    /// The requested URI, such as a path to a file.
    std::string  uri;
    /// Major version number, usually 1.
    int          version_major    = -1;
    /// Minor version number, usually 0 or 1.
    int          version_minor    = -1;
    /// Request state See the parser::result_type for specific values
    int          state            = -1;
    /// The content length
    size_t       content_length   = invalid_content_length;
    /// Bytes received
    size_t       content_received = 0;
    /// Measure transaction duration
    std::chrono::time_point<std::chrono::system_clock> netio, handling;
    /// Start counter for individual operations
    std::chrono::time_point<std::chrono::system_clock> start;
    /// The headers included with the request.
    reqheaders_t headers          {};
    /// The optional content sent with the request.
    content_t    content          {};
    /// Socket for the connection.
    boost::asio::ip::tcp::socket* socket {nullptr};

    /// Copy construction is allowed here
    HttpRequest(const HttpRequest&) = default;
    /// Assignment is allowed here
    HttpRequest& operator=(const HttpRequest&) = default;
    /// Move construction is allowed here
    HttpRequest(HttpRequest&&) = default;
    /// Move assignment is allowed here
    HttpRequest& operator=(HttpRequest&&) = default;
    /// Construct ready to parse the request method.
    HttpRequest() = default;
    
    /// Access header by name
    const HttpHeader* header(const std::string& name)    const;
    /// Access requester host endpoint
    boost::asio::ip::tcp::endpoint remote()  const;
    /// Access request of remote address
    boost::asio::ip::address remote_address()  const;
    /// Access request of remote host name
    std::string remote_host_name()  const;
    /// Access request to remote port
    boost::asio::ip::port_type port()  const;
    /// Access request of remote protocol
    boost::asio::ip::tcp::endpoint::protocol_type protocol()  const;
  };
  typedef HttpRequest Request;
}      // End namespace http
#endif // HTTP_HTTP_HTTPREQUEST_H
