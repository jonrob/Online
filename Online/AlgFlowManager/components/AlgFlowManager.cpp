//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

#include "Kernel/EventContextExt.h"
#include "Kernel/EventLocalAllocator.h"

#include "AlgFlowManager.h"
#include <gsl/pointers>
#include <tbb/task_arena.h>

using namespace Online;

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_COMPONENT( AlgFlowManager )

namespace Online {

  // operator<< overloads for variant nodes
  inline std::ostream& operator<<( std::ostream& os, VNode const& node ) { return os << getNameOfVNode( node ); }

  // be able to print vectors of ordered nodes correctly
  template <typename T>
  inline std::ostream& operator<<( std::ostream& os, gsl::not_null<T*> const& ptr ) {
    return os << *ptr;
  }

  template <nodeType Type>
  inline std::ostream& operator<<( std::ostream& os, Online::CompositeNode<Type> const& node ) {
    return os << node.m_name;
  }

  template <typename Key, typename Compare, typename Allocator>
  inline std::ostream& operator<<( std::ostream& os, std::set<Key, Compare, Allocator> const& x ) {
    return GaudiUtils::details::ostream_joiner( os << "{", x, ", " ) << "}";
  }
} // namespace Online

std::ostream& operator<<( std::ostream& os, NodeState const& state ) {
  return os << state.executionCtr << "|" << state.passed;
}

namespace {
  template <typename F>
  struct EventTask {
    mutable EventContext evtctx;
    F                    f;
    void                 operator()() const { return f( evtctx ); }
  };
  template <typename F>
  EventTask( EventContext&&, F && )->EventTask<F>;

  template <typename fun>
  void enqueue( fun&& f ) {
    // parallelism is controlled via global_control_t from within OnlineEventApp.cpp
    static auto pool = std::make_unique<tbb::task_arena>(std::thread::hardware_concurrency());
    pool->enqueue(std::forward<fun>( f ));
  }
} // namespace

StatusCode AlgFlowManager::initialize() {
  StatusCode sc = Service::initialize();
  if ( !sc.isSuccess() ) {
    error() << "Failed to initialize Service Base class." << endmsg;
    return StatusCode::FAILURE;
  }

  // Setup access to event data services
  m_evtDataMgrSvc = service<IDataManagerSvc>( "EventDataSvc" );
  if ( !m_evtDataMgrSvc ) {
    fatal() << "Error retrieving EventDataSvc interface IDataManagerSvc." << endmsg;
    return StatusCode::FAILURE;
  }

  m_whiteboard = service<IHiveWhiteBoard>( "EventDataSvc" );
  if ( !m_whiteboard ) {
    fatal() << "Error retrieving EventDataSvc interface IHiveWhiteBoard." << endmsg;
    return StatusCode::FAILURE;
  }

  m_algExecStateSvc = service<IAlgExecStateSvc>( "AlgExecStateSvc" );
  if ( !m_algExecStateSvc ) {
    fatal() << "Error retrieving AlgExecStateSvc" << endmsg;
    return StatusCode::FAILURE;
  }

  m_databroker = service<IDataBroker>( "HiveDataBrokerSvc" );
  if ( !m_databroker ) {
    fatal() << "Error retrieving HiveDataBrokerSvc" << endmsg;
    return StatusCode::FAILURE;
  }
  // to finalize algorithms at the right time, which is taken care of by the
  // hivedatabroker. it is the one that should be finalized first, thus high
  // priority (10*default_svc_priority is the next highest)
  auto svcMgr = serviceLocator().as<ISvcManager>();
  svcMgr->addService( "HiveDataBrokerSvc", ISvcManager::DEFAULT_SVC_PRIORITY * 11 ).ignore();

  // ------------------------------- scheduling -------------------------------------------------------
  // configure the lines
  buildLines();

  // configure the order of nodes
  configureScheduling();

  // build the vector of states (to be copied into each thread)
  buildNodeStates();

  // build the m_printableDependencyTree for monitoring
  registerStructuredTree();
  registerTreePrintWidth();

  // // print out the nodes + states
  // if ( msgLevel( MSG::DEBUG ) ) {
  //   for ( std::size_t i = 0; i < m_allVNodes.size(); ++i ) {
  //     debug() << std::left << "initialize " << std::setw( 25 ) << m_allVNodes[i]
  //       << " with state " << m_NodeStates[i] << endmsg;
  //   }
  // }
  // // print out high level nodes + their children
  // if ( msgLevel( MSG::DEBUG ) ) {
  //   for ( auto& vnode : m_allVNodes ) {
  //     std::visit( overload{[&]( auto& node ) {
  //     debug() << std::left << "children of " << std::setw( 12 ) << node.getType() << " "
  // 	    << std::setw( 25 ) << node.m_name << ": " << node.m_children << endmsg;
  //   },
  //     []( BasicNode& ) {}},
  // vnode );
  //   }
  // }
  if ( msgLevel( MSG::DEBUG ) ) debug() << m_compositeCFProperties << endmsg;
  return sc;
}

StatusCode AlgFlowManager::finalize() {
  // print the counters
  info() << boost::format{"\n | Name of Algorithm %|51t| | Execution Count\n"};
  for ( auto const& [ctr, name] : Gaudi::Functional::details::zip::range( m_AlgExecCounters, m_AlgNames ) ) {
    ctr.print( info(), name ) << '\n';
  }
  info() << endmsg;
  // print the counters
  // (since counters cannot be stored in a vector and deque cannot be converted to span,
  // we make a vector of strings to pass to buildPrintableStateTree)
  std::vector<std::string> states;
  std::transform(begin(m_NodeStateCounters), end(m_NodeStateCounters), std::back_inserter(states), [](auto& cnt) {
    return cnt.toString();
  });
  info() << buildPrintableStateTree( LHCb::make_span( as_const( states ) ) ).str() << endmsg;
  m_databroker.reset();
  m_algExecStateSvc.reset();
  m_whiteboard.reset();
  m_evtDataMgrSvc.reset();
  return Service::finalize();
}

StatusCode AlgFlowManager::start() {
  m_createEventCond.notify_all();
  return Service::start();
}

EventContext AlgFlowManager::createEventContext() {
  // setup evtcontext
  EventContext evtContext{m_nextevt, 0};
  m_nextevt++;
#if 0
  evtContext.set( m_nextevt, m_whiteboard->allocateStore( m_nextevt ) );
#endif
  return evtContext;
}

StatusCode AlgFlowManager::executeEvent( EventContext&& ctx ) {
  // Now add event to the task pool
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Event " << ctx.evt() << " submitting in slot " << ctx.slot() << endmsg;
  int evt_status = processEvent( ctx );
  if ( evt_status == EventStatus::Success ) return StatusCode::SUCCESS;
  return StatusCode::FAILURE;
}

/// Process one single event synchronously (1 per thread)
int AlgFlowManager::processEvent( EventContext& evtContext ) {
  StatusCode sc = m_whiteboard->selectStore( evtContext.slot() );
  if ( sc.isFailure() ) {
    fatal() << "Slot " << evtContext.slot() << " could not be selected for the WhiteBoard\n"
            << "Impossible to create event context" << endmsg;
    throw GaudiException( "Slot " + std::to_string( evtContext.slot() ) + " could not be selected for the WhiteBoard",
                          name(), sc );
  }

  // set event root
  sc = m_evtDataMgrSvc->setRoot( "/Event", new DataObject() );
  if ( !sc.isSuccess() ) error() << "Error declaring event root DataObject" << endmsg;

  // Make the LHCb-specific event context extension
  auto& lhcbExt = evtContext.emplaceExtension<LHCb::EventContextExtension>();

  // create a memory pool for algorithms processing this event
  // note: the resource is owned by lhcbExt, memResource is just a non-owning handle
  int   m_estMemoryPoolSize = 0; // TODO
  auto* memResource         = m_estMemoryPoolSize > 0
                          ? lhcbExt.emplaceMemResource<LHCb::Allocators::MonotonicBufferResource>( m_estMemoryPoolSize )
                          : nullptr;

  // Save a shortcut directly into the event context
  LHCb::setMemResource( evtContext, memResource );

  // Note that this is a COPY and the scheduler extension will NOT be
  // present in the copy, but the copy WILL hold a shared_ptr to the
  // memory resource.
  Gaudi::Hive::setCurrentContext( evtContext );

  SmartIF<IProperty> appmgr( serviceLocator() );

  // Copy the scheduler states into the event context so they're globally accessible within an event
  // Note that this will trigger the first allocation from the memory resource
  // Also note that these states will not be present in the thread-local EventContext.
  auto& state = lhcbExt.emplaceSchedulerExtension<LHCb::Interfaces::ISchedulerConfiguration::State>(
      std::vector{m_NodeStates.begin(), m_NodeStates.end(), LHCb::Allocators::EventLocal<NodeState>{memResource}},
      std::vector{m_AlgStates.begin(), m_AlgStates.end(), LHCb::Allocators::EventLocal<AlgState>{memResource}} );

  // Reset the status for the current context. If not done, marking it as okay
  // with updateEventStatus(false, ctx) has no effect.
  m_algExecStateSvc->reset( evtContext );

  for ( AlgWrapper& toBeRun : m_definitelyRunTheseAlgs ) {
    try {
      toBeRun.execute( evtContext, state.algorithms() );
    } catch ( ... ) {
      m_algExecStateSvc->updateEventStatus( true, evtContext );
      fatal() << "ERROR: Event failed in Algorithm " << toBeRun.name() << endmsg;
      if ( !Gaudi::setAppReturnCode( appmgr, Gaudi::ReturnCode::AlgorithmFailure ) )
        warning() << "failed to set application return code to " << Gaudi::ReturnCode::AlgorithmFailure << endmsg;
      break;
    }
  }

  for ( gsl::not_null<BasicNode*> bNode : m_orderedNodesVec ) {
    if ( bNode->requested( state.nodes() ) ) {
            bNode->execute( state.nodes(), state.algorithms(),  evtContext, m_algExecStateSvc, appmgr );
            bNode->notifyParents( state.nodes() );
    }
  }
  m_algExecStateSvc->updateEventStatus( false, evtContext );

  // update node state counters
  auto nodes = state.nodes(); // zip doesn't like rvalues... needs to be told this is a 'borrow' type, so ignore
                              // lifetime of wrapper...
  for ( auto&& [ctr, ns] : Gaudi::Functional::details::zip::range( m_NodeStateCounters, nodes ) )
    if ( ns.executionCtr == 0 ) ctr += ns.passed; // only add when actually executed

  // update scheduler state and Check if the execution failed

  int slot = evtContext.slot();

  // Schedule the cleanup of the event
  int evt_status = m_algExecStateSvc->eventStatus( evtContext );
  if ( evt_status == EventStatus::Success ) {
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Event " << evtContext.evt() << " finished (slot " << slot << ")." << endmsg;
  } else {
    fatal() << "*** Event " << evtContext.evt() << " on slot " << evtContext.slot() << " failed! ***" << endmsg;
    std::ostringstream ost;
    m_algExecStateSvc->dump( ost, evtContext );
    info() << "Dumping Alg Exec State for slot " << evtContext.slot() << ":\n" << ost.str() << endmsg;
  }

  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << "Clearing slot " << slot << " (event " << evtContext.evt() << ") of the whiteboard" << endmsg;

  sc = m_whiteboard->clearStore( slot );
  if ( !sc.isSuccess() ) warning() << "Clear of Event data store failed" << endmsg;
  sc = m_whiteboard->freeStore( slot );
  if ( !sc.isSuccess() ) error() << "Whiteboard slot " << evtContext.slot() << " could not be properly cleared";
  ++m_finishedEvt;
  return evt_status;
}

StatusCode AlgFlowManager::stopRun() {
  // Set the application return code
  auto appmgr = serviceLocator()->as<IProperty>();
  if ( Gaudi::setAppReturnCode( appmgr, Gaudi::ReturnCode::ScheduledStop ).isFailure() ) {
    error() << "Could not set return code of the application (" << Gaudi::ReturnCode::ScheduledStop << ")" << endmsg;
  }
  return StatusCode::SUCCESS;
}

StatusCode AlgFlowManager::nextEvent( int maxevt ) {
  // Reset the application return code.
  auto appmgr = serviceLocator()->as<IProperty>();
  Gaudi::setAppReturnCode( appmgr, Gaudi::ReturnCode::Success, true ).ignore();

  // start with event 0
  m_nextevt = 0;
  // Run the first event before spilling more than one
  bool newEvtAllowed = false;

  auto okToStartNewEvt = [&] {
    return ( newEvtAllowed || m_nextevt == 0 ) && // Launch the first event alone
                                                  // The events are not finished with an unlimited number of events
           // The events are not finished with a limited number of events
           ( m_nextevt < size_t( maxevt ) ) &&
           // There are still free slots in the whiteboard
           m_whiteboard->freeSlots() > 0;
  };

  auto maxEvtNotReached = [&] { return maxevt < 0 || m_finishedEvt < (unsigned int)maxevt; };

  info() << "Starting loop on events" << endmsg;
  using namespace std::chrono_literals;

  while ( maxEvtNotReached() ) {
    std::unique_lock<std::mutex> lock{m_createEventMutex};
    if ( m_createEventCond.wait_for( lock, 2ms, okToStartNewEvt ) ) {
      auto       ctx = createEventContext();
      StatusCode sc  = executeEvent( std::move( ctx ) );
      if ( !sc.isSuccess() ) return StatusCode::FAILURE;
      newEvtAllowed = true;
    }
  } // end main loop on finished events
  return StatusCode::SUCCESS;
}

// scheduling functionality----------------------------------------------------

void AlgFlowManager::buildLines() { // here lines are configured, filled into the node vector m_allVNodes and
  // pointers are adjusted

  std::vector<std::string> allChildNames;

  for ( auto const& cfprop : m_compositeCFProperties.value() ) {
    // NodeDef: ["name", "type", ["child1", "child2"], bool ordered]
    // create CompositeNodes with this configuration
    // TODO better dispatch?
    nodeType const nt = nodeTypeNames_inv.at( cfprop.type );
    switch ( nt ) {
    case nodeType::LAZY_AND:
      m_allVNodes.emplace( cfprop.name,
                           CompositeNode<nodeType::LAZY_AND>{cfprop.name, cfprop.children, cfprop.ordered} );
      break;
    case nodeType::LAZY_OR:
      m_allVNodes.emplace( cfprop.name,
                           CompositeNode<nodeType::LAZY_OR>{cfprop.name, cfprop.children, cfprop.ordered} );
      break;
    case nodeType::NONLAZY_OR:
      m_allVNodes.emplace( cfprop.name,
                           CompositeNode<nodeType::NONLAZY_OR>{cfprop.name, cfprop.children, cfprop.ordered} );
      break;
    case nodeType::NONLAZY_AND:
      m_allVNodes.emplace( cfprop.name,
                           CompositeNode<nodeType::NONLAZY_AND>{cfprop.name, cfprop.children, cfprop.ordered} );
      break;
    case nodeType::NOT:
      assert( cfprop.children.size() == 1 );
      m_allVNodes.emplace( cfprop.name, CompositeNode<nodeType::NOT>{cfprop.name, cfprop.children, cfprop.ordered} );
      break;
    default:
      throw GaudiException( "nodeType" + nodeTypeNames.at( nt ) + " does not exist.", __func__, StatusCode::FAILURE );
    }
    for ( auto& childname : cfprop.children ) { allChildNames.emplace_back( childname ); }
  }

  // all childs that are not yet in m_allVNodes are obviously BasicNodes, so add them
  for ( std::string const& childname : allChildNames ) {
    m_allVNodes.try_emplace( childname, std::in_place_type<BasicNode>, childname, msgStream() );
  }

  // Now we resolve the names of children to pointers
  childrenNamesToPointers( m_allVNodes );

  // detect the motherOfAllNodes (the highest node)
  auto it = std::find_if( begin( m_allVNodes ), end( m_allVNodes ), [&]( auto const& entry ) {
    return std::all_of( begin( allChildNames ), end( allChildNames ),
                        [&]( std::string_view name ) { return name != entry.first; } );
  } );

  if ( it == end( m_allVNodes ) ) {
    throw GaudiException( "The control flow tree does not have a root, thus cannot be resolved. There needs to be one "
                          "node that is no child of any other node.",
                          __func__, StatusCode::FAILURE );
  }

  m_motherOfAllNodes = &it->second;

  if ( msgLevel( MSG::DEBUG ) ) {
    for ( auto const& [_, vnode] : m_allVNodes ) {
      std::visit( overload{[&]( auto const& node ) {
                             debug() << std::left << "children of " << std::setw( 13 ) << node.getType() << " "
                                     << std::setw( 15 ) << node.m_name << ": " << node.m_children << endmsg;
                           },
                           []( BasicNode const& ) {}},
                  vnode );
    }
  }
}

void AlgFlowManager::configureScheduling() {

  // find all basic nodes by traversing the tree recursively
  auto       allBasics     = reachableBasics( gsl::not_null{m_motherOfAllNodes} );
  auto const allComposites = reachableComposites( gsl::not_null{m_motherOfAllNodes} );

  std::set<std::array<gsl::not_null<VNode*>, 2>> additionalEdges;

  // translate user edges, defined only by names, to edges of pointers to VNodes
  for ( auto& edge : m_userDefinedEdges ) {
    assert( edge.size() == 2 );
    additionalEdges.emplace(
        std::array{gsl::not_null{&m_allVNodes.at( edge[0] )}, gsl::not_null{&m_allVNodes.at( edge[1] )}} );
  }

  using Algorithm = Gaudi::Algorithm;
  // get data dependencies right
  // barrier inputs
  std::vector<std::vector<Algorithm*>> BarrierInputs{m_BarrierAlgNames.size()};
  // fill them
  std::transform( begin( m_BarrierAlgNames ), end( m_BarrierAlgNames ), begin( BarrierInputs ),
                  [&]( auto const& name ) { return m_databroker->algorithmsRequiredFor( name ); } );

  // which ones are also explicit CF nodes?
  std::vector<std::set<gsl::not_null<VNode*>>> explicitDataDependencies{BarrierInputs.size()};

  for ( std::size_t i = 0; i != BarrierInputs.size(); ++i ) {
    for ( Algorithm const* alg : BarrierInputs[i] ) {
      auto node = std::find_if( begin( allBasics ), end( allBasics ),
                                [&]( VNode const* vnode ) { return getNameOfVNode( *vnode ) == alg->name(); } );
      if ( node != std::end( allBasics ) ) { explicitDataDependencies[i].emplace( *node ); }
    }
  }

  std::vector<Algorithm*> allAlgos; // temporarily save/count all algorithms
  allAlgos.reserve( 1000 );         // whatever, 1000 is nice
  // fill algorithms that should definitly run
  for ( std::string const& algname : m_definitlyRunThese ) {
    for ( Algorithm* alg : m_databroker->algorithmsRequiredFor( algname ) ) {
      if ( std::find( begin( allAlgos ), end( allAlgos ), alg ) == end( allAlgos ) ) {
        m_definitelyRunTheseAlgs.emplace_back( alg, allAlgos.size() );
        allAlgos.emplace_back( alg );
      }
    }
  }

  for ( gsl::not_null<VNode*> vnode : allBasics ) {
    std::visit( overload{[&]( BasicNode& node ) {
                           // plug in data dependencies
                           auto reqAlgs = m_databroker->algorithmsRequiredFor( node.m_name, m_BarrierAlgNames );
                           node.m_RequiredAlgs.reserve( reqAlgs.size() );

                           for ( Algorithm* alg : reqAlgs ) {
                             auto index = std::find( begin( allAlgos ), end( allAlgos ), alg );
                             node.m_RequiredAlgs.emplace_back( alg, std::distance( begin( allAlgos ), index ) );
                             if ( index == end( allAlgos ) ) { allAlgos.emplace_back( alg ); }
                           }

                           // additional edges for the barrier. Since there is optional input for the barrier, we need
                           // to add additional edges from each input to each output of the barrier to make sure
                           // stuff runs in the right order. Keep in mind that datadependencies are cut away if they are
                           // optional (stopperalg in m_databroker)
                           for ( AlgWrapper const& reqAlg : node.m_RequiredAlgs ) {
                             for ( std::size_t i = 0; i != m_BarrierAlgNames.size(); ++i ) {
                               if ( reqAlg.name() == m_BarrierAlgNames[i] ) {
                                 for ( gsl::not_null<VNode*> explicitDD : explicitDataDependencies[i] ) {
                                   additionalEdges.emplace( std::array{explicitDD, vnode} );
                                 }
                               }
                             }
                           }
                         },
                         []( ... ) {}},
                *vnode );
  }

  m_AlgNames.reserve( allAlgos.size() );
  std::transform( begin( allAlgos ), end( allAlgos ), std::back_inserter( m_AlgNames ),
                  []( auto const* alg ) { return alg->name(); } );

  m_AlgStates.clear();
  m_AlgStates.resize( allAlgos.size() );

  m_AlgExecCounters.clear();
  m_AlgExecCounters.resize( allAlgos.size() );

  // end of Data depdendency handling

  // fill the m_parent list of all nodes
  addParentsToAllNodes( allComposites );

  // get all unwrapped control flow edges
  auto nodePrerequisites = Online::findAllEdges( gsl::not_null{m_motherOfAllNodes}, additionalEdges );

  // print out the edges
  if ( msgLevel( MSG::DEBUG ) ) debug() << "edges: " << nodePrerequisites << endmsg;

  // print out the edges
  if ( msgLevel( MSG::DEBUG ) ) debug() << "additional edges: " << additionalEdges << endmsg;

  // resolve all CF dependencies
  m_orderedNodesVec = resolveDependencies( allBasics, nodePrerequisites );

  // print out the order
  if ( msgLevel( MSG::DEBUG ) ) debug() << "ordered nodes: " << m_orderedNodesVec << endmsg;
}

void AlgFlowManager::buildNodeStates() {

  m_NodeStates.reserve( m_allVNodes.size() );
  int helper_index = 0;

  for ( auto& [_, vNode] : m_allVNodes ) {
    std::visit( overload{[&]( BasicNode& node ) {
                           node.m_NodeID = helper_index++;
                           m_NodeStates.push_back( NodeState{1, true} );
                         },
                         [&]( auto& node ) {
                           node.m_NodeID = helper_index++;
                           m_NodeStates.push_back( NodeState{static_cast<uint16_t>( node.m_children.size() ), true} );
                         }},
                vNode );
  }

  // prepare counters
  m_NodeStateCounters.clear();  // make sure all counters are new and empty
  m_NodeStateCounters.resize( m_NodeStates.size() );
}

// monitoring and printing functions --------------------------------------------------

// defines the tree to print out, where each element of the vector shall be one line
// we will later append execution and passed states to these lines to print
void AlgFlowManager::registerStructuredTree() {
  assert( m_printableDependencyTree.empty() );
  std::stringstream ss;

  auto print_indented = [&]( VNode const* vnode, int const currentIndent, auto& itself ) -> void {
    // to recursively call this lambda, use auto& itself
    std::visit( overload{[&]( auto const& node ) {
                           std::stringstream ss;
                           ss << std::string( currentIndent, ' ' ) << node.getType() << ": " << node.m_name << " ";
                           m_printableDependencyTree.emplace_back( ss.str() );
                           m_mapPrintToNodeStateOrder.emplace_back( node.m_NodeID ); // to later print it
                           for ( VNode* child : node.m_children ) { itself( child, currentIndent + 1, itself ); }
                         },
                         [&]( BasicNode const& node ) {
                           std::stringstream ss;
                           ss << std::string( currentIndent, ' ' ) << node.m_name << " ";
                           m_printableDependencyTree.emplace_back( ss.str() );
                           m_mapPrintToNodeStateOrder.emplace_back( node.m_NodeID );
                         }},
                *vnode );
  };
  print_indented( m_motherOfAllNodes, 0, print_indented );
}

// determines the size of the largest string in the structured tree vector, to print neatly afterwards
void AlgFlowManager::registerTreePrintWidth() {
  assert( !m_printableDependencyTree.empty() );
  m_maxTreeWidth = ( *std::max_element( begin( m_printableDependencyTree ), end( m_printableDependencyTree ),
                                        []( std::string_view s, std::string_view t ) { return s.size() < t.size(); } ) )
                       .size();
}

// build the full tree
template <typename Printable>
std::stringstream AlgFlowManager::buildPrintableStateTree( LHCb::span<Printable const> states ) const {
  assert( !m_printableDependencyTree.empty() );
  std::stringstream ss;
  ss << '\n';
  for ( auto const& [treeEntry, printToStateIndex] :
        Gaudi::Functional::details::zip::range( m_printableDependencyTree, m_mapPrintToNodeStateOrder ) ) {
    ss << std::left << std::setw( m_maxTreeWidth + 1 ) << treeEntry << states[printToStateIndex] << '\n';
  }
  return ss;
}

// build the AlgState printout
std::stringstream
AlgFlowManager::buildAlgsWithStates( LHCb::Interfaces::ISchedulerConfiguration::State const& state ) const {
  auto              states = state.algorithms();
  std::stringstream ss;
  ss << '\n';
  for ( auto const& [name, state] : Gaudi::Functional::details::zip::range( m_AlgNames, states ) ) {
    ss << std::left << std::setw( 20 ) << name << state.isExecuted << '\n';
  }
  return ss;
}

// // build the full tree
// template <typename printable>
// std::stringstream AlgFlowManager::buildPrintableStateTree( std::vector<printable> const& states ) const {
//   assert( !m_printableDependencyTree.empty() );
//   std::stringstream ss;
//   ss << '\n';
//   for ( auto const& [treeEntry, printToStateIndex] :
// 	  Gaudi::Functional::details::zip::range( m_printableDependencyTree, m_mapPrintToNodeStateOrder ) ) {
//     ss << std::left << std::setw( m_maxTreeWidth + 1 ) << treeEntry << states[printToStateIndex] << '\n';
//   }
//   return ss;
// }

// template std::stringstream
// AlgFlowManager::buildPrintableStateTree<NodeState>( std::vector<NodeState> const& states ) const;

// // build the AlgState printout
// std::stringstream AlgFlowManager::buildAlgsWithStates( std::vector<uint16_t> const& states ) const {
//   std::stringstream ss;
//   ss << '\n';
//   for ( auto const& [name, state] : Gaudi::Functional::details::zip::range( m_AlgNames, states ) ) {
//     ss << std::left << std::setw( 20 ) << name << state << '\n';
//   }
//   return ss;
// }

//#define HAVE_OUTPUT_LIST 1

/// IQueueingEventProcessor override: Schedule the processing of an event.
void AlgFlowManager::push( EventContext&& ctx ) {
  using namespace std::chrono_literals;
  auto okToStartNewEvt = [&] { return m_whiteboard->freeSlots() > 0; };
  while ( 1 ) {
    std::unique_lock<std::mutex> lock{m_createEventMutex};
    if ( m_createEventCond.wait_for( lock, 2ms, okToStartNewEvt ) ) {
      ++m_inFlight;
      ctx.setSlot( m_whiteboard->allocateStore( ctx.evt() ) );
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "Event " << ctx.evt() << " submitting in slot " << ctx.slot() << endmsg;

      EventTask event_task{std::move( ctx ), [this]( EventContext& ctx ) {
                             pthread_setname_np(pthread_self(), "worker");
                             auto evt_status = processEvent( ctx );
                             auto sc = evt_status == EventStatus::Success ? StatusCode::SUCCESS : StatusCode::FAILURE;
                             m_createEventCond.notify_all();
#if HAVE_OUTPUT_LIST
                             {
                               std::lock_guard<std::mutex> lock( m_resultLock );
                               m_result.emplace_back( sc, std::move( ctx ) );
                             }
#else
        m_done.emplace( sc, std::move( ctx ) );
#endif
                             --m_inFlight;
                           }};
      enqueue( std::move( event_task ) );
      return;
    }
    std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
  }
}

/// IQueueingEventProcessor override: Tell if the processor has events in the queues.
bool AlgFlowManager::empty() const {
#if HAVE_OUTPUT_LIST
  return m_inFlight == 0 && m_result.empty();
#else
  return m_inFlight == 0 && m_done.size() == 0;
#endif
}

/// IQueueingEventProcessor override: Get the next available result.
std::optional<Gaudi::Interfaces::IQueueingEventProcessor::ResultType> AlgFlowManager::pop() {
#if HAVE_OUTPUT_LIST
  if ( m_result.empty() ) return std::nullopt;
  std::lock_guard<std::mutex> lock( m_resultLock );
  ResultType                  out = std::move( m_result.back() );
  m_result.pop_back();
  return std::move( out );
#else
  ResultType out;
  if ( m_done.try_pop( out ) ) return out;
  return std::nullopt;
#endif
}
