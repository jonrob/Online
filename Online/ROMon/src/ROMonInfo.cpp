//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include "dim/dic.hxx"
#include "dim/dis.hxx"
#include "RTL/rtl.h"
#include "ROMonDefs.h"
#include "ROMon/ROMonInfo.h"
#include "ROMon/RODimListener.h"

using namespace ROMon;

/// Standard constructor
ROMonInfo::ROMonInfo(RODimListener* s) {
  if ( s ) m_servers.push_back(s);
  m_info = ::dic_info_service("DIS_DNS/SERVER_LIST",MONITORED,0,0,0,infoHandler,(long)this,0,0);
}

/// Standard constructor
ROMonInfo::ROMonInfo(const Servers& s)  {
  m_servers = s;
  m_info = ::dic_info_service("DIS_DNS/SERVER_LIST",MONITORED,0,0,0,infoHandler,(long)this,0,0);
}

/// Standard destructor
ROMonInfo::~ROMonInfo()  {
  if ( m_info ) {
    ::dic_release_service(m_info);
    m_info = 0;
  }
}

/// Add handler for a given message source
void ROMonInfo::addHandler(const std::string& node, const std::string& svc) {
  for(Servers::iterator i=m_servers.begin(); i!=m_servers.end();++i)
    (*i)->addHandler(node,svc);
}

/// Remove handler for a given message source
void ROMonInfo::removeHandler(const std::string& node, const std::string& svc) {
  for(Servers::iterator i=m_servers.begin(); i!=m_servers.end();++i)
    (*i)->removeHandler(node,svc);
}

/// DimInfo overload to process messages
void ROMonInfo::infoHandler(void* tag, void* address, int* size)  {
  if ( address && size && *size>0 ) {
    ROMonInfo* h = *(ROMonInfo**)tag;
    char*      m = (char*)address;
    std::string svc, node;
    switch(m[0]) {
    case '+':
      getServiceNode(++m,svc,node);
      h->addHandler(node,svc);
      break;
    case '-':
      getServiceNode(++m,svc,node);
      h->removeHandler(node,svc);
      break;
    case '!':
      getServiceNode(++m,svc,node);
      ::lib_rtl_output(LIB_RTL_INFO,"Service %s in ERROR.",m);
      break;
    default:   {
      char *at, *p = m, *last = m;
      while ( last != 0 && (at=strchr(p,'@')) != 0 )  {
	last = strchr(at,'|');
	if ( last ) *last = 0;
	getServiceNode(p,svc,node);
	h->addHandler(node,svc);
	p = last+1;
      }
      break;
    }
    }
  }
  else  {  // No link (Nameserver dead OR initial call
    //::lib_rtl_output(LIB_RTL_INFO,"DIM nameserver died....");
  }
}

extern "C" int romon_test_dim_info(int, char**) {
  bool run = true;
  ROMonInfo info(0);
  DimServer::start(RTL::processName().c_str());
  ::lib_rtl_output(LIB_RTL_INFO,"Going asleep");
  while(run==true) ::lib_rtl_sleep(1000);
  return 1;
}
