//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include "ROMon/TaskSupervisor.h"
#include "XML/XML.h"

namespace XML {
  class TaskSupervisorParser : public xml_doc_holder_t  {
  public:
    typedef ROMon::Cluster Cluster;
    typedef ROMon::Cluster::Node Node;
    typedef ROMon::Inventory Inventory;
    typedef std::list<ROMon::Cluster> Clusters;
  public:
    TaskSupervisorParser();
    virtual ~TaskSupervisorParser();
    bool parseFile(const std::string& file);
    bool parseBuffer(const std::string& sys, const void* data, size_t len);
    void getNodes(xml_h fde, Cluster& cluster) const;
    void getClusterNodes(Cluster& cluster) const;
    void getClusters(Clusters& clusters) const;
    void getInventory(Inventory& inv) const;
  };
}

