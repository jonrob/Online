//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_MONITORINGDISPLAY_H
#define ROMON_MONITORINGDISPLAY_H 1

// Framework includes
#include "ROMon/ClusterDisplay.h"

// C++ include files
#include <map>

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  // Forward declarations
  class MBMBuffer;

  /**@class MonitoringDisplay ROMon.h GaudiOnline/MonitoringDisplay.h
   *
   *   Monitoring display for the LHCb storage system.
   *
   *   @author M.Frank
   */
  class MonitoringDisplay : public ClusterDisplay  {
  protected:
    /// Reference to the Relay info display
    MonitorDisplay*          m_relay;

    /// reference to the node display
    MonitorDisplay*          m_nodes;

    /// Reference to the display showing monitoring tasks
    MonitorDisplay*          m_tasks;

    /// Reference node selector display if used as sub-dispay
    MonitorDisplay*          m_select;

    /// Partition name for projection(s)
    std::string              m_partName;

    /// Name pof relay node
    std::string              m_relayNode;

  private:
    /// Private copy constructor
    MonitoringDisplay(const MonitoringDisplay&) : ClusterDisplay(0,0) {}
    /// Private assignment operator
    MonitoringDisplay& operator=(const MonitoringDisplay&) { return *this; }

  public:
    struct Stream {
      int received;
      std::map<std::string,int> to;
      const MBMBuffer* buffer;
      Stream() : received(0), buffer(0) {}
      Stream(const Stream& s) : received(s.received), to(s.to), buffer(s.buffer) {}
      Stream& operator=(const Stream& s) { received=s.received; to = s.to; buffer = s.buffer; return *this;}
    };

    /// Standard constructor
    MonitoringDisplay(int argc, char** argv);

    /// Initializing constructor for using display as sub-display
    MonitoringDisplay(int width, int height, int posx, int posy, int argc, char** argv);

    /// Standard destructor
    virtual ~MonitoringDisplay();

    /// Initialize the display
    void init(int flag, int argc, char** argv);

    /// Number of nodes in the dataset
    size_t numNodes() override;

    /// Retrieve cluster name from cluster display
    std::string clusterName() const override;

    /// Retrieve node name from cluster display by offset
    std::string nodeName(size_t offset) override;

    /// Access Node display
    MonitorDisplay* nodeDisplay() const override;

    /// Update selector information
    void showSelector(const Nodeset& ns);

    /// Show the display header information (title, time, ...)
    void showHeader(const Nodeset& ns);

    /// Display the node information
    void showNodes(const Nodeset& ns);

    /// Show the task information
    void showTasks(const Nodeset& ns);

    /// Show the relay information
    void showRelay(const Nodeset& ns);

    /// Update selection window information
    void showSelect(const Nodeset& ns);

    /// Update all displays
    void updateDisplay(const Nodeset& ns) override;

    /// Update display content
    void updateDisplay(const Node& n) override { this->ROMonDisplay::updateDisplay(n); }
  };
  /// Static abstract object creator.
  ClusterDisplay*  createMonitoringDisplay(int width, int height, int posx, int posy, int argc, char** argv);
}      // End namespace ROMon
#endif /* ROMON_MONITORINGDISPLAY_H */

