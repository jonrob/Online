//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/RPCRec.h>
#include <Gaucho/ObjRPC.h>

#include <cstdio>

using namespace Online;

namespace Online  {
  template <typename T> T* RPCComm::make_command(std::size_t len, RPCCommType typ)    {
    void* ptr = ::operator new(len);
    ::memset(ptr, 0, len);
    T* cmd  = (T*)ptr;
    cmd->comm = typ;
    return cmd;
  }
  template <typename T> T* RPCComm::make_command(std::size_t len, RPCCommType typ, const void* cookie)    {
    void* ptr = ::operator new(len);
    ::memset(ptr, 0, len);
    T* cmd  = (T*)ptr;
    cmd->comm = typ;
    cmd->cookie = cookie;
    return cmd;
  }

  template RPCCommRead*  RPCComm::make_command<RPCCommRead>(std::size_t len,  RPCCommType typ);
  template RPCCommRegex* RPCComm::make_command<RPCCommRegex>(std::size_t len, RPCCommType typ);
  template RPCCommClear* RPCComm::make_command<RPCCommClear>(std::size_t len, RPCCommType typ);

  template RPCCommReadCookie*  RPCComm::make_command<RPCCommReadCookie>(std::size_t len,  RPCCommType typ, const void* cookie);
  template RPCCommRegexCookie* RPCComm::make_command<RPCCommRegexCookie>(std::size_t len, RPCCommType typ, const void* cookie);
  template RPCCommClearCookie* RPCComm::make_command<RPCCommClearCookie>(std::size_t len, RPCCommType typ, const void* cookie);
}

char* RPCComm::copy_name(char* ptr, const std::string& name)   {
  ::memcpy(ptr, name.c_str(), name.length());
  ptr  += name.length();
  *ptr  = '\n';
  return ptr+1;
}

char* RPCComm::copy_names(char* ptr, const std::vector<std::string>& names)    {
  for(const auto& n : names) ptr = copy_name(ptr, n);
  return ptr;
}

RPCRec::RPCRec(const std::string& name, int timeout, bool synch) : DimRpcInfo(name.c_str(), timeout, -1)   {
  DirCB = 0;
  DatCB = 0;
  m_synch = synch;
}

int RPCRec::analyseReply()   {
  std::size_t siz = getSize();
  void*       valin = getData();
  void*       valend = add_ptr(valin,siz);
  RPCReply*   rep = (RPCReply*)valin;
  
  m_reply = RPCCIllegal;

  if (rep->status == -1)
    return 1;
  if (rep->status == -2)
    return 2;

  m_reply = rep->comm;
  switch (rep->comm)  {
    case RPCCIllegal:
      return 3;
    case RPCCRead:
    case RPCCReadAll:
    case RPCCReadRegex:
    case RPCCClear:
    case RPCCClearAll:    {
      hists.clear();
      auto *ptr = add_ptr<DimHistbuff1>(valin, sizeof(RPCReply));
      while (ptr < valend)    {
        hists.emplace(add_ptr<char>(ptr, ptr->nameoff), ptr);
        if ( ptr->reclen <= 0 ) break;
        ptr = add_ptr<DimHistbuff1>(ptr,ptr->reclen);
      }
      break;
    }
    case RPCCDirectory:    {
      void *ptr = add_ptr(valin, sizeof(RPCReply));
      names.clear();
      while (ptr<valend)     {
        int titoff = 4;
        char *tptr = add_ptr<char>(ptr, titoff);
        names.push_back(tptr);
        int recl = 4 + ::strlen(tptr)+1;
        ptr = add_ptr(ptr,recl);
      }
      break;
    }
    case RPCCReadCookie:
    case RPCCReadAllCookie:
    case RPCCReadRegexCookie:
    case RPCCClearCookie:
    case RPCCClearAllCookie:    {
      hists.clear();
      auto *ptr = add_ptr<DimHistbuff1>(valin, sizeof(RPCReplyCookie));
      while (ptr < valend)    {
        hists.emplace(add_ptr<char>(ptr, ptr->nameoff), ptr);
        if ( ptr->reclen <= 0 ) break;
        ptr = add_ptr<DimHistbuff1>(ptr,ptr->reclen);
      }
      break;
    }
    case RPCCDirectoryCookie:    {
      void *ptr = add_ptr(valin, sizeof(RPCReplyCookie));
      names.clear();
      while (ptr<valend)     {
        int titoff = 4;
        char *tptr = add_ptr<char>(ptr, titoff);
        names.push_back(tptr);
        int recl = 4 + ::strlen(tptr)+1;
        ptr = add_ptr(ptr, recl);
      }
      break;
    }
  }
  return 0;
}

int RPCRec::analyseReply(NAMEVEC& v)   {
  int stat = analyseReply();
  if ( stat == 0 )
    v = names;
  return stat;
}

int RPCRec::analyseReply(PTRMAP& p)   {
  int stat = analyseReply();
  if ( stat == 0 )
    p = hists;
  return stat;

}
int RPCRec::analyseReply(STRVEC& v)   {
  int stat = analyseReply();
  v.clear();
  if ( stat == 0 )  {
    for (auto i=names.begin();i!=names.end();i++)
      v.push_back(*i);
  }
  return stat;
}

void RPCRec::rpcInfoHandler()    {
  if (m_synch) return;
  int stat = analyseReply();
  if (stat != 0) return;
  switch (m_reply)  {
  case RPCCIllegal:
    return;
  case RPCCRead:
  case RPCCReadCookie:
  case RPCCReadAll:
  case RPCCReadAllCookie:
  case RPCCReadRegex:
  case RPCCReadRegexCookie:
  case RPCCClear:
  case RPCCClearCookie:
  case RPCCClearAll:
  case RPCCClearAllCookie:
    if (DatCB != 0)
      DatCB(hists);
    break;
  case RPCCDirectory:
  case RPCCDirectoryCookie:
    if (DirCB != 0)
      DirCB(names);
    break;
  }
}
