//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/HistTaskJson.h>
#include <Gaucho/HistJson.h>
#include <numeric>

using namespace Online;

HistTaskJson::HistTaskJson(const std::string& task,const std::string& dns, int tmo)
  : TaskRPC("Histos", task, dns, tmo)
{
}

HistTaskJson::json HistTaskJson::histo_directory()    {
  std::vector<std::string> items;
  if ( 0 == this->directory(items) )   {
    return items;
  }
  throw std::runtime_error("Invalid RPC call to HistTaskJson::histo_directory()");
}

HistTaskJson::json HistTaskJson::taskList(const std::string& dns)    {
  std::vector<std::string> items;
  if ( 0 == HistTaskJson::taskList(dns, items) )   {
    return items;
  }
  throw std::runtime_error("Invalid RPC call to HistTaskJson::taskList("+dns+")");
}

HistTaskJson::json HistTaskJson::histos(const std::string& selection)    {
  int status = this->items(selection);
  std::vector<json> hists;
  if ( status == 0 )  {
    for ( const auto& k : m_RPC->hists )    {
      json o = JsonHistDeserialize::de_serialize(k.second);
      o["task"] = this->taskName();
      hists.emplace_back(std::move(o));
    }
    return hists;
  }
  return {};
}

int HistTaskJson::histos(const std::vector<std::string>& names, std::vector<json>& histos)   {
  if (m_RPC != 0)   {
    int cmdlen = std::accumulate(names.begin(), names.end(), names.size()+sizeof(RPCCommRead), sum_string_length);
    RPCCommRead *cmd = RPCComm::make_command<RPCCommRead>(cmdlen, RPCCRead);
    cmd->copy_names(cmd->which, names);
    m_RPC->setData(cmd, cmdlen);
    int status  = m_RPC->analyseReply();
    if ( status == 0 )  {
      for ( auto k : m_RPC->hists )    {
	json o = JsonHistDeserialize::de_serialize(k.second);
	o["task"] = this->taskName();
	histos.emplace_back(std::move(o));
      }
    }
    return status;
  }
  return 1;
}

int HistTaskJson::histos(const std::vector<std::string>& names, std::map<std::string,json>& histos)  {
  int status = this->items(names);
  if ( status == 0 )  {
    for( const auto& k : m_RPC->hists )    {
      json o = JsonHistDeserialize::de_serialize(k.second);
      o["task"] = this->taskName();
      histos.emplace(k.first, std::move(o));
    }
  }
  return status;
}
