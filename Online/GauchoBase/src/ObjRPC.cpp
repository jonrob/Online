//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/ObjRPC.h>
#include <Gaucho/BRTL_Lock.h>
#include <Gaucho/Utilities.h>
#include <RTL/strdef.h>

using namespace Online;

ObjRPC::ObjRPC(std::unique_ptr<Serializer>&& srv, 
	       const std::string& n, const char *f_in, const char *f_out, 
	       BRTLLock *mlid, BRTLLock *olid)
  : DimRpc(n.c_str(), f_in, f_out), dns(), serializer(std::move(srv)), maplock(mlid), objlock(olid)
{
}

ObjRPC::ObjRPC(std::shared_ptr<DimServerDns> d,
	       std::unique_ptr<Serializer>&& s, 
	       const std::string& n, const char *f_in, const char *f_out, 
	       BRTLLock *mlid, BRTLLock *olid)
  : DimRpc(d.get(), n.c_str(), f_in, f_out), dns(std::move(d)), serializer(std::move(s)), maplock(mlid), objlock(olid)
{
}

void ObjRPC::rpcHandler()   {
  RPCCommCookie *comm  = (RPCCommCookie*)getData();
  std::pair<size_t, void*> result;
  {
    BRTLLock::_Lock mlock(this->maplock);

    if ( comm->comm == RPCCRead )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(comm->comm)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReply));
    }
    else if ( comm->comm == RPCCClear )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(comm->comm)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReply), true);
    }
    else if ( comm->comm == RPCCClearAll )   {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReply), true);
    }
    else if ( comm->comm == RPCCReadAll )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReply));
    }
    else if ( comm->comm == RPCCReadRegex )    {
      BRTLLock::_Lock lock(this->objlock);
      std::string match = add_ptr<char>(comm,sizeof(comm->comm));
      match = match.substr(0, match.find("\n"));
      result = serializer->serialize_match(match, sizeof(RPCReply));
    }
    else if ( comm->comm == RPCCDirectory )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_dir(sizeof(RPCReply));
    }
    /// RPC calls supporting client side cookies:
    else if ( comm->comm == RPCCReadCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(RPCCommCookie)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReplyCookie));
    }
    else if ( comm->comm == RPCCClearCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(RPCCommCookie)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReplyCookie), true);
    }
    else if ( comm->comm == RPCCClearAllCookie )   {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReplyCookie), true);
    }
    else if ( comm->comm == RPCCReadAllCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReplyCookie));
    }
    else if ( comm->comm == RPCCReadRegexCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      std::string match = add_ptr<char>(comm,sizeof(RPCCommCookie));
      match = match.substr(0, match.find("\n"));
      result = serializer->serialize_match(match, sizeof(RPCReplyCookie));
    }
    else if ( comm->comm == RPCCDirectoryCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_dir(sizeof(RPCReplyCookie));
    }
    else   {
      RPCReply reply;
      reply.status = -2;
      reply.comm = RPCCIllegal;
      setData(&reply, sizeof(RPCReply));
      if (this->maplock !=0) this->maplock->unlockMutex();
      return;
    }
  }
  RPCReplyCookie *reply = (RPCReplyCookie*)result.second;
  reply->status = 0;
  reply->comm = comm->comm;
  switch( comm->comm )   {
  case RPCCReadCookie:
  case RPCCClearCookie:
  case RPCCClearAllCookie:
  case RPCCReadAllCookie:
  case RPCCReadRegexCookie:
  case RPCCDirectoryCookie:
    reply->cookie = comm->cookie;
    reply->id     = comm->id;
    break;
  default:
    break;
  }
  setData(result.second, result.first);
}
