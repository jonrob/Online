//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/TaskRPC.h>
#include <dim/dic.hxx>

#include <unordered_set>
#include <numeric>
#include <vector>
#include <set>

using namespace Online;

namespace {
  void __insert(std::vector<std::string>& c, const std::string& v)        {    c.push_back(v);  }
  void __insert(std::list<std::string>& c, const std::string& v)          {    c.push_back(v);  }
  void __insert(std::set<std::string>& c, const std::string& v)           {    c.insert(v);     }
  void __insert(std::unordered_set<std::string>& c, const std::string& v) {    c.insert(v);     }
}

TaskRPC::TaskRPC(const std::string& type, const std::string& task, const std::string& dns, int tmo)
  : m_dns(dns), m_task(task)
{
  if ( m_dns.empty() )  {
    m_dns = std::string(getenv("DIM_DNS_NODE"));
  }
  std::string svc_type = type;
  if (      type[0] == '/' && type.find("/",1) == std::string::npos ) svc_type = type+"/";
  else if ( type[0] == '/' && type.find("/",1) != std::string::npos ) svc_type = type;
  else if ( type[0] != '/' && type.find("/",1) == std::string::npos ) svc_type = "/"+type+"/";
  else if ( type[0] != '/' && type.find("/",1) != std::string::npos ) svc_type = "/"+type;
  std::string service = "MON_"+m_task+svc_type+"HistCommand";
  DimClient::setDnsNode(m_dns.c_str());
  DimBrowser b;
  int nservcs = b.getServices((service+"*").c_str());
  if ( nservcs != 0 )  {
    m_RPC = std::make_unique<RPCRec>(service, tmo, true);
  }
}

TaskRPC::~TaskRPC()   {
  m_RPC.reset();
}

int TaskRPC::directory(std::vector<std::string>& cntrs)   {
  if ( m_RPC )    {
    int dat = RPCCDirectory;
    m_RPC->setData(dat);
    return m_RPC->analyseReply(cntrs);
  }
  return 1;
}

int TaskRPC::items(const std::vector<std::string>& names)  {
  if ( m_RPC )   {
    int cmdlen = std::accumulate(names.begin(), names.end(), names.size()+sizeof(RPCCommRead), sum_string_length);
    RPCCommRead *cmd = RPCComm::make_command<RPCCommRead>(cmdlen, RPCCRead);
    cmd->copy_names(cmd->which, names);
    m_RPC->setData(cmd, cmdlen);
    return m_RPC->analyseReply();
  }
  return 1;
}

int TaskRPC::items(const std::string& selection)    {
  if ( m_RPC )   {
    int cmdlen = selection.length()+1+sizeof(RPCCommRegex);
    RPCCommRegex *cmd = RPCComm::make_command<RPCCommRegex>(cmdlen, RPCCReadRegex);
    cmd->copy_name(cmd->which, selection);
    m_RPC->setData(cmd, cmdlen);
    return m_RPC->analyseReply();
  }
  return 1;
}

template <typename CONT> int TaskRPC::_taskList(const std::string& type, const std::string& dns, CONT& tasklist)   {
  auto dns_node = dns;
  if ( dns_node.empty() )  {
    dns_node = ::getenv("DIM_DNS_NODE") ? ::getenv("DIM_DNS_NODE") : "UNKNOWN";
  }
  DimClient::setDnsNode(dns_node.c_str());
  DimBrowser b;
  auto svc_type = type;
  if (      type[0] == '/' && type.find("/",1) == std::string::npos ) svc_type = type+"/";
  else if ( type[0] == '/' && type.find("/",1) != std::string::npos ) svc_type = type;
  else if ( type[0] != '/' && type.find("/",1) == std::string::npos ) svc_type = "/"+type+"/";
  else if ( type[0] != '/' && type.find("/",1) != std::string::npos ) svc_type = "/"+type;
  auto match = "MON_*"+svc_type+"Data";
  int nservcs = b.getServices(match.c_str());
  for( int i=0; i<nservcs; i++ )  {
    char *svcn, *fmt;
    int stat = b.getNextService(svcn, fmt);
    if (stat != 0)    {
      std::string svc = svcn;
      __insert(tasklist, svc.substr(strlen("MON_"),svc.find(svc_type)-strlen("MON_")));
    }
  }
  return 0;
}

template int TaskRPC::_taskList(const std::string& type, const std::string& dns, std::vector<std::string>& tasklist);
template int TaskRPC::_taskList(const std::string& type, const std::string& dns, std::list<std::string>& tasklist);
template int TaskRPC::_taskList(const std::string& type, const std::string& dns, std::set<std::string>& tasklist);
template int TaskRPC::_taskList(const std::string& type, const std::string& dns, std::unordered_set<std::string>& tasklist);
