//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

/// Online namespace declaration
namespace Online  {

  /// specialization of MonCounter class for array types
  template <typename T> struct MonCounter<T*> : MonCounterBase {

  MonCounter(const std::string& nam, const std::string& tit,const std::string& fmt, T* const &data , unsigned int siz) :
    MonCounterBase{Type(), nam, tit, siz, siz, data, false, fmt} {}

    MONTYPE Type() const {
      if constexpr (std::is_same_v<T, int>) {
	  return C_INTSTAR;
	} else if constexpr (std::is_same_v<T, long>) {
	  return C_LONGSTAR;
	} else if constexpr (std::is_same_v<T, long long>) {
	  return C_LONGSTAR;
	} else if constexpr (std::is_same_v<T, float>) {
	  return C_FLOATSTAR;
	} else if constexpr (std::is_same_v<T, double>) {
	  return C_DOUBLESTAR;
	} else if constexpr (std::is_same_v<T, unsigned int>) {
	  return C_UINTSTAR;
	} else if constexpr (std::is_same_v<T, unsigned long>) {
	  return C_ULONGSTAR;
	} else {
	static_assert(false_v<T>, "Unsupported type for array specialization of MonCounter");
      }
    }

    unsigned int serialSize() const { return 0; }

    unsigned int serializeData(DimBuffBase *pp, const void *src, int len)  const override {
      ::memcpy(add_ptr(pp, pp->dataoff), src, len);
      return pp->reclen;
    }

    void create_OutputService(const std::string & infix) override {
      this->dim_service = 0;
      std::string nam = m_srvcprefix + infix + this->name;
      if (nam.length() <= 128 /* maximum length of a DIM service Name*/) {
	this->dim_service.reset(new DimService(nam.c_str(), (char*) m_fmt.c_str(), (void*) m_contents, m_contsiz));
      }
    }
  };
}
