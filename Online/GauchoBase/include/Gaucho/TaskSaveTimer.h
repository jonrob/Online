//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_TASKSAVETIMER_H
#define ONLINE_GAUCHO_TASKSAVETIMER_H

#include <Gaucho/GenTimer.h>
#include <Gaucho/HistSaver.h>
#include <Gaucho/MonSubSys.h>

/// Online namespace declaration
namespace Online   {

  class TaskSaveTimer : public GenTimer, public HistSaver  {
  public:
    // Histogram Sub-system for saving
    std::shared_ptr<MonSubSys>  sub_system;

  public:
    /// Initializing constructor
    TaskSaveTimer(int period = 900);
    /// Default destructor
    virtual ~TaskSaveTimer();
    /// Timer callback when timer expires
    virtual void timerHandler() override;
  };
}
#endif
