//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef HISTSERVER_H
#define HISTSERVER_H
#include "dim/dis.hxx"

/// Online namespace declaration
namespace Online   {

  class HistServer : public DimServer
  {
  public:
    HistServer();
    virtual ~HistServer();
  };
}

#endif
