//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_HISTTASKJSON_H_
#define ONLINE_GAUCHO_HISTTASKJSON_H_

#include <Gaucho/TaskRPC.h>
#include <nlohmann/json.hpp>

/// Online namespace declaration
namespace Online  {

  class HistTaskJson : public TaskRPC  {
  public:
    typedef nlohmann::json json;
  public:
    HistTaskJson(const std::string& task,const std::string& dns="",int tmo = 3);
    virtual ~HistTaskJson() = default;
    int histos(const std::vector<std::string>& hists, std::vector<json>& histos);
    int histos(const std::vector<std::string>& hists, std::map<std::string,json>& histos);
    json histo_directory();
    json histos(const std::string& selection);
    template <typename CONT> static int taskList(const std::string& dns, CONT& tasks)
      {   return TaskRPC::_taskList("Histos", dns, tasks);  }
    static json taskList(const std::string& dns);
 };
}
#endif /* ONLINE_GAUCHO_HISTTASKJSON_H_ */
