//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/CounterTaskJson.h>
#include <RTL/rtl.h>

/// C/C++ includes
#include <iostream>
#include <iomanip>

using namespace Online;

static void print_tasks(const std::set<std::string>& tasks)   {
  size_t i = 0;
  for( const auto& t : tasks )
    ::printf("Task[%3ld]  %s\n", ++i, t.c_str());
}

int main(int argc, char *argv[])  {  // Taskname, DNS Node
  std::string dns, task;
  int continuous = 0;
  RTL::CLI cli(argc, argv, [] (){
      std::cout 
	<< " Usage: taskCounters -arg [-arg]  \n"
	<< "  -d(ns)=<dns>            DNS name to interrogate for services\n"
	<< "  -t(ask)=<task-name>     Supply task name for detailed counter printout\n"
	<< "  -c(continuous)=<number> Continuous mode. Give seconds between updates."
	<< std::endl;
    });
  bool help = cli.getopt("help",1);
  cli.getopt("continuous", 1, continuous);
  cli.getopt("task", 1, task);
  cli.getopt("dns",  1, dns);
  if ( help )  {
    cli.call_help();
  }
  if ( dns.empty() )  {
    std::cout << "Insufficient Number of arguments" << std::endl;
    cli.call_help();
  }
  if ( continuous == 1 )   {
    continuous = 2;
  }

  std::set<std::string> tasks;
  CounterTaskJson::taskList(dns, tasks);
  if ( task.empty() )   {
    print_tasks(tasks);
  }
  else   {
    auto h = std::make_unique<CounterTaskJson>(task, dns);
    std::vector<std::string> counters;
    std::vector<std::string> hsts;
    std::vector<nlohmann::json> objs;

    int status = h->directory(counters);
    if ( status == 1 )    {
      std::cout << "Task " << task << " does not exist..." << std::endl;
      return 1;
    }
    std::cout << "Number of Counters for Task " << task << " " << counters.size() << std::endl;
    std::cout << "Trying to retrieve the following Counters:"  << std::endl;
    for ( size_t i=0; i < counters.size(); i++ )     {
      hsts.push_back(counters[i]);
      std::cout << hsts[i] << std::endl;
    }
    std::cout << std::flush;

  Again:
    objs.clear();
    h->counters(hsts, objs);
    std::cout << std::endl 
	      << RTL::timestr() << "  Retrieved " << objs.size()
	      << " Counters  from " << task
	      << std::endl;
    for (size_t i=0; i<objs.size(); i++)   {
      std::cout << objs[i] << std::endl;
    }
    if ( continuous > 0 )    {
      std::cout << "=========================================================================================" << std::endl;
      ::lib_rtl_sleep(continuous*1000);
      goto Again;
    }
  }
  return 0;
}
