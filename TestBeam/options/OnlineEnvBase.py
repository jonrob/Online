# ---------------- General partition parameters:    
PartitionID              = 65535
PartitionIDName          = "FFFF"
PartitionName            = "TEST"
Activity                 = "Upgrade"
OnlineVersion            = "v0"
MooreVersion             = "v0"
MooreOnlineVersion       = "v0"
MooreStartupMode         = 0
OutputLevel              = 3
HasMonitoring            = False
HltArchitecture          = "None"
